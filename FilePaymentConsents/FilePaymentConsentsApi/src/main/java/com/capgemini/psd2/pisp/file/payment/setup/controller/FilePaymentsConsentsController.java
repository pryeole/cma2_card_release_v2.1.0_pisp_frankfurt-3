package com.capgemini.psd2.pisp.file.payment.setup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.capgemini.psd2.cma2.aisp.domain.NoPayloadLogging;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBWriteFileConsent1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.file.payment.setup.service.FilePaymentsConsentsService;
import com.capgemini.psd2.pisp.file.payment.setup.transformer.FPaymentConsentsResponseTransformer;
import com.capgemini.psd2.pisp.validation.adapter.FilePaymentValidator;

@RestController
public class FilePaymentsConsentsController {

	@Autowired
	private FilePaymentValidator filePaymentValidator;

	@Autowired
	private FilePaymentsConsentsService filePaymentsConsentsService;

	@Autowired
	private FPaymentConsentsResponseTransformer transformer;
	
	@RequestMapping(value = "/file-payment-consents", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<CustomFilePaymentConsentsPOSTResponse> createFilePaymentConsentResourceStep1(
			@RequestBody OBWriteFileConsent1 obWriteFileConsent1) {

		CustomFilePaymentSetupPOSTRequest customFilePaymentSetupResource = new CustomFilePaymentSetupPOSTRequest();
		customFilePaymentSetupResource.setData(obWriteFileConsent1.getData());
		boolean isValidFilePaymentSetupRequest;
		/*Added for date validation*/
		customFilePaymentSetupResource=transformer.paymentConsentRequestTransformer(customFilePaymentSetupResource);
		isValidFilePaymentSetupRequest = filePaymentValidator
				.validateFilePaymentSetupPOSTRequest(customFilePaymentSetupResource);
		CustomFilePaymentConsentsPOSTResponse customFilePaymentConsentsPOSTResponse = filePaymentsConsentsService
				.createFilePaymentConsentsResource(customFilePaymentSetupResource, isValidFilePaymentSetupRequest);
		filePaymentValidator.validateFilePaymentSetupPOSTResponse(customFilePaymentConsentsPOSTResponse);

		return new ResponseEntity<>(customFilePaymentConsentsPOSTResponse, HttpStatus.CREATED);

	}

	@NoPayloadLogging
	@RequestMapping(value = "file-payment-consents/{ConsentId}/file", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<?> createFilePaymentConsentResourceStep2(PaymentRetrieveGetRequest paymentRetrieveRequest,
			@RequestParam("FileParam") MultipartFile file) {
		if (file.isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_MISSING, ErrorMapKeys.FILE_NOT_FOUND));
		}
		filePaymentValidator.validateFilePaymentConsentsPOSTRequest(paymentRetrieveRequest);
		filePaymentsConsentsService.uploadPaymentConsentFile(file, paymentRetrieveRequest.getConsentId());
		return new ResponseEntity<>(HttpStatus.OK.toString(), HttpStatus.OK);

	}

	@RequestMapping(value = "/file-payment-consents/{ConsentId}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public ResponseEntity<CustomFilePaymentConsentsPOSTResponse> retrieveFilePaymentConsentsResource(
			PaymentRetrieveGetRequest paymentRetrieveRequest) {
		filePaymentValidator.validateFilePaymentConsentsGETRequest(paymentRetrieveRequest);
		CustomFilePaymentConsentsPOSTResponse paymentConsentsResponse = filePaymentsConsentsService
				.retrieveFilePaymentConsentsResource(paymentRetrieveRequest);
		filePaymentValidator.validateFilePaymentSetupPOSTResponse(paymentConsentsResponse);
		return new ResponseEntity<>(paymentConsentsResponse, HttpStatus.OK);
	}

	@NoPayloadLogging
	@RequestMapping(value = "file-payment-consents/{ConsentId}/file", method = RequestMethod.GET)
	public ResponseEntity<?> downloadFilePaymentConsentResourceStep2(
			PaymentRetrieveGetRequest paymentRetrieveGetRequest) {

		filePaymentValidator.validateFilePaymentConsentsGETRequest(paymentRetrieveGetRequest);

		PlatformFilePaymentsFileResponse fileResponse = filePaymentsConsentsService
				.downloadTransactionFileByConsentId(paymentRetrieveGetRequest.getConsentId());

		HttpHeaders headers = new HttpHeaders();

		if (fileResponse.getFileType().equals(MediaType.APPLICATION_XML_VALUE)) {
			headers.add("Content-Disposition", "attachment; filename=" + fileResponse.getFileName() + ".xml");
			headers.setContentType(MediaType.APPLICATION_XML);
		} else if (fileResponse.getFileType().equals(MediaType.APPLICATION_JSON_VALUE)) {
			headers.add("Content-Disposition", "attachment; filename=" + fileResponse.getFileName() + ".json");
			headers.setContentType(MediaType.APPLICATION_JSON);
		}

		return ResponseEntity.ok().headers(headers).body(new ByteArrayResource(fileResponse.getFileByteArray()));

	}
}
