/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.constants.DomesticPaymentConsentsFoundationConstants;
//import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.AddressCountry;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.Amount;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.AuthorisationType;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.ChargeBearer;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.Country;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.Currency;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.PaymentInstructionCharge;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.PaymentInstructionProposal;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.ProposalStatus;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.ProposingPartyAccount;
//import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain.ProposingPartyPostalAddress;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.repository.DomesticPaymentConsentsRepository;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.service.DomesticPaymentConsentsService;



// TODO: Auto-generated Javadoc
/**
 * The Class DomesticPaymentConsentsServiceImpl.
 */
@Service
public class DomesticPaymentConsentsServiceImpl implements DomesticPaymentConsentsService {

	/** The repository. */
	@Autowired
	private DomesticPaymentConsentsRepository repository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.
	 * service.DomesticPaymentConsentsService#retrieveAccountInformation(java.
	 * lang.String)
	 */
	@Override
	public PaymentInstructionProposal retrieveAccountInformation(String paymentInstructionProposalId) throws Exception {

		PaymentInstructionProposal paymentInstProposal = repository
				.findByPaymentInstructionProposalId(paymentInstructionProposalId);

		if (null == paymentInstProposal) {
			throw new RecordNotFoundException(DomesticPaymentConsentsFoundationConstants.RECORD_NOT_FOUND);
		}

		return paymentInstProposal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.
	 * service.DomesticPaymentConsentsService#
	 * createDomesticPaymentConsentsResource(com.capgemini.psd2.domestic.
	 * payments.consents.mock.foundationservice.domain.
	 * PaymentInstructionProposal)
	 */
	@Override
	public PaymentInstructionProposal createDomesticPaymentConsentsResource(
			PaymentInstructionProposal paymentInstProposalReq) throws RecordNotFoundException {
		String consentID = null;
		if (null == paymentInstProposalReq) {

			throw new RecordNotFoundException(DomesticPaymentConsentsFoundationConstants.RECORD_NOT_FOUND);
		}
		if (!((paymentInstProposalReq.getInstructionEndToEndReference())
				.equalsIgnoreCase(DomesticPaymentConsentsFoundationConstants.INSTRUCTION_ENDTOEND_REF))) {
			
			paymentInstProposalReq.setProposalStatus(ProposalStatus.AWAITINGAUTHORISATION);
			
		} else {
			
			paymentInstProposalReq.setProposalStatus(ProposalStatus.REJECTED);
		}
		consentID = UUID.randomUUID().toString();
		paymentInstProposalReq.setPaymentInstructionProposalId(consentID);
		//paymentInstProposalReq.setAuthorisationType(AuthorisationType.SINGLE);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		paymentInstProposalReq.setProposalCreationDatetime(dateFormat.format(new Date()));
		paymentInstProposalReq.setProposalStatusUpdateDatetime(dateFormat.format(new Date()));
		/*PaymentInstructionCharge charge= new PaymentInstructionCharge();
		charge.setChargeBearer(ChargeBearer.BORNEBYDEBTOR);
		charge.setType("UK.OBIE.ChapsOut");
		Amount amount = new Amount();
		amount.setTransactionCurrency(500.01);
		charge.setAmount(amount);
		Currency currency= new Currency();
		currency.setIsoAlphaCode("GBP");
		charge.setCurrency(currency);
		List<PaymentInstructionCharge> List= new ArrayList();
		List.add(charge);
		paymentInstProposalReq.setCharges(List);*/
		repository.save(paymentInstProposalReq);
		return paymentInstProposalReq;

	}
	
	@Override
	public PaymentInstructionProposal validateDomesticPaymentConsentsResource(
			PaymentInstructionProposal paymentInstProposalReq) throws RecordNotFoundException {
		/*String consentID = null;
		if (null == paymentInstProposalReq) {

			throw new RecordNotFoundException(DomesticPaymentConsentsFoundationConstants.RECORD_NOT_FOUND);
		}
		if ((paymentInstProposalReq.getInstructionEndToEndReference())
				.equalsIgnoreCase(DomesticPaymentConsentsFoundationConstants.INSTRUCTION_ENDTOEND_REF)) {
		} else {
			consentID = UUID.randomUUID().toString();
			paymentInstProposalReq.setPaymentInstructionProposalId(consentID);
			paymentInstProposalReq.setProposalStatus(ProposalStatus.Authorised);
		}
		consentID = UUID.randomUUID().toString();
		paymentInstProposalReq.setPaymentInstructionProposalId(consentID);*/
		
		repository.save(paymentInstProposalReq);
		return paymentInstProposalReq;
	}
	
	@Override
	public PaymentInstructionProposal updateDomesticPaymentConsentsResource(String paymentInstructionProposalId,PaymentInstructionProposal paymentInstProposalReq) throws Exception {
		

		repository.save(paymentInstProposalReq);
		return paymentInstProposalReq;
	}

}
