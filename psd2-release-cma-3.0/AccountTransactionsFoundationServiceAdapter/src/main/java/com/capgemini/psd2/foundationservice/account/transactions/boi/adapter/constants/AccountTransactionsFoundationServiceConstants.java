/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.constants;

/**
 * The Class AccountBalanceFoundationServiceConstants.
 */
public class AccountTransactionsFoundationServiceConstants {
	
	/** The Constant ACCOUNT_ID. */
	public static final String ACCOUNT_ID = "accountId";
	/** The Constant CR. */
	public static final String CR = "Cr";
	/** The Constant DR. */
	public static final String DR = "Dr";
	/** The Constant BOOKED. */
	public static final String BOOKED = "BOOKED";
	/** The Constant PENDING. */
	public static final String PENDING = "PENDING";
	/** The Constant AVAILABLE. */
	public static final String AVAILABLE = "AVAILABLE";
	/** The Constant POSTED. */
	public static final String POSTED = "POSTED";
	/** The Constant FROM_BOOKING_DATE_TIME. */
	public static final String FROM_BOOKING_DATE_TIME = "FromBookingDateTime";
	/** The Constant TO_BOOKING_DATE_TIME. */
	public static final String TO_BOOKING_DATE_TIME = "ToBookingDateTime";
	/** The Constant REQUESTED_PAGE_NUMBER. */
	public static final String REQUESTED_PAGE_NUMBER = "RequestedPageNumber";
	/** The Constant REQUESTED_FROM_DATE_TIME. */
	public static final String REQUESTED_FROM_DATETIME = "RequestedFromDateTime";
	/** The Constant REQUESTED_TO_DATE_TIME. */
	public static final String REQUESTED_TO_DATETIME = "RequestedToDateTime";
	/** The Constant REQUESTED_TXN_FILTER. */
	public static final String REQUESTED_TXN_FILTER = "RequestedTxnFilter";
	/** The Constant REQUESTED_TXN_KEY. */
	public static final String REQUESTED_TXN_KEY = "RequestedTxnKey";
	/** The Constant REQUESTED_PAGE_SIZE. */
	public static final String REQUESTED_PAGE_SIZE = "RequestedPageSize";
	/** The Constant REQUESTED_MIN_PAGE_SIZE. */
	public static final String REQUESTED_MIN_PAGE_SIZE = "RequestedMinPageSize";
	/** The Constant REQUESTED_MAX_PAGE_SIZE. */
	public static final String REQUESTED_MAX_PAGE_SIZE = "RequestedMaxPageSize";
	/** The Constant START_DATE. */
	public static final String START_DATE = "startDate";
	/** The Constant END_DATE. */
	public static final String END_DATE = "endDate";
	/** The Constant PAGE_NUMBER. */
	public static final String PAGE_NUMBER = "pageNumber";
	/** The Constant TRANSACTION_FILTER. */
	public static final String TRANSACTION_FILTER = "TransactionFilter";
	/** The Constant TRANSACTION_TYPE */
	public static final String TRANSACTION_TYPE = "txnType";
	/** The Constant PAGE_SIZE. */
	public static final String PAGE_SIZE= "pageSize";
	/** The Constant MIN_PAGE_SIZE. */
	public static final String MIN_PAGE_SIZE= "minPageSize";
	/** The Constant MAX_PAGE_SIZE. */
	public static final String MAX_PAGE_SIZE= "maxPageSize";
	/** The Constant TRANSACTION_RETRIEVAL_KEY. */
	public static final String TRANSACTION_RETRIEVAL_KEY = "transactionRetrievalKey";
	/** The Constant ALL. */
	public static final String ALL = "ALL";
	/** The Constant CREDIT. */
	public static final String CREDIT = "CREDIT";
	/** The Constant DEBIT. */
	public static final String DEBIT = "DEBIT";
	/** The Constant FIRST_PAGE. */
	public static final String FIRST_PAGE = "1";
	/** The Constant CHANNEL_ID. */
	public static final String CHANNEL_ID = "channelId";
	/** The Constant error code 404. */
	public static final String ERROR_CODE_404 = "404";
	/** The Constant error code 500. */
	public static final String ERROR_CODE_500 = "500";
	/** The Constant error code 500. */
	public static final String BASE_URL = "baseUrl";
	
	public static final String REQUESTED_TO_CONSENT_DATETIME = "RequestedToConsentDateTime";
	
	public static final String REQUESTED_FROM_CONSENT_DATETIME = "RequestedFromConsentDateTime";
	
	public static final String CONSENT_EXPIRATION_DATETIME = "ConsentExpirationDateTime"; 
	
	public static final String REFID = "refID";
	
	public static final String ACCOUNT_SUBTYPE = "account_subtype" ;
	
	public static final String TO_DATE = "dateTo";
	
	public static final String FROM_DATE = "dateFrom";
	
	public static final String CREDIT_DEBIT_INDICATOR = "creditDebitEventCode";
	
	public static final String ORDER = "order";
	
	public static final String ORDERBY = "orderBy";
	
	public static final String CURRENT_ACCOUNT = "CurrentAccount";
	
	public static final String SAVINGS = "Savings";
	
	public static final String CREDIT_CARD= "CreditCard";
	
	public static final String OFFSET= "offset";
}
