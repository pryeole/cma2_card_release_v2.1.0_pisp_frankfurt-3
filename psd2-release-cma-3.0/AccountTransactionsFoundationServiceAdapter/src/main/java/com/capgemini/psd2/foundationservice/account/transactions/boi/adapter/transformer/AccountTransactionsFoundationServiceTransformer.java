/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.enumerator.CurrencyEnum;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.capgemini.psd2.aisp.domain.OBBalanceType1Code;
import com.capgemini.psd2.aisp.domain.OBEntryStatus1Code;
import com.capgemini.psd2.aisp.domain.OBMerchantDetails1;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3Data;
import com.capgemini.psd2.aisp.domain.OBTransaction3;
import com.capgemini.psd2.aisp.domain.OBTransaction3ProprietaryBankTransactionCode;
import com.capgemini.psd2.aisp.domain.OBTransactionCashBalance;
import com.capgemini.psd2.aisp.domain.OBTransactionCashBalance.CreditDebitIndicatorEnum;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.aisp.transformer.AccountTransactionTransformer;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.constants.AccountTransactionsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.BalanceTypeEnum;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Transaction;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.TransactionList;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountTransactionsFoundationServiceTransformer.
 */
@Component
public class AccountTransactionsFoundationServiceTransformer implements AccountTransactionTransformer {

	/** The psd2 validator. */
	@Autowired
	private PSD2Validator psd2Validator;

	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	@Override
	public <T> PlatformAccountTransactionResponse transformAccountTransaction(T input, Map<String, String> params) {
		PlatformAccountTransactionResponse platformAccountTransactionResponse = new PlatformAccountTransactionResponse();
		OBReadTransaction3 obReadTransaction3 = new OBReadTransaction3();
		OBReadTransaction3Data oBReadTransaction3Data = new OBReadTransaction3Data();

		int pageNumber = Integer
				.parseInt(params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER));

		List<OBTransaction3> transaction = new ArrayList<>();
		oBReadTransaction3Data.setTransaction(transaction);
		obReadTransaction3.setData(oBReadTransaction3Data);

		TransactionList transactions = (TransactionList) input;
		if (transactions.getTransactionsList().isEmpty() || transactions.getTransactionsList() == null
				|| oBReadTransaction3Data.getTransaction() == null) {
			platformAccountTransactionResponse.setoBReadTransaction3(obReadTransaction3);
			return platformAccountTransactionResponse;
		}
		for (Transaction transactionsList : transactions.getTransactionsList()) {
			OBTransaction3 data = new OBTransaction3();
			data.setAccountId(params.get(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID));

			// setTransactionId
			if (!NullCheckUtils.isNullOrEmpty(transactionsList.getTransactionIdentificationNumber()))
				data.setTransactionId(transactionsList.getTransactionIdentificationNumber());

			// setAmount
			OBActiveOrHistoricCurrencyAndAmount amount = new OBActiveOrHistoricCurrencyAndAmount();
			String resAmount = null;
			if (transactionsList.getFinancialEvent().getFinancialEventAmount().getTransactionCurrency() != null) {
				resAmount = BigDecimal.valueOf(Double.valueOf(transactionsList.getFinancialEvent()
						.getFinancialEventAmount().getTransactionCurrency().toString())).toPlainString();
				if (!resAmount.contains("."))
					resAmount = resAmount + ".0";
				resAmount = resAmount.replaceFirst("-", "");
			}

			// setCurrency
			String currency = transactionsList.getFinancialEvent().getCurrency().getIsoAlphaCode();
			psd2Validator.validateEnum(CurrencyEnum.class, currency);
			amount.setAmount(resAmount);
			amount.setCurrency(transactionsList.getFinancialEvent().getCurrency().getIsoAlphaCode());
			psd2Validator.validate(amount);
			data.setAmount(amount);

			// setCreditDebitIndicator
			if (AccountTransactionsFoundationServiceConstants.CR
					.equalsIgnoreCase(transactionsList.getCreditDebitEventCode().toString())) {
				data.setCreditDebitIndicator(
						com.capgemini.psd2.aisp.domain.OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
			} else if (AccountTransactionsFoundationServiceConstants.DR
					.equalsIgnoreCase(transactionsList.getCreditDebitEventCode().toString())) {
				data.setCreditDebitIndicator(
						com.capgemini.psd2.aisp.domain.OBTransaction3.CreditDebitIndicatorEnum.DEBIT);
			}

			// setStatus
			if (AccountTransactionsFoundationServiceConstants.BOOKED
					.equalsIgnoreCase(transactionsList.getFinancialEvent().getFinancialEventStatusCode())) {
				data.setStatus(OBEntryStatus1Code.BOOKED);
			} else if (AccountTransactionsFoundationServiceConstants.PENDING
					.equalsIgnoreCase(transactionsList.getFinancialEvent().getFinancialEventStatusCode())) {
				data.setStatus(OBEntryStatus1Code.PENDING);
			}

			// setTransactionInformation
			if (!NullCheckUtils.isNullOrEmpty(transactionsList.getTransactionDecription()))
				data.setTransactionInformation(transactionsList.getTransactionDecription());

			// setAddressLine
			if (!NullCheckUtils.isNullOrEmpty(transactionsList.getTransactionLocation()))
				data.setAddressLine(transactionsList.getTransactionLocation());

			// setBookingDateTime
			String bookingDateStr = timeZoneDateTimeAdapter.parseDateCMA(transactionsList.getRunDate());
			data.setBookingDateTime(bookingDateStr);

			// setValueDateTime
			if (!NullCheckUtils.isNullOrEmpty(transactionsList.getValueDate())) {
				String valueDateStr = timeZoneDateTimeAdapter.parseDateCMA(transactionsList.getValueDate());
				data.setValueDateTime(valueDateStr);
			}

			// setCode
			if (!NullCheckUtils.isNullOrEmpty(transactionsList.getFinancialEvent().getFinancialEventSubtype())) {
				OBTransaction3ProprietaryBankTransactionCode proprietaryBankTransactionCode = new OBTransaction3ProprietaryBankTransactionCode();
				proprietaryBankTransactionCode.setCode(transactionsList.getFinancialEvent().getFinancialEventSubtype());
				data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
			}

			// setAmount
			if (!NullCheckUtils.isNullOrEmpty(transactionsList.getBalance())) {
				OBTransactionCashBalance balance = new OBTransactionCashBalance();
				OBActiveOrHistoricCurrencyAndAmount balAmount = new OBActiveOrHistoricCurrencyAndAmount();
				String txnAmount = null;
				if (transactionsList.getBalance().getBalanceAmount().getTransactionCurrency() != null) {
					txnAmount = BigDecimal.valueOf(Double.valueOf(
							transactionsList.getBalance().getBalanceAmount().getTransactionCurrency().toString()))
							.toPlainString();
					if (!txnAmount.contains("."))
						txnAmount = txnAmount + ".0";
					txnAmount = txnAmount.replaceFirst("-", "");
				}

				// setCurrency
				String balCurrency = transactionsList.getBalance().getBalanceCurrency().getIsoAlphaCode();
				psd2Validator.validateEnum(CurrencyEnum.class, balCurrency);
				balAmount.setAmount(txnAmount);
				balAmount.setCurrency(balCurrency);
				psd2Validator.validate(balAmount);
				balance.setAmount(balAmount);
				BigDecimal bigDecimalCurrency = new BigDecimal(
						transactionsList.getBalance().getBalanceAmount().getTransactionCurrency().toString());
				if (bigDecimalCurrency.compareTo(BigDecimal.ZERO) >= 0) {
					balance.setCreditDebitIndicator(CreditDebitIndicatorEnum.CREDIT);
				} else {
					balance.setCreditDebitIndicator(CreditDebitIndicatorEnum.DEBIT);
				}

				// setType
				if (params.get(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE)
						.equals(AccountTransactionsFoundationServiceConstants.CURRENT_ACCOUNT)
						|| params.get(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE)
								.equals(AccountTransactionsFoundationServiceConstants.SAVINGS)) {
					if (transactionsList.getBalance().getBalanceType().equals(BalanceTypeEnum.LEDGER_BALANCE)
							|| transactionsList.getBalance().getBalanceType().equals(BalanceTypeEnum.SHADOW_BALANCE))
						balance.setType(OBBalanceType1Code.EXPECTED);
				} else if (params.get(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE)
						.equals(AccountTransactionsFoundationServiceConstants.CREDIT_CARD)) {
					if (transactionsList.getBalance().getBalanceType()
							.equals(BalanceTypeEnum.STATEMENT_CLOSING_BALANCE))
						balance.setType(OBBalanceType1Code.OPENINGBOOKED);
					else if (transactionsList.getBalance().getBalanceType().equals(BalanceTypeEnum.LEDGER_BALANCE))
						balance.setType(OBBalanceType1Code.INTERIMBOOKED);
					else if (transactionsList.getBalance().getBalanceType().equals(BalanceTypeEnum.AVAILABLE_LIMIT))
						balance.setType(OBBalanceType1Code.INTERIMAVAILABLE);
				}
				data.setBalance(balance);

				if (params.get(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE)
						.equals(AccountTransactionsFoundationServiceConstants.CREDIT_CARD)) {
					if (!NullCheckUtils.isNullOrEmpty(transactionsList.getParty().getMerchant())) {
						OBMerchantDetails1 merchantDetails = new OBMerchantDetails1();

						// setMerchantName
						if (!NullCheckUtils.isNullOrEmpty(transactionsList.getParty().getMerchant().getMerchantName()))
							merchantDetails
									.setMerchantName(transactionsList.getParty().getMerchant().getMerchantName());

						// setMerchantCategoryCode
						if (!NullCheckUtils
								.isNullOrEmpty(transactionsList.getParty().getMerchant().getMerchantCategoryCode()))
							merchantDetails.setMerchantCategoryCode(
									transactionsList.getParty().getMerchant().getMerchantCategoryCode());
						data.setMerchantDetails(merchantDetails);
					}
				}
			}
			obReadTransaction3.getData().getTransaction().add(data);
			psd2Validator.validate(data);
		}

		// Prepare links based on the value of isHasMoreTxns
		if (params.get(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE)
				.equals(AccountTransactionsFoundationServiceConstants.CURRENT_ACCOUNT)
				|| params.get(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE)
						.equals(AccountTransactionsFoundationServiceConstants.SAVINGS)) {
			Links links = new Links();
			if (pageNumber != 0) {
				if (transactions.isHasMoreRecords()) {
					links.setNext(Integer.toString(pageNumber + 1));
				}
				if (pageNumber > 1) {
					links.setPrev(Integer.toString(pageNumber - 1));
				}
				if (transactions.getTransactionRetrievalKey() != null) {
					links.setSelf(Integer.toString(pageNumber) + ":" + transactions.getTransactionRetrievalKey());
				} else {
					links.setSelf(Integer.toString(pageNumber));
				}
				if (!"1".equals(links.getSelf())) {
					links.setFirst(AccountTransactionsFoundationServiceConstants.FIRST_PAGE);
				}
				obReadTransaction3.setLinks(links);
			}
		}
		platformAccountTransactionResponse.setoBReadTransaction3(obReadTransaction3);
		psd2Validator.validate(platformAccountTransactionResponse);
		return platformAccountTransactionResponse;
	}

	@Override
	public <T> PlatformAccountTransactionResponse transformAccountTransactions(T input, Map<String, String> params) {
		return null;
	}

}
