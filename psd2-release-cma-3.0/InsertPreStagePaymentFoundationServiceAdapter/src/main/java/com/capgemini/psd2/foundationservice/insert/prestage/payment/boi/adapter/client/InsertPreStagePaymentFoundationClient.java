package com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.client;

import org.springframework.http.HttpHeaders;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;

import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.rest.client.model.RequestInfo;

public interface InsertPreStagePaymentFoundationClient {

	@Retryable(maxAttemptsExpression = "#{${foundationService.enableRetry} ? ${foundationService.retryCount:2} : 1}", 
	            backoff = @Backoff(delayExpression = "#{${foundationService.retryDelay:300}}", 
	            maxDelayExpression = "#{${foundationService.retryDelay:300}}"), 
	            exceptionExpression = "#{message.contains('Retry_Error_Code')}")
	public ValidationPassed restTransportForInsertPreStagePayment(RequestInfo reqInfo, PaymentInstruction paymentInstruction, Class <ValidationPassed> responseType, HttpHeaders headers);
	
	@Retryable(maxAttemptsExpression = "#{${foundationService.enableRetry} ? ${foundationService.retryCount:2} : 1}", 
	            backoff = @Backoff(delayExpression = "#{${foundationService.retryDelay:300}}", 
	            maxDelayExpression = "#{${foundationService.retryDelay:300}}"), 
	            exceptionExpression = "#{message.contains('Retry_Error_Code')}")
	public ValidationPassed restTransportForUpdatePreStagePayment(RequestInfo reqInfo, PaymentInstruction paymentInstruction, Class <ValidationPassed> responseType, HttpHeaders headers);
	
	
   @Retryable(maxAttemptsExpression = "#{${foundationService.enableRetry} ? ${foundationService.retryCount:2} : 1}", 
	            backoff = @Backoff(delayExpression = "#{${foundationService.retryDelay:300}}", 
	            maxDelayExpression = "#{${foundationService.retryDelay:300}}"), 
	            exceptionExpression = "#{message.contains('Retry_Error_Code')}")
	public PaymentInstruction restTransportForPaymentRetrieval(RequestInfo reqInfo, Class <PaymentInstruction> responseType, HttpHeaders headers);
}
