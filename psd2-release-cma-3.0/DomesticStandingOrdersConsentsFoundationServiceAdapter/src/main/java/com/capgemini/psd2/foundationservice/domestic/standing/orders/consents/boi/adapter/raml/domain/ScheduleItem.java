package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.time.LocalDate;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * ScheduleItem
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class ScheduleItem   {
  @JsonProperty("scheduleItemDate")
  private LocalDate scheduleItemDate = null;

  @JsonProperty("scheduleItemDateType")
  private ScheduleItemDateType scheduleItemDateType = null;

  @JsonProperty("scheduleItemAmountTransactionCurrency")
  private PaymentAmount scheduleItemAmountTransactionCurrency = null;

  @JsonProperty("scheduleItemTransactionCurrency")
  private Currency scheduleItemTransactionCurrency = null;

  @JsonProperty("paymentInstruction")
  private PaymentInstruction2 paymentInstruction = null;

  @JsonProperty("paymentInstructionToCounterpartyAccount")
  private PaymentInstructionCounterpartyAccountBasic paymentInstructionToCounterpartyAccount = null;

  public ScheduleItem scheduleItemDate(LocalDate scheduleItemDate) {
    this.scheduleItemDate = scheduleItemDate;
    return this;
  }

  /**
   * The value date of the payment
   * @return scheduleItemDate
  **/
  @ApiModelProperty(required = true, value = "The value date of the payment")
  @NotNull

  @Valid

  public LocalDate getScheduleItemDate() {
    return scheduleItemDate;
  }

  public void setScheduleItemDate(LocalDate scheduleItemDate) {
    this.scheduleItemDate = scheduleItemDate;
  }

  public ScheduleItem scheduleItemDateType(ScheduleItemDateType scheduleItemDateType) {
    this.scheduleItemDateType = scheduleItemDateType;
    return this;
  }

  /**
   * Get scheduleItemDateType
   * @return scheduleItemDateType
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ScheduleItemDateType getScheduleItemDateType() {
    return scheduleItemDateType;
  }

  public void setScheduleItemDateType(ScheduleItemDateType scheduleItemDateType) {
    this.scheduleItemDateType = scheduleItemDateType;
  }

  public ScheduleItem scheduleItemAmountTransactionCurrency(PaymentAmount scheduleItemAmountTransactionCurrency) {
    this.scheduleItemAmountTransactionCurrency = scheduleItemAmountTransactionCurrency;
    return this;
  }

  /**
   * Get scheduleItemAmountTransactionCurrency
   * @return scheduleItemAmountTransactionCurrency
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PaymentAmount getScheduleItemAmountTransactionCurrency() {
    return scheduleItemAmountTransactionCurrency;
  }

  public void setScheduleItemAmountTransactionCurrency(PaymentAmount scheduleItemAmountTransactionCurrency) {
    this.scheduleItemAmountTransactionCurrency = scheduleItemAmountTransactionCurrency;
  }

  public ScheduleItem scheduleItemTransactionCurrency(Currency scheduleItemTransactionCurrency) {
    this.scheduleItemTransactionCurrency = scheduleItemTransactionCurrency;
    return this;
  }

  /**
   * Get scheduleItemTransactionCurrency
   * @return scheduleItemTransactionCurrency
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Currency getScheduleItemTransactionCurrency() {
    return scheduleItemTransactionCurrency;
  }

  public void setScheduleItemTransactionCurrency(Currency scheduleItemTransactionCurrency) {
    this.scheduleItemTransactionCurrency = scheduleItemTransactionCurrency;
  }

  public ScheduleItem paymentInstruction(PaymentInstruction2 paymentInstruction) {
    this.paymentInstruction = paymentInstruction;
    return this;
  }

  /**
   * Get paymentInstruction
   * @return paymentInstruction
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PaymentInstruction2 getPaymentInstruction() {
    return paymentInstruction;
  }

  public void setPaymentInstruction(PaymentInstruction2 paymentInstruction) {
    this.paymentInstruction = paymentInstruction;
  }

  public ScheduleItem paymentInstructionToCounterpartyAccount(PaymentInstructionCounterpartyAccountBasic paymentInstructionToCounterpartyAccount) {
    this.paymentInstructionToCounterpartyAccount = paymentInstructionToCounterpartyAccount;
    return this;
  }

  /**
   * Get paymentInstructionToCounterpartyAccount
   * @return paymentInstructionToCounterpartyAccount
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PaymentInstructionCounterpartyAccountBasic getPaymentInstructionToCounterpartyAccount() {
    return paymentInstructionToCounterpartyAccount;
  }

  public void setPaymentInstructionToCounterpartyAccount(PaymentInstructionCounterpartyAccountBasic paymentInstructionToCounterpartyAccount) {
    this.paymentInstructionToCounterpartyAccount = paymentInstructionToCounterpartyAccount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ScheduleItem scheduleItem = (ScheduleItem) o;
    return Objects.equals(this.scheduleItemDate, scheduleItem.scheduleItemDate) &&
        Objects.equals(this.scheduleItemDateType, scheduleItem.scheduleItemDateType) &&
        Objects.equals(this.scheduleItemAmountTransactionCurrency, scheduleItem.scheduleItemAmountTransactionCurrency) &&
        Objects.equals(this.scheduleItemTransactionCurrency, scheduleItem.scheduleItemTransactionCurrency) &&
        Objects.equals(this.paymentInstruction, scheduleItem.paymentInstruction) &&
        Objects.equals(this.paymentInstructionToCounterpartyAccount, scheduleItem.paymentInstructionToCounterpartyAccount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(scheduleItemDate, scheduleItemDateType, scheduleItemAmountTransactionCurrency, scheduleItemTransactionCurrency, paymentInstruction, paymentInstructionToCounterpartyAccount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ScheduleItem {\n");
    
    sb.append("    scheduleItemDate: ").append(toIndentedString(scheduleItemDate)).append("\n");
    sb.append("    scheduleItemDateType: ").append(toIndentedString(scheduleItemDateType)).append("\n");
    sb.append("    scheduleItemAmountTransactionCurrency: ").append(toIndentedString(scheduleItemAmountTransactionCurrency)).append("\n");
    sb.append("    scheduleItemTransactionCurrency: ").append(toIndentedString(scheduleItemTransactionCurrency)).append("\n");
    sb.append("    paymentInstruction: ").append(toIndentedString(paymentInstruction)).append("\n");
    sb.append("    paymentInstructionToCounterpartyAccount: ").append(toIndentedString(paymentInstructionToCounterpartyAccount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

