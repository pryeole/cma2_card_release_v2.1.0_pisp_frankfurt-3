package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.DomesticStandingOrdersConsentsFoundationServiceAdapterApplication;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.client.DomesticStandingOrdersConsentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.delegate.DomesticStandingOrdersConsentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.Amount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.PaymentTransaction;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.transformer.DomesticStandingOrdersConsentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FinalPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1RecurringPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPermissions2Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsent1;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringRunner.class)
public class DomesticStandingOrdersConsentsFoundationServiceTransformerTest {
	
	
	@InjectMocks
	DomesticStandingOrdersConsentsFoundationServiceTransformer transformer;
	
	@Mock
	DomesticStandingOrdersConsentsFoundationServiceAdapterApplication adapter;
	
	@Mock
	DomesticStandingOrdersConsentsFoundationServiceDelegate delegate;
	
	@Mock
	DomesticStandingOrdersConsentsFoundationServiceClient client;
	
	@Mock
	private PSD2Validator psd2Validator;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */

	@Test
	public void contextLoads() {
	}
	@Test
	public void testTransformDomesticStandingOrderResponse(){
		
		StandingOrderInstructionProposal inputBalanceObjStanding = new StandingOrderInstructionProposal();
		PaymentInstructionCharge paymentInstructionCharge = new PaymentInstructionCharge();
		Amount am = new Amount();
		Currency c1 = new Currency();
		
		inputBalanceObjStanding.setPaymentInstructionProposalId("asad");
		inputBalanceObjStanding.setProposalCreationDatetime(OffsetDateTime.MAX);
		inputBalanceObjStanding.setProposalStatus(ProposalStatus.AWAITINGAUTHORISATION);
		inputBalanceObjStanding.setProposalStatusUpdateDatetime(OffsetDateTime.MAX);
		inputBalanceObjStanding.setPermission("safdf");
		
		paymentInstructionCharge.setChargeBearer(com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.ChargeBearer.BORNEBYCREDITOR);
		paymentInstructionCharge.setType("adfdf");
		paymentInstructionCharge.setAmount(am);
		paymentInstructionCharge.setCurrency(c1);
		am.setTransactionCurrency(16.00);
		c1.setIsoAlphaCode("afsf");
		List<PaymentInstructionCharge> chargeList = new ArrayList<PaymentInstructionCharge>();
		chargeList.add(paymentInstructionCharge);
		
		inputBalanceObjStanding.setFrequency("afdaf");
		inputBalanceObjStanding.setReference("gghfh");
		inputBalanceObjStanding.setNumberOfPayments(123.00);
		inputBalanceObjStanding.setFirstPaymentDateTime(OffsetDateTime.MAX);
		inputBalanceObjStanding.setRecurringPaymentDateTime(OffsetDateTime.MAX);
		inputBalanceObjStanding.setFinalPaymentDateTime(OffsetDateTime.MAX);
		
		PaymentTransaction payment = new PaymentTransaction();
		FinancialEventAmount amount = new FinancialEventAmount();
		amount.setTransactionCurrency(12.00);
		payment.setFinancialEventAmount(amount);
		Currency c = new Currency();
		c.isoAlphaCode("asdfaf");
		payment.setTransactionCurrency(c);
		inputBalanceObjStanding.setFirstPaymentAmount(payment);
		
		amount.setTransactionCurrency(24.00);
		c.isoAlphaCode("adfsaf");
		inputBalanceObjStanding.setRecurringPaymentAmount(payment);
		
		amount.setTransactionCurrency(24.00);
		c.isoAlphaCode("adfsaf");
		inputBalanceObjStanding.setFinalPaymentAmount(payment);
		
		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
		authorisingPartyAccount.setSchemeName("afsdfa");
		authorisingPartyAccount.setAccountName("dfhhf");
		authorisingPartyAccount.setAccountNumber("dgdggdg");
		authorisingPartyAccount.setSecondaryIdentification("fghfhhj");
		inputBalanceObjStanding.setAuthorisingPartyAccount(authorisingPartyAccount);
		
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		proposingPartyAccount.setSchemeName("afsdfa");
		proposingPartyAccount.setAccountName("dfhhf");
		proposingPartyAccount.setAccountNumber("dgdggdg");
		proposingPartyAccount.setSecondaryIdentification("fghfhhj");
		inputBalanceObjStanding.setProposingPartyAccount(proposingPartyAccount);
		
		inputBalanceObjStanding.authorisationType(AuthorisationType.ANY);
		inputBalanceObjStanding.authorisationDatetime(OffsetDateTime.MAX);
		
		PaymentInstrumentRiskFactor paymentInstrumentRiskFactor = new PaymentInstrumentRiskFactor();
		paymentInstrumentRiskFactor.setPaymentContextCode("asfdf");
		paymentInstrumentRiskFactor.setMerchantCategoryCode("aafaff");
		paymentInstrumentRiskFactor.setMerchantCustomerIdentification("dadff");
		
		
		Address address = new Address();
		address.setFirstAddressLine("sdfdf");
		address.setSecondAddressLine("asdfaff");
		address.setGeoCodeBuildingName("asdddf");
		address.setGeoCodeBuildingNumber("asffsaf");
		address.setPostCodeNumber("asdads");
		address.setThirdAddressLine("adfdfa");
		address.setFourthAddressLine("adffd");
		address.setFifthAddressLine("gfsgfs");
		paymentInstrumentRiskFactor.setCounterPartyAddress(address);
		
		Country country = new Country();
		country.setIsoCountryAlphaTwoCode("adadfa");
		address.setAddressCountry(country);
		inputBalanceObjStanding.setPaymentInstructionRiskFactorReference(paymentInstrumentRiskFactor);
		
		List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
		PaymentInstructionCharge element = new PaymentInstructionCharge();
		element.setChargeBearer(ChargeBearer.BORNEBYCREDITOR);
		element.setType("454dwd");
		Amount amt = new Amount();
		amt.setTransactionCurrency(12345.2314);
		element.setAmount(amt);
		Currency currency = new Currency();
		currency.setIsoAlphaCode("45dc4d5");
		element.setCurrency(currency);
		charges.add(element);
		inputBalanceObjStanding.setCharges(charges );
		transformer.transformDomesticStandingOrderResponse(inputBalanceObjStanding);
		transformer.transformDomesticStandingOrdersResponseFromFDToAPIForInsert(inputBalanceObjStanding);
	}
	
	@Test
	public void testTransformDomesticStandingOrderResponseForNull(){
		
		StandingOrderInstructionProposal inputBalanceObjStanding = new StandingOrderInstructionProposal();
		PaymentInstructionCharge paymentInstructionCharge = new PaymentInstructionCharge();
		Amount am = new Amount();
		Currency c1 = new Currency();
		
		StandingOrderInstructionProposal inputBalanceObjStanding1 = null;
		
		inputBalanceObjStanding.setPaymentInstructionProposalId("asad");
		inputBalanceObjStanding.setProposalCreationDatetime(OffsetDateTime.MAX);
		inputBalanceObjStanding.setProposalStatus(ProposalStatus.REJECTED);
		inputBalanceObjStanding.setProposalStatusUpdateDatetime(OffsetDateTime.MAX);
		inputBalanceObjStanding.setPermission("safdf");
		inputBalanceObjStanding.setPaymentInstructionProposalId(null);
		inputBalanceObjStanding.setProposalCreationDatetime(null);
		inputBalanceObjStanding.setProposalStatus(null);
		inputBalanceObjStanding.setProposalStatusUpdateDatetime(null);
		inputBalanceObjStanding.setPermission(null);
		
		
		paymentInstructionCharge.setChargeBearer(com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.ChargeBearer.BORNEBYCREDITOR);
		paymentInstructionCharge.setType("adfdf");
		paymentInstructionCharge.setAmount(am);
		paymentInstructionCharge.setCurrency(c1);
		am.setTransactionCurrency(16.00);
		c1.setIsoAlphaCode("afsf");
		List<PaymentInstructionCharge> chargeList = new ArrayList<PaymentInstructionCharge>();
		chargeList.add(paymentInstructionCharge);
		
		inputBalanceObjStanding.setFrequency("afdaf");
		inputBalanceObjStanding.setReference("gghfh");
		inputBalanceObjStanding.setNumberOfPayments(123.00);
		inputBalanceObjStanding.setFirstPaymentDateTime(OffsetDateTime.MAX);
		inputBalanceObjStanding.setRecurringPaymentDateTime(OffsetDateTime.MAX);
		inputBalanceObjStanding.setFinalPaymentDateTime(OffsetDateTime.MAX);
		inputBalanceObjStanding.setFrequency(null);
		inputBalanceObjStanding.setReference(null);
		inputBalanceObjStanding.setNumberOfPayments(null);
		inputBalanceObjStanding.setFirstPaymentDateTime(null);
		inputBalanceObjStanding.setRecurringPaymentDateTime(null);
		inputBalanceObjStanding.setFinalPaymentDateTime(null);
		
		PaymentTransaction payment = new PaymentTransaction();
		FinancialEventAmount amount = new FinancialEventAmount();
		amount.setTransactionCurrency(12.00);
		payment.setFinancialEventAmount(amount);
		Currency c = new Currency();
		c.isoAlphaCode("asdfaf");
		payment.setTransactionCurrency(c);
		inputBalanceObjStanding.setFirstPaymentAmount(payment);
		inputBalanceObjStanding.setFirstPaymentAmount(null);
		
		
		amount.setTransactionCurrency(24.00);
		c.isoAlphaCode("adfsaf");
		inputBalanceObjStanding.setRecurringPaymentAmount(payment);
		inputBalanceObjStanding.setRecurringPaymentAmount(null);
		
		amount.setTransactionCurrency(24.00);
		c.isoAlphaCode("adfsaf");
		inputBalanceObjStanding.setFinalPaymentAmount(payment);
		inputBalanceObjStanding.setFinalPaymentAmount(null);
		
		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
		authorisingPartyAccount.setSchemeName("afsdfa");
		authorisingPartyAccount.setAccountName("dfhhf");
		authorisingPartyAccount.setAccountNumber("dgdggdg");
		authorisingPartyAccount.setSecondaryIdentification("fghfhhj");
		inputBalanceObjStanding.setAuthorisingPartyAccount(authorisingPartyAccount);
		inputBalanceObjStanding.setAuthorisingPartyAccount(null);
		
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		proposingPartyAccount.setSchemeName("afsdfa");
		proposingPartyAccount.setAccountName("dfhhf");
		proposingPartyAccount.setAccountNumber("dgdggdg");
		proposingPartyAccount.setSecondaryIdentification("fghfhhj");
		inputBalanceObjStanding.setProposingPartyAccount(proposingPartyAccount);
		inputBalanceObjStanding.setProposingPartyAccount(null);
		
		inputBalanceObjStanding.authorisationType(AuthorisationType.ANY);
		inputBalanceObjStanding.authorisationDatetime(OffsetDateTime.MAX);
		inputBalanceObjStanding.authorisationType(null);
		inputBalanceObjStanding.authorisationDatetime(null);
		
		PaymentInstrumentRiskFactor paymentInstrumentRiskFactor = new PaymentInstrumentRiskFactor();
		paymentInstrumentRiskFactor.setPaymentContextCode("asfdf");
		paymentInstrumentRiskFactor.setMerchantCategoryCode("aafaff");
		paymentInstrumentRiskFactor.setMerchantCustomerIdentification("dadff");
		
		
		Address address = new Address();
		address.setFirstAddressLine("sdfdf");
		address.setSecondAddressLine("asdfaff");
		address.setGeoCodeBuildingName("asdddf");
		address.setGeoCodeBuildingNumber("asffsaf");
		address.setPostCodeNumber("asdads");
		address.setThirdAddressLine("adfdfa");
		address.setFourthAddressLine("adffd");
		address.setFifthAddressLine("gfsgfs");
		paymentInstrumentRiskFactor.setCounterPartyAddress(address);

		
		Country country = new Country();
		country.setIsoCountryAlphaTwoCode("adadfa");
		address.setAddressCountry(country);
		inputBalanceObjStanding.setPaymentInstructionRiskFactorReference(paymentInstrumentRiskFactor);
		inputBalanceObjStanding.setPaymentInstructionRiskFactorReference(null);
		
		
		transformer.transformDomesticStandingOrderResponse(inputBalanceObjStanding);
		transformer.transformDomesticStandingOrdersResponseFromFDToAPIForInsert(inputBalanceObjStanding);
	}
	
	@Test
	public void testTransformDomesticStandingOrdersResponseFromAPIToFDForInsert(){
		
		CustomDStandingOrderConsentsPOSTRequest standingOrderConsentsRequest = new CustomDStandingOrderConsentsPOSTRequest();
		OBWriteDataDomesticStandingOrderConsent1 data = new OBWriteDataDomesticStandingOrderConsent1();
		OBRisk1 risk = new OBRisk1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBDomesticStandingOrder1 obDomestic1 = new OBDomesticStandingOrder1();
		List<String> addressLine = new ArrayList<String>();
		addressLine.add("fbf");
		addressLine.add("sfsdf");
		data.setPermission(OBExternalPermissions2Code.CREATE);
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		
		deliveryAddress.setStreetName("abc");
		deliveryAddress.setBuildingNumber("1233");
		deliveryAddress.setPostCode("3111455");
		deliveryAddress.setTownName("pune");
		deliveryAddress.setCountrySubDivision("gfh562");
		deliveryAddress.setCountry("India");
		
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		risk.setMerchantCategoryCode("gfh562");
		risk.setMerchantCustomerIdentification("gfh562");
		risk.setDeliveryAddress(deliveryAddress);
		debtorAccount.setSchemeName("gfh562");
		debtorAccount.setIdentification("gfh562");
		debtorAccount.setName("gfh562");
		debtorAccount.setSecondaryIdentification("gfh562");
		creditorAccount.setSchemeName("gfh562");
		creditorAccount.setIdentification("gfh562");
		creditorAccount.setName("gfh562i");
		creditorAccount.setSecondaryIdentification("gfh562");
		
		obDomestic1.setCreditorAccount(creditorAccount);
		obDomestic1.setDebtorAccount(debtorAccount);
		OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount = new OBDomesticStandingOrder1FinalPaymentAmount();
		finalPaymentAmount.setAmount("12345.2314");
		finalPaymentAmount.setCurrency("gfh562");
		obDomestic1.setFinalPaymentAmount(finalPaymentAmount);
		OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();
		firstPaymentAmount.setAmount("12345.2314");
		firstPaymentAmount.setCurrency("gfh562");
		OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount = new OBDomesticStandingOrder1RecurringPaymentAmount();
		recurringPaymentAmount.setAmount("12345.2314");
		recurringPaymentAmount.setCurrency("gfh562");
		obDomestic1.setRecurringPaymentAmount(recurringPaymentAmount );
		obDomestic1.setFirstPaymentAmount(firstPaymentAmount );
		obDomestic1.setDebtorAccount(debtorAccount);
		obDomestic1.setCreditorAccount(creditorAccount);
		obDomestic1.setFrequency("gfh562");
		obDomestic1.setReference("gfh562");
		obDomestic1.setNumberOfPayments("12345");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		obDomestic1.setFirstPaymentDateTime(dateFormat.format(new Date()));
		obDomestic1.setRecurringPaymentDateTime(dateFormat.format(new Date()));
		obDomestic1.setFirstPaymentDateTime(dateFormat.format(new Date()));

		data.setInitiation(obDomestic1);
		authorisation.completionDateTime(dateFormat.format(new Date()));
		deliveryAddress.setAddressLine(addressLine);
		data.setAuthorisation(authorisation);
		standingOrderConsentsRequest.setData(data);
		standingOrderConsentsRequest.setRisk(risk);
		transformer.transformDomesticStandingOrdersResponseFromAPIToFDForInsert(standingOrderConsentsRequest);
	}

	@Test
	public void testTransformDomesticStandingOrdersResponseFromAPIToFDForInsertForNull(){
		
		CustomDStandingOrderConsentsPOSTRequest standingOrderConsentsRequest = new CustomDStandingOrderConsentsPOSTRequest();
		OBWriteDataDomesticStandingOrderConsent1 data = new OBWriteDataDomesticStandingOrderConsent1();
		OBRisk1 risk = new OBRisk1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBDomesticStandingOrder1 obDomestic1 = new OBDomesticStandingOrder1();
		List<String> addressLine = new ArrayList<String>();
		addressLine.add("fbf");
		addressLine.add("sfsdf");
		data.setPermission(OBExternalPermissions2Code.CREATE);
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		
		deliveryAddress.setStreetName("abc");
		
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		risk.setDeliveryAddress(deliveryAddress);
		debtorAccount.setSchemeName("gfh562");
		creditorAccount.setSchemeName("gfh562");

		data.setInitiation(obDomestic1);
		deliveryAddress.setAddressLine(addressLine);
		data.setAuthorisation(authorisation);
		standingOrderConsentsRequest.setData(data);
		standingOrderConsentsRequest.setRisk(risk);
		transformer.transformDomesticStandingOrdersResponseFromAPIToFDForInsert(standingOrderConsentsRequest);
	}
}













