/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStandingOrdersResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.client.AccountStandingOrderFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.constants.AccountStandingOrderFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.delegate.AccountStandingOrderFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.StandingOrderScheduleInstructionsresponse;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountStandingOrderFoundationServiceAdapter.
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
public class AccountStandingOrderFoundationServiceAdapter implements AccountStandingOrdersAdapter {

	@Value("${foundationService.standingOrdersBaseURL}")
	private String standingOrdersBaseURL;

	@Value("${foundationService.standingOrdersEndURL}")
	private String standingOrdersEndURL;

	/** The account StandingOrder foundation service delegate. */
	@Autowired
	private AccountStandingOrderFoundationServiceDelegate accountStandingOrderFoundationServiceDelegate;

	/** The account StandingOrder foundation service client. */
	@Autowired
	private AccountStandingOrderFoundationServiceClient accountStandingOrderFoundationServiceClient;

	@Value("${foundationService.consentFlowType}")
	private String consentFlowType;

	@Value("${foundationService.version}")
	private String version;

	@Override
	public PlatformAccountStandingOrdersResponse retrieveAccountStandingOrders(AccountMapping accountMapping,
			Map<String, String> params) {

		if (accountMapping == null || accountMapping.getPsuId() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		/* For Filtering */
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CHANNEL_ID, params.get("channelId"));
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());

		RequestInfo requestinfo = new RequestInfo();

		AccountDetails accountDetails;
		if (accountMapping.getAccountDetails() != null && !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		
		HttpHeaders httpHeaders = accountStandingOrderFoundationServiceDelegate.createRequestHeaders(requestinfo,
				accountMapping, params);
		StandingOrderScheduleInstructionsresponse standingOrderScheduleInstructionsresponse = null;
		if ((NullCheckUtils.isNullOrEmpty(accountDetails.getAccountSubType())
				&& params.get(PSD2Constants.CMAVERSION) == null)
				|| accountDetails.getAccountSubType().toString().contentEquals("CurrentAccount")
				|| accountDetails.getAccountSubType().toString().contentEquals("Savings")) {
			String finalURL = accountStandingOrderFoundationServiceDelegate.getFoundationServiceURL(
					standingOrdersBaseURL, version, accountDetails.getAccountNSC(), accountDetails.getAccountNumber(),
					standingOrdersEndURL);
			params.put(AccountStandingOrderFoundationServiceConstants.ACCOUNT_ID, accountDetails.getAccountId());
			requestinfo.setUrl(finalURL);

			standingOrderScheduleInstructionsresponse = accountStandingOrderFoundationServiceClient
					.restTransportForAccountStandingOrder(requestinfo, StandingOrderScheduleInstructionsresponse.class,
							httpHeaders);

			if (NullCheckUtils.isNullOrEmpty(standingOrderScheduleInstructionsresponse.getStandingOrdersList()))
				standingOrderScheduleInstructionsresponse = new StandingOrderScheduleInstructionsresponse();

		} else if (accountDetails.getAccountSubType().toString().contentEquals("CreditCard")) {
			standingOrderScheduleInstructionsresponse = new StandingOrderScheduleInstructionsresponse();
		}

		return accountStandingOrderFoundationServiceDelegate
				.transformResponseFromFDToAPI(standingOrderScheduleInstructionsresponse, params);

	}

}
