package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.client.DomesticPaymentConsentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.delegate.DomesticPaymentConsentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.transformer.DomesticPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;
import com.capgemini.psd2.rest.client.model.RequestInfo;
@Component
public class DomesticPaymentConsentsFoundationServiceAdapter implements DomesticPaymentStagingAdapter {
	@Autowired
	private DomesticPaymentConsentsFoundationServiceDelegate domPayConsDelegate;

	@Autowired
	private DomesticPaymentConsentsFoundationServiceClient domPayConsClient;

	@Autowired
	private DomesticPaymentConsentsFoundationServiceTransformer domPayConsTransformer;

	public CustomDPaymentConsentsPOSTResponse retrieveStagedDomesticPaymentConsents(CustomPaymentStageIdentifiers c,
			Map<String, String> params) {
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = domPayConsDelegate.createPaymentRequestHeaders(c,requestInfo, params);
		String domesticPaymentURL = domPayConsDelegate.getPaymentFoundationServiceURL(c.getPaymentSetupVersion(),
				c.getPaymentConsentId());
		requestInfo.setUrl(domesticPaymentURL);
		PaymentInstructionProposal paymentInstructionProposal = domPayConsClient.restTransportForDomesticPaymentFoundationService(requestInfo, PaymentInstructionProposal.class, httpHeaders);
		return domPayConsTransformer.transformDomesticPaymentResponse(paymentInstructionProposal, params);
	}

	public void updateStagedDomesticPaymentConsents(CustomPaymentStageIdentifiers customStageIdentifiers,
			CustomPaymentStageUpdateData stageUpdateData, Map<String, String> params) {
		// TODO Auto-generated method stub

	}

	public CustomDPaymentConsentsPOSTResponse createStagingDomesticPaymentConsents(
			CustomDPaymentConsentsPOSTRequest paymentConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params) {
		
		RequestInfo requestInfo = new RequestInfo();
		
		HttpHeaders httpHeaders = domPayConsDelegate.createPaymentRequestHeadersPost(customStageIdentifiers,requestInfo, params);
		
		String domesticPaymentPostURL = domPayConsDelegate.postPaymentFoundationServiceURL(customStageIdentifiers.getPaymentSetupVersion());
		
		requestInfo.setUrl(domesticPaymentPostURL);
		
		PaymentInstructionProposal paymentInstructionProposalRequest = domPayConsDelegate.transformDomesticConsentResponseFromAPIToFDForInsert(paymentConsentsRequest,params);
		
		PaymentInstructionProposal paymentInstructionProposalResponse = domPayConsClient.restTransportForDomesticPaymentFoundationServicePost(requestInfo, paymentInstructionProposalRequest, PaymentInstructionProposal.class, httpHeaders);
		System.out.println("response from FS="+paymentInstructionProposalResponse.toString());
		return domPayConsDelegate.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposalResponse,params);
	}

	@Override
	public CustomDPaymentConsentsPOSTResponse processDomesticPaymentConsents(
			CustomDPaymentConsentsPOSTRequest paymentConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		
		RequestInfo requestInfo = new RequestInfo();
		
		HttpHeaders httpHeaders = domPayConsDelegate.createPaymentRequestHeadersPost(customStageIdentifiers,requestInfo, params);
		
		String domesticPaymentPostURL = domPayConsDelegate.postPaymentFoundationServiceURL(customStageIdentifiers.getPaymentSetupVersion());
		
		requestInfo.setUrl(domesticPaymentPostURL);
		
		PaymentInstructionProposal paymentInstructionProposalRequest = domPayConsDelegate.transformDomesticConsentResponseFromAPIToFDForInsert(paymentConsentsRequest,params);
		
		PaymentInstructionProposal paymentInstructionProposalResponse = domPayConsClient.restTransportForDomesticPaymentFoundationServicePost(requestInfo, paymentInstructionProposalRequest, PaymentInstructionProposal.class, httpHeaders);
		
		return domPayConsDelegate.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposalResponse,params);
	
	}

}
