package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain;

import java.util.Objects;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.AuthenticationMethodCode;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Customer Authentication Session object
 */
@ApiModel(description = "Customer Authentication Session object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-08T13:41:31.197+05:30")

public class CustomerAuthenticationSession   {
  @JsonProperty("authenticationSessionIdentifier")
  private String authenticationSessionIdentifier = null;

  @JsonProperty("authenticationMethodCode")
  private AuthenticationMethodCode authenticationMethodCode = null;

  @JsonProperty("secureAccessKeyUsed")
  private String secureAccessKeyUsed = null;

  @JsonProperty("authenticationParameterText")
  @Valid
  private List<String> authenticationParameterText = null;

  @JsonProperty("sessionStartDateTime")
  private OffsetDateTime sessionStartDateTime = null;

  @JsonProperty("sessionEndDateTime")
  private OffsetDateTime sessionEndDateTime = null;

  public CustomerAuthenticationSession authenticationSessionIdentifier(String authenticationSessionIdentifier) {
    this.authenticationSessionIdentifier = authenticationSessionIdentifier;
    return this;
  }

  /**
   * A unique identifier for this particular Authentication attempt
   * @return authenticationSessionIdentifier
  **/
  @ApiModelProperty(value = "A unique identifier for this particular Authentication attempt")


  public String getAuthenticationSessionIdentifier() {
    return authenticationSessionIdentifier;
  }

  public void setAuthenticationSessionIdentifier(String authenticationSessionIdentifier) {
    this.authenticationSessionIdentifier = authenticationSessionIdentifier;
  }

  public CustomerAuthenticationSession authenticationMethodCode(AuthenticationMethodCode authenticationMethodCode) {
    this.authenticationMethodCode = authenticationMethodCode;
    return this;
  }

  /**
   * Get authenticationMethodCode
   * @return authenticationMethodCode
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AuthenticationMethodCode getAuthenticationMethodCode() {
    return authenticationMethodCode;
  }

  public void setAuthenticationMethodCode(AuthenticationMethodCode authenticationMethodCode) {
    this.authenticationMethodCode = authenticationMethodCode;
  }

  public CustomerAuthenticationSession secureAccessKeyUsed(String secureAccessKeyUsed) {
    this.secureAccessKeyUsed = secureAccessKeyUsed;
    return this;
  }

  /**
   * Get secureAccessKeyUsed
   * @return secureAccessKeyUsed
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getSecureAccessKeyUsed() {
    return secureAccessKeyUsed;
  }

  public void setSecureAccessKeyUsed(String secureAccessKeyUsed) {
    this.secureAccessKeyUsed = secureAccessKeyUsed;
  }

  public CustomerAuthenticationSession authenticationParameterText(List<String> authenticationParameterText) {
    this.authenticationParameterText = authenticationParameterText;
    return this;
  }

  public CustomerAuthenticationSession addAuthenticationParameterTextItem(String authenticationParameterTextItem) {
    if (this.authenticationParameterText == null) {
      this.authenticationParameterText = new ArrayList<String>();
    }
    this.authenticationParameterText.add(authenticationParameterTextItem);
    return this;
  }

  /**
   * Get authenticationParameterText
   * @return authenticationParameterText
  **/
  @ApiModelProperty(value = "")


  public List<String> getAuthenticationParameterText() {
    return authenticationParameterText;
  }

  public void setAuthenticationParameterText(List<String> authenticationParameterText) {
    this.authenticationParameterText = authenticationParameterText;
  }

  public CustomerAuthenticationSession sessionStartDateTime(OffsetDateTime sessionStartDateTime) {
    this.sessionStartDateTime = sessionStartDateTime;
    return this;
  }

  /**
   * Get sessionStartDateTime
   * @return sessionStartDateTime
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getSessionStartDateTime() {
    return sessionStartDateTime;
  }

  public void setSessionStartDateTime(OffsetDateTime sessionStartDateTime) {
    this.sessionStartDateTime = sessionStartDateTime;
  }

  public CustomerAuthenticationSession sessionEndDateTime(OffsetDateTime sessionEndDateTime) {
    this.sessionEndDateTime = sessionEndDateTime;
    return this;
  }

  /**
   * Get sessionEndDateTime
   * @return sessionEndDateTime
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getSessionEndDateTime() {
    return sessionEndDateTime;
  }

  public void setSessionEndDateTime(OffsetDateTime sessionEndDateTime) {
    this.sessionEndDateTime = sessionEndDateTime;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CustomerAuthenticationSession customerAuthenticationSession = (CustomerAuthenticationSession) o;
    return Objects.equals(this.authenticationSessionIdentifier, customerAuthenticationSession.authenticationSessionIdentifier) &&
        Objects.equals(this.authenticationMethodCode, customerAuthenticationSession.authenticationMethodCode) &&
        Objects.equals(this.secureAccessKeyUsed, customerAuthenticationSession.secureAccessKeyUsed) &&
        Objects.equals(this.authenticationParameterText, customerAuthenticationSession.authenticationParameterText) &&
        Objects.equals(this.sessionStartDateTime, customerAuthenticationSession.sessionStartDateTime) &&
        Objects.equals(this.sessionEndDateTime, customerAuthenticationSession.sessionEndDateTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(authenticationSessionIdentifier, authenticationMethodCode, secureAccessKeyUsed, authenticationParameterText, sessionStartDateTime, sessionEndDateTime);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CustomerAuthenticationSession {\n");
    
    sb.append("    authenticationSessionIdentifier: ").append(toIndentedString(authenticationSessionIdentifier)).append("\n");
    sb.append("    authenticationMethodCode: ").append(toIndentedString(authenticationMethodCode)).append("\n");
    sb.append("    secureAccessKeyUsed: ").append(toIndentedString(secureAccessKeyUsed)).append("\n");
    sb.append("    authenticationParameterText: ").append(toIndentedString(authenticationParameterText)).append("\n");
    sb.append("    sessionStartDateTime: ").append(toIndentedString(sessionStartDateTime)).append("\n");
    sb.append("    sessionEndDateTime: ").append(toIndentedString(sessionEndDateTime)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

