/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.capgemini.psd2.aisp.domain.OBBalanceType1Code;
import com.capgemini.psd2.aisp.domain.OBCashBalance1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.aisp.transformer.AccountBalanceTransformer;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.constants.AccountBalanceFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.BalanceBaseType;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.BalanceResponse;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.BalanceTypeEnum;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.CreditcardsBalanceResponse;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountBalanceFoundationServiceTransformer.
 */
@Component
public class AccountBalanceFoundationServiceTransformer implements AccountBalanceTransformer {

	/** The psd2 validator. */
	@Autowired
	private PSD2Validator psd2Validator;

	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	@Override
	public <T> PlatformAccountBalanceResponse transformAccountBalance(T inputBalanceObj, Map<String, String> params) {
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBCashBalance1> balanceList = new ArrayList<>();
		OBCashBalance1 responseDataObj = null;
		OBActiveOrHistoricCurrencyAndAmount amount = null;
		obReadBalance1Data.setBalance(balanceList);

		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);
		List<OBCashBalance1> oBCashBalance1 = new ArrayList<OBCashBalance1>();

		if (params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE)
				.equals(AccountBalanceFoundationServiceConstants.CURRENT_ACCOUNT)
				|| params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE)
						.equals(AccountBalanceFoundationServiceConstants.SAVINGS)
				|| NullCheckUtils.isNullOrEmpty(params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE))) {
			BalanceResponse balanceResponse = (BalanceResponse) inputBalanceObj;
			for (BalanceBaseType listOfBalancesItem : balanceResponse.getBalancesList()) {
				responseDataObj = new OBCashBalance1();
				amount = new OBActiveOrHistoricCurrencyAndAmount();
				responseDataObj.setAccountId(params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_ID));

				if (listOfBalancesItem.getBalanceType().equals(BalanceTypeEnum.LEDGER_BALANCE)
						|| listOfBalancesItem.getBalanceType().equals(BalanceTypeEnum.SHADOW_BALANCE)) {
					if (listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency() != null) {
						String localReportingCurrency = null;
						localReportingCurrency = BigDecimal
								.valueOf(Double.valueOf(
										listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency().toString()))
								.toPlainString();

						if (!localReportingCurrency.contains("."))
							localReportingCurrency = localReportingCurrency + ".0";
						if (!NullCheckUtils.isNullOrEmpty(listOfBalancesItem.getBalanceCurrency())) {
							String isocurrency = listOfBalancesItem.getBalanceCurrency().getIsoAlphaCode();
							amount.setCurrency(isocurrency);
						}
						if (new BigDecimal(listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency())
								.compareTo(BigDecimal.ZERO) == 0
								|| new BigDecimal(listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency())
										.compareTo(BigDecimal.ZERO) > 0) {
							responseDataObj.setCreditDebitIndicator(OBCashBalance1.CreditDebitIndicatorEnum.CREDIT);
							amount.setAmount(localReportingCurrency);
						} else {
							responseDataObj.setCreditDebitIndicator(OBCashBalance1.CreditDebitIndicatorEnum.DEBIT);
							if (localReportingCurrency != null) {
								localReportingCurrency = localReportingCurrency.replaceFirst("-", "");
								amount.setAmount(localReportingCurrency);
							}
						}
					}
					responseDataObj.setType(OBBalanceType1Code.EXPECTED);
					String balanceDate = timeZoneDateTimeAdapter
							.parseOffsetDateTimeToCMA(listOfBalancesItem.getBalanceDate());
					responseDataObj.setDateTime(balanceDate);
					responseDataObj.setAccountId(params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_ID));
					psd2Validator.validate(amount);
					responseDataObj.setAmount(amount);
					oBCashBalance1.add(responseDataObj);
				}
				psd2Validator.validate(oBCashBalance1);
			}
			platformAccountBalanceResponse.getoBReadBalance1().getData().getBalance().addAll(oBCashBalance1);
			psd2Validator.validate(platformAccountBalanceResponse);
		} else if (params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE)
				.equals(AccountBalanceFoundationServiceConstants.CREDIT_CARD)) {
			CreditcardsBalanceResponse creditcardsBalanceResponse = (CreditcardsBalanceResponse) inputBalanceObj;
			for (BalanceBaseType listOfBalancesItem : creditcardsBalanceResponse.getBalancesList()) {
				responseDataObj = new OBCashBalance1();
				amount = new OBActiveOrHistoricCurrencyAndAmount();
				responseDataObj.setAccountId(params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_ID));

				if (listOfBalancesItem.getBalanceType().equals(BalanceTypeEnum.LEDGER_BALANCE)) {
					if (listOfBalancesItem.getBalanceAmount().getTransactionCurrency() != null) {
						String transactionCurrency = null;
						transactionCurrency = BigDecimal
								.valueOf(Double.valueOf(
										listOfBalancesItem.getBalanceAmount().getTransactionCurrency().toString()))
								.toPlainString();

						if (!transactionCurrency.contains("."))
							transactionCurrency = transactionCurrency + ".0";
						if (new BigDecimal(listOfBalancesItem.getBalanceAmount().getTransactionCurrency())
								.compareTo(BigDecimal.ZERO) == 0
								|| new BigDecimal(listOfBalancesItem.getBalanceAmount().getTransactionCurrency())
										.compareTo(BigDecimal.ZERO) > 0) {
							responseDataObj.setCreditDebitIndicator(OBCashBalance1.CreditDebitIndicatorEnum.CREDIT);
							amount.setAmount(transactionCurrency);
						} else {
							responseDataObj.setCreditDebitIndicator(OBCashBalance1.CreditDebitIndicatorEnum.DEBIT);
							if (transactionCurrency != null) {
								transactionCurrency = transactionCurrency.replaceFirst("-", "");
								amount.setAmount(transactionCurrency);
							}
						}
						if (!NullCheckUtils.isNullOrEmpty(listOfBalancesItem.getBalanceCurrency())) {
							String isocurrency = listOfBalancesItem.getBalanceCurrency().getIsoAlphaCode();
							amount.setCurrency(isocurrency);
						}
					}
					responseDataObj.type(OBBalanceType1Code.INTERIMBOOKED);
					String balanceDate = timeZoneDateTimeAdapter
							.parseOffsetDateTimeToCMA(listOfBalancesItem.getBalanceDate());
					responseDataObj.setDateTime(balanceDate);
					responseDataObj.setAccountId(params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_ID));
					psd2Validator.validate(amount);
					responseDataObj.setAmount(amount);
					oBCashBalance1.add(responseDataObj);
				}

				if (listOfBalancesItem.getBalanceType().equals(BalanceTypeEnum.STATEMENT_CLOSING_BALANCE)) {
					if (listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency() != null) {
						String localReportingCurrency = null;
						localReportingCurrency = BigDecimal
								.valueOf(Double.valueOf(
										listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency().toString()))
								.toPlainString();
						if (!localReportingCurrency.contains("."))
							localReportingCurrency = localReportingCurrency + ".0";
						if (new BigDecimal(listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency())
								.compareTo(BigDecimal.ZERO) == 0
								|| new BigDecimal(listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency())
										.compareTo(BigDecimal.ZERO) > 0) {
							responseDataObj.setCreditDebitIndicator(OBCashBalance1.CreditDebitIndicatorEnum.CREDIT);
							amount.setAmount(localReportingCurrency);
						} else {
							responseDataObj.setCreditDebitIndicator(OBCashBalance1.CreditDebitIndicatorEnum.DEBIT);
							if (localReportingCurrency != null) {
								localReportingCurrency = localReportingCurrency.replaceFirst("-", "");
								amount.setAmount(localReportingCurrency);
							}
						}
						if (!NullCheckUtils.isNullOrEmpty(listOfBalancesItem.getBalanceCurrency())) {
							String isocurrency = listOfBalancesItem.getBalanceCurrency().getIsoAlphaCode();
							amount.setCurrency(isocurrency);
						}
					}
					responseDataObj.type(OBBalanceType1Code.OPENINGBOOKED);
					String balanceDate = timeZoneDateTimeAdapter
							.parseOffsetDateTimeToCMA(listOfBalancesItem.getBalanceDate());
					responseDataObj.setDateTime(balanceDate);
					responseDataObj.setAccountId(params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_ID));
					psd2Validator.validate(amount);
					responseDataObj.setAmount(amount);
					oBCashBalance1.add(responseDataObj);
				}

				if (listOfBalancesItem.getBalanceType().equals(BalanceTypeEnum.AVAILABLE_LIMIT)) {
					if (listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency() != null) {
						String localReportingCurrency = null;
						localReportingCurrency = BigDecimal
								.valueOf(Double.valueOf(
										listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency().toString()))
								.toPlainString();
						if (!localReportingCurrency.contains("."))
							localReportingCurrency = localReportingCurrency + ".0";
						if (new BigDecimal(listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency())
								.compareTo(BigDecimal.ZERO) == 0
								|| new BigDecimal(listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency())
										.compareTo(BigDecimal.ZERO) > 0) {
							responseDataObj.setCreditDebitIndicator(OBCashBalance1.CreditDebitIndicatorEnum.CREDIT);
							amount.setAmount(localReportingCurrency);
						} else {
							responseDataObj.setCreditDebitIndicator(OBCashBalance1.CreditDebitIndicatorEnum.DEBIT);
							if (localReportingCurrency != null) {
								localReportingCurrency = localReportingCurrency.replaceFirst("-", "");
								amount.setAmount(localReportingCurrency);
							}
						}
						if (!NullCheckUtils.isNullOrEmpty(listOfBalancesItem.getBalanceCurrency())) {
							String isocurrency = listOfBalancesItem.getBalanceCurrency().getIsoAlphaCode();
							amount.setCurrency(isocurrency);
						}
					}
					responseDataObj.type(OBBalanceType1Code.INTERIMAVAILABLE);
					String balanceDate = timeZoneDateTimeAdapter
							.parseOffsetDateTimeToCMA(listOfBalancesItem.getBalanceDate());
					responseDataObj.setDateTime(balanceDate);
					responseDataObj.setAccountId(params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_ID));
					psd2Validator.validate(amount);
					responseDataObj.setAmount(amount);
					oBCashBalance1.add(responseDataObj);
				}
				psd2Validator.validate(oBCashBalance1);
			}
			platformAccountBalanceResponse.getoBReadBalance1().getData().getBalance().addAll(oBCashBalance1);
			psd2Validator.validate(platformAccountBalanceResponse);
		}
		return platformAccountBalanceResponse;
	}
}
