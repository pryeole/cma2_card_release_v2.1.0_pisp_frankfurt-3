package com.capgemini.psd2.revoke.consent.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentDataResponse1;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.revoke.consent.mock.data.RevokeConsentMockData;
import com.capgemini.psd2.revoke.consent.mongo.db.adapter.impl.RevokeConsentMongoDbAdaptorImpl;
import com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository.RevokeAccountRequestCMA3Repository;
import com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository.RevokeAccountRequestRepository;

public class RevokeConsentMongoDbAdaptorImplTest {

	@Mock
	private RevokeAccountRequestCMA3Repository accountRequestRepository;

	@Mock
	private RevokeAccountRequestRepository accountRequestRepositoryV2;

	@Mock
	private AispConsentAdapterImpl aispConsentAdapter;

	@Mock
	private MongoTemplate mongoTemplate;
	
	@InjectMocks
	private RevokeConsentMongoDbAdaptorImpl revokeConsentMongoDbAdaptorImpl;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testRevokeConsentRequestAispV3Setup() {
		when(accountRequestRepository.findByConsentId(anyString())).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV3());
		when(accountRequestRepository.save(any(OBReadConsentResponse1Data.class))).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV3());
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentData());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("12345","12345");
	}
	
	@Test
	public void testRevokeConsentRequestAispV2Setup() {
		when(accountRequestRepository.findByConsentId(anyString())).thenReturn(null);
		when(accountRequestRepositoryV2.findByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV2());
		when(accountRequestRepository.save(any(OBReadConsentResponse1Data.class))).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV3());
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentData());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("12345","12345");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testRevokeConsentRequestAispV2SetupDataResourceException() {
		when(accountRequestRepository.findByConsentId(anyString())).thenReturn(null);
		when(accountRequestRepositoryV2.findByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV2());
		when(accountRequestRepository.save(any(OBReadConsentResponse1Data.class))).thenThrow(DataAccessResourceFailureException.class);
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentData());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("12345","12345");
	}
	
	@Test
	public void testRevokeConsentRequestPisp() {
		when(accountRequestRepository.findByConsentId(anyString())).thenReturn(null);
		when(accountRequestRepositoryV2.findByAccountRequestId(anyString())).thenReturn(null);
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentData());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(PaymentConsentsPlatformResource.class))).thenReturn(RevokeConsentMockData.getWriteResult());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(PispConsent.class))).thenReturn(RevokeConsentMockData.getWriteResult());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("12345","12345");
	}
	
	@Test
	public void testRevokeConsentRequestCisp() {
		when(accountRequestRepository.findByConsentId(anyString())).thenReturn(null);
		when(accountRequestRepositoryV2.findByAccountRequestId(anyString())).thenReturn(null);
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentData());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(PaymentConsentsPlatformResource.class))).thenReturn(RevokeConsentMockData.getWriteResultWithNoN());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(OBFundsConfirmationConsentDataResponse1.class))).thenReturn(RevokeConsentMockData.getWriteResult());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(CispConsent.class))).thenReturn(RevokeConsentMockData.getWriteResult());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("12345","12345");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testRevokeConsentRequestResourceNotFoundException() {
		when(accountRequestRepository.findByConsentId(anyString())).thenReturn(null);
		when(accountRequestRepositoryV2.findByAccountRequestId(anyString())).thenReturn(null);
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentData());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(PaymentConsentsPlatformResource.class))).thenReturn(RevokeConsentMockData.getWriteResultWithNoN());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(OBFundsConfirmationConsentDataResponse1.class))).thenReturn(RevokeConsentMockData.getWriteResultWithNoN());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("12345","12345");
	}
	
}
