/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

/**
 * The Class PSD2Constants.
 */
public class PSD2Constants {

	// ** The Constant CORRELATION_ID. *//*

	public static final String CORRELATION_ID = "x-fapi-interaction-id";

	/** The Constant ACCESS_TOKEN_NAME. */
	public static final String ACCESS_TOKEN_NAME = "Authorization";

	/** The Constant COLON. */
	public static final String COLON = " : ";

	/** The Constant SEMI_COLON. */
	public static final String SEMI_COLON = ";";

	/** The Constant COMMA. */
	public static final String COMMA = ",";

	/** The Constant CUSTOMER_IP_ADDRESS. */
	public static final String CUSTOMER_IP_ADDRESS = "x-fapi-customer-ip-address";

	/** The Constant CUSTOMER_LAST_LOGGED_TIME. */
	public static final String CUSTOMER_LAST_LOGGED_TIME = "x-fapi-customer-last-logged-time";

	/** The Constant FINANCIAL_ID. */
	public static final String FINANCIAL_ID = "x-fapi-financial-id";

	public static final String TOKEN_INTROSPECTION_DATA = "x-token-introspection-data";

	public static final String EQUAS = "=";

	public static final String QUESTIONMARK = "?";

	public static final String AMPERSAND = "&";

	public static final String OAUTH_ENC_URL = "oAuthEncUrl";

	public static final String CONSENT_FLOW_TYPE = "consentFlowType";

	public static final String CONSENT_FLOW_TYPE_LOWERCASE = "consentFlowTypeLowerCase";

	public static final String SLASH = "/";

	public static final String IDEMPOTENCY_KEY = "x-idempotency-key";

	public static final String TPP_CID = "tppCID";

	public static final String USER_ID = "x-user-id";

	public static final String CHANNEL_ID = "x-channel-id";

	public static final String CORRELATION_REQ_HEADER = "X-CORRELATION-ID";

	public static final String PLATFORM_IN_REQ_HEADER = "X-BOI-PLATFORM";

	public static final String CHANNEL_IN_REQ_HEADER = "X-BOI-CHANNEL";

	public static final String USER_IN_REQ_HEADER = "X-BOI-USER";

	public static final String PAYMENT_CREATED_ON = "PAYMENT-CREATED-ON";

	public static final String PAYMENT_VALIDATION_STATUS = "PAYMENT-VALIDATION-STATUS";

	public static final String PAYMENT_SUBMISSION_STATUS = "PAYMENT-SUBMISSION-STATUS";

	public static final String PISP_SUBMISSION_AUTHORIZATION_FLOW = "PISP-SUBMISSION-AUTHORIZATION-FLOW";

	public static final String PAYER_CURRENCY = "PAYER-CURRENCY";

	public static final String PAYER_JURISDICTION = "PAYER-JURISDICTION";

	public static final String JS_MSG = "JAVASCRIPT_ENABLE_MSG";

	public static final String ERROR_MESSAGE = "ERROR_MESSAGES";

	public static final String ACCOUNT_NUMBER = "ACCOUNT-NUMBER";

	public static final String ACCOUNT_NSC = "ACCOUNT-NSC";

	public static final String IBAN = "IBAN";

	public static final String SORTCODEACCOUNTNUMBER = "SORTCODEACCOUNTNUMBER";

	public static final String PAN = "PAN";

	public static final String BIC = "BICFI";

	public static final String CO_RELATION_ID = "correlationId";

	public static final String SERVER_ERROR_FLAG_ATTR = "serverErrorFlag";

	public static final String CHANNEL_NAME = "channelId";

	public static final String APPLICATION_NAME = "applicationName";

	public static final String OPENID_ACCOUNTS = "openid accounts";

	public static final String ACCOUNTS = "accounts";

	public static final String OPENID_PAYMENTS = "openid payments";

	public static final String OPENID_FUNDSCONFIRMATIONS = "openid fundsconfirmations";

	public static final String PAYMENTS = "payments";

	public static final String FUNDSCONFIRMATIONS = "fundsconfirmations";

	public static final String AISP = "AISP";

	public static final String PISP = "PISP";

	public static final String CISP = "CISP";

	public static final String OPEN_ID = "openid";

	public static final String FLOWTYPE = "flowType";

	public static final String PAGETYPE = "pageType";

	public static final String AUTHENTICATIONSTATUS = "authenticationStatus";

	public static final String ACTION = "action";

	public static final String OUTCOME = "outcome";

	public static final String CONSENTSTATUS = "consentStatus";

	public static final String PSUID = "psuId";

	public static final String USERNAME = "userName";

	public static final String ACCOUNT_PERMISSION = "ACCOUNT-PERMISSION";

	public static final String UICONTENT = "UIContent";

	public static final String CONTENT_SECURITY_POLICY = "Content-Security-Policy";

	public static final String REFERRER_ORIGIN = "referrer origin";

	public static final String SCHEME_NAME = "schemeName";

	public static final String PLAPPLID = "plApplId";

	public static final String X_SSL_Client_DN = "X-SSL-Client-DN";

	public static final String X_SSL_Client_CN = "X-SSL-Client-CN";

	public static final String X_SSL_Client_OU = "X-SSL-Client-OU";

	public static final String JWS_HEADER = "x-jws-signature";

	public static final String JOSE_HEADER_TYP = "typ";

	public static final String JOSE_HEADER_CTY = "cty";

	public static final String JOSE_HEADER_ALG = "alg";

	public static final String JOSE_HEADER_KID = "kid";

	public static final String JOSE_HEADER_B64 = "b64";

	public static final String JOSE_HEADER_ISSUER = "http://openbanking.org.uk/iss";

	public static final String JOSE_HEADER_IAT = "http://openbanking.org.uk/iat";

	public static final String JOSE_HEADER_CRITICAL_ARRAY = "crit";

	public static final String X5C = "x5c";

	public static final String KEYS = "keys";

	public static final String X509 = "X.509";

	public static final String KID = "kid";

	public static final String JSON = "json";

	public static final String JOSE = "JOSE";

	public static final String JWKS = ".jwks";

	public static final String CLIENT_ID = "clientId";
	
	public static final String CMA1 = "1.0";

	public static final String CMA2 = "2.0";
	
	public static final String CMA3 = "3.0";

	public static final String CMAVERSION = "cmaVersion";

	public static final String PAYMENT_CONSENT_DATA = "paymentConsentData";

	public static final String OB_SCHEME_PREFIX = "UK.OBIE.";
	
	public static final String IS_SANDBOX_ENABLED = "isSandboxEnabled";
	
	public static final String PAYMENT_RESOURCE_IDEMPOTENT = "idempoent";
	
	public static final String PAYMENT_RESOURCE_NON_IDEMPOTENT = "NonIdempotent";
	
	public static final String BORNEBYCREDITOR = "borneByCreditor";
	  
	public static final String BORNEBYDEBTOR = "borneByDebtor";
	  
	public static final String FOLLOWINGSERVICELEVEL = "followingServiceLevel";
	  
	public static final String SHARED = "shared";
	
	public static final String M_AUTHORISED = "m_authorised";
	
	public static final String M_AWAITINGFURTHERAUTHORISATION = "m_awaitingFurtherAuthorisation";
	
	public static final String M_REJECTED = "m_rejected";
	
	public static final String PROCESS_EXEC_INCOMPLETE = "process_exec_incomplete";
	
	public static final String PROCESS_EXEC_ABORT = "process_exec_abort";
	
	public static final String CURRENCY_USD = "USD";
	
	public static final String CURRENCY_INR = "INR";
	
	public static final String CURRENCY_EUR = "EUR";
	
	public static final String CURRENCY_GBP = "GBP";
	
	public static final String TENANT_ID = "tenant_id";
	
	public static final String NETSCALER_TENANT_ID_HEADER_NAME = "tenantid";
	
	public static final String PARTY_IDENTIFIER = "partyIdentifier";
	
	public static final String PROCESS_EXEC_INCOMPLETE_CONSENT = "process_exec_incomplete_consent";
	
	public static final String SANDBOX_PISP_GET_MOCKING = "internalErrorGET";
	
	public static final String SANDBOX_PISP_POST_MOCKING = "internalErrorPOST";
	
	public static final String REJECTED_CONDITION = "rejectedStatus";
	
	public static final String CUT_OFF_DATE_EXCEPTION = "cutOffDateException";
	
	public static final String SECONDARY_IDENTIFICATION_SANDBOX = "secondaryIdentificationRule";
	
	public static final String NOT_DEFINED = "not_defined";
	
	public static final String GET = "GET";
	
	public static final String NCA_ID_IDENTIFIER = "2.5.4.97";
	
	public static final String CN = "CN";
	
	public static final String PEM_BEGIN_MARKER = "-----BEGIN CERTIFICATE-----";

	public static final String PEM_END_MARKER = "-----END CERTIFICATE-----";
	
	public static final String REVOKED_REASON = "Consent Revoked Internally";
	
	public static final String NCAID_HEADER = "x-ssl-nca-id";
	
	public static final String FILE_JWKS_URL = "jwks-file-url";
	
	public static final String OID_2_5_4_97 = "2.5.4.97";
	
	public static final String UK_OBIE_IBAN = "UK_OBIE_IBAN";

	public static final Object BICFI = "BICFI";
	
}
