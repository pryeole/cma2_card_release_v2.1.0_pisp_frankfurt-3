/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.utilities;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.fasterxml.jackson.databind.DeserializationFeature;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonParser;

/**
 * The Class JSONUtilities.
 */
public final class JSONUtilities implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The mapper. */
	private static ObjectMapper mapper = null;
	
	/**
	 * Instantiates a new JSON utilities.
	 */
	private JSONUtilities(){
		
	}
	
	static {
		mapper = new ObjectMapper();
	}
	
	/**
	 * Gets the JSON out put from object.
	 *
	 * @param <T> the generic type
	 * @param obj the obj
	 * @return the JSON out put from object
	 */
	public static <T> String getJSONOutPutFromObject(T obj){
		String jsonString = null;
		try{
			jsonString = mapper.writeValueAsString(obj);
		}
		catch(JsonProcessingException je) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					je.getMessage()));
		}
		return jsonString;
	}
	
	public static <T> String getJSONOutPutFromArgsObject(T obj){
		String jsonString = null;
		try{
			List<Object> objList = Arrays.asList((Object[])obj);
			List<Object> convertedList = objList.stream()
												.filter(object -> !(object instanceof HttpServletResponse))
												.map(object -> mapReqObj(object))
												.collect(Collectors.toList());
			jsonString = mapper.writeValueAsString(convertedList);
		}
		catch(JsonProcessingException je) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.JSON_PROCESSING_ERROR));
		}
		return jsonString;
	}
	
	/**
	 * Gets the object from JSON string.
	 *
	 * @param <T> the generic type
	 * @param jsonString the json string
	 * @param classType the class type
	 * @return the object from JSON string
	 */
	public static <T> T getObjectFromJSONString(String jsonString,Class<T> classType) {
		T obj = null;
		try {
			mapper.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE);
			obj = mapper.readValue(jsonString, classType);
		} catch (IOException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					e.getMessage()));
		}
		return obj; 
	}
	
	/**
	 * Gets the object from JSON string.
	 *
	 * @param <T> the generic type
	 * @param jsonString the json string
	 * @param obj the obj
	 * @return the object from JSON string
	 */
	public static <T> T getObjectFromJSONString(String jsonString, T obj) {
		T jsonObj = null;
		try {
			mapper.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE);
			jsonObj = mapper.readerForUpdating(obj).readValue(jsonString);
		} catch (IOException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					e.getMessage()));
		}
		return jsonObj;
	}
	
	public static <T> T getObjectFromJSONString(ObjectMapper mapper,String jsonString, T obj) {
		T jsonObj = null;
		try {
			jsonObj = mapper.readerForUpdating(obj).readValue(jsonString);
		} catch (IOException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					e.getMessage()));
		}
		return jsonObj;
	}
	
	public static <T> T getObjectFromJSONString(ObjectMapper mapper,String jsonString,Class<T> classType) {
		T obj = null;
		try {
			obj = mapper.readValue(jsonString, classType);
		} catch (IOException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					e.getMessage()));
		}
		return obj; 
	}
	
	private static Object mapReqObj(Object object){
		if(object instanceof HttpServletRequest){
			Map<String,Object> reqObj = new HashMap<>();
			HttpServletRequest request = (HttpServletRequest)object;
			reqObj.put("RequestParams", request.getParameterMap());
			
			if(!request.getMethod().equalsIgnoreCase(RequestMethod.GET.toString())){
				try{
					reqObj.put("RequestBody", request.getReader().lines().collect(Collectors.toList()));
				}catch(IOException | IllegalStateException e){
				
				}
				
			}
			return reqObj;
		}else{
			return object;
		}
	}
	
	public static void jsonParser(String jsonString) {
		try {
			JsonParser parser = new JsonParser();
			parser.parse(jsonString);
		} catch (Exception e1) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT, e1.getMessage(), true));
		}
	}
}
