/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.utilities;

import java.io.StringReader;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.bc.BouncyCastleProviderSingleton;
import com.nimbusds.jose.jwk.ECKey;
/**
 * The Utility class provides methods to create public key.
 * 
 * @author nagoswam
 *
 */
public final class JWKSUtility {

	private JWKSUtility() {
	}

	static {
		org.apache.xml.security.Init.init();
		Security.addProvider(BouncyCastleProviderSingleton.getInstance());
	}

	
	public static Map<String, Key> buildKeyMap(final JsonObject obj)
			throws InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException {
		JsonObject keyJson = null;
		String kid = null;
		PublicKey key = null;
		Map<String, Key> keyMap = new HashMap<>();
		for (final JsonValue v : obj.getJsonArray("keys")) {
			keyJson = (JsonObject) v;
			kid = keyJson.getString("kid");
				key = buildPublicKey(keyJson);
				if (key != null) {
					keyMap.put(kid, key);
				}
		}
		return keyMap;
	}

	public static PublicKey buildPublicKey(final JsonObject keyJson)
			throws InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException {
		PublicKey publicKey = null;
		String kty = keyJson.getString("kty");
		if ("RSA".equals(kty)) {
			publicKey = buildRSAPublicKey(keyJson);
		}
		else if ("EC".equals(kty)) {
			publicKey = buildECPublicKey(keyJson);
		}
		return publicKey;
	}

	private static PublicKey buildRSAPublicKey(final JsonObject keyJson)
			throws InvalidKeySpecException, NoSuchAlgorithmException {

		BigInteger modulus = new BigInteger(1,
				org.apache.commons.codec.binary.Base64.decodeBase64(keyJson.getString("n")));
		BigInteger publicExponent = new BigInteger(1,
				org.apache.commons.codec.binary.Base64.decodeBase64(keyJson.getString("e")));
		return KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(modulus, publicExponent));
	}

	private static PublicKey buildECPublicKey(final JsonObject keyJson)
			throws ParseException, JOSEException {
		ECKey ec = ECKey.parse(keyJson.toString());
		return ec.toECPublicKey();
	}

	public static JsonObject getJWKSJsonObject(String jwks) {
		JsonReader reader = Json.createReader(new StringReader(jwks));
		JsonObject jwksJsonObject = reader.readObject();
		reader.close();
		return jwksJsonObject;
	}
	
}
