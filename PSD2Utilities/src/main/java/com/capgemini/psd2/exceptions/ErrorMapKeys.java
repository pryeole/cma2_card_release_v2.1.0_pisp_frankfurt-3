package com.capgemini.psd2.exceptions;

public interface ErrorMapKeys {

	String SIGNATURE_MISSING = "signature_missing";

	String SIGNATURE_INVALID_CONTENT = "signature_invalid_content";

	String SIGNATURE_PARSE_ERROR = "signature_parse_error";

	String SIGNATURE_INVALID_CLAIMS = "signature_invalid_claims";

	String SIGNATURE_HEADERTYPE_INVALID = "signature_headerType_invalid";

	String SIGNATURE_UNEXPECTED = "signature_unexpected";

	String SIGNATURE_HEADERCONTENTTYPE_INVALID = "signature_headerContentType_invalid";

	String SIGNATURE_ALGO_MISSING = "signature_algo_missing";

	String SIGNATURE_ALGO_INVALID = "signature_algo_invalid";

	String SIGNATURE_CRITARRAY_MISSING = "signature_critArray_missing";

	String SIGNATURE_CRITARRAY_INVALID = "signature_critArray_invalid";

	String SIGNATURE_IAT_MISSING = "signature_IAT_missing";

	String SIGNATURE_IAT_INVALID = "signature_IAT_invalid";

	String SIGNATURE_B64_MISSING = "signature_b64_missing";

	String SIGNATURE_KID_MISSING = "signature_kid_missing";

	String SIGNATURE_KID_INVALID = "signature_kid_invalid";

	String SIGNATURE_ISSUER_MISSING = "signature_issuer_missing";

	String SIGNATURE_ISSUER_INVALID = "signature_issuer_invalid";

	String SIGNATURE_INVALID = "signature_invalid";

	String SIGNATURE_MALFORMED = "signature_malformed";

	String SIGNATURE_B64_INVALID = "signature_b64_invalid";

	String RESOURCE_CONSENT_MISMATCH = "resource_consent_mismatch";

	String RESOURCE_INVALIDCONSENTSTATUS = "resource_invalidconsentstatus";

	String RESOURCE_INVALIDFORMAT = "resource_invalidformat";

	String RESOURCE_NOT_FOUND = "resource_not_found";

	String CURRENCY_NOT_SUPPORTED = "currency_not_supported";

	String UNEXPECTED_ERROR = "unexpected_error";

	String ACCOUNT_IDENTIFIER_INVALID = "account_identifier_invalid";

	String ACCOUNT_SECONDARYIDENTIFIER_INVALID = "account_secondaryidentifier_invalid";

	String AFTER_CUTOFF_TIME = "after_cutoff_time";

	String FREQUENCY = "frequency";

	String PAYMENT_CONTEXT_ERROR = "payment_context_error";

	String REMITTANCE_ERROR = "remittance_error";

	String INVALID_COUNTRY_SUBDIVISION = "invalid_country_subdivision";

	String INVALID_ADDRESS_LINE = "invalid_address_line";

	String INVALID_SCHEME = "invalid_scheme";

	String LOCALINSTRUMENT_INVALID = "localinstrument_invalid";

	String INVALID_COUNTRY_CODE = "invalid_country_code";

	String HEADER = "header";

	String CUSTOMER_IP_ADDRESS_HEADER = "customer_ip_address_header";

	String EXPECTED_EXCHANGE_DETAILS = "expected_exchange_details";

	String UNEXPECTED_EXCHANGE_DETAILS = "unexpected_exchange_details";

	String EXPECTED_PAYMENT_SETUP_ID = "expected_payment_setup_id";

	String INVALID_DATE = "invalid_date";

	String INVALID_DATE_FORMAT = "invalid_date_format";

	String INVALID_PAYMENT_ID = "invalid_payment_id";

	String INVALID_INTENT_ID = "invalid_intent_id";

	String NO_ACCOUNT_DETAILS_FOUND = "no_account_details_found";

	String INVALID_INTERACTION_ID = "invalid_interaction_id";

	String NO_PAYMENT_SETUP_RESOURCES_FOUND = "no_payment_setup_resources_found";

	String PAYMENT_ID_MISMATCH = "payment_id_mismatch";

	String MISSING_PAYMENT_SETUP_ID = "missing_payment_setup_id";

	String INVALID_PAYMENT_SETUP_STATUS = "invalid_payment_setup_status";

	String PAYMENT_SETUP_RESOURCE_EXPIRED = "payment_setup_resource_expired";

	String MISSING_PAYMENT_SUBMISSION_ID = "missing_payment_submission_id";

	String NO_EXCHANGE_RATE_FOUND = "no_exchange_rate_found";

	String SAME_CURRENCY_EXCHANGE = "same_currency_exchange_not_allowed";

	String CURRENCY_MISMATCH = "instructed_currency_has_to_match_currency_of_transfer_or_unit_currency";

	String COMPLETION_DATE = "completion_date_older";

	String FIELD = "FIELD";

	String SIGNATURE = "SIGNATURE";

	String INTERNAL = "INTERNAL";

	String HEADER_GENERIC = "HEADER";

	String RES_NOTFOUND = "RES_NOTFOUND";

	String INCOMPATIBLE = "INCOMPATIBLE";

	String FREQUENCY_GENERIC = "FREQUENCY";

	String AFTER_CUTOFF_DATE = "AFTER_CUTOFF_DATE";

	String ACCOUNT_REQUEST_NOT_FOUND = "account_request_data_not_found";

	String INTENT_ID_VALIDATION_ERROR = "intent_id_validation_error";

	String INVALID_ACCOUNT_ID = "invalid_account_id";

	String RESTRICTED_ACCESS = "restricted_access";

	String INSTRUCTION_PRIORITY_LOCAL_INSTRUMENT = "instruction_priority_local_instrument";

	String INSTRUCTION_PRIORITY_RATE_TYPE = "instruction_priority_rate_type";

	String NO_ACCOUNT_ID_FOUND = "no_account_id_found";

	String VALIDATION_ERROR = "validation_error";

	String VALIDATION_ERROR_OUTPUT = "validation_error_output";

	String PERMISSIONS_VALIDATION_ERROR = "permissions_validation_error";

	String REQUIRED_PERMISSION_NOT_PRESENT = "required_permission_not_present";

	String EXCLUDED_PERMISSIONS_FOUND = "excluded_permission_found";

	String NO_ACCOUNT_REQUEST_DATA_FOUND = "no_account_request_data_found";

	String NO_STATEMENT_ID_FOUND = "no_statement_id_found";

	String MERCHANT_CATEGORY_CODE_INVALID = "merchant_category_code_invalid";

	String CUSTOMER_LAST_LOGGEDIN = "customer_last_loggedin";

	String CREDITOR_AGENT_INCOMPLETE = "creditor_agent_incomplete";

	String INVALID_DATE_FIELDS_PRESENT = "invalid_date_combination";

	String INVALID_FIELDS_COMBINATION = "invalid_field_combination";

	String NO_CONSENT_ID_FOUND = "no_consent_id_found";

	String NO_STATEMENT_DATA_FOUND = "no_statement_data_found";

	String PISP_FILETYPE_NOT_VALID = "pisp_fileType_not_valid";

	String INVALID_FILE = "invalid_file";

	String FILE_METADATA_MISMATCH = "file_metadata_mismatch";

	String INVALID_DATE_RANGE = "invalid_date_range_provided";

	String REQ_PARAM_GENERIC = "REQ_PARAM_GENERIC";

	String INVALID_PRIOR_DATE = "invalid_prior_date";

	String INVALID_IDEMPOTENCY = "invalid_idempotency";

	String FILE_NOT_FOUND = "file_not_found";

}
