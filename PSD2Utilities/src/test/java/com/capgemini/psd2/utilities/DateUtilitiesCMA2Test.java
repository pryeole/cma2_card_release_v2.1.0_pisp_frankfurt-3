package com.capgemini.psd2.utilities;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.exceptions.PSD2Exception;


public class DateUtilitiesCMA2Test {

	@InjectMocks
	private DateUtilitiesCMA2 dateUtilitiesCMA2 = new DateUtilitiesCMA2() {
	};



	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void transformDateTimeInRequestExceptionTest(){
		dateUtilitiesCMA2.transformDateTimeInRequest("2007-12-03");
	}

	@Test
	public void transformDateTimeInRequestTest(){
		dateUtilitiesCMA2.transformDateTimeInRequest("2007-12-03T10:15:30+01:00");
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateDateTimeInRequestExceptionTest(){
		dateUtilitiesCMA2.validateDateTimeInRequest("2007-12-03");
	}

	@Test
	public void validateDateTimeInRequestTest(){
		dateUtilitiesCMA2.validateDateTimeInRequest("2007-12-03T10:15:30+01:00");
	}

	@Test(expected=PSD2Exception.class)
	public void validateDateTimeInResponseTest(){
		dateUtilitiesCMA2.validateDateTimeInResponse("2007-12-03");
	}

}
