/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.consent.cisp.rest.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.consent.cisp.helpers.CispConsentCreationDataHelper;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.StringUtils;

/**
 * ConsentAuthorize Controller
 * 
 * @author Capgemini
 */

@RestController
public class CispConsentAuthorizeController {

	private static final Logger LOG = LoggerFactory.getLogger(CispConsentAuthorizeController.class);

	@Autowired
	private LoggerUtils loggerUtils;
	
	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;

	@Autowired
	private CispConsentCreationDataHelper consentCreationDataHelper;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	@Qualifier("cispConsentAuthorizationHelper")
	private ConsentAuthorizationHelper consentAuthorizationHelper;

	@Autowired
	private SCAConsentHelperService helperService;

	@Autowired
	private CispConsentAdapter cispConsentApdater;

	@Autowired
	private PFConfig pfConfig;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpServletResponse response;

	@Value("${app.longlifeExpDuration:19}")
	private Integer years;
	
	/**
	 * This method is used for creating the consent
	 * 
	 * @param contract-
	 *            ConsentTemplate
	 * @return ConsentResponse - Response after creating consent
	 * @throws NamingException
	 */
	@RequestMapping(value = "/cisp/consent", method = RequestMethod.POST)
	public String createConsent(ModelAndView model, @RequestBody PSD2AccountsAdditionalInfo accountAdditionalInfo,
			@RequestParam String resumePath, @RequestParam String refreshTokenRenewalFlow) throws NamingException {

		CispConsent cispConsent = null;

		PSD2Account customerAccount = null;
		OBFundsConfirmationConsentResponse1 setupData = null;

		if (accountAdditionalInfo != null) {
			customerAccount = accountAdditionalInfo.getAccountdetails().get(0);
		}
		if (customerAccount == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}

		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);

		if (refreshTokenRenewalFlow != null && Boolean.valueOf(refreshTokenRenewalFlow)) {
			LOG.info("{\"Enter\":\"{}\",\"{}\"}",
					"com.capgemini.psd2.security.consent.cisp.rest.controllers.createConsent()",
					loggerUtils.populateLoggerData("renewalConsentSubmissionFlow"));
			cispConsent = cispConsentApdater.retrieveConsentByFundsIntentIdAndStatus(pickupDataModel.getIntentId(),
					ConsentStatusEnum.AUTHORISED);
			if (cispConsent == null) {
				throw PSD2Exception.populatePSD2Exception("Funds confirmation setup is not having authorized status.",
						ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}
		}
		if (cispConsent != null) {

			helperService.revokeAllPreviousGrants(pickupDataModel.getIntentId(),
					this.pfConfig.getGrantsrevocationapiuser(), this.pfConfig.getGrantsrevocationapipwd());
		} else {

			LOG.info("{\"Enter\":\"{}\",\"{}\"}",
					"com.capgemini.psd2.security.consent.cisp.rest.controllers.createConsent()",
					loggerUtils.populateLoggerData("freshConsentSubmissionFlow"));
			setupData = consentCreationDataHelper
					.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId());
			OBReadAccount2 OBReadAccount2 = consentCreationDataHelper.retrieveCustomerAccountListInfo(
					pickupDataModel.getUserId(), IntentTypeEnum.CISP_INTENT_TYPE.getIntentType(),
					requestHeaderAttributes.getCorrelationId(), pickupDataModel.getChannelId(),
					setupData.getData().getDebtorAccount().getSchemeName().toString(),
					requestHeaderAttributes.getTenantId(), pickupDataModel.getIntentId());

			PSD2Account unmaskedAccount = (PSD2Account) consentAuthorizationHelper.matchAccountFromList(OBReadAccount2,
					customerAccount);

			validateAccount(setupData, unmaskedAccount);

			Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId());
			String tppApplicationName = tppInformationAdaptor.fetchApplicationName(pickupDataModel.getClientId());

			consentCreationDataHelper.createConsent(unmaskedAccount, pickupDataModel.getUserId(),
					pickupDataModel.getClientId(), pickupDataModel.getIntentId(), pickupDataModel.getChannelId(),
					tppApplicationName, tppInformationObj, requestHeaderAttributes.getTenantId());
		}
		Long totalMinutes = null;
		if(setupData != null && setupData.getData().getExpirationDateTime() != null) {
			String expiryDateStr = setupData.getData().getExpirationDateTime();
			totalMinutes = getDurationInMinutes(expiryDateStr);
		}
		else {
			totalMinutes = computeLongLifeExpiryInMin();
		}

		pickupDataModel.setConsentExpiryInMinutes(totalMinutes);

		
		DropOffResponse dropOffResponse = helperService.dropOffOnConsentSubmission(pickupDataModel,
				pfConfig.getCispinstanceId(), pfConfig.getCispinstanceusername(), pfConfig.getAispinstancepassword());

		resumePath = pfConfig.getTenantSpecificResumePathBaseUrl(requestHeaderAttributes.getTenantId()).concat(resumePath);

		String redirectURI = UriComponentsBuilder.fromHttpUrl(resumePath)
				.queryParam(PFConstants.REF, dropOffResponse.getRef()).toUriString();

		model.addObject(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, redirectURI);
		SCAConsentHelper.invalidateCookie(response);
		
		LOG.info("{\"Exit\":\"{}\",\"{}\",\"consentCreated\":{}}",
				"com.capgemini.psd2.security.consent.cisp.rest.controllers.createConsent()",
				loggerUtils.populateLoggerData("createConsent"));
		
		return JSONUtilities.getJSONOutPutFromObject(model);
	}

	@RequestMapping(value = "/cisp/cancelConsent", method = RequestMethod.PUT)
	public String cancelConsent(ModelAndView model, @RequestParam Map<String, String> paramsMap) {

		CispConsent cispConsent = null;

		String serverErrorFlag = paramsMap.get(PSD2Constants.SERVER_ERROR_FLAG_ATTR);

		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);

		String refreshTokenRenewalFlow = paramsMap.get(PSD2SecurityConstants.REFRESH_TOKEN_RENEWAL_FLOW);

		//Defect Fix For P000428-791 : Once access token is generated, we can not cancel consent.Fetching consent inside if condition allows to cancel consent in OBFundsConfirmationConsent 
		cispConsent = cispConsentApdater.retrieveConsentByFundsIntentIdAndStatus(pickupDataModel.getIntentId(),
							ConsentStatusEnum.AUTHORISED);
		
		if (refreshTokenRenewalFlow != null && Boolean.valueOf(refreshTokenRenewalFlow)) {
		
			if (cispConsent == null) {
				throw PSD2Exception.populatePSD2Exception("Funds confirmation setup is not having authorized status.",
						ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}

		}

		if (cispConsent == null) {
			paramsMap.put(PSD2Constants.USER_IN_REQ_HEADER, pickupDataModel.getUserId());
			paramsMap.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, pickupDataModel.getChannelId());

			/* Log here for reporting : aisp consent cancel */
			LOG.info("{\"Enter\":\"{}\",\"{}\"}", "com.capgemini.psd2.security.consent.cisp.rest.controllers.cancelConsent()",
					loggerUtils.populateLoggerData("cancelConsent"));
			
			consentCreationDataHelper.cancelFundsConfirmationSetup(pickupDataModel.getIntentId(), paramsMap);
			
			LOG.info("{\"Exit\":\"{}\",\"{}\",\"cancelledDataModel\":{}}",
					"com.capgemini.psd2.security.consent.cisp.rest.controllers.cancelConsent()",
					loggerUtils.populateLoggerData("cancelConsent"), JSONUtilities.getJSONOutPutFromObject(pickupDataModel));
		}

		String resumePath = paramsMap.get(PFConstants.RESUME_PATH);

		resumePath = pfConfig.getTenantSpecificResumePathBaseUrl(requestHeaderAttributes.getTenantId()).concat(resumePath);

		if (serverErrorFlag != null && !serverErrorFlag.isEmpty() && Boolean.valueOf(serverErrorFlag)) {
			resumePath = UriComponentsBuilder.fromHttpUrl(resumePath).queryParam(PFConstants.REF, null).toUriString();
		} else {
			PFInstanceData pfInstanceData = new PFInstanceData();
			pfInstanceData.setPfInstanceId(pfConfig.getCispinstanceId());
			pfInstanceData.setPfInstanceUserName(pfConfig.getCispinstanceusername());
			pfInstanceData.setPfInstanceUserPwd(pfConfig.getAispinstancepassword());
			resumePath = helperService.cancelJourney(resumePath, pfInstanceData);
		}
		model.addObject(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, resumePath);
		SCAConsentHelper.invalidateCookie(response);
		return JSONUtilities.getJSONOutPutFromObject(model);
	}

	private void validateAccount(OBFundsConfirmationConsentResponse1 setUpData, PSD2Account account) {
		String hashValue = StringUtils.generateHashedValue(setUpData.getData().getDebtorAccount().getIdentification());
		if (!account.getHashedValue().equals(hashValue)) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CISP_DEBTOR_ACCOUNT_MATCH_FAILED);
		}

	}
	
	private Long getDurationInMinutes(String diffInDate) {
		Date expiryDate = DateUtilites.getDateFromISOStringFormat(diffInDate);
		Calendar cal = Calendar.getInstance();
		Long currentTime = cal.getTimeInMillis();
		cal.setTime(expiryDate);
		Long expiryTime = cal.getTimeInMillis();
		Long diffTime = expiryTime - currentTime;
		Long totalMinutes = (diffTime / 1000) /60 ;
		return totalMinutes;
	}
	
	private Long computeLongLifeExpiryInMin() {
		Calendar cal = Calendar.getInstance();
		Long currentTime = cal.getTimeInMillis();
		cal.add(Calendar.YEAR, years);
		Long expiryTime = cal.getTimeInMillis();
		Long diffInMillis = expiryTime - currentTime;
		Long minutes = diffInMillis/60000;
		return minutes;
	}

}