package com.capgemini.psd2.security.consent.aisp.helpers;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.aisp.domain.OBReadAccount2Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
@ConfigurationProperties
public class AispConsentCreationDataHelperImpl implements AispConsentCreationDataHelper {

	private static final Logger LOG = LoggerFactory.getLogger(AispConsentCreationDataHelperImpl.class);
	@Autowired
	private LoggerUtils loggerUtils;
	
	@Autowired
	@Qualifier("CustomerAccountListAdapter")
	private CustomerAccountListAdapter customerAccountListAdapter;

	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	@Qualifier("AccountRequestAdapter")
	private AccountRequestAdapter accountRequestAdapter;

	@Autowired
	private FraudSystemHelper fraudSystemHelper;

	@Autowired
	@Qualifier("aispConsentAuthorizationHelper")
	private ConsentAuthorizationHelper consentAuthorizationHelper;
	
	@Override
	public String retrieveAccountRequestSetupData(String intentId) {

		OBReadConsentResponse1 accountReq = accountRequestAdapter.getAccountRequestGETResponse(intentId);
		if (accountReq == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_REQUEST_DATA_FOUND);

		}
		Collections.sort(accountReq.getData().getPermissions());
		return JSONUtilities.getJSONOutPutFromObject(accountReq);
	}

	@Override
	public OBReadAccount2 retrieveCustomerAccountListInfo(String userId, String clientId, String flowType,
			String correlationId, String channelId, String tenantId, String intentId) {
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PSD2Constants.CMAVERSION, getCMAVersion(intentId));
		paramsMap.put(PSD2Constants.CHANNEL_ID, channelId);
		paramsMap.put(PSD2Constants.USER_ID, userId);
		paramsMap.put(PSD2Constants.CORRELATION_ID, correlationId);
		paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, flowType);
		paramsMap.put(PSD2Constants.TENANT_ID, tenantId);
		return customerAccountListAdapter.retrieveCustomerAccountList(userId, paramsMap);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void createConsent(List<OBAccount2> accountList, String userId, String cid, String intentId,
			String channelId, String headers, String tppAppName, Object tppInformation, String tennantId)
			throws NamingException {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.security.consent.aisp.helpers.createConsent()",
				loggerUtils.populateLoggerData("createConsent"));
		
		// Fraudnet Call
		Map<String, String> params = new HashMap<>();
		params.putAll(captureFraudParam(headers));
		params.put(PSD2Constants.CHANNEL_NAME, channelId);
		params.put(PSD2Constants.TENANT_ID, tennantId);
		fraudSystemHelper.captureFraudEvent(userId, (List<PSD2Account>) (List<?>) accountList, params);

		String legalEntityName = null;
		List<AccountDetails> accountDetails = new ArrayList<>();
		AispConsent consent = new AispConsent();

		for (OBAccount2 custAcctInfo : accountList) {
			AccountDetails acctDetail = new AccountDetails();
			acctDetail.setHashValue(((PSD2Account) custAcctInfo).getHashedValue());

			OBCashAccount3 oBAccountInfo = custAcctInfo.getAccount().get(0);
			Map<String, String> additionalInfo = ((PSD2Account) custAcctInfo).getAdditionalInformation();

			String accountNumber = null;
			String accountNSC = additionalInfo.get(PSD2Constants.ACCOUNT_NSC);
			accountNumber = additionalInfo.get(PSD2Constants.ACCOUNT_NUMBER);

			acctDetail.setAccountNumber(accountNumber);
			acctDetail.setAccountNSC(accountNSC);
			acctDetail.setAccountSubType(custAcctInfo.getAccountSubType());
			
			if(oBAccountInfo.getSchemeName()!=null && oBAccountInfo.getSchemeName().contains("UK")) {
				oBAccountInfo.setSchemeName((oBAccountInfo.getSchemeName().replace("UK.OBIE.", "")).toUpperCase());
			}
			
			acctDetail.setAccount(oBAccountInfo);
			acctDetail.setServicer(custAcctInfo.getServicer());
			acctDetail.setCurrency(custAcctInfo.getCurrency());
			acctDetail.setNickname(custAcctInfo.getNickname());
			accountDetails.add(acctDetail);
		}

		if (tppInformation != null && tppInformation instanceof BasicAttributes) {
			BasicAttributes tppInfoAttributes = (BasicAttributes) tppInformation;
			legalEntityName = returnLegalEntityName(tppInfoAttributes);
		}
		
		// addition of new field as per banks requirement 21-12-2018
		consent.setPartyIdentifier(
				((PSD2Account) accountList.get(0)).getAdditionalInformation().get(PSD2Constants.PARTY_IDENTIFIER));
		consent.setTenantId(tennantId);
		consent.setTppApplicationName(tppAppName);
		consent.setTppLegalEntityName(legalEntityName);
		consent.setChannelId(channelId);
		consent.setAccountDetails(accountDetails);
		consent.setPsuId(userId);
		consent.setTppCId(cid);
		consent.setAccountRequestId(intentId);
		ZonedDateTime startDate = ZonedDateTime.now();
		consent.setStartDate(DateUtilites.getCurrentDateInISOFormat(startDate));

		OBReadConsentResponse1 accountRequestData = accountRequestAdapter.getAccountRequestGETResponse(intentId);

		if (!NullCheckUtils.isNullOrEmpty(accountRequestData.getData().getExpirationDateTime())) {
			String consentExpiry = accountRequestData.getData().getExpirationDateTime();
			consent.setEndDate(consentExpiry);
		}

		if (!NullCheckUtils.isNullOrEmpty(accountRequestData.getData().getTransactionFromDateTime())) {
			consent.setTransactionFromDateTime(accountRequestData.getData().getTransactionFromDateTime());
		}

		if (!NullCheckUtils.isNullOrEmpty(accountRequestData.getData().getTransactionToDateTime())) {
			consent.setTransactionToDateTime(accountRequestData.getData().getTransactionToDateTime());
		}
		
		/* Consent Version should be same as Setup Version */
		consent.setCmaVersion(accountRequestData.getData().getCmaVersion());

		aispConsentAdapter.createConsent(consent);

		LOG.info("{\"Exit\":\"{}\",\"{}\",\"createdAispConsent\":{}}",
				"com.capgemini.psd2.security.consent.aisp.helpers.createConsent()",
				loggerUtils.populateLoggerData("createConsent"),
				JSONUtilities.getJSONOutPutFromObject(consent));
		
	}

	public static Map<String, String> captureFraudParam(String headers) {
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.FLOWTYPE, IntentTypeEnum.AISP_INTENT_TYPE.getIntentType());
		params.put(FraudSystemRequestMapping.FS_HEADERS, new String(Base64.getDecoder().decode(headers)));
		return params;
	}

	@Override
	public void cancelAccountRequest(String intentId) {
		// The case is if the consent present but token has not been issued yet
		// and allowing the TPP go initate auth flow again with the same intent.
		AispConsent consent = aispConsentAdapter.retrieveConsentByAccountRequestId(intentId);
		
		//Defect Fix For P000428-791 : Consent status will be updated only for AwaitingAuthorization
		if (consent != null){
			if(ConsentStatusEnum.REVOKED.equals(consent.getStatus()))
				throw PSD2Exception.populatePSD2Exception("Consent is not in correct status", ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			else if(ConsentStatusEnum.AWAITINGAUTHORISATION.equals(consent.getStatus()))
				aispConsentAdapter.updateConsentStatus(consent.getConsentId(), ConsentStatusEnum.REJECTED);
		}
						
		accountRequestAdapter.updateAccountRequestResponse(intentId, OBExternalRequestStatus1Code.REJECTED);
		
	}

	@Override
	public OBReadAccount2 findExistingConsentAccounts(String intentId) {
		OBReadAccount2 account = new OBReadAccount2();
		OBReadAccount2Data accountData = new OBReadAccount2Data();
		List<OBAccount2> accountList = new ArrayList<>();
		accountData.setAccount(accountList);
		AispConsent aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(intentId,
				ConsentStatusEnum.AUTHORISED);
		if (aispConsent != null) {
			List<OBAccount2> consentAccounts = consentAuthorizationHelper
					.populateAccountListFromAccountDetails(aispConsent);
			accountData.getAccount().addAll(consentAccounts);
			account.setData(accountData);
		}
		return account;
	}

	private String returnLegalEntityName(BasicAttributes basicAttributes) throws NamingException {
		return getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
	}

	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}
	
	@Override
	public String getCMAVersion(String intentId) {
		OBReadConsentResponse1 accountReq = accountRequestAdapter.getAccountRequestGETResponse(intentId);
		if (accountReq == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_REQUEST_DATA_FOUND);
		}
		String cmaVersion1 = accountReq.getData().getCmaVersion();
		if(cmaVersion1==null) {
			cmaVersion1 = "1.0";
		}
		return cmaVersion1;
	}

}