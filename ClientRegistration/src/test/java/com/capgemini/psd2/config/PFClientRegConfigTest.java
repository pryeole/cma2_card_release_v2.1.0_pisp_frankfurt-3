package com.capgemini.psd2.config;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.tpp.dtos.GrantScopes;

@RunWith(SpringJUnit4ClassRunner.class)
public class PFClientRegConfigTest {
	
	
	@InjectMocks
	private PFClientRegConfig pfClientRegConfig;
	
	@Before
	public void setUp() {
		
	}
	
	
	@Test
	public void pfClientRegConfigSuccessTest() throws Exception {
		
	//	pfClientRegConfig.setAdminpwd("password");
	//	pfClientRegConfig.setAdminuser("user");
		pfClientRegConfig.setClientByIdUrl("https://localhost:8000/123455");
		pfClientRegConfig.setClientsUrl("https://localhost:8000");
		pfClientRegConfig.setIdTokenSigningAlgorithm("alg");
		pfClientRegConfig.setPersistentGrantExpirationTime(1234234);
		pfClientRegConfig.setPersistentGrantExpirationTimeUnit("Second");
		pfClientRegConfig.setPersistentGrantExpirationType("time");
		pfClientRegConfig.setPolicyGroupId("1234e256");
	//	pfClientRegConfig.setPrefix("");
		pfClientRegConfig.setRefreshRolling("refresh");
		List<String> list = new ArrayList<>();
		list.add("str");
		pfClientRegConfig.setRestrictedResponseTypes(list);
		pfClientRegConfig.setSecretType("secret");
		pfClientRegConfig.getTokenMgrRefId().put("BOIUK","INTREFATM");
		pfClientRegConfig.setReplicateUrl("test");
		pfClientRegConfig.setCertIssuer("OB");
		pfClientRegConfig.setCaImportUrl("https://test");
		pfClientRegConfig.setClientCertIssuerDn("dn=123");
	//	pfClientRegConfig.setClientCertSubjectDn("cn=11");
		GrantScopes value=new GrantScopes();
		value.setScopes(Arrays.asList("AISP"));
		pfClientRegConfig.getGrantsandscopes().put("1", value);
		
		pfClientRegConfig.getPrefix().put("BOI", "https://localhost:8000/123455");
		pfClientRegConfig.getAdminuser().put("Admin", "user");
		pfClientRegConfig.getAdminpwdV2().put("pwd", "admin123");
		
		assertTrue(pfClientRegConfig.getAdminpwd("pwd").equals("admin123"));
		assertTrue(pfClientRegConfig.getAdminuser("Admin").equals("user"));
		assertTrue(pfClientRegConfig.getClientByIdUrl("BOI").equals("https://localhost:8000/123455https://localhost:8000/123455"));
		assertTrue(pfClientRegConfig.getClientsUrl("BOI").equals("https://localhost:8000/123455https://localhost:8000"));
		assertTrue(pfClientRegConfig.getIdTokenSigningAlgorithm().equals("alg"));
		assertTrue(pfClientRegConfig.getPersistentGrantExpirationTime().equals(1234234));
		assertTrue(pfClientRegConfig.getPersistentGrantExpirationTimeUnit().equals("Second"));
		assertTrue(pfClientRegConfig.getPersistentGrantExpirationType().equals("time"));
		assertTrue(pfClientRegConfig.getPolicyGroupId().equals("1234e256"));
		assertNotNull(pfClientRegConfig.getPrefix());
		assertTrue(pfClientRegConfig.getRefreshRolling().equals("refresh"));
		assertTrue(pfClientRegConfig.getRestrictedResponseTypes().get(0).equals("str"));
		assertTrue(pfClientRegConfig.getSecretType().equals("secret"));
		assertTrue(pfClientRegConfig.getTokenMgrRefId("BOIUK").equals("INTREFATM"));
		assertTrue(pfClientRegConfig.getReplicateUrl("BOI").equals("https://localhost:8000/123455test"));
		assertTrue(pfClientRegConfig.getCertIssuer().equals("OB"));
		assertTrue(pfClientRegConfig.getCaImportUrl("BOI").equals("https://localhost:8000/123455https://test"));
		assertTrue(pfClientRegConfig.getClientCertIssuerDn().equals("dn=123"));
	//	assertTrue(pfClientRegConfig.getClientCertSubjectDn().equals("cn=11"));
		assertTrue(pfClientRegConfig.getGrantsandscopes().get("1").equals(value));
	}
	

	
	
}