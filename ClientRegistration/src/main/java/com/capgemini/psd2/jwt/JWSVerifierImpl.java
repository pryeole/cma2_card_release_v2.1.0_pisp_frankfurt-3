package com.capgemini.psd2.jwt;

import java.security.Key;

import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.utilities.AbstractJWSVerifier;
import com.nimbusds.jose.Header;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.factories.DefaultJWSVerifierFactory;
import com.nimbusds.jose.proc.JWSVerifierFactory;

public class JWSVerifierImpl extends AbstractJWSVerifier{

	@Override
	public JWSVerifier createJWSVerifier(Key key,Header header) {
		JWSVerifierFactory jwsVerifierFactory = new DefaultJWSVerifierFactory();
		try {
			return jwsVerifierFactory.createJWSVerifier((JWSHeader) header,key);
		} catch (JOSEException e) {
			throw ClientRegistrationException.populatePortalException(ClientRegistrationErrorCodeEnum.INVALID_SSA_SIGNATURE);
		}
	}

}
