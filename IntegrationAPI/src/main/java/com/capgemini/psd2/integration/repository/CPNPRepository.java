package com.capgemini.psd2.integration.repository;

import java.util.List;

import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.capgemini.psd2.integration.dtos.CPNPEntity;

@Repository
@Lazy
@DependsOn({"CollectionNameLocator"})
public interface CPNPRepository extends MongoRepository<CPNPEntity, String> {

	public List<CPNPEntity> findByPsuIdAndTppIdAndCmaVersionIn(String psuId, String tppId, List<String> cmaVersion);

}
