package com.capgemini.psd2.integration.service.impl;

import java.time.LocalDateTime;
import java.util.Calendar;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestCMA3Repository;
import com.capgemini.psd2.aisp.consent.adapter.repository.AispConsentMongoRepository;
import com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.consent.adapter.repository.CispConsentMongoRepository;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentDataResponse1;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.repository.FundsConfirmationConsentRepository;
import com.capgemini.psd2.integration.service.IntentIdValidationService;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.DateUtilites;

@Service
public class IntentIdValidationServiceImpl implements IntentIdValidationService {

	@Autowired
	private AccountRequestCMA3Repository accountRequestRepository;

	@Autowired
	private AispConsentMongoRepository accountConsentRepository;

	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Autowired
	private FundsConfirmationConsentRepository fundsConfirmationConsentRepository;

	@Autowired
	private CispConsentMongoRepository cispConsentRepository;

	@Autowired
	private CompatibleVersionList compatibleVersionList;

	/** Account request expiry time. */
	@Value("${app.accountRequestExpiryTime:#{24}}")
	private Integer accountRequestExpiryTime;

	@Value("${app.paymentSetupExpiryTime:#{24}}")
	private Integer paymentSetupExpiryTime;

	@Override
	public void validateIntentId(String intentId, String clientId, String scope) {
		if ("openid accounts".equals(scope)) {

			validateAispIntent(intentId, clientId);

		} else if ("openid payments".equals(scope)) {

			validatePispIntent(intentId, clientId);

		} else if ("openid fundsconfirmations".equals(scope)) {

			validateCispIntent(intentId, clientId);

		} else {

			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SCOPE_MISSING_IN_REQUEST);
		}

	}

	public void validateAispIntent(String intentId, String clientId) {
		OBReadConsentResponse1Data accountRequestResponse = accountRequestRepository
				.findByConsentIdAndCmaVersionIn(intentId, compatibleVersionList.fetchVersionList());

		if (accountRequestResponse != null) {

			AispConsent aispConsent = accountConsentRepository.findByAccountRequestIdAndStatusAndCmaVersionIn(
					accountRequestResponse.getConsentId(), ConsentStatusEnum.AUTHORISED,
					compatibleVersionList.fetchVersionList());

			Calendar creationDate = DatatypeConverter.parseDate(accountRequestResponse.getCreationDateTime());
			creationDate.add(Calendar.HOUR, accountRequestExpiryTime);

			if (!accountRequestResponse.getTppCID().equalsIgnoreCase(clientId)
					|| accountRequestResponse.getStatus().toString()
							.equalsIgnoreCase(OBExternalRequestStatus1Code.REJECTED.toString())
					|| accountRequestResponse.getStatus().toString()
							.equalsIgnoreCase(OBExternalRequestStatus1Code.REVOKED.toString())) {
				throw PSD2Exception.populatePSD2Exception("Client Id does not match or intent is rejected / revoked",
						ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}

			if (accountRequestResponse.getExpirationDateTime() != null) {
				if (DateUtilites.getISODateFromString(accountRequestResponse.getExpirationDateTime())
						.isBefore(LocalDateTime.now())) {
					throw PSD2Exception.populatePSD2Exception("Intent has been expired now.",
							ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
				}
			}

			if (aispConsent != null) {
				if (!accountRequestResponse.getStatus().toString()
						.equalsIgnoreCase(OBExternalRequestStatus1Code.AUTHORISED.toString())) {
					throw PSD2Exception.populatePSD2Exception("Account setup is not having authorized status.",
							ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
				}
			} else {
				if (!accountRequestResponse.getStatus().toString()
						.equalsIgnoreCase(OBExternalRequestStatus1Code.AWAITINGAUTHORISATION.toString())
						|| creationDate.before(Calendar.getInstance())) {
					throw PSD2Exception.populatePSD2Exception(
							"Account setup is not having correct status or Account Setup has not been requested within allowable timeframe.",
							ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
				}
			}

		} else {
			throw PSD2Exception.populatePSD2Exception("Account request intent does not exist",
					ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
		}

	}

	public void validatePispIntent(String intentId, String clientId) {
		PaymentConsentsPlatformResource paymentSetUpResponse = paymentSetupPlatformAdapter
				.retrievePaymentSetupPlatformResource(intentId, intentId);

		if (paymentSetUpResponse != null) {

			Calendar creationDate = DatatypeConverter.parseDate(paymentSetUpResponse.getCreatedAt());
			creationDate.add(Calendar.HOUR, accountRequestExpiryTime);

			// CMA3 pisp status modified from ACCEPTEDTECHNICALVALIDATION to
			// AWAITINGAUTHORISATION
			// Keeping ACCEPTEDTECHNICALVALIDATION for backward compatibility.
			if (!paymentSetUpResponse.getTppCID().equals(clientId)
					|| (!paymentSetUpResponse.getStatus()
							.equals(PaymentStatusEnum.ACCEPTEDTECHNICALVALIDATION.getStatusCode())
							&& !paymentSetUpResponse.getStatus()
									.equals(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION.toString()))
					|| creationDate.before(Calendar.getInstance()))
				throw PSD2Exception.populatePSD2Exception(
						"Payment setup is not having correct status or Payment setup has not been requested within allowable timeframe.",
						ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
		} else {
			throw PSD2Exception.populatePSD2Exception("Payment request intent does not exist",
					ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
		}

	}

	public void validateCispIntent(String intentId, String clientId) {
		OBFundsConfirmationConsentDataResponse1 fundsConfirmationConsentResponse = fundsConfirmationConsentRepository
				.findByConsentIdAndCmaVersionIn(intentId, compatibleVersionList.fetchVersionList());

		if (fundsConfirmationConsentResponse != null) {

			CispConsent cispConsent = cispConsentRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(intentId,
					ConsentStatusEnum.AUTHORISED, compatibleVersionList.fetchVersionList());

			Calendar creationDate = DatatypeConverter.parseDate(fundsConfirmationConsentResponse.getCreationDateTime());
			creationDate.add(Calendar.HOUR, accountRequestExpiryTime);

			if (!fundsConfirmationConsentResponse.getTppCID().equalsIgnoreCase(clientId)
					|| !fundsConfirmationConsentResponse.getStatus().toString()
							.equalsIgnoreCase(OBExternalRequestStatus1Code.AWAITINGAUTHORISATION.toString())) {
				throw PSD2Exception.populatePSD2Exception("Client Id does not match or intent is rejected / revoked",
						ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}

			if (fundsConfirmationConsentResponse.getExpirationDateTime() != null) {
				if (DateUtilites.getISODateFromString(fundsConfirmationConsentResponse.getExpirationDateTime())
						.isBefore(LocalDateTime.now())) {
					throw PSD2Exception.populatePSD2Exception("Intent has been expired now.",
							ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
				}
			}

			if (cispConsent != null) {
				if (fundsConfirmationConsentResponse.getStatus().toString()
						.equalsIgnoreCase(OBExternalRequestStatus1Code.AUTHORISED.toString())) {
					throw PSD2Exception.populatePSD2Exception(
							"funds confirmation setup is not having authorized status.",
							ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
				}
			} else {
				if (!fundsConfirmationConsentResponse.getStatus().toString()
						.equalsIgnoreCase(OBExternalRequestStatus1Code.AWAITINGAUTHORISATION.toString())
						|| creationDate.before(Calendar.getInstance())) {
					throw PSD2Exception.populatePSD2Exception(
							"funds confirmation setup is not having correct status or funds confirmation setup has not been requested within allowable timeframe.",
							ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
				}
			}

		} else {
			throw PSD2Exception.populatePSD2Exception("funds confirmation setup intent does not exist",
					ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
		}
	}

}