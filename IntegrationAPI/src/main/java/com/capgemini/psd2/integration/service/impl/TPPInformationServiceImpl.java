package com.capgemini.psd2.integration.service.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.integration.dtos.TPPInformationDTO;
import com.capgemini.psd2.integration.service.TPPInformationService;
import com.capgemini.psd2.token.TPPInformation;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class TPPInformationServiceImpl implements TPPInformationService{

	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;

	@Override
	public TPPInformationDTO findTPPInformation(String clientId) {

		TPPInformationDTO tppInformationDTO = null;
		BasicAttributes tppInfoAttributes;
		
		Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(clientId);
		
		if(tppInformationObj instanceof BasicAttributes){
			tppInfoAttributes = (BasicAttributes)tppInformationObj;
			if(tppInfoAttributes != null){
				tppInformationDTO = populateTppInformationDTO(clientId, tppInfoAttributes);
			}
		}
		return tppInformationDTO;
	}

	private TPPInformationDTO populateTppInformationDTO(String clientId,BasicAttributes basicAttributes) {
		try{
			String registeredId = getAttributeValue(basicAttributes, TPPInformationConstants.CN);
			String legalEntityName = getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
			String roles = getAttributeValue(basicAttributes, TPPInformationConstants.X_ROLES);
			String blockFlag = getAttributeValue(basicAttributes, TPPInformationConstants.X_BLOCK);

			TPPInformation tppInformation = new TPPInformation();
			TPPInformationDTO tppInformationDTO = new TPPInformationDTO();

			tppInformationDTO.setClientId(clientId);

			tppInformation.setTppLegalEntityName(legalEntityName);
			tppInformation.setTppRegisteredId(registeredId);
			if(roles!=null){
				tppInformation.setTppRoles(new HashSet<String>(Arrays.asList(roles.split(","))));
			}	
			
			tppInformation.setTppBlock(blockFlag);
			tppInformationDTO.setTppInformation(tppInformation);
			return tppInformationDTO;
		}catch (NamingException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.NO_TPP_DATA_AVAILABLE);
		}
	}

	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}

	@Override
	public String fetchClientInformation(String clientId,String attribute) {
		List<Object> listObj = tppInformationAdaptor.fetchTppAppMappingForClient(clientId);
		BasicAttributes object = (BasicAttributes) listObj.get(0);
		try {
			return getAttributeValue(object, attribute);
		} catch (NamingException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.NO_CLIENT_APP_DATA_FOUND);
		}
	}

	@Override
	public String fetchJWKSUrl(String clientId) {
		String requestUrl=null;
		List<Object> listObj=tppInformationAdaptor.fetchTppAppMappingForClient(clientId);
		BasicAttributes object = (BasicAttributes) listObj.get(0);
		try {
			requestUrl= getAttributeValue(object, TPPInformationConstants.SOFTWARE_JWKS_ENDPOINT);
			if(NullCheckUtils.isNullOrEmpty(requestUrl)) {
				String ssaToken=getAttributeValue(object, TPPInformationConstants.SSA_TOKEN);
				Jwt byRefJwt = JwtHelper.decode(ssaToken);
				requestUrl=(String)JSONUtilities.getObjectFromJSONString(byRefJwt.getClaims(), Map.class).get(TPPInformationConstants.SSA_JWKS_ENDPOINT);
			}
			
		} catch (NamingException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.NO_CLIENT_APP_DATA_FOUND);
		}
		return requestUrl;
	}
		
}
