package com.capgemini.psd2.pisp.international.payments.routing;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.InternationalStandingOrderAdapter;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("iStandingOrderRoutingAdapter")
public class IStandingOrderRoutingAdapterImpl implements InternationalStandingOrderAdapter, ApplicationContextAware {

	private InternationalStandingOrderAdapter beanInstance;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.paymentSubmissionExecutionAdapter}")
	private String paymentSubmissionExecutionAdapter;

	/** The application context. */
	private ApplicationContext applicationContext;

	public InternationalStandingOrderAdapter getRoutingInstance(String adapterName) {
		if (beanInstance == null)
			beanInstance = (InternationalStandingOrderAdapter) applicationContext.getBean(adapterName);
		return beanInstance;
	}

	private Map<String, String> getHeaderParams(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params))
			params = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		params.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		params.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return params;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext=applicationContext;
		
	}


	/*@Override
	public CustomIStandingOrderConsentsPOSTResponse processStandingOrderConsents(
			CustomIStandingOrderConsentsPOSTRequest standingOrderConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		return getRoutingInstance(paymentSubmissionExecutionAdapter).processStandingOrderConsents(standingOrderConsentsRequest, customStageIdentifiers, getHeaderParams(params), successStatus, failureStatus);
	}


	@Override
	public CustomIStandingOrderConsentsPOSTResponse retrieveStagedInternationalStandingOrdersConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		return getRoutingInstance(paymentSubmissionExecutionAdapter).retrieveStagedInternationalStandingOrdersConsents(customPaymentStageIdentifiers, getHeaderParams(params));
		}*/

	@Override
	public CustomIStandingOrderPOSTResponse processInternationalStandingOrder(CustomIStandingOrderPOSTRequest request,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
		return getRoutingInstance(paymentSubmissionExecutionAdapter).processInternationalStandingOrder(request, paymentStatusMap, getHeaderParams(params));
		}

	@Override
	public CustomIStandingOrderPOSTResponse retrieveStagedInternationalStandingOrder(
			CustomPaymentStageIdentifiers identifiers, Map<String, String> params) {
		return getRoutingInstance(paymentSubmissionExecutionAdapter).retrieveStagedInternationalStandingOrder(identifiers, getHeaderParams(params));
		}




}
