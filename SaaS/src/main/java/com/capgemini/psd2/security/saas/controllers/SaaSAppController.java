package com.capgemini.psd2.security.saas.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServiceGetResponse;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostRequest;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostResponse;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.saas.constants.SaaSAppConstants;
import com.capgemini.psd2.security.saas.service.SaaSAppService;
import com.capgemini.psd2.utilities.JSONUtilities;

@RestController
public class SaaSAppController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SaaSAppController.class);
	
	@Autowired
	private SaaSAppService saaSAppService;
	
	@Autowired
	private HttpServletResponse response;
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttribute;
	
	@Autowired
	private PFConfig pfConfig;

	@RequestMapping(value = "/authenticate-dob", method=RequestMethod.POST, consumes= "application/json")
		public String authenticateDOB(@RequestBody CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest,
			@RequestParam(required = true, value ="channelId") String channelCode,
			@RequestParam(required = true, value ="digitalUserIdentifier") String digitalUserId) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("channelCode", channelCode);
		params.put("digitalUserId", digitalUserId);
		params.put("channelType", customAuthenticationServicePostRequest.getChannelType());
		LOGGER.info("Entering controller for submission of date of birth details" + customAuthenticationServicePostRequest.toString());
		ModelAndView model = new ModelAndView();
		CustomAuthenticationServiceGetResponse customAuthenticationServiceGetResponse=new CustomAuthenticationServiceGetResponse() ;
		customAuthenticationServiceGetResponse = saaSAppService.authenticateDOB(customAuthenticationServicePostRequest,params);
		if(!customAuthenticationServiceGetResponse.getSessionInitiationFailureIndicator()){
			model.addObject(SaaSAppConstants.CUSTOMER_AUTHENTICATION_SESSION, customAuthenticationServiceGetResponse.getAuthenticationParameters().getCustomerAuthenticationSessions());
			model.addObject(SaaSAppConstants.ELECTRONIC_DEVICES, customAuthenticationServiceGetResponse.getAuthenticationParameters().getElectronicDevices());
			model.addObject(SaaSAppConstants.DIGITAL_USER, customAuthenticationServiceGetResponse.getAuthenticationParameters().getDigitalUser());	
		}
		
		model.addObject(SaaSAppConstants.SESSION_INITIATION_FAILURE_INDICATOR, customAuthenticationServiceGetResponse.getSessionInitiationFailureIndicator());
		
		LOGGER.info("Exiting controller for submission of date of birth details" + model.toString());
		
		return JSONUtilities.getJSONOutPutFromObject(model);	
	}
	
	
	@RequestMapping(value = "/authenticate-pin-listOfDevices", method=RequestMethod.POST, consumes= "application/json")
	public String submitPINDetailsandListOfDevices(@RequestBody CustomAuthenticationServicePostRequest postRequest, @RequestParam String resumePath,
			@RequestParam("channelId") String channelCode,
			@RequestParam("digitalUserIdentifier") String digitalUserId) throws NamingException{
		Map<String, String> params = new HashMap<String, String>();
		params.put("channelCode", channelCode);
		params.put("digitalUserId", digitalUserId);
		
		LOGGER.info("Entering controller for submission of device and pin"  + "resumePath"+ resumePath +  postRequest.toString());
		
		ModelAndView model = new ModelAndView();
		CustomAuthenticationServicePostResponse customAuthenticationServicePostResponse =  saaSAppService.submitPINDetailsandSelectedDevices(postRequest,params);
		model.addObject(SaaSAppConstants.DIGITAL_USER, customAuthenticationServicePostResponse.getLoginResponse().getDigitalUser());
		model.addObject(SaaSAppConstants.DIGITAL_USER_SESSION, customAuthenticationServicePostResponse.getLoginResponse().getDigitalUserSession());
		model.addObject(SaaSAppConstants.SESSION_INITIATION_FAILURE_INDICATOR, customAuthenticationServicePostResponse.getLoginResponse().getDigitalUserSession().isSessionInitiationFailureIndicator());
		
		resumePath = pfConfig.getTenantSpecificResumePathBaseUrl(requestHeaderAttribute.getTenantId()).concat(resumePath);
		
		String redirectURI = UriComponentsBuilder.fromHttpUrl(resumePath).queryParam(PFConstants.REF, customAuthenticationServicePostResponse.getDropOffRef()).toUriString();
		
		model.addObject(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, redirectURI);
		
		LOGGER.info("Exiting controller for submission of device and pin"  + "resumePath"+ resumePath +  model.toString());
		
		if( !(customAuthenticationServicePostResponse.getLoginResponse().getDigitalUserSession().isSessionInitiationFailureIndicator()) 
				&& customAuthenticationServicePostResponse.getLoginResponse().getDigitalUser().getSecureKeyAttemptsRemainingCount().intValue() !=0 
				&&  !customAuthenticationServicePostResponse.getLoginResponse().getDigitalUser().isDigitalUserLockedOutIndicator()){
			SCAConsentHelper.invalidateCookie(response);
		}
		
		return JSONUtilities.getJSONOutPutFromObject(model);
	}
	
}