package com.capgemini.psd2.security.saas.cancel.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.LogAttributesPlatformResources;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.saas.model.IntentTypeEnum;
import com.capgemini.psd2.utilities.JSONUtilities;

@RestController
public class SaaSCancelController {

	private static final Logger LOG = LoggerFactory.getLogger(SaaSCancelController.class);
	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	@Qualifier("pispScaConsentOperationsRoutingAdapter")
	private PispScaConsentOperationsAdapter pispStageOperationsAdapter;

	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Autowired
	private SCAConsentHelperService helperService;

	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	private CispConsentAdapter cispConsentAdapter;

	@Autowired
	private PispConsentAdapter pispConsentAdapter;

	@Autowired
	@Qualifier(value = "accountRequestMongoDbAdaptor")
	private AccountRequestAdapter accountRequestAdapter;

	@Autowired
	@Qualifier(value = "fundsConfirmationConsentMongoDbAdapter")
	private FundsConfirmationConsentAdapter fundsConfirmationsConsentAdapter;

	@Autowired
	private PFConfig pfConfig;

	@Value("${spring.application.name}")
	private String applicationName;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpServletResponse response;

	@RequestMapping(value = "/cancel", method = RequestMethod.PUT)
	public String cancelSetUp(ModelAndView model, @RequestParam String oAuthUrl, @RequestParam String serverErrorFlag,
			@RequestParam String channelId) throws ParseException {
		
		LOG.info("EnteringCancelSetup");
		String redirectURI;
		redirectURI = pfConfig
				.getTenantSpecificResumePathBaseUrl(request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME))
				.concat(oAuthUrl);
		LOG.info("BeforeIntentData");
		PickupDataModel intentData = (PickupDataModel) request.getAttribute(SCAConsentHelperConstants.INTENT_DATA);
		LOG.info("AfterIntentData");
		if (serverErrorFlag != null && !serverErrorFlag.isEmpty() && Boolean.valueOf(serverErrorFlag)) {
			redirectURI = UriComponentsBuilder.fromHttpUrl(redirectURI).queryParam(PFConstants.REF, null).toUriString();
		} else {
			PFInstanceData pfInstanceData = new PFInstanceData();
			pfInstanceData.setPfInstanceId(pfConfig.getScainstanceId());
			pfInstanceData.setPfInstanceUserName(pfConfig.getScainstanceusername());
			pfInstanceData.setPfInstanceUserPwd(pfConfig.getScainstancepassword());
			redirectURI = helperService.cancelJourney(redirectURI, pfInstanceData);
		}
		if (channelId == null || channelId.trim().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception("Channel Id is not provided",
					ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
		}
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, channelId);
		LOG.info("BeforeMethodCall");
		cancelSetupRequest(intentData, paramsMap);
		LOG.info("BeforeMethodCall");
		model.addObject(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, redirectURI);
		model.addObject(PSD2Constants.APPLICATION_NAME, applicationName);
		SCAConsentHelper.invalidateCookie(response);
		return JSONUtilities.getJSONOutPutFromObject(model);

	}

	private void cancelSetupRequest(PickupDataModel intentData, Map<String, String> paramsMap) throws ParseException {
		LOG.info("intentDataEnumType" + intentData.getIntentTypeEnum().getIntentType());
		if (intentData.getIntentTypeEnum().getIntentType()
				.equalsIgnoreCase(IntentTypeEnum.AISP_INTENT_TYPE.getIntentType())) {

			cancelAispSetUpRequest(intentData, paramsMap);

		} else if (intentData.getIntentTypeEnum().getIntentType()
				.equalsIgnoreCase(IntentTypeEnum.PISP_INTENT_TYPE.getIntentType())) {

			cancelPispSetUpRequest(intentData, paramsMap);

		} else if (intentData.getIntentTypeEnum().getIntentType()
				.equalsIgnoreCase(IntentTypeEnum.CISP_INTENT_TYPE.getIntentType())) {

			cancelCispSetUpRequest(intentData, paramsMap);

		}
	}

	public void cancelAispSetUpRequest(PickupDataModel intentData, Map<String, String> paramsMap) {
		/* saas cancel call */
		/* Log here for reporting : Consent platform status change */
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.security.saas.cancel.controller.cancelAispSetUpRequest()",
				loggerUtils.populateLoggerData("cancelAispSetUpRequest"));
		
		AispConsent consent;
		OBReadConsentResponse1 accountSetupResponse = accountRequestAdapter
				.getAccountRequestGETResponse(intentData.getIntentId());

		consent = aispConsentAdapter.retrieveConsentByAccountRequestId(intentData.getIntentId());
		
		if (accountSetupResponse != null && accountSetupResponse.getData() != null) {
			//Defect Fix For P000428-791 : Once access token is generated, we can not cancel consent.
			
			LOG.info("cancelAispSetUpRequest accountSetupResponseStatus" + accountSetupResponse.getData().getStatus());
			if(consent!=null){
				LOG.info("cancelAispSetUpRequest ConsentStatus" + consent.getStatus());
			}
			if (!com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code.AWAITINGAUTHORISATION.equals(accountSetupResponse.getData().getStatus())
					&& (consent == null || !ConsentStatusEnum.AWAITINGAUTHORISATION.equals(consent.getStatus()))) {
			//throw PSD2Exception.populatePSD2Exception("Consent is not in correct status", ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
				LOG.info("cancelAispSetUpRequest insideDefectFixBlockBeforeRetrurning");
				return;
			} 
			 
			if (accountSetupResponse.getData().getStatus()
					.equals(com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code.AUTHORISED)
					&& (consent == null || !consent.getStatus().equals(ConsentStatusEnum.AUTHORISED))) {
				LOG.info("insideAuthorisedBlock");
				throw PSD2Exception.populatePSD2Exception("Consent is not in correct status", ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}

			if (accountSetupResponse.getData().getStatus()
					.equals(com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code.AWAITINGAUTHORISATION)
					&& (consent != null && !consent.getStatus().equals(ConsentStatusEnum.AWAITINGAUTHORISATION))) {
				LOG.info("insidethirdblock");
				throw PSD2Exception.populatePSD2Exception("Consent is not in correct status",
						ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}

		}

		if (consent != null && consent.getStatus().equals(ConsentStatusEnum.AWAITINGAUTHORISATION)) {
			LOG.info("insideUpdateBlockBeforeUpdatingConsent");
			aispConsentAdapter.updateConsentStatus(consent.getConsentId(), ConsentStatusEnum.REJECTED);
		}
		LOG.info("BeforeUpdatingAccountSetup");
		accountRequestAdapter.updateAccountRequestResponse(intentData.getIntentId(),
				com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code.REJECTED);

		LOG.info("{\"Exit\":\"{}\",\"{}\",\"cancelledIntentData\":{}}",
				"com.capgemini.psd2.security.saas.cancel.controller.cancelAispSetUpRequest()",
				loggerUtils.populateLoggerData("cancelAispSetUpRequest"),
				JSONUtilities.getJSONOutPutFromObject(intentData));
	}

	public void cancelPispSetUpRequest(PickupDataModel intentData, Map<String, String> paramsMap) {
		
		//This logger is only for MI Report
				LOG.info("{\"Enter\":\"{}\",\"{}\"}",
						"com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl.updatePaymentSetupPlatformResource()",
						loggerUtils.populateLoggerData("updatePaymentSetupPlatformResource"));

		PispConsent pispConsent = pispConsentAdapter.retrieveConsentByPaymentId(intentData.getIntentId());
		//Defect Fix For P000428-791 : Once access token is generated, we can not cancel consent.
		if((pispConsent != null) && !ConsentStatusEnum.AWAITINGAUTHORISATION.equals(pispConsent.getStatus()))
			throw PSD2Exception.populatePSD2Exception("Consent is not in correct status", ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
		
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		updateData.setSetupStatusUpdated(Boolean.TRUE);
		updateData.setSetupStatus(PaymentStatusEnum.REJECTED.getStatusCode());
		updateData.setSetupStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		/* Update platform status to Rejected */
		paymentSetupPlatformAdapter.updatePaymentSetupPlatformResource(intentData.getIntentId(), updateData);

		/* Removed call for retrieve payment stage details */
		/* Update payment setup status to Rejected */
		CustomPaymentStageIdentifiers stageIdentifiers = paymentSetupPlatformAdapter
				.populateStageIdentifiers(intentData.getIntentId());
			
		pispStageOperationsAdapter.updatePaymentStageData(stageIdentifiers, updateData, paramsMap);
		

		LogAttributesPlatformResources updatedConsentPlatformResource = new LogAttributesPlatformResources();
		updatedConsentPlatformResource.setStatus(PaymentStatusEnum.REJECTED.getStatusCode());
		updatedConsentPlatformResource.setPaymentConsentId(stageIdentifiers.getPaymentConsentId());
		updatedConsentPlatformResource.setPaymentType(stageIdentifiers.getPaymentTypeEnum());
		
		//This logger is only for MI report.
		LOG.info("{\"Exit\":\"{}\",\"{}\",\"updatedConsentPlatformResource\":{}}",
				"com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl.updatePaymentSetupPlatformResource()",
				loggerUtils.populateLoggerData("updatePaymentSetupPlatformResource"),
				JSONUtilities.getJSONOutPutFromObject(updatedConsentPlatformResource));

		if (pispConsent != null) {
			pispConsentAdapter.updateConsentStatus(pispConsent.getConsentId(), ConsentStatusEnum.REJECTED);
		}

	}

	public void cancelCispSetUpRequest(PickupDataModel intentData, Map<String, String> paramsMap) {
		/* saas cancel call */
		/* Log here for reporting : Consent platform status change */
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.security.saas.cancel.controller.cancelCispSetUpRequest()",
				loggerUtils.populateLoggerData("cancelCispSetUpRequest"));

		CispConsent consent;
		OBFundsConfirmationConsentResponse1 fundsConfirmationSetup = fundsConfirmationsConsentAdapter
				.getFundsConfirmationConsentPOSTResponse(intentData.getIntentId());

		consent = cispConsentAdapter.retrieveConsentByFundsIntentId(intentData.getIntentId());

		//Defect Fix For P000428-791 : Once access token is generated, we can not cancel consent.
		if (!OBExternalRequestStatus1Code.AWAITINGAUTHORISATION.equals(fundsConfirmationSetup.getData().getStatus())
								&& (consent == null || !ConsentStatusEnum.AWAITINGAUTHORISATION.equals(consent.getStatus()))) {
						throw PSD2Exception.populatePSD2Exception("Consent is not in correct status",
									ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
		}
		 
				
		if (fundsConfirmationSetup.getData().getStatus().equals(OBExternalRequestStatus1Code.AUTHORISED)
				&& (consent == null || !consent.getStatus().equals(ConsentStatusEnum.AUTHORISED))) {

			throw PSD2Exception.populatePSD2Exception("Consent is not in correct status",
					ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
		}

		if (fundsConfirmationSetup.getData().getStatus().equals(OBExternalRequestStatus1Code.AWAITINGAUTHORISATION)
				&& (consent != null && !consent.getStatus().equals(ConsentStatusEnum.AWAITINGAUTHORISATION))) {
			throw PSD2Exception.populatePSD2Exception("Consent is not in correct status",
					ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
		}

		if (consent != null && consent.getStatus().equals(ConsentStatusEnum.AWAITINGAUTHORISATION)) {
			cispConsentAdapter.updateConsentStatus(consent.getConsentId(), ConsentStatusEnum.REJECTED);
		}
		fundsConfirmationsConsentAdapter.updateFundsConfirmationConsentResponse(intentData.getIntentId(),
				OBExternalRequestStatus1Code.REJECTED);

		LOG.info("{\"Exit\":\"{}\",\"{}\",\"cancelledIntentData\":{}}",
				"com.capgemini.psd2.security.saas.cancel.controller.cancelCispSetUpRequest()",
				loggerUtils.populateLoggerData("cancelCispSetUpRequest"),
				JSONUtilities.getJSONOutPutFromObject(intentData));
	}
}