package com.capgemini.psd2.security.saas.service;

import java.util.Map;

import javax.naming.NamingException;

import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServiceGetResponse;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostRequest;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostResponse;

public interface SaaSAppService {

	public CustomAuthenticationServiceGetResponse authenticateDOB(CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest, Map<String, String> params1);

	public CustomAuthenticationServiceGetResponse retrievePINandListOfDevices(String username);

	public CustomAuthenticationServicePostResponse submitPINDetailsandSelectedDevices(CustomAuthenticationServicePostRequest postRequest, Map<String, String> params1) throws NamingException;
}
