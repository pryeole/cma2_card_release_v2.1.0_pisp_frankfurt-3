package com.capgemini.psd2.pisp.payment.setup.transformer;


import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;

public interface InternationalPaymentConsentsResponseTransformer {
	
	//Interfaces for DomesticPayment Payments v3.0 API 
	public CustomIPaymentConsentsPOSTResponse internationalPaymentConsentsResponseTransformer(CustomIPaymentConsentsPOSTResponse paymentConsentsResponse, PaymentConsentsPlatformResource paymentSetupPlatformResource, String methodType);
	
}

