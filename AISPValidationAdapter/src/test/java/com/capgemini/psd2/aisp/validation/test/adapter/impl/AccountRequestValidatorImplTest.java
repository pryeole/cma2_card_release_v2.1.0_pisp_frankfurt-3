package com.capgemini.psd2.aisp.validation.test.adapter.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.validation.adapter.constants.CommonAccountValidations;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountBalanceValidatorImpl;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.DPaymentsRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomestic1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticConsent1;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRequestValidatorImplTest {

	/*
	 * Creating required objects
	 */

	List<String> addressLineList = new ArrayList<String>();
	List<String> countrySubDivisionList = new ArrayList<String>();
	OBWriteDataDomesticConsent1 data = new OBWriteDataDomesticConsent1();
	OBDomestic1 initiation = new OBDomestic1();
	OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
	OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
	OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
	OBWriteDomesticConsent1 paymentConsentsRequest = new OBWriteDomesticConsent1();
	OBRisk1 risk = new OBRisk1();
	OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
	OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6();
	OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
	OBAuthorisation1 authorisation = new OBAuthorisation1();
	OBWriteDataDomestic1 paymentData = new OBWriteDataDomestic1();
	CustomDPaymentConsentsPOSTRequest req = new CustomDPaymentConsentsPOSTRequest();
	CustomDPaymentsPOSTRequest paymentSubmissionRequest = new CustomDPaymentsPOSTRequest();
	DPaymentsRetrieveGetRequest submissionRetrieveRequest = new DPaymentsRetrieveGetRequest();
	Field reqValidationEnabled = null;
	Field paymentIdRegexValidator = null;
	PaymentDomesticSubmitPOST201Response pResponse = new PaymentDomesticSubmitPOST201Response();
	OBWriteDataDomesticResponse1 domesticData = new OBWriteDataDomesticResponse1();
	CustomDPaymentConsentsPOSTResponse cResponse = new CustomDPaymentConsentsPOSTResponse();
	OBWriteDataDomesticConsentResponse1 domData = new OBWriteDataDomesticConsentResponse1();

	@InjectMocks
	private AccountBalanceValidatorImpl domesticPaymentValidatorImpl;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> genericErrorMessages = new HashMap<>();
		genericErrorMessages.put("FIELD", "freq ma");
		genericErrorMessages.put("INTERNAL", "freq ma");

		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);

		try {
			reqValidationEnabled = AccountBalanceValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(domesticPaymentValidatorImpl, true);
			paymentIdRegexValidator = AccountBalanceValidatorImpl.class.getDeclaredField("paymentIdRegexValidator");
			paymentIdRegexValidator.setAccessible(true);
			paymentIdRegexValidator.set(domesticPaymentValidatorImpl, "[a-zA-Z]*");
		} catch (Exception e) {

		}
	}

	@Before
	public void setValues() {

		/*
		 * Creating required lists
		 */
		List<String> addressLineList = new ArrayList<>();
		addressLineList.add("fghfgh");
		addressLineList.add("bbfdff");

		String countrySubDivisionList = "hrgthrt";

		/*
		 * Setting data in the created objects
		 */

		creditorPostalAddress.setAddressLine(addressLineList);
		creditorPostalAddress.setBuildingNumber("dds");
		creditorPostalAddress.setCountry("IN");
		creditorPostalAddress.setCountrySubDivision("asd");
		creditorPostalAddress.setDepartment("dept");
		creditorPostalAddress.setPostCode("asda");
		creditorPostalAddress.setStreetName("hyufdbg");
		creditorPostalAddress.setSubDepartment("subDepartment");
		creditorPostalAddress.setTownName("townName");

		debtorAccount.setSchemeName("gdfgsdf");
		debtorAccount.setIdentification("hghghdfsd");
		debtorAccount.setName("ruiot ouo");
		debtorAccount.setSecondaryIdentification("klhjklhg");
		creditorAccount.setIdentification("asdasd");
		creditorAccount.setName("sdfjkghkf");
		creditorAccount.setSchemeName("ahgtrgegfdgsdasd");
		creditorAccount.setSecondaryIdentification("utyjkdfngui");
		instructedAmount.setCurrency("INR");
		remittanceInformation.setReference("asdasd");
		remittanceInformation.setUnstructured("unstructured");
		initiation.setLocalInstrument("ghdfgdfgdfg");
		initiation.setRemittanceInformation(remittanceInformation);
		initiation.setCreditorPostalAddress(creditorPostalAddress);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setDebtorAccount(debtorAccount);
		authorisation.setCompletionDateTime("gdfgdfgdfgdf");
		data.setInitiation(initiation);
		data.setAuthorisation(authorisation);
		deliveryAddress.setBuildingNumber("asdfsdf");
		deliveryAddress.setStreetName("sdfsdf");
		deliveryAddress.setTownName("dfdfgsdfsdf");
		deliveryAddress.setAddressLine(addressLineList);
		deliveryAddress.setCountrySubDivision(countrySubDivisionList);
		deliveryAddress.setCountry("India");
		deliveryAddress.setPostCode("123453");
		risk.setDeliveryAddress(deliveryAddress);
		paymentConsentsRequest.setRisk(risk);
		paymentConsentsRequest.setData(data);
		paymentData.setConsentId("jkghghgh");
		paymentData.setInitiation(initiation);
		req.setData(data);
		req.setRisk(risk);
		domesticData.setInitiation(initiation);
		pResponse.setData(domesticData);
		paymentSubmissionRequest.setData(paymentData);
		paymentSubmissionRequest.setRisk(risk);
		domData.setConsentId("consent");
		domData.setInitiation(initiation);
		cResponse.setData(domData);
		cResponse.setRisk(risk);

	}

	@Mock
	private CommonAccountValidations commonPaymentValidations;

	@Mock
	private PSD2Validator psd2Validator;

	}
