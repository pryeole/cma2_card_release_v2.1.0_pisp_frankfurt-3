package com.capgemini.psd2.security.consent.rest.test.controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.security.consent.rest.controllers.ConsentApplicationRestController;

public class ConsentApplicationRestControllerTest {
	
	private ConsentApplicationRestController consentApplicationRestController;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		consentApplicationRestController = new ConsentApplicationRestController();

	}
	
	@Test
	public void checkSessionTest(){
		consentApplicationRestController.checkSession(null);
	}

}
