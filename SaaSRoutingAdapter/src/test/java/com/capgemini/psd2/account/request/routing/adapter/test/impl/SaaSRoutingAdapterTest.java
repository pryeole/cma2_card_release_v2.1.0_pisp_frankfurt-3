package com.capgemini.psd2.account.request.routing.adapter.test.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.request.routing.adapter.impl.SaaSRoutingAdapter;
import com.capgemini.psd2.account.request.routing.adapter.routing.SaaSAdapterFactory;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;

public class SaaSRoutingAdapterTest {
	
	@Mock
	private SaaSAdapterFactory saaSAdapterFactory;
	
	@InjectMocks
	private SaaSRoutingAdapter adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testAuthenticate() {
		
		AuthenticationAdapter authenticationAdapter = new AuthenticationAdapter() {
			
			@Override
			public <T> T authenticate(T authenticaiton, Map<String, String> headers) {
				return null;
			}
		};
		
		Mockito.when(saaSAdapterFactory.getAdapterInstance(anyString())).thenReturn(authenticationAdapter);
		adapter.authenticate(authenticationAdapter, new HashMap<String, String>());
		assertNotNull(authenticationAdapter);
	}

}
