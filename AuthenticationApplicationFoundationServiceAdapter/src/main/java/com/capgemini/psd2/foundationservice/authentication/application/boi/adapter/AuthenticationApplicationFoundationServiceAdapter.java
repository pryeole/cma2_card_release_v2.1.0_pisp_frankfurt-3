
package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.security.AdapterAuthenticationException;
import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterException;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.security.domain.AuthenticationMethodCode1;
import com.capgemini.psd2.adapter.security.domain.AuthenticationSystemCode;
import com.capgemini.psd2.adapter.security.domain.CustomerAuthenticationSession;
import com.capgemini.psd2.adapter.security.domain.DigitalUser13;
import com.capgemini.psd2.adapter.security.domain.Login;
import com.capgemini.psd2.adapter.security.domain.LoginResponse;
import com.capgemini.psd2.adapter.security.domain.SecureAccessKeyPositionValue;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.client.AuthenticationApplicationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.delegate.AuthenticationApplicationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain.AuthenticationRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class AuthenticationApplicationFoundationServiceAdapter implements AuthenticationAdapter {

	@Autowired
	private AdapterUtility adapterUtility;

	@Autowired
	private AuthenticationApplicationFoundationServiceDelegate authenticationApplicationFoundationServiceDelegate;

	@Autowired
	private AuthenticationApplicationFoundationServiceClient authenticationApplicationFoundationServiceClient;

	@Override
	public <T> T authenticate(T authentication, Map<String, String> params) {

		RequestInfo requestInfo = new RequestInfo();

		Authentication authenticationObject = (Authentication) authentication;

		String channelId = params.get(AdapterSecurityConstants.CHANNELID_HEADER);		
		params.put(AdapterSecurityConstants.USER_HEADER,String.valueOf(authenticationObject.getPrincipal()));
		// New BOL
 		if (AuthenticationSystemCode.BOL.getValue().equalsIgnoreCase(channelId)) {
			Login request = this.createnewBOLRequest(authenticationObject, params);

			ObjectMapper mapper = new ObjectMapper();
			try {
				System.out.println(mapper.writeValueAsString(request));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			HttpHeaders httpHeaders = authenticationApplicationFoundationServiceDelegate
					.createRequestHeadersBOL(requestInfo, params);	
			String finalURL = authenticationApplicationFoundationServiceDelegate.postAuthenticationFoundationServiceURLBOL(params);
			if (NullCheckUtils.isNullOrEmpty(finalURL)) {
				throw SecurityAdapterException.populatePSD2SecurityException("No valid foundation url found.",
						SecurityAdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
			requestInfo.setUrl(finalURL);
			LoginResponse loginResponse = authenticationApplicationFoundationServiceClient
					.restTransportForAuthenticationServicenewBOL(requestInfo, request, LoginResponse.class,
							httpHeaders);

			if (loginResponse.getDigitalUserSession().isSessionInitiationFailureIndicator()) {
				throw SecurityAdapterException.populatePSD2SecurityException("User authentication failed",
						SecurityAdapterErrorCodeEnum.LOGON_UNSUCCESSFUL);
			}
		}
	 else if (AuthenticationSystemCode.B365.getValue().equalsIgnoreCase(channelId)) {
			AuthenticationRequest authenticationRequest = new AuthenticationRequest();

			HttpHeaders httpHeaders = authenticationApplicationFoundationServiceDelegate
					.createRequestHeadersB365(requestInfo, params);
			authenticationRequest.setUserName(authenticationObject.getPrincipal().toString());
			authenticationRequest.setPassword(authenticationObject.getCredentials().toString());

			String finalURL = adapterUtility.retriveFoundationServiceURL(channelId);
			if (NullCheckUtils.isNullOrEmpty(finalURL)) {
				throw AdapterAuthenticationException.populateAuthenticationFailedException(
						"No valid foundation url found.", SecurityAdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
			requestInfo.setUrl(finalURL);
			authenticationApplicationFoundationServiceClient.restTransportForAuthenticationApplication(requestInfo,
					authenticationRequest, String.class, httpHeaders);
		}
		return authentication;
	}

	private Login createnewBOLRequest(Authentication authenticationObject, Map<String, String> params) {

		Login login = new Login();
		DigitalUser13 digitalUser = new DigitalUser13();

		List<SecureAccessKeyPositionValue> seclist = new ArrayList<SecureAccessKeyPositionValue>();
		if (!NullCheckUtils.isNullOrEmpty(authenticationObject)) {
			List<CustomerAuthenticationSession> customerAuthenticationSession = new ArrayList<CustomerAuthenticationSession>();
			CustomerAuthenticationSession custAuthSession = new CustomerAuthenticationSession();

			SecureAccessKeyPositionValue secKey = new SecureAccessKeyPositionValue();
			custAuthSession.setAuthenticationMethodCode(AuthenticationMethodCode1.ONE_TIME_PASSWORD);
			secKey.setValue(String.valueOf(authenticationObject.getCredentials()));
			seclist.add(secKey);
			custAuthSession.setSecureAccessKeyUsed(seclist);
			customerAuthenticationSession.add(custAuthSession);
			digitalUser.setCustomerAuthenticationSession(customerAuthenticationSession);
			login.setDigitalUser(digitalUser);
		}
		return login;
	}

}
