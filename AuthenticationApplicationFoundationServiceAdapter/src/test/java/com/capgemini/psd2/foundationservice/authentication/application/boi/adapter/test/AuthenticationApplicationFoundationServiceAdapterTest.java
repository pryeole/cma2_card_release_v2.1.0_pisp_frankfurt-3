package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.security.domain.DigitalUserSession;
import com.capgemini.psd2.adapter.security.domain.LoginResponse;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.AuthenticationApplicationFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.client.AuthenticationApplicationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.delegate.AuthenticationApplicationFoundationServiceDelegate;
import com.capgemini.psd2.logger.PSD2Constants;


@RunWith(SpringJUnit4ClassRunner.class)
public class AuthenticationApplicationFoundationServiceAdapterTest{

	@InjectMocks
	private AuthenticationApplicationFoundationServiceAdapter authenticationApplicationFoundationServiceAdapter;

	@Mock
	private AuthenticationApplicationFoundationServiceClient authenticationApplicationFoundationServiceClient;

	@Mock
	private AuthenticationApplicationFoundationServiceDelegate authenticationApplicationFoundationServiceDelegate;
	
	@Mock
	private AdapterUtility adapterUtility;
	
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	
	@Test
	public void testAuthenticationApplicationFS() {
		
		PrincipalImpl principal = new PrincipalImpl("8888888");
		CredentialsImpl credentials = new CredentialsImpl("888888");
		AuthenticationImpl authentication = new AuthenticationImpl(principal, credentials);
		LoginResponse loginResponse=new LoginResponse(); 
		DigitalUserSession digitalUserSession = new DigitalUserSession();
		
		HttpHeaders httpHeaders = new HttpHeaders();
		
		httpHeaders.add("X-API-SOURCE-USER", "User99");
		httpHeaders.add("X-API-SOURCE-SYSTEM", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "BOI_Platform");
		httpHeaders.add("X-API-CORRELATION-ID", "cce68b69-2c40-425c-9926-d972420c1772");
		httpHeaders.add("X-API-TRANSANCTION-ID", "720079cf-8874-4190-aa9a-7afcd1147fa0");
		Map<String, String> params = new HashMap<String, String>();
		params.put(AdapterSecurityConstants.CHANNELID_HEADER, "BOL");
		 //params.get(AdapterSecurityConstants.CHANNELID_HEADER);		
		params.put(AdapterSecurityConstants.USER_HEADER,"Id1234");
	
		digitalUserSession.setSessionInitiationFailureIndicator(false);
		loginResponse.setDigitalUserSession(digitalUserSession);
			
//		Mockito.when(authenticationApplicationFoundationServiceDelegate.createRequestHeadersBOL(anyObject(), anyObject()))
//		.thenReturn(httpHeaders);
		
		Mockito.when(
				authenticationApplicationFoundationServiceDelegate.postAuthenticationFoundationServiceURLBOL(any())).thenReturn("https://localhost:8081");			
		
		Mockito.when(authenticationApplicationFoundationServiceClient
				.restTransportForAuthenticationServicenewBOL(any(), any(), any(), any()))
				.thenReturn(loginResponse);
		Authentication authenticationTest = authenticationApplicationFoundationServiceAdapter.authenticate(authentication, params);
	    
	    assertNotNull(authenticationTest);
	    
	 
	}
	@Test
	public void testAuthenticationApplicationFS_B365() {
		
		PrincipalImpl principal = new PrincipalImpl("8888888");
		CredentialsImpl credentials = new CredentialsImpl("888888");
		AuthenticationImpl authentication = new AuthenticationImpl(principal, credentials);
		LoginResponse loginResponse=new LoginResponse(); 
		DigitalUserSession digitalUserSession = new DigitalUserSession();
		
		HttpHeaders httpHeaders = new HttpHeaders();
		
		httpHeaders.add("X-API-SOURCE-USER", "User99");
		httpHeaders.add("X-API-SOURCE-SYSTEM", "B365");
		httpHeaders.add("X-BOI-PLATFORM", "BOI_Platform");
		httpHeaders.add("X-API-CORRELATION-ID", "cce68b69-2c40-425c-9926-d972420c1772");
		httpHeaders.add("X-API-TRANSANCTION-ID", "720079cf-8874-4190-aa9a-7afcd1147fa0");
		Map<String, String> params = new HashMap<String, String>();
		params.put(AdapterSecurityConstants.CHANNELID_HEADER, "B365");
		
		digitalUserSession.setSessionInitiationFailureIndicator(false);
		loginResponse.setDigitalUserSession(digitalUserSession);
			
		Mockito.when(authenticationApplicationFoundationServiceDelegate.createRequestHeadersB365(anyObject(), anyObject()))
		.thenReturn(httpHeaders);
		
		Mockito.when(
				adapterUtility.retriveFoundationServiceURL(anyString()))
				.thenReturn("http://localhost:9095/authentication-service/1.0.0/it-boi/authentication/p/v1.0/digital-user/login");
		
		Mockito.when(authenticationApplicationFoundationServiceClient
				.restTransportForAuthenticationApplication(any(), any(), any(), any()))
				.thenReturn("Response");
		Authentication authenticationTest = authenticationApplicationFoundationServiceAdapter.authenticate(authentication, params);
	    
	    assertNotNull(authenticationTest);
	    
	 
	}	
	
	
	
}
