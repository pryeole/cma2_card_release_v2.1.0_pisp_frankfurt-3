package com.capgemini.psd2.internal.apis.domain;

public enum ActionEnum {
	BLOCK(true), UNBLOCK(false);

	private Boolean value;
	
	public Boolean getValue() {
		return value;
	}
	public void setValue(Boolean value) {
		this.value = value;
	}
	private ActionEnum(Boolean action) {
		this.value = action;
	}
}
