package com.capgemini.psd2.pisp.stage.operations.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface DomesticPaymentStagingAdapter {
	
	//Called from Domestic Consents API
	public CustomDPaymentConsentsPOSTResponse processDomesticPaymentConsents(CustomDPaymentConsentsPOSTRequest paymentConsentsRequest, CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,OBExternalConsentStatus1Code successStatus,OBExternalConsentStatus1Code failureStatus);
	
	//Called from Domestic Consents API, Consent Application
	public CustomDPaymentConsentsPOSTResponse retrieveStagedDomesticPaymentConsents(CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params);
	
}
