package com.capgemini.psd2.pisp.stage.operations.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface FilePaymentStagingAdapter {
	// createStage
	// retrieveStage
	
	public CustomFilePaymentConsentsPOSTResponse retrieveStagedFilePaymentConsents(CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params);		 

}
