package com.capgemini.psd2.pisp.stage.domain;

import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
 
public class CustomPaymentStageUpdateData {
	private boolean setupStatusUpdated;
	private String setupStatus;
	private String setupStatusUpdateDateTime;

	private boolean debtorDetailsUpdated;
	private OBCashAccountDebtor3 debtorDetails;

	private boolean fraudScoreUpdated;
	private Object fraudScore;
	
	//This field is added for MI Report, do not use it.
	private String status;


	/* Add additional fields here with its indicator */

	/**
	 * @return the setupStatusUpdated
	 */
	public boolean getSetupStatusUpdated() {
		return setupStatusUpdated;
	}

	/**
	 * @param setupStatusUpdated
	 *            the setupStatusUpdated to set
	 */
	public void setSetupStatusUpdated(boolean setupStatusUpdated) {
		this.setupStatusUpdated = setupStatusUpdated;
	}

	
	/**
	 * @return the setupStatusUpdateDateTime
	 */
	public String getSetupStatusUpdateDateTime() {
		return setupStatusUpdateDateTime;
	}

	/**
	 * @param setupStatusUpdateDateTime
	 *            the setupStatusUpdateDateTime to set
	 */
	public void setSetupStatusUpdateDateTime(String setupStatusUpdateDateTime) {
		this.setupStatusUpdateDateTime = setupStatusUpdateDateTime;
	}


	/**
	 * @return the debtorDetailsUpdated
	 */
	public boolean getDebtorDetailsUpdated() {
		return debtorDetailsUpdated;
	}

	/**
	 * @param debtorDetailsUpdated
	 *            the debtorDetailsUpdated to set
	 */
	public void setDebtorDetailsUpdated(boolean debtorDetailsUpdated) {
		this.debtorDetailsUpdated = debtorDetailsUpdated;
	}

	/**
	 * @return the debtorDetails
	 */
	public OBCashAccountDebtor3 getDebtorDetails() {
		return debtorDetails;
	}

	/**
	 * @param debtorDetails
	 *            the debtorDetails to set
	 */
	public void setDebtorDetails(OBCashAccountDebtor3 debtorDetails) {
		this.debtorDetails = debtorDetails;
	}

	/**
	 * @return the fraudScoreUpdated
	 */
	public boolean getFraudScoreUpdated() {
		return fraudScoreUpdated;
	}

	/**
	 * @param fraudScoreUpdated
	 *            the fraudScoreUpdated to set
	 */
	public void setFraudScoreUpdated(boolean fraudScoreUpdated) {
		this.fraudScoreUpdated = fraudScoreUpdated;
	}

	/**
	 * @return the fraudScore
	 */
	public Object getFraudScore() {
		return fraudScore;
	}

	/**
	 * @param fraudScore
	 *            the fraudScore to set
	 */
	public void setFraudScore(Object fraudScore) {
		this.fraudScore = fraudScore;
	}


	/**
	 * @return the setupStatus
	 */
	public String getSetupStatus() {
		return setupStatus;
	}

	/**
	 * @param setupStatus the setupStatus to set
	 */
	public void setSetupStatus(String setupStatus) {
		this.setupStatus = setupStatus;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "CustomPaymentStageUpdateData [setupStatusUpdated=" + setupStatusUpdated + ", setupStatus=" + setupStatus
				+ ", setupStatusUpdateDateTime=" + setupStatusUpdateDateTime + ", debtorDetailsUpdated="
				+ debtorDetailsUpdated + ", debtorDetails=" + debtorDetails + ", fraudScoreUpdated=" + fraudScoreUpdated
				+ ", fraudScore=" + fraudScore + ", status=" + status + "]";
	}

	

}
