package com.capgemini.psd2.pisp.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface InternationalStandingOrderAdapter {

	CustomIStandingOrderPOSTResponse processInternationalStandingOrder(CustomIStandingOrderPOSTRequest request,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params);

	CustomIStandingOrderPOSTResponse retrieveStagedInternationalStandingOrder(CustomPaymentStageIdentifiers identifiers,
			Map<String, String> params);

}
