package com.capgemini.psd2.aisp.adapter;

import java.util.Map;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountOffersResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public interface AccountOffersAdapter {
	
	public PlatformAccountOffersResponse retrieveAccountOffers(AccountMapping accountMapping,Map<String, String> params);

}
