package com.capgemini.psd2.pisp.domestic.payments.test.mock.data;

import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PaymentSubmissionPOSTRequestResponseMockData {
	
	public static CustomDPaymentsPOSTRequest getPaymentSubmissionPOSTRequest() {
		return new CustomDPaymentsPOSTRequest();
		
	}
	
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
