package com.capgemini.psd2.pisp.domestic.payments.comparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticPaymentsPayloadComparator implements Comparator<CustomDPaymentConsentsPOSTResponse> {

	@Override
	public int compare(CustomDPaymentConsentsPOSTResponse paymentResponse,
			CustomDPaymentConsentsPOSTResponse adaptedPaymentResponse) {

		int value = 1;
		System.out.println("B3");
		if (Double.compare(Double.parseDouble(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount()),Double
				.parseDouble(adaptedPaymentResponse.getData().getInitiation().getInstructedAmount().getAmount()))==0) {
			System.out.println("C3");
			adaptedPaymentResponse.getData().getInitiation().getInstructedAmount()
					.setAmount(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount());
		}

		if (paymentResponse.getData().getInitiation().equals(adaptedPaymentResponse.getData().getInitiation())
				&& paymentResponse.getRisk().equals(adaptedPaymentResponse.getRisk()))
			System.out.println("D3");
			value = 0;
		
		System.out.println("E3");
		return value;
	}

	public int comparePaymentDetails(Object response, Object request,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {
		CustomDPaymentConsentsPOSTResponse paymentSetupResponse = null;
		CustomDPaymentConsentsPOSTResponse tempPaymentSetupResponse = null;
		CustomDPaymentConsentsPOSTResponse adaptedPaymentResponse = null;
		CustomDPaymentConsentsPOSTRequest paymentSetupRequest = null;
		CustomDPaymentsPOSTRequest paymentSubmissionRequest = null;
		int returnValue = 1;

		if (response instanceof CustomDPaymentConsentsPOSTResponse) {
			paymentSetupResponse = (CustomDPaymentConsentsPOSTResponse) response;
			String creationDateTime = paymentSetupResponse.getData().getCreationDateTime();
			paymentSetupResponse.getData().setCreationDateTime(null);
			String strPaymentResponse = JSONUtilities.getJSONOutPutFromObject(paymentSetupResponse);
			tempPaymentSetupResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strPaymentResponse, CustomDPaymentConsentsPOSTResponse.class);
			paymentSetupResponse.getData().setCreationDateTime(creationDateTime);
		}

		if (request instanceof CustomDPaymentConsentsPOSTRequest) {
			paymentSetupRequest = (CustomDPaymentConsentsPOSTRequest) request;
			String strPaymentRequest = JSONUtilities.getJSONOutPutFromObject(paymentSetupRequest);
			adaptedPaymentResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strPaymentRequest, CustomDPaymentConsentsPOSTResponse.class);
		} else if (request instanceof CustomDPaymentsPOSTRequest) {
			paymentSubmissionRequest = (CustomDPaymentsPOSTRequest) request;
			String strSubmissionRequest = JSONUtilities.getJSONOutPutFromObject(paymentSubmissionRequest);
			adaptedPaymentResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strSubmissionRequest, CustomDPaymentConsentsPOSTResponse.class);
		}
		
		System.out.println("Temp : " + tempPaymentSetupResponse.getData().getInitiation().toString());
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("Adapted : " + adaptedPaymentResponse.getData().getInitiation().toString());

		if (tempPaymentSetupResponse != null && adaptedPaymentResponse != null) {

			if (!validateDebtorDetails(tempPaymentSetupResponse.getData().getInitiation(),
					adaptedPaymentResponse.getData().getInitiation(), paymentSetupPlatformResource))
					{
				System.out.println("A");
				return 1;
				}
			compareAmount(tempPaymentSetupResponse.getData().getInitiation().getInstructedAmount(),
					adaptedPaymentResponse.getData().getInitiation().getInstructedAmount());

			/*
			 * Date: 13-Feb-2018 Changes are made for CR-10
			 */
			System.out.println("A2");
			checkAndModifyEmptyFields(adaptedPaymentResponse, (CustomDPaymentConsentsPOSTResponse) response);
			// End of CR-10
			
			System.out.println("A3");
			returnValue = compare(tempPaymentSetupResponse, adaptedPaymentResponse);
			System.out.println("A4");
		}
		return returnValue;

	}

	public boolean validateDebtorDetails(OBDomestic1 responseInitiation, OBDomestic1 requestInitiation,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {

			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be true, then
			 * name will participate in comparison else product will set foundation response
			 * DebtorAccount.Name as null so that it can be passed in comparison. Both
			 * fields would have the value as 'Null'.
			 */
			System.out.println("B");
			if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
			{
			System.out.println("C");
				responseInitiation.getDebtorAccount().setName(null);
				}
				
				System.out.println("Response : " + responseInitiation.getDebtorAccount().toString());
				System.out.println("Request : " + requestInitiation.getDebtorAccount().toString());
				
			return Objects.equals(responseInitiation.getDebtorAccount(), requestInitiation.getDebtorAccount());
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& requestInitiation.getDebtorAccount() != null) {
			System.out.println("D");
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& responseInitiation.getDebtorAccount() != null) {
			responseInitiation.setDebtorAccount(null);
			System.out.println("E");
			return Boolean.TRUE;
		}
		System.out.println("F");
		return Boolean.TRUE;

	}

	/*
	 * Date: 12-Jan-2018, SIT defect#956 Comparing two float values received from FS
	 * and TPP. Both values must be treated equal mathematically if values are in
	 * below format. i.e 1.2 == 1.20, 0.20000 == 0.20, 0.2 == 00.20000, 0.2 ==
	 * 0.20000, 001.00 == 1.00 etc. If values are equal then set the amount received
	 * from TPP to the amount object of FS response. By doing this, comparison would
	 * be passed in swagger's java file(compare method of this class) and same
	 * amount would be available to TPP also in response of idempotent request.
	 */
	private void compareAmount(OBDomestic1InstructedAmount responseInstructedAmount,
			OBDomestic1InstructedAmount requestInstructedAmount) {
		System.out.println("A1");
		if (Double.compare(Double.parseDouble(responseInstructedAmount.getAmount()),
				Double.parseDouble(requestInstructedAmount.getAmount())) == 0)
				{
			responseInstructedAmount.setAmount(requestInstructedAmount.getAmount());
			System.out.println("B1");
			}
	}

	private void checkAndModifyEmptyFields(OBWriteDomesticConsentResponse1 adaptedPaymentResponse,
			CustomDPaymentConsentsPOSTResponse response) {

		System.out.println("B2");
		checkAndModifyRemittanceInformation(adaptedPaymentResponse.getData().getInitiation(), response);
		checkAndModifyCreditorPostalAddressInformation(adaptedPaymentResponse.getData().getInitiation(), response);

		System.out.println("G2");
		if (adaptedPaymentResponse.getRisk().getDeliveryAddress() != null) {
		System.out.println("H2");
			checkAndModifyAddressLine(adaptedPaymentResponse.getRisk().getDeliveryAddress(), response);
		}

		System.out.println("L2");
		if (adaptedPaymentResponse.getData().getInitiation().getCreditorPostalAddress() != null
				&& adaptedPaymentResponse.getData().getInitiation().getCreditorPostalAddress()
						.getAddressLine() != null) {
			System.out.println("M2");
			checkAndModifyCreditorPostalAddressLine(
					adaptedPaymentResponse.getData().getInitiation().getCreditorPostalAddress(), response);
		}
		System.out.println("P2");
	}

	private void checkAndModifyRemittanceInformation(OBDomestic1 responseInitiation,
			CustomDPaymentConsentsPOSTResponse response) {
		
		System.out.println("C2");
		OBRemittanceInformation1 remittanceInformation = responseInitiation.getRemittanceInformation();
		if (remittanceInformation != null && response.getData().getInitiation().getRemittanceInformation() == null
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getReference())
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getUnstructured())) {
			System.out.println("D2");
			responseInitiation.setRemittanceInformation(null);
		}
	}

	private void checkAndModifyCreditorPostalAddressInformation(OBDomestic1 responseInitiation,
			CustomDPaymentConsentsPOSTResponse response) {

		System.out.println("E2");
		OBPostalAddress6 crPostaladdInformation = responseInitiation.getCreditorPostalAddress();
		if (crPostaladdInformation != null && response.getData().getInitiation().getCreditorPostalAddress() == null
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getAddressLine())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getAddressType())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getDepartment())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getSubDepartment())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getStreetName())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getBuildingNumber())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getPostCode())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getTownName())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getCountrySubDivision())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getCountry())) {

			System.out.println("F2");
			responseInitiation.setCreditorPostalAddress(null);
		}
	}

	private void checkAndModifyAddressLine(OBRisk1DeliveryAddress riskDeliveryAddress,
			CustomDPaymentConsentsPOSTResponse response) {

		System.out.println("I2");
		if (riskDeliveryAddress.getAddressLine() != null) {
			List<String> addressLineList = new ArrayList<>();
			for (String addressLine : riskDeliveryAddress.getAddressLine()) {
				if (NullCheckUtils.isNullOrEmpty(addressLine))
					continue;
				addressLineList.add(addressLine);
			}
			System.out.println("J2");
			if (addressLineList.isEmpty() && response.getRisk().getDeliveryAddress().getAddressLine() == null)
				addressLineList = null;

			riskDeliveryAddress.setAddressLine(addressLineList);
			System.out.println("K2");
		}
	}

	private void checkAndModifyCreditorPostalAddressLine(OBPostalAddress6 obPostalAddress6,
			CustomDPaymentConsentsPOSTResponse response) {
		List<String> addressLineList = new ArrayList<>();
		for (String addressLine : (obPostalAddress6.getAddressLine())) {
			if (NullCheckUtils.isNullOrEmpty(addressLine))
				continue;
			addressLineList.add(addressLine);
		}
		System.out.println("N2");
		if (addressLineList.isEmpty()
				&& response.getData().getInitiation().getCreditorPostalAddress().getAddressLine() == null)
			addressLineList = null;

		obPostalAddress6.setAddressLine(addressLineList);
		System.out.println("O2");
	}

}
