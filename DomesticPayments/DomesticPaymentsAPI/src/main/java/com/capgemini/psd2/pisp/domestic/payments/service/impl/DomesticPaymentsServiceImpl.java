package com.capgemini.psd2.pisp.domestic.payments.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.HttpMethod;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.DomesticPaymentsAdapter;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.domestic.payments.comparator.DomesticPaymentsPayloadComparator;
import com.capgemini.psd2.pisp.domestic.payments.service.DomesticPaymentsService;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticPaymentsServiceImpl implements DomesticPaymentsService {

	@Autowired
	private DomesticPaymentsPayloadComparator dPaymentComparator;

	@Autowired
	@Qualifier("dPaymentConsentsStagingRoutingAdapter")
	private DomesticPaymentStagingAdapter domesticPaymentStagingAdapter;

	@Autowired
	private PispConsentAdapter pispConsentAdapter;
	
	@Autowired
	@Qualifier("dPaymentsRoutingAdapter")
	private DomesticPaymentsAdapter domesticPaymentsAdapter;

	@Autowired
	private PaymentSubmissionProcessingAdapter<CustomDPaymentsPOSTRequest, PaymentDomesticSubmitPOST201Response> paymentsProcessingAdapter;
	
	@Value("${cmaVersion}")
	private String cmaVersion;

	@Override
	public PaymentDomesticSubmitPOST201Response createDomesticPaymentsResource(
			CustomDPaymentsPOSTRequest domesticPaymentsRequest) {

		PaymentDomesticSubmitPOST201Response paymentsFoundationResponse = null;
		CustomDPaymentConsentsPOSTResponse consentsFoundationResource = null;
		PaymentConsentsPlatformResource paymentConsentPlatformResource;
		PaymentsPlatformResource paymentsPlatformResource;

		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter.preSubmissionProcessing(
				domesticPaymentsRequest, populatePlatformDetails(domesticPaymentsRequest),
				domesticPaymentsRequest.getData().getConsentId().trim());

		paymentConsentPlatformResource = (PaymentConsentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.CONSENT);
		paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);

		consentsFoundationResource = domesticPayLoadCompare(domesticPaymentsRequest, paymentConsentPlatformResource,
				domesticPaymentsRequest.getData().getConsentId().trim());

		if (paymentsPlatformResource == null) {
			paymentsPlatformResource = paymentsProcessingAdapter.createInitialPaymentsPlatformResource(
					domesticPaymentsRequest.getData().getConsentId(), cmaVersion,
					PaymentTypeEnum.DOMESTIC_PAY);
			paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		}

		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.INCOMPLETE)) {
			domesticPaymentsRequest = populateDomesticStagedProcessingFields(domesticPaymentsRequest,
					consentsFoundationResource);
			Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap = new HashMap<>();
			paymentStatusMap.put(PaymentConstants.INITIAL, OBTransactionIndividualStatus1Code.PENDING);
			paymentStatusMap.put(PaymentConstants.FAIL, OBTransactionIndividualStatus1Code.REJECTED);
			paymentStatusMap.put(PaymentConstants.PASS_M_AUTH,
					OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTINPROCESS);
			paymentStatusMap.put(PaymentConstants.PASS_M_AWAIT, OBTransactionIndividualStatus1Code.PENDING);
			paymentStatusMap.put(PaymentConstants.PASS_M_REJECT, OBTransactionIndividualStatus1Code.REJECTED);
			domesticPaymentsRequest.setCreatedOn(paymentsPlatformResource.getCreatedAt());
			
			PispConsent pispConsent = pispConsentAdapter.retrieveConsentByPaymentId(domesticPaymentsRequest.getData().getConsentId());
			String partyIdentifier = pispConsent.getPartyIdentifier();
			Map<String, String> paramsMap = new HashMap<>();
			paramsMap.put(PSD2Constants.PARTY_IDENTIFIER, partyIdentifier);
			
			paymentsFoundationResponse = domesticPaymentsAdapter.processDomesticPayments(domesticPaymentsRequest,
					paymentStatusMap, paramsMap);
		}

		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.COMPLETED)) {
			CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
			identifiers.setPaymentSetupVersion(cmaVersion);
			identifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
			identifiers.setPaymentConsentId(paymentsPlatformResource.getPaymentConsentId());
			identifiers.setPaymentSubmissionId(paymentsPlatformResource.getSubmissionId());
			paymentsFoundationResponse = domesticPaymentsAdapter.retrieveStagedDomesticPayments(identifiers, null);

		}

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = OBExternalStatus2Code.AUTHORISED;
		if (paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		
		/* Fix for SIT issue #2272 
		 * Tpp providing Blank remittance info, and FS is returning null,
		 * Thus in such case returning same Blank remittance info to TPP.
		 * 27.02.2019 */
		OBRemittanceInformation1 requestRemittanceInfo = domesticPaymentsRequest.getData().getInitiation().getRemittanceInformation();
		OBPostalAddress6 reqCreditorPostalAddress = domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress();
		paymentsFoundationResponse.getData().getInitiation().setRemittanceInformation(requestRemittanceInfo);
		paymentsFoundationResponse.getData().getInitiation().setCreditorPostalAddress(reqCreditorPostalAddress);
		
		return paymentsProcessingAdapter.postPaymentProcessFlows(paymentsPlatformResourceMap,
				paymentsFoundationResponse, paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getDomesticPaymentId(), multiAuthStatus,
				HttpMethod.POST.toString());
	}

	private CustomDPaymentsPOSTRequest populateDomesticStagedProcessingFields(
			CustomDPaymentsPOSTRequest submissionRequest,
			CustomDPaymentConsentsPOSTResponse paymentSetupFoundationResponse) {
		/*
		 * If TPP has not sent DebtorAccount.Name then product will set the name
		 * details from staging record which is sent by FS
		 */
		if (submissionRequest.getData().getInitiation().getDebtorAccount() != null && NullCheckUtils
				.isNullOrEmpty(submissionRequest.getData().getInitiation().getDebtorAccount().getName())) {

			submissionRequest.getData().getInitiation().getDebtorAccount()
					.setName(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAccount().getName());
		}
		/*
		 * If debtor details are not provided by TPP then Platform will set
		 * debtor details received from FS. Same will be sent to FS for
		 * Pre-SubmissionValidation and Account Permission
		 */

		if (submissionRequest.getData().getInitiation().getDebtorAccount() == null) {
			submissionRequest.getData().getInitiation()
					.setDebtorAccount(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAccount());
		}
		submissionRequest.setFraudSystemResponse(paymentSetupFoundationResponse.getFraudScore());
		return submissionRequest;
	}

	private CustomPaymentSetupPlatformDetails populatePlatformDetails(
			CustomDPaymentsPOSTRequest customDPaymentsPOSTRequest) {
		CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails = new CustomPaymentSetupPlatformDetails();
		customPaymentSetupPlatformDetails.setSetupCmaVersion(cmaVersion);
		customPaymentSetupPlatformDetails.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY);
		return customPaymentSetupPlatformDetails;
	}

	private CustomDPaymentConsentsPOSTResponse domesticPayLoadCompare(CustomDPaymentsPOSTRequest submissionRequest,
			PaymentConsentsPlatformResource paymentConsentPlatformResource, String consentId) {

		System.out.println("Submission Request : " + submissionRequest.getData().getInitiation().toString() );
		
		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentConsentId(consentId);
		identifiers.setPaymentSetupVersion(cmaVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);

		CustomDPaymentConsentsPOSTResponse paymentSetupFoundationResponse = domesticPaymentStagingAdapter
				.retrieveStagedDomesticPaymentConsents(identifiers, null);

		System.out.println("Payment Setup Response : " + paymentSetupFoundationResponse.getData().getInitiation().toString() );
		
		if (dPaymentComparator.comparePaymentDetails(paymentSetupFoundationResponse, submissionRequest,
				paymentConsentPlatformResource) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));
		return paymentSetupFoundationResponse;
	}

	// GET END POINT
	@Override
	public PaymentDomesticSubmitPOST201Response retrieveDomesticPaymentsResource(String submissionId) {

		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter
				.prePaymentProcessGETFlows(submissionId, PaymentTypeEnum.DOMESTIC_PAY);
		PaymentsPlatformResource paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);
		String paymentConsentId;
		String paymentSetUpVersion;
		if (NullCheckUtils.isNullOrEmpty(paymentsPlatformResource.getSubmissionId())) {
			paymentConsentId = paymentsPlatformResource.getPaymentId();
			paymentSetUpVersion = PaymentConstants.CMA_FIRST_VERSION;
		} else {
			paymentConsentId = paymentsPlatformResource.getPaymentConsentId();
			paymentSetUpVersion = cmaVersion;
		}
		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentSetupVersion(paymentSetUpVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		identifiers.setPaymentConsentId(paymentConsentId);
		identifiers.setPaymentSubmissionId(submissionId);
		PaymentDomesticSubmitPOST201Response paymentsFoundationResponse = domesticPaymentsAdapter
				.retrieveStagedDomesticPayments(identifiers, null);

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = OBExternalStatus2Code.AUTHORISED;

		if (paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		return (PaymentDomesticSubmitPOST201Response) paymentsProcessingAdapter.postPaymentProcessFlows(
				paymentsPlatformResourceMap, paymentsFoundationResponse,
				paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getDomesticPaymentId(), multiAuthStatus,
				HttpMethod.GET.toString());
	}
}
