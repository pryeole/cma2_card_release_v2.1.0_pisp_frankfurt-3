package com.capgemini.psd2.pisp.domestic.payments.mongo.db.test.adapter.impl;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.DPaymentsFoundationResource;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domestic.payments.mongo.db.adapter.impl.DomesticPaymentsMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.domestic.payments.mongo.db.adapter.repository.DomesticPaymentsFoundationRepository;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

public class DomesticPaymentsMongoDbAdapterImplTest {

	@Mock
	private DomesticPaymentsFoundationRepository repository;

	@Mock
	private SandboxValidationUtility utility;

	@Mock
	private RequestHeaderAttributes reqAttributes;

	@InjectMocks
	private List<OBCharge1> chargesList = new ArrayList<OBCharge1>();

	@InjectMocks
	private DomesticPaymentsMongoDbAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testProcessDomesticPaymentsMultiAuthorised() {
		CustomDPaymentsPOSTRequest request = DomesticPaymentsMongoDbAdapterImplTestMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		DPaymentsFoundationResource resource = new DPaymentsFoundationResource();
		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(repository.save(any(DPaymentsFoundationResource.class))).thenReturn(resource);
		adapter.processDomesticPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessDomesticPaymentsAwaitingFurtherAuthorisatio() {
		CustomDPaymentsPOSTRequest request = DomesticPaymentsMongoDbAdapterImplTestMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		DPaymentsFoundationResource resource = new DPaymentsFoundationResource();
		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);

		when(repository.save(any(DPaymentsFoundationResource.class))).thenReturn(resource);
		adapter.processDomesticPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessDomesticPaymentsRejectedAuthorisation() {
		CustomDPaymentsPOSTRequest request = DomesticPaymentsMongoDbAdapterImplTestMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		DPaymentsFoundationResource resource = new DPaymentsFoundationResource();
		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);

		when(repository.save(any(DPaymentsFoundationResource.class))).thenReturn(resource);
		adapter.processDomesticPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessDomesticPaymentsNoAuthorisation() {
		CustomDPaymentsPOSTRequest request = DomesticPaymentsMongoDbAdapterImplTestMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		DPaymentsFoundationResource resource = new DPaymentsFoundationResource();
		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.FAIL;

		OBMultiAuthorisation1 multiAuthorisation = null;

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);

		when(repository.save(any(DPaymentsFoundationResource.class))).thenReturn(resource);
		adapter.processDomesticPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessDomesticPaymentsNullSubmissionId() {
		CustomDPaymentsPOSTRequest request = DomesticPaymentsMongoDbAdapterImplTestMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		DPaymentsFoundationResource resource = new DPaymentsFoundationResource();

		ProcessExecutionStatusEnum processStatus = null;
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);

		when(repository.save(any(DPaymentsFoundationResource.class))).thenReturn(resource);
		adapter.processDomesticPayments(request, new HashMap<>(), new HashMap<>());
		assertNull(processStatus);
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessDomesticPaymentsDataAccessResourceFailureException() {
		CustomDPaymentsPOSTRequest request = DomesticPaymentsMongoDbAdapterImplTestMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(repository.save(any(DPaymentsFoundationResource.class)))
				.thenThrow(new DataAccessResourceFailureException("Test"));
		adapter.processDomesticPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testRetrieveStagedDomesticPayments() {

		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();

		DPaymentsFoundationResource resource = DomesticPaymentsMongoDbAdapterImplTestMockData.getResource();

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);

		when(repository.findOneByDataDomesticPaymentId(anyString())).thenReturn(resource);
		adapter.retrieveStagedDomesticPayments(customPaymentStageIdentifiers, new HashMap<>());
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRetrieveStagedDomesticPaymentsException() {

		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();

		DPaymentsFoundationResource resource = DomesticPaymentsMongoDbAdapterImplTestMockData.getResource();

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);

		when(repository.findOneByDataDomesticPaymentId(anyString())).thenReturn(resource);
		adapter.retrieveStagedDomesticPayments(customPaymentStageIdentifiers, new HashMap<>());
	}

	@After
	public void tearDown() {
		utility = null;
		reqAttributes = null;
		repository = null;
		adapter = null;
	}

}