package com.capgemini.psd2.pisp.operations.transformer;

import java.util.Map;

import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;

public interface PaymentStageDataTransformer<T> {

	public CustomConsentAppViewData transformDStageToConsentAppViewData(
			T stageResponse);

	public CustomFraudSystemPaymentData transformDStageToFraudData(
			T stageResponse);
	
	public void updateStagedPaymentConsents(CustomPaymentStageIdentifiers customStageIdentifiers,
			CustomPaymentStageUpdateData stageUpdateData, Map<String, String> params);
}
