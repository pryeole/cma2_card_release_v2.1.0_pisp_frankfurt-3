package com.capgemini.psd2.pisp.operations.transformer.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.operations.transformer.PaymentStageDataTransformer;
import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.ChargeDetails;
import com.capgemini.psd2.pisp.stage.domain.CreditorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDSOData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.ExchangeRateDetails;
import com.capgemini.psd2.pisp.standing.order.mongodb.adapter.repository.DStandingOrderConsentFoundationRepository;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Conditional(MongoDbMockCondition.class)
@Component("dStandingOrderStageDataTransformer")
public class DSOPaymentStageDataTransformerImpl
		implements PaymentStageDataTransformer<CustomDStandingOrderConsentsPOSTResponse> {

	@Autowired
	private DStandingOrderConsentFoundationRepository dStandingOrderConsentsBankRepository;

	@Override
	public CustomConsentAppViewData transformDStageToConsentAppViewData(
			CustomDStandingOrderConsentsPOSTResponse dStandingOrderConsentResponse) {

		CustomConsentAppViewData response = new CustomConsentAppViewData();

		// Required for Domestic Standing Order Field Population
		CustomDSOData customDStandingOrderField = new CustomDSOData();

		/* Payment Type needed on consent page */
		response.setPaymentType(PaymentTypeEnum.DOMESTIC_ST_ORD.getPaymentType());

		/* Amount details needed on consent page */
		AmountDetails amountDetails = new AmountDetails();
		if (!NullCheckUtils
				.isNullOrEmpty(dStandingOrderConsentResponse.getData().getInitiation().getFirstPaymentAmount())) {
			amountDetails.setAmount(
					dStandingOrderConsentResponse.getData().getInitiation().getFirstPaymentAmount().getAmount());
			amountDetails.setCurrency(
					dStandingOrderConsentResponse.getData().getInitiation().getFirstPaymentAmount().getCurrency());
		}

		if (!NullCheckUtils
				.isNullOrEmpty(dStandingOrderConsentResponse.getData().getInitiation().getFinalPaymentAmount())) {
			AmountDetails amount = new AmountDetails();
			amount.setAmount(
					dStandingOrderConsentResponse.getData().getInitiation().getFinalPaymentAmount().getAmount());
			amount.setCurrency(
					dStandingOrderConsentResponse.getData().getInitiation().getFinalPaymentAmount().getCurrency());
			customDStandingOrderField.setFinalChargeDetails(amount);
		}

		/* Debtor details needed on consent page */
		CustomDebtorDetails debtorDetails = null;
		if (!NullCheckUtils.isNullOrEmpty(dStandingOrderConsentResponse.getData().getInitiation().getDebtorAccount())) {
			debtorDetails = new CustomDebtorDetails();
			debtorDetails.setIdentification(
					dStandingOrderConsentResponse.getData().getInitiation().getDebtorAccount().getIdentification());
			debtorDetails.setName(dStandingOrderConsentResponse.getData().getInitiation().getDebtorAccount().getName());
			String stagedDebtorSchemeName = dStandingOrderConsentResponse.getData().getInitiation().getDebtorAccount()
					.getSchemeName();

			// Fix Scheme name validation failed. In MongoDB producing
			// exception: can't have. In field names [UK.OBIE.IBAN] while
			// creating test data. Hence, in mock data, we used "_" instead of
			// "." character
			// Replacing "_" to "." to convert valid Scheme Name into mongo db
			// adapter only. there is no impact on CMA api flow. Its specifc to
			// Sandbox functionality
			debtorDetails.setSchemeName(stagedDebtorSchemeName.replace("_", "."));
			debtorDetails.setSecondaryIdentification(dStandingOrderConsentResponse.getData().getInitiation()
					.getDebtorAccount().getSecondaryIdentification());
		}

		/* Creditor details needed on consent page */
		CreditorDetails creditorDetails = new CreditorDetails();
		if (!NullCheckUtils
				.isNullOrEmpty(dStandingOrderConsentResponse.getData().getInitiation().getCreditorAccount())) {
			creditorDetails.setIdentification(
					dStandingOrderConsentResponse.getData().getInitiation().getCreditorAccount().getIdentification());
			creditorDetails
					.setName(dStandingOrderConsentResponse.getData().getInitiation().getCreditorAccount().getName());
			creditorDetails.setSchemeName(
					dStandingOrderConsentResponse.getData().getInitiation().getCreditorAccount().getSchemeName());
			creditorDetails.setSecondaryIdentification(dStandingOrderConsentResponse.getData().getInitiation()
					.getCreditorAccount().getSecondaryIdentification());
		}

		/* Charge details needed on consent page */
		ChargeDetails chargeDetails = new ChargeDetails();
		chargeDetails.setChargesList(dStandingOrderConsentResponse.getData().getCharges());

		/* No Exchange Rate for Domestic Payment on consent page */
		ExchangeRateDetails exchangeRateDetails = null;

		if (!NullCheckUtils
				.isNullOrEmpty(dStandingOrderConsentResponse.getData().getInitiation().getFinalPaymentDateTime())) {
			customDStandingOrderField.setFirstPaymentDateTime(
					dStandingOrderConsentResponse.getData().getInitiation().getFinalPaymentDateTime());
		}

		if (!NullCheckUtils
				.isNullOrEmpty(dStandingOrderConsentResponse.getData().getInitiation().getFinalPaymentDateTime())) {
			customDStandingOrderField.setFinalPaymentDateTime(
					dStandingOrderConsentResponse.getData().getInitiation().getFinalPaymentDateTime());
		}

		customDStandingOrderField.setFrequency(dStandingOrderConsentResponse.getData().getInitiation().getFrequency());

		customDStandingOrderField
				.setNumberOfPayment(dStandingOrderConsentResponse.getData().getInitiation().getNumberOfPayments());

		if (!NullCheckUtils
				.isNullOrEmpty(dStandingOrderConsentResponse.getData().getInitiation().getRecurringPaymentAmount())) {
			AmountDetails amount = new AmountDetails();
			amount.setAmount(
					dStandingOrderConsentResponse.getData().getInitiation().getRecurringPaymentAmount().getAmount());
			amount.setCurrency(
					dStandingOrderConsentResponse.getData().getInitiation().getRecurringPaymentAmount().getCurrency());
			customDStandingOrderField.setRecurringChargeDetails(amount);
		}

		response.setAmountDetails(amountDetails);
		response.setChargeDetails(chargeDetails);
		response.setCreditorDetails(creditorDetails);
		response.setDebtorDetails(debtorDetails);
		response.setExchangeRateDetails(exchangeRateDetails);
		response.setCustomDSOData(customDStandingOrderField);
		return response;
	}

	@Override
	public CustomFraudSystemPaymentData transformDStageToFraudData(
			CustomDStandingOrderConsentsPOSTResponse domesticStageResponse) {
		CustomFraudSystemPaymentData fraudSystemData = new CustomFraudSystemPaymentData();
		fraudSystemData.setPaymentType(PaymentTypeEnum.DOMESTIC_ST_ORD.getPaymentType());
		fraudSystemData
				.setTransferAmount(domesticStageResponse.getData().getInitiation().getFirstPaymentAmount().getAmount());
		fraudSystemData.setTransferCurrency(
				domesticStageResponse.getData().getInitiation().getFirstPaymentAmount().getCurrency());
		/*
		 * if (!NullCheckUtils.isNullOrEmpty(domesticStageResponse.getData().
		 * getInitiation().getRemittanceInformation()))
		 * fraudSystemData.setTransferMemo(
		 * domesticStageResponse.getData().getInitiation().
		 * getRemittanceInformation().getReference());
		 */
		fraudSystemData.setTransferTime(domesticStageResponse.getData().getCreationDateTime());
		CreditorDetails fraudSystenCreditorInfo = new CreditorDetails();
		fraudSystenCreditorInfo.setIdentification(
				domesticStageResponse.getData().getInitiation().getCreditorAccount().getIdentification());
		fraudSystemData.setCreditorDetails(fraudSystenCreditorInfo);
		return fraudSystemData;
	}

	@Override
	public void updateStagedPaymentConsents(CustomPaymentStageIdentifiers customStageIdentifiers,
			CustomPaymentStageUpdateData stageUpdateData, Map<String, String> params) {

		CustomDStandingOrderConsentsPOSTResponse updatedStagedResource = dStandingOrderConsentsBankRepository
				.findOneByDataConsentId(customStageIdentifiers.getPaymentConsentId());
		
		if (stageUpdateData.getDebtorDetailsUpdated() == Boolean.TRUE) {
			updatedStagedResource.getData().getInitiation().setDebtorAccount(stageUpdateData.getDebtorDetails());
		}
		if (stageUpdateData.getFraudScoreUpdated()) {
			updatedStagedResource.setFraudScore(stageUpdateData.getFraudScore());
		}
		if (stageUpdateData.getSetupStatusUpdated() == Boolean.TRUE) {
			updatedStagedResource.getData()
					.setStatus(OBExternalConsentStatus1Code.fromValue(stageUpdateData.getSetupStatus()));
			updatedStagedResource.getData().setStatusUpdateDateTime(stageUpdateData.getSetupStatusUpdateDateTime());
		}
		System.out.println("DSOPaymentStageDataTransformerImpl : updateStagedPaymentConsents : updatedStagedResource"+updatedStagedResource.toString());
		System.out.println("DSOPaymentStageDataTransformerImpl : updateStagedPaymentConsents : getStreetname"+updatedStagedResource.getRisk().getDeliveryAddress().getStreetName());
		System.out.println("DSOPaymentStageDataTransformerImpl : updateStagedPaymentConsents : getCountrySubDivision"+updatedStagedResource.getRisk().getDeliveryAddress().getCountrySubDivision());
		dStandingOrderConsentsBankRepository.save(updatedStagedResource);

	}
}
