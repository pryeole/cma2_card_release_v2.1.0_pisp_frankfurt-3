package com.capgemini.psd2.pisp.payments.standing.order.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.payments.standing.order.transformer.DomesticStandingOrderTransformerImpl;
import com.capgemini.psd2.pisp.payments.standing.order.transformer.impl.Mock.DomesticStandingOrderTransformerImplMockData;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticStandingOrderTransformerImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PaymentsPlatformAdapter paymentsPlatformAdapter;

	@Mock
	private PispDateUtility pispDateUtility;

	@InjectMocks
	private DomesticStandingOrderTransformerImpl transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map = new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap = new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content",
				"Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");

		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@After
	public void tearDown() {
		reqHeaderAtrributes = null;
		paymentsPlatformAdapter = null;
		pispDateUtility = null;
		transformer = null;
	}

	@Test
	public void updateMetaAndLinksTest() {
		DStandingOrderPOST201Response domesticStandingOrderResonse = new DStandingOrderPOST201Response();
		PaymentSetupPOSTResponseLinks links = new PaymentSetupPOSTResponseLinks();
		PaymentSetupPOSTResponseMeta meta = new PaymentSetupPOSTResponseMeta();
		OBWriteDataDomesticStandingOrderResponse1 data = new OBWriteDataDomesticStandingOrderResponse1();
		data.setDomesticStandingOrderId("abcdssd123332sd");

		domesticStandingOrderResonse.setLinks(links);
		domesticStandingOrderResonse.setMeta(meta);
		domesticStandingOrderResonse.setData(data);

		String methodType = RequestMethod.POST.toString();

		transformer.updateMetaAndLinks(domesticStandingOrderResonse, methodType);

	}

	@Ignore
	@Test
	public void paymentSubmissionResponseTransformerTest_POST() {

		DomesticStandingOrderTransformerImplMockData domesticStandingOrderTransformerImplMockData = new DomesticStandingOrderTransformerImplMockData();

		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.POST.toString();

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus("InitiationCompleted");
		paymentsPlatformResource.setStatusUpdateDateTime("");

		DStandingOrderPOST201Response domesticStandingOrderResonse = new DStandingOrderPOST201Response();

		// Setting Consents
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResponse.setTppDebtorDetails("True");
		paymentConsentsPlatformResponse.setTppDebtorNameDetails(null);
		domesticStandingOrderResonse
				.setData(domesticStandingOrderTransformerImplMockData.getOBWriteDataDomesticStandingOrderResponse1());

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse);

		domesticStandingOrderResonse.getData().setStatus(OBExternalStatus1Code.INITIATIONCOMPLETED);
		domesticStandingOrderResonse.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		domesticStandingOrderResonse.getData().getInitiation().getDebtorAccount().setSchemeName("IBAN");
		domesticStandingOrderResonse.getData().getInitiation().setDebtorAccount(null);

		transformer.paymentSubmissionResponseTransformer(domesticStandingOrderResonse, paymentsPlatformResourceMap,
				methodType);

	}

	@Test
	public void paymentSubmissionResponseTransformerTest_InitiationCompleted() {
		DomesticStandingOrderTransformerImplMockData domesticStandingOrderTransformerImplMockData = new DomesticStandingOrderTransformerImplMockData();

		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus("InitiationCompleted");
		paymentsPlatformResource.setStatusUpdateDateTime("");

		DStandingOrderPOST201Response domesticStandingOrderResonse = new DStandingOrderPOST201Response();

		// Setting Consents
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();

		domesticStandingOrderResonse
				.setData(domesticStandingOrderTransformerImplMockData.getOBWriteDataDomesticStandingOrderResponse1());

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse);

		domesticStandingOrderResonse.getData().setStatus(OBExternalStatus1Code.INITIATIONCOMPLETED);

		transformer.paymentSubmissionResponseTransformer(domesticStandingOrderResonse, paymentsPlatformResourceMap,
				methodType);

	}

	@Test
	public void paymentSubmissionResponseTransformerTest_PendingAuthorized() {
		DomesticStandingOrderTransformerImplMockData domesticStandingOrderTransformerImplMockData = new DomesticStandingOrderTransformerImplMockData();

		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus(OBExternalStatus1Code.INITIATIONPENDING.toString());
		paymentsPlatformResource.setStatusUpdateDateTime("");

		DStandingOrderPOST201Response domesticStandingOrderResonse = new DStandingOrderPOST201Response();

		// Setting Consents
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResponse.setTppDebtorDetails("false");

		domesticStandingOrderResonse
				.setData(domesticStandingOrderTransformerImplMockData.getOBWriteDataDomesticStandingOrderResponse1());

		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.AUTHORISED);
		domesticStandingOrderResonse.getData().setMultiAuthorisation(multiAuth);

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse);

		domesticStandingOrderResonse.getData().setStatus(OBExternalStatus1Code.INITIATIONPENDING);

		domesticStandingOrderResonse.getData().getInitiation().setDebtorAccount(null);

		transformer.paymentSubmissionResponseTransformer(domesticStandingOrderResonse, paymentsPlatformResourceMap,
				methodType);
	}

	@Test
	public void paymentSubmissionResponseTransformerTest_PendingRejected() {
		DomesticStandingOrderTransformerImplMockData domesticStandingOrderTransformerImplMockData = new DomesticStandingOrderTransformerImplMockData();

		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus(OBExternalStatus1Code.INITIATIONPENDING.toString());
		paymentsPlatformResource.setStatusUpdateDateTime("");

		DStandingOrderPOST201Response domesticStandingOrderResonse = new DStandingOrderPOST201Response();

		// Setting Consents
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResponse.setTppDebtorDetails("false");

		domesticStandingOrderResonse
				.setData(domesticStandingOrderTransformerImplMockData.getOBWriteDataDomesticStandingOrderResponse1());

		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.REJECTED);
		domesticStandingOrderResonse.getData().setMultiAuthorisation(multiAuth);

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse);

		domesticStandingOrderResonse.getData().setStatus(OBExternalStatus1Code.INITIATIONCOMPLETED);

		domesticStandingOrderResonse.getData().getInitiation().setDebtorAccount(null);

		transformer.paymentSubmissionResponseTransformer(domesticStandingOrderResonse, paymentsPlatformResourceMap,
				methodType);
	}
	
	@Test
	public void testPaymentSubmissionRequestTransformer() {
		CustomDStandingOrderPOSTRequest paymentSubmissionRequest = new CustomDStandingOrderPOSTRequest();
		paymentSubmissionRequest.setData(new OBWriteDataDomesticStandingOrder1());
		paymentSubmissionRequest.getData().setInitiation(new OBDomesticStandingOrder1());
		paymentSubmissionRequest.getData().getInitiation().setFirstPaymentDateTime("");
		paymentSubmissionRequest.getData().getInitiation().setRecurringPaymentDateTime("");
		paymentSubmissionRequest.getData().getInitiation().setFinalPaymentDateTime("");
		
		transformer.paymentSubmissionRequestTransformer(paymentSubmissionRequest);
	}

}
