package com.capgemini.psd2.pisp.payments.standing.order.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.amazonaws.HttpMethod;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.adapter.DomesticStandingOrderAdapter;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payments.standing.order.comparator.DomesticStandingOrderPayloadComparator;
import com.capgemini.psd2.pisp.payments.standing.order.service.DomesticStandingOrderService;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Lazy
@Service
public class DomesticStandingOrderServiceImpl implements DomesticStandingOrderService {

	@Autowired
	@Qualifier("dStandingOrderConsentsStagingRoutingAdapter")
	private DomesticStandingOrdersPaymentStagingAdapter dPaymentStagingAdapter;

	@Autowired
	@Qualifier("dStandingOrderRoutingAdapter")
	private DomesticStandingOrderAdapter dStandingOrderAdapter;

	@Autowired
	private DomesticStandingOrderPayloadComparator comparator;

	@Autowired
	private PaymentSubmissionProcessingAdapter<CustomDStandingOrderPOSTRequest, DStandingOrderPOST201Response> paymentsProcessingAdapter;
	
	@Value("${cmaVersion}")
	private String cmaVersion;

	@Override
	public DStandingOrderPOST201Response createDomesticStandingOrderResource(CustomDStandingOrderPOSTRequest request) {
		DStandingOrderPOST201Response paymentsFoundationResponse = null;
		CustomDStandingOrderConsentsPOSTResponse consentsFoundationResource = null;
		PaymentConsentsPlatformResource paymentConsentPlatformResource = null;
		PaymentsPlatformResource paymentsPlatformResource = null;

		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter.preSubmissionProcessing(request,
				populatePlatformDetails(), request.getData().getConsentId().trim());

		paymentConsentPlatformResource = (PaymentConsentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.CONSENT);
		paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);

		consentsFoundationResource = domesticPayLoadCompare(request, paymentConsentPlatformResource,
				request.getData().getConsentId().trim());

		if (paymentsPlatformResource == null) {
			paymentsPlatformResource = paymentsProcessingAdapter.createInitialPaymentsPlatformResource(
					request.getData().getConsentId(), cmaVersion,
					PaymentTypeEnum.DOMESTIC_ST_ORD);
			paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		}

		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.INCOMPLETE)) {
			populateDomesticStagedProcessingFields(request, consentsFoundationResource);
			Map<String, OBExternalStatus1Code> paymentStatusMap = new HashMap<>();
			paymentStatusMap.put(PaymentConstants.INITIAL, OBExternalStatus1Code.INITIATIONPENDING);
			paymentStatusMap.put(PaymentConstants.FAIL, OBExternalStatus1Code.INITIATIONFAILED);
			paymentStatusMap.put(PaymentConstants.PASS_M_AUTH, OBExternalStatus1Code.INITIATIONCOMPLETED);
			paymentStatusMap.put(PaymentConstants.PASS_M_AWAIT, OBExternalStatus1Code.INITIATIONPENDING);
			paymentStatusMap.put(PaymentConstants.PASS_M_REJECT, OBExternalStatus1Code.INITIATIONFAILED);
			request.setCreatedOn(paymentsPlatformResource.getCreatedAt());
			paymentsFoundationResponse = dStandingOrderAdapter.processDomesticStandingOrder(request, paymentStatusMap,
					null);
		}

		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.COMPLETED)) {
			CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
			identifiers.setPaymentSetupVersion(cmaVersion);
			identifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
			identifiers.setPaymentConsentId(paymentsPlatformResource.getPaymentConsentId());
			identifiers.setPaymentSubmissionId(paymentsPlatformResource.getSubmissionId());
			paymentsFoundationResponse = dStandingOrderAdapter.retrieveStagedDomesticStandingOrder(identifiers, null);
		}

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = OBExternalStatus2Code.AUTHORISED;
		if (paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		return (DStandingOrderPOST201Response) paymentsProcessingAdapter.postPaymentProcessFlows(
				paymentsPlatformResourceMap, paymentsFoundationResponse,
				paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getDomesticStandingOrderId(), multiAuthStatus,
				HttpMethod.POST.toString());
	}

	private CustomPaymentSetupPlatformDetails populatePlatformDetails() {
		CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails = new CustomPaymentSetupPlatformDetails();
		customPaymentSetupPlatformDetails.setSetupCmaVersion(cmaVersion);
		customPaymentSetupPlatformDetails.setPaymentType(PaymentTypeEnum.DOMESTIC_ST_ORD);
		return customPaymentSetupPlatformDetails;
	}

	private CustomDStandingOrderConsentsPOSTResponse domesticPayLoadCompare(
			CustomDStandingOrderPOSTRequest submissionRequest,
			PaymentConsentsPlatformResource paymentConsentPlatformResource, String consentId) {

		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentConsentId(consentId);
		identifiers.setPaymentSetupVersion(cmaVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		
		if(submissionRequest != null)
			System.out.println("DomesticStandingOrderServiceImpl :domesticPayLoadCompare: "+JSONUtilities.getJSONOutPutFromObject(submissionRequest));
		if(paymentConsentPlatformResource !=null)
			System.out.println("DomesticStandingOrderServiceImpl :domesticPayLoadCompare: "+JSONUtilities.getJSONOutPutFromObject(paymentConsentPlatformResource)); 
			
		System.out.println("DomesticStandingOrderServiceImpl :domesticPayLoadCompare: consentId: "+consentId); 

		CustomDStandingOrderConsentsPOSTResponse paymentSetupFoundationResponse = dPaymentStagingAdapter
				.retrieveStagedDomesticStandingOrdersConsents(identifiers, null);
		
		if(paymentSetupFoundationResponse !=null)
			System.out.println("DomesticStandingOrderServiceImpl : retrived domesticPayLoadCompare: "+JSONUtilities.getJSONOutPutFromObject(paymentSetupFoundationResponse)); 

		if (comparator.comparePaymentDetails(paymentSetupFoundationResponse, submissionRequest,
				paymentConsentPlatformResource) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));
		return paymentSetupFoundationResponse;
	}

	private CustomDStandingOrderPOSTRequest populateDomesticStagedProcessingFields(
			CustomDStandingOrderPOSTRequest submissionRequest,
			CustomDStandingOrderConsentsPOSTResponse paymentSetupFoundationResponse) {
		/*
		 * If TPP has not sent DebtorAccount.Name then product will set the name
		 * details from staging record which is sent by FS
		 */
		if (submissionRequest.getData().getInitiation().getDebtorAccount() != null && NullCheckUtils
				.isNullOrEmpty(submissionRequest.getData().getInitiation().getDebtorAccount().getName())) {

			submissionRequest.getData().getInitiation().getDebtorAccount()
					.setName(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAccount().getName());
		}
		/*
		 * If debtor details are not provided by TPP then Platform will set
		 * debtor details received from FS. Same will be sent to FS for
		 * Pre-SubmissionValidation and Account Permission
		 */

		if (submissionRequest.getData().getInitiation().getDebtorAccount() == null) {
			submissionRequest.getData().getInitiation()
					.setDebtorAccount(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAccount());
		}
		submissionRequest.setFraudSystemResponse(paymentSetupFoundationResponse.getFraudScore());
		return submissionRequest;
	}

	@Override
	public DStandingOrderPOST201Response retrieveDomesticStandingOrderResource(String submissionId) {
		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter
				.prePaymentProcessGETFlows(submissionId, PaymentTypeEnum.DOMESTIC_ST_ORD);
		PaymentsPlatformResource paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);
		String paymentConsentId = paymentsPlatformResource.getPaymentConsentId();
		String paymentSetUpVersion = cmaVersion;

		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentSetupVersion(paymentSetUpVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		identifiers.setPaymentConsentId(paymentConsentId);
		identifiers.setPaymentSubmissionId(submissionId);
		DStandingOrderPOST201Response paymentsFoundationResponse = dStandingOrderAdapter
				.retrieveStagedDomesticStandingOrder(identifiers, null);

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = null;

		if (paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		return (DStandingOrderPOST201Response) paymentsProcessingAdapter.postPaymentProcessFlows(
				paymentsPlatformResourceMap, paymentsFoundationResponse,
				paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getDomesticStandingOrderId(), multiAuthStatus,
				HttpMethod.GET.toString());
	}

}
