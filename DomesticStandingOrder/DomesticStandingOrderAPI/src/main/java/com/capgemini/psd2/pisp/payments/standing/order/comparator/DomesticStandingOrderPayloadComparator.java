package com.capgemini.psd2.pisp.payments.standing.order.comparator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FinalPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1RecurringPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticStandingOrderConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticStandingOrderPayloadComparator {

	public int compare(CustomDStandingOrderConsentsPOSTResponse paymentResponse,
			CustomDStandingOrderConsentsPOSTResponse adaptedPaymentResponse) {

		int value = 1;

		if (Double.compare(
				Double.parseDouble(paymentResponse.getData().getInitiation().getFirstPaymentAmount().getAmount()),
				Double.parseDouble(
						adaptedPaymentResponse.getData().getInitiation().getFirstPaymentAmount().getAmount())) == 0) {
			adaptedPaymentResponse.getData().getInitiation().getFirstPaymentAmount()
					.setAmount(paymentResponse.getData().getInitiation().getFirstPaymentAmount().getAmount());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentResponse.getData().getInitiation().getRecurringPaymentAmount())
				&& !NullCheckUtils
						.isNullOrEmpty(adaptedPaymentResponse.getData().getInitiation().getRecurringPaymentAmount()))
			if (Double.parseDouble(paymentResponse.getData().getInitiation().getRecurringPaymentAmount()
					.getAmount()) == Double.parseDouble(
							adaptedPaymentResponse.getData().getInitiation().getRecurringPaymentAmount().getAmount())) {
				adaptedPaymentResponse.getData().getInitiation().getRecurringPaymentAmount()
						.setAmount(paymentResponse.getData().getInitiation().getRecurringPaymentAmount().getAmount());
			}
		if (!NullCheckUtils.isNullOrEmpty(paymentResponse.getData().getInitiation().getFinalPaymentAmount())
				&& !NullCheckUtils
						.isNullOrEmpty(adaptedPaymentResponse.getData().getInitiation().getFinalPaymentAmount()))
			if (Double.parseDouble(
					paymentResponse.getData().getInitiation().getFinalPaymentAmount().getAmount()) == Float.parseFloat(
							adaptedPaymentResponse.getData().getInitiation().getFinalPaymentAmount().getAmount())) {
				adaptedPaymentResponse.getData().getInitiation().getFinalPaymentAmount()
						.setAmount(paymentResponse.getData().getInitiation().getFinalPaymentAmount().getAmount());
			}

		if (paymentResponse.getData().getInitiation().equals(adaptedPaymentResponse.getData().getInitiation())
				&& paymentResponse.getRisk().equals(adaptedPaymentResponse.getRisk()))
			value = 0;
		return value;
	}

	public int comparePaymentDetails(Object response, Object request,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {
		CustomDStandingOrderConsentsPOSTResponse paymentSetupResponse = null;
		CustomDStandingOrderConsentsPOSTResponse tempPaymentSetupResponse = null;
		CustomDStandingOrderConsentsPOSTResponse adaptedPaymentResponse = null;
		CustomDStandingOrderPOSTRequest paymentSubmissionRequest = null;
		int returnValue = 1;

		if (response instanceof CustomDStandingOrderConsentsPOSTResponse) {
			paymentSetupResponse = (CustomDStandingOrderConsentsPOSTResponse) response;
			String creationDateTime = paymentSetupResponse.getData().getCreationDateTime();
			paymentSetupResponse.getData().setCreationDateTime(null);
			String strPaymentResponse = JSONUtilities.getJSONOutPutFromObject(paymentSetupResponse);
			tempPaymentSetupResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strPaymentResponse, CustomDStandingOrderConsentsPOSTResponse.class);
			paymentSetupResponse.getData().setCreationDateTime(creationDateTime);
		}

		if (request instanceof CustomDStandingOrderPOSTRequest) {
			paymentSubmissionRequest = (CustomDStandingOrderPOSTRequest) request;
			String strSubmissionRequest = JSONUtilities.getJSONOutPutFromObject(paymentSubmissionRequest);
			adaptedPaymentResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strSubmissionRequest, CustomDStandingOrderConsentsPOSTResponse.class);
		}

		if (tempPaymentSetupResponse != null && adaptedPaymentResponse != null) {

			if (!validateDebtorDetails(tempPaymentSetupResponse.getData().getInitiation(),
					adaptedPaymentResponse.getData().getInitiation(), paymentSetupPlatformResource))
				return 1;
			compareAmount(tempPaymentSetupResponse.getData().getInitiation().getFirstPaymentAmount(),
					adaptedPaymentResponse.getData().getInitiation().getFirstPaymentAmount());
			if (!NullCheckUtils
					.isNullOrEmpty(tempPaymentSetupResponse.getData().getInitiation().getRecurringPaymentAmount())) {

				compareAmount(tempPaymentSetupResponse.getData().getInitiation().getRecurringPaymentAmount(),
						adaptedPaymentResponse.getData().getInitiation().getRecurringPaymentAmount());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(tempPaymentSetupResponse.getData().getInitiation().getFinalPaymentAmount())) {

				compareAmount(tempPaymentSetupResponse.getData().getInitiation().getFinalPaymentAmount(),
						adaptedPaymentResponse.getData().getInitiation().getFinalPaymentAmount());
			}

			/*
			 * Date: 13-Feb-2018 Changes are made for CR-10
			 */
			checkAndModifyEmptyFields(adaptedPaymentResponse, (CustomDStandingOrderConsentsPOSTResponse) response);
			// End of CR-10

			returnValue = compare(tempPaymentSetupResponse, adaptedPaymentResponse);
		}
		return returnValue;
	}

	private void compareAmount(OBDomesticStandingOrder1FinalPaymentAmount resAmount,
			OBDomesticStandingOrder1FinalPaymentAmount reqAmount) {
		if (resAmount != null && reqAmount != null) {
			if (Double.compare(Double.parseDouble(resAmount.getAmount()),
					Double.parseDouble(reqAmount.getAmount())) == 0)
				resAmount.setAmount(reqAmount.getAmount());
		}
	}

	private void compareAmount(OBDomesticStandingOrder1RecurringPaymentAmount resAmount,
			OBDomesticStandingOrder1RecurringPaymentAmount reqAmount) {
		if (resAmount != null && reqAmount != null) {
			if (Double.compare(Double.parseDouble(resAmount.getAmount()),
					Double.parseDouble(reqAmount.getAmount())) == 0)
				resAmount.setAmount(reqAmount.getAmount());
		}
	}

	private void compareAmount(OBDomesticStandingOrder1FirstPaymentAmount resAmount,
			OBDomesticStandingOrder1FirstPaymentAmount reqAmount) {
		if (resAmount != null && reqAmount != null) {
			if (Double.compare(Double.parseDouble(resAmount.getAmount()),
					Double.parseDouble(reqAmount.getAmount())) == 0)
				resAmount.setAmount(reqAmount.getAmount());
		}
	}

	public boolean validateDebtorDetails(OBDomesticStandingOrder1 responseInitiation,
			OBDomesticStandingOrder1 requestInitiation, PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (paymentSetupPlatformResource != null)

			if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {
				try {
					/*
					 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be set
					 * foundation response DebtorAccount.Name as null so that it can be passed in
					 * comparison. Both fields would have the value as 'Null'.
					 */
					if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
						responseInitiation.getDebtorAccount().setName(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return Objects.equals(responseInitiation.getDebtorAccount(), requestInitiation.getDebtorAccount());
			}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& requestInitiation.getDebtorAccount() != null) {
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& responseInitiation.getDebtorAccount() != null) {
			responseInitiation.setDebtorAccount(null);
			return Boolean.TRUE;
		}
		return Boolean.TRUE;

	}

	private void checkAndModifyEmptyFields(OBWriteDomesticStandingOrderConsentResponse1 adaptedPaymentResponse,
			CustomDStandingOrderConsentsPOSTResponse response) {

		if (adaptedPaymentResponse.getRisk().getDeliveryAddress() != null) {
			checkAndModifyAddressLine(adaptedPaymentResponse.getRisk().getDeliveryAddress(), response);
		}
	}

	private void checkAndModifyAddressLine(OBRisk1DeliveryAddress riskDeliveryAddress,
			CustomDStandingOrderConsentsPOSTResponse response) {

		if (riskDeliveryAddress.getAddressLine() != null) {
			List<String> addressLineList = new ArrayList<>();
			for (String addressLine : riskDeliveryAddress.getAddressLine()) {
				if (NullCheckUtils.isNullOrEmpty(addressLine))
					continue;
				addressLineList.add(addressLine);
			}
			if (addressLineList.isEmpty() && response.getRisk().getDeliveryAddress().getAddressLine() == null)
				addressLineList = null;

			riskDeliveryAddress.setAddressLine(addressLineList);

		}
	}

	private void checkAndModifyCountrySubDivision(OBRisk1DeliveryAddress riskDeliveryAddress) {
		if (riskDeliveryAddress.getCountrySubDivision() != null) {

			riskDeliveryAddress.setCountrySubDivision(riskDeliveryAddress.getCountrySubDivision());

		}
	}
}
