/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.exceptions;

/**
 * The Enum AdapterErrorCodeEnum.
 */
public enum AdapterErrorCodeEnum {

	/** The authentication failure error. */
	AUTHENTICATION_FAILURE_ERROR("500","Technical Error. Please try again later","There was a problem processing your request. Please try again later","500","UK.OBIE.UnexpectedError"),
	
	/** The account not found. */
	ACCOUNT_NOT_FOUND("501","Account Not Found","Account Not Found","400","UK.OBIE.Resource.NotFound"),
	
	/** The account details not found. */
	ACCOUNT_DETAILS_NOT_FOUND("502","No account details found for the requested account(s)","400"),
	
	/** The technical error. */
	TECHNICAL_ERROR("503","Technical Error. Please try again later","There was a problem processing your request. Please try again later","500","UK.OBIE.UnexpectedError"),
	
	/** The bad request. */
	BAD_REQUEST("504","Technical Error. Please try again later","There was a problem processing your request. Please try again later","500","UK.OBIE.UnexpectedError"),
	
	/** The no account data found. */
	NO_ACCOUNT_DATA_FOUND("505","No Account data found","404"),
	
	/** The no account data found foundation service. */
	NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE("506","No Account data found in foundation service","400"),
	
	/** The invalid amount. */
	INVALID_AMOUNT("507","Invalid amount found in foundation service","500"),
	
	/** The invalid currency. */
	INVALID_CURRENCY("508","Invalid currency found in foundation service","500"),
	
	/** The invalid nickname. */
	INVALID_NICKNAME("509","Invalid nick name found in foundation service","500"),
	
	/** The invalid identification. */
	INVALID_IDENTIFICATION("510","Invalid identification found in foundation service","500"),
	
	/** The no account id found. */
	NO_ACCOUNT_ID_FOUND("511","No Account ID found","404"),
	
	/** The no channel id in request. */
	NO_CHANNEL_ID_IN_REQUEST("512","No channel present in the request","400"),
	
	/** The no account nsc in request. */
	NO_ACCOUNT_NSC_IN_REQUEST("513","No account nsc present in the request","400"),
	
	/*
	 * PISP Error Code
	 */
	
	CONNECTION_ERROR("514", "Connection error", "500"),
	
	GENERAL_ERROR("515", "General error", "500" ),
	
	INVALID_IBAN_LENGTH("516", "IBAN provided/length is invalid", "500"),
	
	INVALID_BIC("517", "BIC provided is invalid", "400"),
	
	INVALID_IBAN_CHECKSUM("518", "Invalid IBAN checksum", "500"),
	
	ACCOUNT_NOT_FOUND_OR_CLOSED("519", "Account not found or closed", "400"),
	
	BIC_IS_NOT_REACHABLE("520", "BIC is not reachable", "500"),
	
	BIC_AND_IBAN_DO_NOT_MATCH("521", "BIC and IBAN do not match", "400"),
	
	NOT_A_VALID_NSC("522", "NSC provided is invalid", "400"),
	
	NOT_A_VALID_ACCOUNT_NUMBER("523", "Account number provided is invalid", "400"),
	
	NOT_A_VALID_CURRENCY("524", "Transaction currency code is invalid", "500"),	
	
	BANK_DOES_NOT_PROCESS_COUNTRY("525", "Bank does not process payments to the requested beneficiary country", "400"),	
	
	BANK_DOES_NOT_PROCESS_CURRENCY("526", "Bank does not deal in the requested transaction currency",  "400"),
	
	COUNTRY_CODE_IS_INVALID("527","Country code is invalid", "400"),
	
	PAYER_ACCOUNT_BLOCKED_TO_DEBITS("528","Account is blocked to debits",  "500"),
	
	PAYER_ACCOUNT_BLOCKED_TO_CREDITS("529","Account is blocked to credits", "500"),
	
	PAYER_ACCOUNT_BLOCKED_TO_ALL("530","Account is blocked to All",  "500"),
	
	CREDIT_GRADE_NOT_6_OR_7("531","Credit grade cannot be 6 or 7",  "500"),
	
	ACCOUNT_CANNOT_BE_DORMANT("532","Account cannot be dormant", "500"),
	
	ACCOUNT_CANNOT_BE_LIEN("533","Account cannot be lien",  "500"),
	
	JOINT_ACCOUNT_MUST_BE_PERMITTED("534","Joint account must be permitted by all the customer associated",  "500"),
	
	FUNDS_NOT_AVAILABLE("535","Account does not have enough funds",  "500"),
	
	LIMIT_EXCEEDED_FOR_CUSTOMER("536","Limit exceeded for this customer",  "500"),
	
	LIMIT_EXCEEDED_FOR_TRANSACTION_TYPE("537","Limit exceeded for this payment type for the customer",  "500"),
	
	LIMIT_EXCEEDED_FOR_AGENT("538","Limit exceeded for this agent", "500"),
	
	INVALID_PAYMENT_REQUEST("539","Invalid Payment Request",  "500"),
	
	/*
	 *SaaS and ConsentApplication Error Code Mapping for UI Specific message. The below error code is specific for SaaS and ConsentApplication YML mapping.
	 */
	
	AUTHENTICATION_FAILURE_ERROR_PMV("540","You are not authorized to use this service","500"),
	
	AUTHENTICATION_FAILURE_ERROR_PMR("541","You are not authorized to use this service","500"),
	
	NO_PAYMENT_INSTRUCTION_FOUND_PMV("542", "No Payment instruction found for the requested information", "500"),
	
	NO_TRANSACTION_DETAILS_FOUND_PMU("543", "No transaction details found for the requested account(s)", "400"),
	
	TECHNICAL_ERROR_PMR("544","Technical Error.Please try again later","500"),
	
	TECHNICAL_ERROR_PMU("545","Technical Error.Please try again later","500"),
	
	TECHNICAL_ERROR_CAP("546","Technical Error.Please try again later","500"),
	
	ACCOUNT_DETAILS_NOT_FOUND_CAP("547","No account details found for the requested account(s)","400"),
	
	GENERAL_ERROR_PMV("548", "General error", "500" ),
	
	TECHNICAL_ERROR_PMV("549","Technical Error.Please try again later","500"),
	
	
	/*CR028 error code added*/
	
	NO_PAYMENT_INSTRUCTION_FOUND("550", "No Payment instruction found for the requested information", "500"),
	
	NO_TRANSACTION_DETAILS_FOUND("551", "No transaction details found for the requested account(s)", "400"),
	
	NOT_A_SEPA_IBAN("552", "IBAN does not belong to SEPA zone", "500"),
	
	NO_SUCH_SEPA_BIC("553", "BIC does not belong to SEPA Zone", "500"),
	
	NO_OPEN_ACCOUNT_EXISTS("554", "Account does not exists", "500"),
	
	ACCOUNT_STATUS_MUST_BE_ACTIVE("555", "Account status must be active", "500"),
	
	INVALID_ACCOUNT_TYPE("556", "Account Type is invalid", "500"),
	
	SWIFT_ADDRESS_IS_INVALID("557","Swift address is invalid", "500"),
	
	ABA_CODE_IS_INVALID("558","ABA code is invalid", "500"),
	
	COUNTRY_IS_INVALID("559","Country is invalid", "500"),

	COUNTRY_RULES_LIMIT_EXCEEDED("560","Country rules limit exceeded", "500"),

	USER_ID_INVALID("561","User id is invalid", "500"),

	JURISDICTION_CODE_INVALID("562","Jurisdiction code is invalid", "500"),

	CIS_ID_INVALID("563","CIS id is invalid","500"),

	PAYER_PAYEE_ACCOUNT_SAME("564","The payer and the payee account cannot be the same", "500"),

	BIC_PROVIDER_IS_INVALID("565","BIC provided is invalid", "500"),
	
	PAYMENT_REJECTED("566","Payment rejected due to the suspicion of fraud", "400"),
	
	INVALID_ACCOUNT_NUMBER_LENGTH("567","Invalid Account Number or NSC","500"),
	
	USER_IS_BLOCKED("568","The Customer Account Profile is blocked","400"),
	
	/*For AccountBeneficiaryAdapter*/
	
	NO_BENEFICIARY_FOUND("569","This request cannot be processed. No benificiary found","200"),
	
	/*For AccountStandingOrdersAdapter*/
	
	NO_STANDINGORDER_FOUND("570","This request cannot be processed. No StandingOrder found" ,"200"),
	
	/*For AccountProductAdapter*/ 
	
	NO_PRODUCT_FOUND("571","This request cannot be processed. No Product found" ,"200"),
	
	
	/*For AccountTransactionAdapter*/
	
	CONSENT_EXPIRED("572","Consent has expired","403"),
	
	INVALID_FILTER("573","Invalid transaction filter passed" ,"400"),
	
	/*CR046 error code added*/
	
	BAD_REQUEST_PISP("574","Bad request.Please check your request","400"),

	FORBIDDEN_ERROR("575","Access to requested resource denied","Forbidden","403","UK.OBIE.Resource.Forbidden"),
	
	SERVER_RESOURCE_NOT_FOUND_PPS_PIPV("576","Resource not found","400"),
	
	TECHNICAL_ERROR_PPS_PIPV("577","There was a problem processing your request. Please try again later","500"),
	
	RESOURCE_NOT_FOUND("578","Resource Not Found","Resource Not Found","400","UK.OBIE.Resource.NotFound"),
	
	NO_ACCOUNT_FOUND("579","Access to requested resource denied","Forbidden","403","UK.OBIE.Resource.Forbidden"),
	
	CURRENCY_MISMATCH("580","Invalid Currency","Payer Currency does not match with Account","400","UK.OBIE.Resource.NotFound"),
	
	BAD_REQUEST_PISP_FREQUENCY("581","Technical Error. Please try again later","The payment frequency is not supported. ","400","UK.OBIE.Unsupported.Frequency"),
	
	BAD_REQUEST_PISP_FREQUENCY_INVALID_DATE("582","Technical Error. Please try again later","The provided frequency does not match with the first payment date","400","UK.OBIE.Field.InvalidDate"),
	
	ACCOUNTS_NOT_FOUND("583","Account Not Found","Account Not Found","400","UK.OBIE.Resource.NotFound"),
	
	/**FS error codes for PISP (Validate)  0.56 Error Mapping Sheet **/
	
	BENEFICIARY_COUNTRY_NOT_SUPPORTED("584","Payment to beneficiary country not supported","Payment to beneficiary country not supported","500","UK.OBIE.UnexpectedError"),
	
	PANEL_LIMIT_EXCEEDED("585","Payment is rejected due to panel limit exceeded","Payment is rejected due to panel limit exceeded","500","UK.OBIE.UnexpectedError"),
	
	CUSTOMER_LIMIT_EXCEEDED("586","Payment is rejected due to customer limit exceeded","Payment is rejected due to customer limit exceeded","500","UK.OBIE.UnexpectedError"),
	
	OUTSIDE_BUSINESS_HOURS("587","Payment made outside business hours","Payment made outside business hours","400","UK.OBIE.Rules.AfterCutOffDateTime"),
	
	INVALID_DATE_FROM_BOL("588","Invalid date from BOL payment customer limit","Invalid date from BOL payment customer limit","500","UK.OBIE.UnexpectedError"),
	
	PAYMENT_AUTHORISATION_PERMISSONS("589","User doesnt have payment authorisation permissons","User doesnt have payment authorisation permissons","500","UK.OBIE.UnexpectedError"),
	
	INVALID_CUSTOMERID("590","CustomerId/UserId is invalid","CustomerId/UserId is invalid","500","UK.OBIE.UnexpectedError"),
	
	INVALID_PAYMENT_TYPE("591","Invalid payment type","Invalid payment type","500","UK.OBIE.UnexpectedError"),
	
	INVALID_PAYMENT_DATE_FORMAT("592","Invalid payment date format","Invalid payment date format","500","UK.OBIE.UnexpectedError"),
	
	NO_RECORDS_FOUND("593","No records found for PaymentDate/PaymentType/Currency","No records found for PaymentDate/PaymentType/Currency","500","UK.OBIE.UnexpectedError"),
	
	NATIONALID_NOT_EXIST("594","Unable to derive BIC from IBAN as national id does not exist. Please correct payee IBAN","Unable to derive BIC from IBAN as national id does not exist. Please correct payee IBAN","500","UK.OBIE.UnexpectedError"),
	
	INVALID_PAYEE_NAME("595","Invalid payee name","Invalid payee name","500","UK.OBIE.UnexpectedError"),
	
	PAYEE_ADDRESS_INVALID("596","Payee address format invalid","Payee address format invalid","500","UK.OBIE.UnexpectedError"),
	
	GENERIC_ERROR_MESSAGE("597","We are unable to process your request at this time, please return to the Third Party Site and try again later.","We are unable to process your request at this time, please return to the Third Party Site and try again later.","400","UK.OBIE.UnexpectedError"),
	
	TECHNICAL_ERROR_BOL("816","Technical Error. Please try again later","500"),
	
	DEVICE_PROVISION_SCRREN("817","Source Resource not found","400"),
	
	LIMIT_EXCEEDED_FOR_CUSTOMER1("820","","400"),
	
	OUTSIDE_BUSINESS_HOURS1("821","Payment made outside business hours. Cut off time missed for value date or value date is not a working day.","400"),
	
	COUNTRY_LIMIT_EXCEEDED("598","Country Limit exceeded. Request cannot be processed.","400"),
	
	INVALID_PAYMENT_REQUEST_PISP("516","Invalid payment details provided. Request cannot be processed.","400"),
	
	CUTOFF_TIME_MISSED("599","Cut off time missed for value date or value date is not a working day.","400");

	/** The error code. */
	private String errorCode;
	
	/** The error message. */
	private String errorMessage;
	
	private String detailErrorMessage;	
	
	/** The status code. */
	private String statusCode;
	
	private String obErrorCode; 

	/**
	 * Instantiates a new adapter error code enum.
	 *
	 * @param errorCode the error code
	 * @param errorMesssage the error messsage
	 * @param statusCode the status code
	 */
	
	
	private AdapterErrorCodeEnum(String errorCode, String errorMessage, String detailErrorMessage, String statusCode, String obErrorCode) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.detailErrorMessage = detailErrorMessage;
		this.statusCode = statusCode;
		this.obErrorCode = obErrorCode;
	}
	
	
	private AdapterErrorCodeEnum(String errorCode,String errorMesssage,String statusCode){
		this.errorCode=errorCode;
		this.errorMessage=errorMesssage;
		this.statusCode = statusCode;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	public String getStatusCode() {
		return statusCode;
	}

	public String getDetailErrorMessage() {
		return detailErrorMessage;
	}

	public String getObErrorCode() {
		return obErrorCode;
	}
	
}
