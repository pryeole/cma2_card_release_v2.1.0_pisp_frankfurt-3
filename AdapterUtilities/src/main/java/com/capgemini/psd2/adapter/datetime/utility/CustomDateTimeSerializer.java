package com.capgemini.psd2.adapter.datetime.utility;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CustomDateTimeSerializer extends JsonSerializer<Date> {
	
	private static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	
	@Override
	public void serialize(Date fsDate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {

		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
		String dateString = dateFormat.format(fsDate);
		jsonGenerator.writeString(dateString);
	}
}
