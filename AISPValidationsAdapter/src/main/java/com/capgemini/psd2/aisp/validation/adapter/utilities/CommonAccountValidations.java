package com.capgemini.psd2.aisp.validation.adapter.utilities;

import static java.util.stream.Collectors.toList;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Currency;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.iban4j.CountryCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.constraints.EnumAccountConstraint;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification4;
import com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code;
import com.capgemini.psd2.aisp.domain.OBPostalAddress6;
import com.capgemini.psd2.aisp.domain.OBReadConsent1;
import com.capgemini.psd2.aisp.utilities.AispDateUtility;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
@ConfigurationProperties("app")
public class CommonAccountValidations {

	private final Pattern bbanPattern = Pattern.compile("^[A-Z]{4}\\d{14}$");
	
	private final Pattern amountPattern = Pattern.compile("^\\d*\\.?\\d+$");
	
	@Value("${app.validAccountRequestIdChars:[a-zA-Z0-9-]{1,40}}")
	private String validAccountRequestIdChars;
	
	@Value("${app.sortCodeAccountNumberPattern:^[0-9]{6}[0-9]{8}$}")
	private String sortCodeAccountNumberPattern;
	
	@Value("${app.validPanIdentification:[a-zA-Z0-9]+}")
	private String validPanIdentification;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Autowired
	private PermissionsValidationRules permissionsValidationRules;

	@Autowired
	private AispDateUtility dateUtility;

	private List<String> validAgentSchemeList;
	private List<String> validSchemeNameList = new ArrayList<>();


	public List<String> getValidAgentSchemeList() {
		return validAgentSchemeList;
	}

	public void setValidAgentSchemeList(List<String> validAgentSchemeList) {
		this.validAgentSchemeList = validAgentSchemeList;
	}

	public List<String> getValidSchemeNameList() {
		return validSchemeNameList;
	}

	public OBReadConsent1 validatePermissions(OBReadConsent1 accountRequestPOSTRequest) {

		List<OBExternalPermissions1Code> permissions;
		Set<OBExternalPermissions1Code> setOfPermissions;

		if (null == accountRequestPOSTRequest.getData().getPermissions()
				|| accountRequestPOSTRequest.getData().getPermissions().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.PERMISSIONS_VALIDATION_ERROR));
		} else {
			permissions = accountRequestPOSTRequest.getData().getPermissions();
			if (permissions.contains(null)) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
						ErrorMapKeys.PERMISSIONS_VALIDATION_ERROR));
			}

			setOfPermissions = validateMandatoryRules(permissions);
			validateRequirePermission(setOfPermissions);
			validateExcludedPermission(setOfPermissions);
			validateOrConditionRules(setOfPermissions);

		}
		permissions = new ArrayList<>(setOfPermissions);
		accountRequestPOSTRequest.getData().setPermissions(permissions);
		return accountRequestPOSTRequest;
	}

	private void validateRequirePermission(Set<OBExternalPermissions1Code> setOfPermissions) {
		boolean flag = false;
		List<OBExternalPermissions1Code> requiredPermission = permissionsValidationRules.getRequiredPermission();
		for (OBExternalPermissions1Code permission : requiredPermission) {
			if (setOfPermissions.contains(permission)) {
				flag = true;
			}
		}
		if (!flag) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_MISSING,
					ErrorMapKeys.REQUIRED_PERMISSION_NOT_PRESENT));
		}
	}

	private void validateExcludedPermission(Set<OBExternalPermissions1Code> setOfPermissions) {
		if (!(permissionsValidationRules.getExcludedPermissionForSpecificTenant(reqHeaderAtrributes.getTenantId()).stream().filter(setOfPermissions::contains)
				.collect(toList()).isEmpty())) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.EXCLUDED_PERMISSIONS_FOUND));
		}
	}

	private void validateOrConditionRules(Set<OBExternalPermissions1Code> setOfPermissions) {
		Map<String, List<OBExternalPermissions1Code>> orConditonRules = permissionsValidationRules.getOrConditonRules();
		for (Map.Entry<String, List<OBExternalPermissions1Code>> entry : orConditonRules.entrySet()) {
			if (setOfPermissions.contains(OBExternalPermissions1Code.valueOf(entry.getKey()))) {
				validateOrConditionPermissions(setOfPermissions, entry.getValue());
			}
		}
	}

	private void validateOrConditionPermissions(Set<OBExternalPermissions1Code> setOfPermissions,
			List<OBExternalPermissions1Code> permissions) {

		boolean flag = Boolean.FALSE;
		for (OBExternalPermissions1Code orPermission : permissions) {
			if (setOfPermissions.contains(orPermission)) {
				flag = true;
				break;
			}
		}

		if (Boolean.FALSE == flag) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.PERMISSIONS_VALIDATION_ERROR));
		}
	}

	private Set<OBExternalPermissions1Code> validateMandatoryRules(List<OBExternalPermissions1Code> permissions) {

		Set<OBExternalPermissions1Code> setOfPermissions = new LinkedHashSet<>(permissions);
		Map<String, List<OBExternalPermissions1Code>> mandatoryRules = permissionsValidationRules.getMandatoryRules();
		for (Map.Entry<String, List<OBExternalPermissions1Code>> entry : mandatoryRules.entrySet()) {
			if (setOfPermissions.contains(OBExternalPermissions1Code.valueOf(entry.getKey()))) {
				setOfPermissions.addAll(entry.getValue());
			}
		}
		return setOfPermissions;
	}

	public OBReadConsent1 validateAccountRequestInputs(OBReadConsent1 accountRequestPOSTRequest) {
		OBReadConsent1 accountRequest;
		if (null == accountRequestPOSTRequest || null == accountRequestPOSTRequest.getData()) {

			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));
		} else {

			String expirationDateTime = accountRequestPOSTRequest.getData().getExpirationDateTime();
			String transactionFromDateTime = accountRequestPOSTRequest.getData().getTransactionFromDateTime();
			String transactionToDateTime = accountRequestPOSTRequest.getData().getTransactionToDateTime();

			// Validate the format of Date
			dateUtility.validateDateTimeInRequest(expirationDateTime);
			dateUtility.validateDateTimeInRequest(transactionFromDateTime);
			dateUtility.validateDateTimeInRequest(transactionToDateTime);

			// Transform the date based on the availability of Offset or not
			expirationDateTime = dateUtility.transformDateTimeInRequest(expirationDateTime);
			transactionFromDateTime = dateUtility.transformDateTimeInRequest(transactionFromDateTime);
			transactionToDateTime = dateUtility.transformDateTimeInRequest(transactionToDateTime);

			OffsetDateTime parsedExpirationDate = validateAndParseDate(expirationDateTime);
			OffsetDateTime parsedToDateTime = validateAndParseDate(transactionToDateTime);
			OffsetDateTime parsedFromDateTime = validateAndParseDate(transactionFromDateTime);
			
			boolean expirationDateTimeCheck = false;

			if (!NullCheckUtils.isNullOrEmpty(parsedExpirationDate)) {
				expirationDateTimeCheck = DateUtilites.isOffsetDateComparisonPassed(parsedExpirationDate,
						OffsetDateTime.now());
			}

			if (null != parsedFromDateTime && null != parsedToDateTime) {
				boolean fromAndTodateTimeComparison = DateUtilites.isOffsetDateComparisonPassed(parsedFromDateTime,
						parsedToDateTime);
				if (!fromAndTodateTimeComparison) {
					throw PSD2Exception.populatePSD2Exception(
							new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALIDDATE, ErrorMapKeys.VALIDATION_ERROR));
				}
			}

			if (expirationDateTimeCheck)
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALIDDATE, ErrorMapKeys.VALIDATION_ERROR));

			accountRequestPOSTRequest.getData().setExpirationDateTime(expirationDateTime);
			accountRequestPOSTRequest.getData().setTransactionFromDateTime(transactionFromDateTime);
			accountRequestPOSTRequest.getData().setTransactionToDateTime(transactionToDateTime);

			accountRequest = validatePermissions(accountRequestPOSTRequest);

		}
		return accountRequest;
	}

	private OffsetDateTime validateAndParseDate(String dateTime) {
		OffsetDateTime offsetDateTime = null;
		if (!NullCheckUtils.isNullOrEmpty(dateTime)) {
			offsetDateTime = OffsetDateTime.parse(dateTime);
		}
		return offsetDateTime;
	}

	public void validateUniqueUUID(String consentId) {

		if (consentId.length() < 1 || consentId.length() > 128) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.VALIDATION_ERROR));
		}

		if (!consentId.matches(validAccountRequestIdChars)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.VALIDATION_ERROR));
		}
	}

	public void validateSchemeNameWithIdentification(String inputScheme, String identification) {
		if (!NullCheckUtils.isNullOrEmpty(inputScheme)) {
			if (!validSchemeNameList.contains(inputScheme)) {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, ErrorMapKeys.INVALID_SCHEME));
			}

			switch (inputScheme) {
			case AccountsConstants.UK_OBIE_SORTCODEACCOUNTNUMBER:
			case AccountsConstants.SORTCODEACCOUNTNUMBER:
				validateSortCodeAccountNumber(identification);
				break;
			case AccountsConstants.UK_OBIE_PAN:
			case AccountsConstants.PAN:
				validatePAN(identification);
				break;
			case AccountsConstants.UK_OBIE_BBAN:
				validateBBAN(identification);
				break;
			case AccountsConstants.UK_OBIE_PAYM:
				validatePaym(identification);
				break;
			default:
				break;
			}
		}
	}

	private void validateSortCodeAccountNumber(String identification) {
		if (!NullCheckUtils.isNullOrEmpty(identification) && !identification.matches(sortCodeAccountNumberPattern)) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
		}
	}

	private void validateBBAN(String bban) {
		if (!NullCheckUtils.isNullOrEmpty(bban)) {
			try {
				Matcher matcher = bbanPattern.matcher(bban);
				if (!matcher.find()) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
				}
			} catch (Exception exception) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
			}
		}
	}

	private void validatePAN(String pan) {
		if (!NullCheckUtils.isNullOrEmpty(pan) && !pan.matches(validPanIdentification)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
		} 
	}

	private void validatePaym(String identification) {
		/*
		 * 
		 */
	}

	private void validateISOCountry(String countryCode) {
		if (!NullCheckUtils.isNullOrEmpty(countryCode) && CountryCode.getByCode(countryCode) == null) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, ErrorMapKeys.INVALID_COUNTRY_CODE));
		}
	}

	public boolean isValidCurrency(String currencyCode) {

		if (isEmpty(currencyCode)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.UNSUPPORTED_CURRENCY));
		}
		try {
			Currency.getInstance(currencyCode.trim());
			return true;
		} catch (Exception exception) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.UNSUPPORTED_CURRENCY));
		}
	}

	private boolean isEmpty(String data) {
		boolean status = false;
		if (data == null || data.trim().length() == 0)
			status = true;

		return status;
	}

	public void validateSchemeNameWithSecondaryIdentification(String inputScheme, String secondaryIdentification) {
		if (!validSchemeNameList.contains(inputScheme))
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME, ErrorMapKeys.INVALID_SCHEME));

		switch (inputScheme) {
		case AccountsConstants.UK_OBIE_IBAN:
		case AccountsConstants.IBAN:
		case AccountsConstants.UK_OBIE_SORTCODEACCOUNTNUMBER:
		case AccountsConstants.SORTCODEACCOUNTNUMBER:
			validateSecondaryIdentification(secondaryIdentification);
			break;
		default:
			break;
		}
	}

	private void validateSecondaryIdentification(String secondaryIdentification) {
		if (NullCheckUtils.isNullOrEmpty(secondaryIdentification)
				|| secondaryIdentification.length() > EnumAccountConstraint.SECONDARYIDENTIFCATION_LENGTH.getSize())
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTSECONDARYIDENTIFIER,
							ErrorMapKeys.ACCOUNT_SECONDARYIDENTIFIER_INVALID));

	}

	public void validateDomesticAddressLine(List<String> addressLine) {
		if (!NullCheckUtils.isNullOrEmpty(addressLine)) {
			for (String addressLineValue : addressLine) {
				if (!NullCheckUtils.isNullOrEmpty(addressLineValue) && addressLineValue.length() > EnumAccountConstraint.ADDRESSLINE_LENGTH.getSize()) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
							OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, ErrorMapKeys.INVALID_ADDRESS_LINE));
				}
			}
		}
	}

	private void validateDomesticCountrySubDivision(String countrySubDivision) {
		if (!NullCheckUtils.isNullOrEmpty(countrySubDivision)
				&& countrySubDivision.length() > EnumAccountConstraint.COUNTRYSUBDIVISION_LENGTH.getSize())
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.INVALID_COUNTRY_SUBDIVISION));

	}

	public void validateDomesticCreditorPostalAddress(OBPostalAddress6 creditorPostalAddress) {
		if (!NullCheckUtils.isNullOrEmpty(creditorPostalAddress)) {
			validateDomesticAddressLine(creditorPostalAddress.getAddressLine());
			validateDomesticCountrySubDivision(creditorPostalAddress.getCountrySubDivision());
			validateISOCountry(creditorPostalAddress.getCountry());
		}
	}

	/* validate for response dates */
	public void validateAndParseDateTimeFormatForResponse(String dateTimeString) {
		// Validate that Completion Date is not prior to current date
		dateUtility.validateDateTimeInResponse(dateTimeString);
	}
	
	public void validateAmount(String amount) {
		if (!NullCheckUtils.isNullOrEmpty(amount)) {
			try {
				Matcher matcher = amountPattern.matcher(amount);
				if (!matcher.find()) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							InternalServerErrorMessage.AMOUNT_VALIDATION_FAILED));
				}
			} catch (Exception exception) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.AMOUNT_VALIDATION_FAILED));

			}
		}
	}

	/**
	 * @param creditorAgent
	 * 
	 *            CreditorAgent must at least have either or both of these pairs
	 *            - Scheme Name and Identification or Name and Postal Address
	 */
	public void validateCreditorOrDebtorAgent(OBBranchAndFinancialInstitutionIdentification3 creditorAgent) {
		if (!NullCheckUtils.isNullOrEmpty(creditorAgent)) {
			if (!NullCheckUtils.isNullOrEmpty(creditorAgent.getSchemeName()) && !validAgentSchemeList.contains(creditorAgent.getSchemeName())) {
					throw PSD2Exception.populatePSD2Exception(
							new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, ErrorMapKeys.INVALID_SCHEME));
			}
			// validating Scheme Name and Identification pair
			if ((NullCheckUtils.isNullOrEmpty(creditorAgent.getSchemeName())
					&& !NullCheckUtils.isNullOrEmpty(creditorAgent.getIdentification()))
					|| (NullCheckUtils.isNullOrEmpty(creditorAgent.getIdentification())))
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.CREDITOR_OR_DEBTOR_AGENT_INCOMPLETE));
			// validating Name and Postal Address pair
			else if ((NullCheckUtils.isNullOrEmpty(creditorAgent.getName())
					&& !NullCheckUtils.isNullOrEmpty(creditorAgent.getPostalAddress()))
					|| (NullCheckUtils.isNullOrEmpty(creditorAgent.getPostalAddress())
							&& !NullCheckUtils.isNullOrEmpty(creditorAgent.getName())))
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.CREDITOR_OR_DEBTOR_AGENT_INCOMPLETE));
				// validating the presence of atleast one pair
			else if (NullCheckUtils.isNullOrEmpty(creditorAgent.getSchemeName())
					&& NullCheckUtils.isNullOrEmpty(creditorAgent.getIdentification())
					&& NullCheckUtils.isNullOrEmpty(creditorAgent.getName())
					&& NullCheckUtils.isNullOrEmpty(creditorAgent.getPostalAddress()))
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.CREDITOR_OR_DEBTOR_AGENT_INCOMPLETE));
		}
	}

	public void validateCreditorAgent(OBBranchAndFinancialInstitutionIdentification4 creditorAgent) {
		// validating Scheme Name and Identification pair
		if (!NullCheckUtils.isNullOrEmpty(creditorAgent)) {
			if (!NullCheckUtils.isNullOrEmpty(creditorAgent.getSchemeName()) && !validAgentSchemeList.contains(creditorAgent.getSchemeName())) {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, ErrorMapKeys.INVALID_SCHEME));
			}
			if ((NullCheckUtils.isNullOrEmpty(creditorAgent.getSchemeName())
					&& !NullCheckUtils.isNullOrEmpty(creditorAgent.getIdentification()))
					|| (NullCheckUtils.isNullOrEmpty(creditorAgent.getIdentification())
							&& !NullCheckUtils.isNullOrEmpty(creditorAgent.getSchemeName())))
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.CREDITOR_OR_DEBTOR_AGENT_INCOMPLETE));

		}
	}
}