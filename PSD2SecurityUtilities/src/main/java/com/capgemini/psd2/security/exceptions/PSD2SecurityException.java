package com.capgemini.psd2.security.exceptions;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;

public class PSD2SecurityException extends PSD2Exception{

	private static final long serialVersionUID = 1L;
	
	public PSD2SecurityException(String message, ErrorInfo errorInfo){
		super(message,errorInfo);
	}		
	
	public static PSD2SecurityException populatePSD2SecurityException(SCAConsentErrorCodeEnum errorCodeEnum){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), errorCodeEnum.getDetailErrorMessage(), errorCodeEnum.getStatusCode());
		/*errorInfo.setErrorCode(errorCodeEnum.getErrorCode());			
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(errorCodeEnum.getDetailErrorMessage());
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());*/
		return new PSD2SecurityException(errorCodeEnum.getDetailErrorMessage(), errorInfo);
	}	
	
	public static PSD2SecurityException populatePSD2SecurityException(String detailErrorMessage,SCAConsentErrorCodeEnum errorCodeEnum){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), errorCodeEnum.getDetailErrorMessage(), errorCodeEnum.getStatusCode());
		/*errorInfo.setErrorCode(errorCodeEnum.getErrorCode());			
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(detailErrorMessage);
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());*/
		return new PSD2SecurityException(detailErrorMessage, errorInfo);
	}

}