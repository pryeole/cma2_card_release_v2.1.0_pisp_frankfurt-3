package com.capgemini.psd2.scaconsenthelper.models;

public enum IntentTypeEnum {

	AISP_INTENT_TYPE("AISP", "accounts"), PISP_INTENT_TYPE("PISP", "payments"), CISP_INTENT_TYPE("CISP", "fundsconfirmations");

	private String scope;
	private String intentType;

	private IntentTypeEnum(String intentType, String scope) {
		this.intentType = intentType;
		this.scope = scope;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getIntentType() {
		return intentType;
	}

	public void setIntentType(String intentType) {
		this.intentType = intentType;
	}

	public static IntentTypeEnum findCurrentIntentType(String scope) {
		IntentTypeEnum currIntentTyppeEnum = null;
		IntentTypeEnum[] intentTypeEnums = IntentTypeEnum.values();
		for (IntentTypeEnum intentTypeEnum : intentTypeEnums) {
			if (intentTypeEnum.scope.equalsIgnoreCase(scope)) {
				currIntentTyppeEnum = intentTypeEnum;
				break;
			}
		}
		return currIntentTyppeEnum;
	}
}
