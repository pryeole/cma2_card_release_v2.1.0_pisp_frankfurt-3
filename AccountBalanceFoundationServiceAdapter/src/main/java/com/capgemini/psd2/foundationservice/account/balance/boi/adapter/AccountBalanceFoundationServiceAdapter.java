/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.client.AccountBalanceFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.constants.AccountBalanceFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.delegate.AccountBalanceFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.Balanceresponse;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.CreditcardsBalanceresponse;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountBalanceFoundationServiceAdapter.
 */
@Component
public class AccountBalanceFoundationServiceAdapter implements AccountBalanceAdapter {

	/** The single account balance base URL. */
	@Value("${foundationService.singleAccountBalanceBaseURL}")
	private String singleAccountBalanceBaseURL;

	@Value("${foundationService.singleAccountCreditCardBalanceBaseURL}")
	private String singleAccountCreditCardBalanceBaseURL;

	@Value("${foundationService.consentFlowType}")
	private String consentFlowType;

	@Value("${foundationService.version}")
	private String version;

	/** The account balance foundation service delegate. */
	@Autowired
	private AccountBalanceFoundationServiceDelegate accountBalanceFoundationServiceDelegate;

	/** The account balance foundation service client. */
	@Autowired
	private AccountBalanceFoundationServiceClient accountBalanceFoundationServiceClient;

	private PlatformAccountBalanceResponse platformAccountBalanceResponse;

	@Override
	public PlatformAccountBalanceResponse retrieveAccountBalance(AccountMapping accountMapping,
			Map<String, String> params) {

		if (accountMapping == null || accountMapping.getPsuId() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
		params.put(PSD2Constants.CHANNEL_ID, params.get("channelId"));
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());

		AccountDetails accountDetails;
		if (accountMapping.getAccountDetails() != null && !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
			params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_ID, accountDetails.getAccountId());
			params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE,
					String.valueOf(accountDetails.getAccountSubType()));
		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);

		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = null;
		Balanceresponse balanceResponse = null;
		CreditcardsBalanceresponse creditcardsBalanceResponse = null;

		if ((NullCheckUtils.isNullOrEmpty(accountDetails.getAccountSubType())
				&& params.get(PSD2Constants.CMAVERSION) == null)
				|| accountDetails.getAccountSubType().toString().contentEquals("CurrentAccount")
				|| accountDetails.getAccountSubType().toString().contentEquals("Savings")) {
			httpHeaders = accountBalanceFoundationServiceDelegate.createAccountRequestHeaders(requestInfo,
					accountMapping, params);
			String finalURL = accountBalanceFoundationServiceDelegate.getFoundationServiceURL(version,
					accountDetails.getAccountNSC(), accountDetails.getAccountNumber(), singleAccountBalanceBaseURL);
			requestInfo.setUrl(finalURL);
			balanceResponse = accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(requestInfo,
					Balanceresponse.class, httpHeaders);

			if (NullCheckUtils.isNullOrEmpty(balanceResponse) || balanceResponse.getBalancesList().isEmpty()
					|| null == balanceResponse.getBalancesList()) {
				throw AdapterException
						.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE);
			}
			platformAccountBalanceResponse = accountBalanceFoundationServiceDelegate
					.transformResponseFromFDToAPI(balanceResponse, params);

		} else if (accountDetails.getAccountSubType().toString().contentEquals("CreditCard")) {
			String customerReference = accountDetails.getAccount().getSecondaryIdentification();
			params.put("maskedPan", accountDetails.getAccount().getIdentification().substring(accountDetails.getAccount().getIdentification().length() - 4));
			httpHeaders = accountBalanceFoundationServiceDelegate.createCreditcardRequestHeaders(requestInfo,
					accountMapping, params);
			String finalURL = accountBalanceFoundationServiceDelegate.getCreditCardFoundationServiceURL(version,
					customerReference, accountDetails.getAccountNumber(), singleAccountCreditCardBalanceBaseURL);
			requestInfo.setUrl(finalURL);
			creditcardsBalanceResponse = accountBalanceFoundationServiceClient
					.restTransportForCreditCardBalance(requestInfo, CreditcardsBalanceresponse.class, httpHeaders);
			if (NullCheckUtils.isNullOrEmpty(creditcardsBalanceResponse)
					|| creditcardsBalanceResponse.getBalancesList().isEmpty()
					|| null == creditcardsBalanceResponse.getBalancesList()) {
				throw AdapterException
						.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE);
			}
			platformAccountBalanceResponse = accountBalanceFoundationServiceDelegate
					.transformResponseFromFDToAPI(creditcardsBalanceResponse, params);

		}
		return platformAccountBalanceResponse;
	}

}
