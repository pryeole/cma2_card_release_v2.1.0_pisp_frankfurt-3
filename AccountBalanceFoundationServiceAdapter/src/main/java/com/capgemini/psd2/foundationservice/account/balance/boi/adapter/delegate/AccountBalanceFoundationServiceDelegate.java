/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.constants.AccountBalanceFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.Balanceresponse;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.CreditcardsBalanceresponse;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.transformer.AccountBalanceFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountBalanceFoundationServiceDelegate.
 */
@Component
public class AccountBalanceFoundationServiceDelegate {
	
	/** The account balance FS transformer. */
	@Autowired
	private AccountBalanceFoundationServiceTransformer accountBalanceFSTransformer;
	
	/** The user in req header. */
	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	/** The channel in req header. */
	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	/** The platform in req header. */
	@Value("${foundationService.platformInReqHeader:#{X-BOI-WORKSTATION}}")
	private String platformInReqHeader;

	/** The correlation req header. */
	@Value("${foundationService.correlationReqHeader:#{X-BOI-WORKSTATION}}")
	private String correlationReqHeader;
	
	/** The platform. */
	@Value("$app.platform")
	private String platform;
	
	@Value("${foundationService.sourcesystem:#{X-API-SOURCE-SYSTEM}}")
	private String sourcesystem;
	
	@Value("${foundationService.transactioReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactioReqHeader;
	
	@Value("${foundationService.correlationMuleReqHeader:#{X-API-CORRELATION-ID}}")
	private String correlationMuleReqHeader;
	
	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;
	
	@Value("${foundationService.apiChannelCode:#{X-API-CHANNEL-CODE}}")
	private String apiChannelCode;
	
	@Value("${foundationService.maskedPan:#{X-MASKED-PAN}}")
	private String maskedPan;
	
	@Value("${foundationService.partySourceNumber:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partySourceNumber;

	
	/**
	 * Gets the foundation service URL.
	 *
	 * @param accountNSC the account NSC
	 * @param accountNumber the account number
	 * @param baseURL the base URL
	 * @return the foundation service URL
	 */
	public String getFoundationServiceURL(String version, String accountNSC, String accountNumber, String baseURL){
		if(NullCheckUtils.isNullOrEmpty(accountNSC)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		return baseURL + "/" + version + "/" + "accounts" + "/" + accountNSC + "/" + accountNumber + "/" + "balance";
	}
	
	/**
	 * Transform response from FD to API.
	 *
	 * @param accounts the accounts
	 * @param params the params
	 * @return the balances GET response
	 */
	
	public <T> PlatformAccountBalanceResponse transformResponseFromFDToAPI(T inputTransactionObj, Map<String, String> params){
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		if(params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE).equals(AccountBalanceFoundationServiceConstants.CURRENT_ACCOUNT)
				|| params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE).equals(AccountBalanceFoundationServiceConstants.SAVINGS) 
				|| NullCheckUtils.isNullOrEmpty(params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE))){
			Balanceresponse accounts = (Balanceresponse) inputTransactionObj;
			platformAccountBalanceResponse = accountBalanceFSTransformer.transformAccountBalance(accounts, params);
		}
		else if(params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE).equals(AccountBalanceFoundationServiceConstants.CREDIT_CARD)){
			CreditcardsBalanceresponse creditcards = (CreditcardsBalanceresponse) inputTransactionObj;
			platformAccountBalanceResponse = accountBalanceFSTransformer.transformAccountBalance(creditcards, params);
		}
		return platformAccountBalanceResponse;
	}
	
	/**
	 * Creates the request headers.
	 *
	 * @param requestInfo the request info
	 * @param accountMapping the account mapping
	 * @return the http headers
	 */
	public HttpHeaders createAccountRequestHeaders(RequestInfo requestInfo, AccountMapping accountMapping,Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		String channelCode = params.get(AccountBalanceFoundationServiceConstants.CHANNEL_ID).toUpperCase();
		httpHeaders.add(transactioReqHeader, accountMapping.getCorrelationId());
		httpHeaders.add(correlationMuleReqHeader,"");
		httpHeaders.add(sourcesystem, "PSD2API");
		httpHeaders.add(sourceUserReqHeader, accountMapping.getPsuId());
		httpHeaders.add(apiChannelCode, channelCode);
		if(channelCode.equalsIgnoreCase("BOL"))
		httpHeaders.add(partySourceNumber, params.get(PSD2Constants.PARTY_IDENTIFIER));
		return httpHeaders;
	}
	
	public HttpHeaders createCreditcardRequestHeaders(RequestInfo requestInfo, AccountMapping accountMapping,Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		String channelCode = params.get(AccountBalanceFoundationServiceConstants.CHANNEL_ID).toUpperCase();
		httpHeaders.add(maskedPan, params.get("maskedPan"));
		httpHeaders.add(apiChannelCode, channelCode);
		httpHeaders.add(sourceUserReqHeader, accountMapping.getPsuId());
		httpHeaders.add(transactioReqHeader, accountMapping.getCorrelationId());
		httpHeaders.add(correlationMuleReqHeader, "");
		httpHeaders.add(sourcesystem, "PSD2API");
		if(channelCode.equalsIgnoreCase("BOL"))
		httpHeaders.add(partySourceNumber, params.get(PSD2Constants.PARTY_IDENTIFIER));
		return httpHeaders;
	}

	public String getCreditCardFoundationServiceURL(String version, String creditCardNumber, String plApplId, String baseURL) {
		if(NullCheckUtils.isNullOrEmpty(creditCardNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(plApplId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		return baseURL + "/" + version + "/" + "accounts/creditcards" + "/" + creditCardNumber + "/" + plApplId + "/" + "balance";
	}


}
