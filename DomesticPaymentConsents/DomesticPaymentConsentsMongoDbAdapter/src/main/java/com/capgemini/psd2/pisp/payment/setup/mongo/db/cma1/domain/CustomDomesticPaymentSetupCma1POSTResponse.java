package com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.domain;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;

@Document(collection="paymentSetupFoundationResources")
public class CustomDomesticPaymentSetupCma1POSTResponse extends OBWriteDomesticConsentResponse1{
	//removing paymentStatus
	@Id
	private String id;
	private String paymentSubmissionId;
	private String paymentSubmissionStatus;
	private String paymentSubmissionStatusUpdateDateTime;
	private String payerCurrency;
	private String payerJurisdiction;
	private String accountNumber;
	private String accountNSC;
	private String accountBic;
	private String accountIban;
	private Object fraudnetResponse;
	private String paymentCharges;
	private String exchangeRate;
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPaymentSubmissionId() {
		return paymentSubmissionId;
	}

	public void setPaymentSubmissionId(String paymentSubmissionId) {
		this.paymentSubmissionId = paymentSubmissionId;
	}

	public String getPaymentCharges() {
		return paymentCharges;
	}

	public void setPaymentCharges(String paymentCharges) {
		this.paymentCharges = paymentCharges;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getPayerCurrency() {
		return payerCurrency;
	}

	public void setPayerCurrency(String payerCurrency) {
		this.payerCurrency = payerCurrency;
	}

	public String getPayerJurisdiction() {
		return payerJurisdiction;
	}

	public void setPayerJurisdiction(String payerJurisdiction) {
		this.payerJurisdiction = payerJurisdiction;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountNSC() {
		return accountNSC;
	}

	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}

	public String getAccountBic() {
		return accountBic;
	}

	public void setAccountBic(String accountBic) {
		this.accountBic = accountBic;
	}

	public String getAccountIban() {
		return accountIban;
	}

	public void setAccountIban(String accountIban) {
		this.accountIban = accountIban;
	}

	public Object getFraudnetResponse() {
		return fraudnetResponse;
	}

	public void setFraudnetResponse(Object fraudnetResponse) {
		this.fraudnetResponse = fraudnetResponse;
	}
	
	public String getPaymentSubmissionStatus() {
		return paymentSubmissionStatus;
	}

	public void setPaymentSubmissionStatus(String paymentSubmissionStatus) {
		this.paymentSubmissionStatus = paymentSubmissionStatus;
	}

	public String getPaymentSubmissionStatusUpdateDateTime() {
		return paymentSubmissionStatusUpdateDateTime;
	}

	public void setPaymentSubmissionStatusUpdateDateTime(String paymentSubmissionStatusUpdateDateTime) {
		this.paymentSubmissionStatusUpdateDateTime = paymentSubmissionStatusUpdateDateTime;
	}

	@Override
	public String toString() {
		return "CustomDomesticPaymentSetupPOSTResponse [id=" + id + ", paymentSubmissionId=" + paymentSubmissionId
				+ ", paymentSubmissionStatus=" + paymentSubmissionStatus + ", paymentSubmissionStatusUpdateDateTime="
				+ paymentSubmissionStatusUpdateDateTime + ", payerCurrency=" + payerCurrency + ", payerJurisdiction="
				+ payerJurisdiction + ", accountNumber=" + accountNumber + ", accountNSC=" + accountNSC
				+ ", accountBic=" + accountBic + ", accountIban=" + accountIban + ", fraudnetResponse="
				+ fraudnetResponse + ", paymentCharges=" + paymentCharges + ", exchangeRate=" + exchangeRate + "]";
	}	

}
