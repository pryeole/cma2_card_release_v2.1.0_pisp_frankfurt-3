/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.statement.mock.foundationservice.controller;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.statement.mock.foundationservice.raml.domain.Statementsresponse;
import com.capgemini.psd2.account.statement.mock.foundationservice.service.AccountStatementService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;

/**
 * The Class AccountStatementsController.
 */
@RestController
@RequestMapping("/fs-abt-service/services")
public class AccountStatementController {

	@Autowired
	private ValidationUtility validationUtility;
	
	/** The account statements service. */
	@Autowired
	private AccountStatementService accountStatementService;

	@RequestMapping(value = "/accounts/{accountNSC}/{accountId}/statements", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Statementsresponse reteriveAccountStatements(@PathVariable("accountId") String accountId,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestParam(required = false, value="fromStatementDateTime") String fromStatementDateTime,
			@RequestParam(required = false, value="toStatementDateTime") String toStatementDateTime,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelInReqHeader) throws Exception {

		if (NullCheckUtils.isNullOrEmpty(accountId))
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_CS_STL);
		if (!Pattern.matches("[a-zA-Z0-9-]{1,40}", accountId))
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_CS_STL);
		validationUtility.validateErrorCode(correlationId);
		//return accountStatementService.retrieveAccountStatements(accountId, startDate, endDate);
		return accountStatementService.retrieveAccountStatements(accountId, fromStatementDateTime, toStatementDateTime);

	}
	
	@RequestMapping(value = "/accounts/creditcards/{accountId}/statements", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Statementsresponse reteriveAccountStatementsCreditCard(@PathVariable("accountId") String accountId,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelInReqHeader,
			@RequestParam(required = false, value="fromStatementDateTime") String fromStatementDateTime,
			@RequestParam(required = false, value="toStatementDateTime") String toStatementDateTime,
			@RequestHeader(required = false, value = "X-MASKED-PAN") String xMaskedPan) throws Exception {

		if(NullCheckUtils.isNullOrEmpty(accountId) || NullCheckUtils.isNullOrEmpty(sourceSystemReqHeader) || NullCheckUtils.isNullOrEmpty(xMaskedPan))
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_CS_STL);

		validationUtility.validateErrorCode(correlationId);

		return accountStatementService.retrieveAccountStatements(accountId, fromStatementDateTime, toStatementDateTime);

	}

	@RequestMapping(value = "/accounts/{accountNSC}/{accountId}/statements/{statementId}/file", method = RequestMethod.GET,produces ={ "application/json", "application/pdf"})
	@ResponseBody
	public byte[] retriveStatementsPDF(@PathVariable("accountId") String accountId,
			@PathVariable("statementId") String statementId,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelInReqHeader) throws Exception {
		
		if(NullCheckUtils.isNullOrEmpty(accountId)  || NullCheckUtils.isNullOrEmpty(statementId))
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_CS_STP);
		
		validationUtility.validateErrorCode(correlationId);
		return accountStatementService.retrivePdf(accountId, statementId);
	}

	@RequestMapping(value = "/accounts/creditcards/{accountId}/statements/{statementId}/file", method = RequestMethod.GET,produces ={ "application/json", "application/pdf"})
	@ResponseBody
	public byte[] retriveStatementsCreditCardPDF(@PathVariable("accountId") String accountId,
			@PathVariable("statementId") String statementId,	
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelInReqHeader,
			@RequestHeader(required = false, value = "X-MASKED-PAN") String xMaskedPan) throws Exception {
		
		if(NullCheckUtils.isNullOrEmpty(accountId) || NullCheckUtils.isNullOrEmpty(sourceSystemReqHeader) || NullCheckUtils.isNullOrEmpty(xMaskedPan) || NullCheckUtils.isNullOrEmpty(statementId))
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_CS_STP);
		
		validationUtility.validateErrorCode(correlationId);
		return accountStatementService.retrivePdf(accountId, statementId);
	}
}
