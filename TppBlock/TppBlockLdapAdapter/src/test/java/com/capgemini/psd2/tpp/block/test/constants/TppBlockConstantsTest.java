package com.capgemini.psd2.tpp.block.test.constants;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.junit.Test;

import com.capgemini.psd2.tpp.block.constants.TppBlockConstants;

public class TppBlockConstantsTest {

	@Test(expected=Throwable.class)
	public void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
	  Constructor<TppBlockConstants> constructor = TppBlockConstants.class.getDeclaredConstructor();
	  assertTrue(Modifier.isPrivate(constructor.getModifiers()));
	  constructor.setAccessible(true);
	  constructor.newInstance();
	}

}
