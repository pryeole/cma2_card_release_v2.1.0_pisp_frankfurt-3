package com.capgemini.psd2.revoke.consent.service;

public interface RevokeConsentService {
	
	public void removeConsentRequest(String consentId, String tenantId);

}
