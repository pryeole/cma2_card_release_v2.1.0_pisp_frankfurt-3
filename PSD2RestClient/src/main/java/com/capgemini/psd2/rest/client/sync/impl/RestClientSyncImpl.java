/*******************************************************************************

 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.rest.client.sync.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;

import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class RestClientSyncImpl.
 */
@ConfigurationProperties("app")
public class RestClientSyncImpl implements RestClientSync {

	/** The rest template. */
	@Autowired
	private RestTemplate restTemplate;

	@Value("${aws.proxy:#{false}}")
	private boolean proxy;

	@Value("${aws.proxyHost:null}")
	private String proxyHost;

	@Value("${aws.proxyPort:null}")
	private String proxyPort;

	@Value("${app.proxyProtocol:HTTPS}")
	private String protocol;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestClientSyncImpl.class);
	
	private List<String> excludeProxyUrls = new ArrayList<>();

	public void setExcludeProxyUrls(List<String> excludeProxyUrls) {
		this.excludeProxyUrls = excludeProxyUrls;
	}

	/** The connection time out. */
	@Value("${app.apiConnectionTimeOut:#{30000}}")
	private String connectionTimeOut;

	/** The read time out. */
	@Value("${app.apiReadTimeOut:#{90000}}")
	private String readTimeOut;

	/** The exception handler. */
	private ExceptionHandler exceptionHandler;

	/** The max conn per route. */
	@Value("${app.maxConnPerRoute:#{30}}")
	private Integer maxConnPerRoute;

	/** The max conn total. */
	@Value("${app.maxConnTotal:#{100}}")
	private Integer maxConnTotal;
	
	@Value("${app.istlsMAEnabled:#{false}}")
	private boolean isMAenabled;
	
	@Value("${app.tlsMAKeyAlias:#{null}}")
	private String alias;
	
	@Value("${server.ssl.key-store:#{null}}")
	private String keystorePath;
	
	@Value("${server.ssl.key-store-password:#{null}}")
	private String keystorePassword;
	
	@Value("${javax.net.ssl.trustStorePassword:null}")
	private String trustStorePassword;
	
	/**
	 * Instantiates a new rest client sync impl.
	 *
	 * @param handler the handler
	 */
	@Autowired
	public RestClientSyncImpl(ExceptionHandler handler){
		this.exceptionHandler = handler;
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
	System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
		buildSSLRequest(alias);
	}


	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.sync.RestClientSync#callForGet(com.capgemini.psd2.rest.client.model.RequestInfo, java.lang.Class, org.springframework.http.HttpHeaders)
	 */
	@Override
	public <T> T callForGet(RequestInfo requestInfo, Class<T> responseType, HttpHeaders httpHeaders) {
		if(requestInfo == null || requestInfo.getUrl() == null)
			throw PSD2Exception.populatePSD2Exception("Url cannot be null",ErrorCodeEnum.REST_TRANSPORT_ADAPTER_TECHNICAL_ERROR);
		return callForGetWithParams(requestInfo,responseType,null, httpHeaders);
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.sync.RestClientSync#callForGetWithParams(com.capgemini.psd2.rest.client.model.RequestInfo, java.lang.Class, java.util.Map, org.springframework.http.HttpHeaders)
	 */
	@Override
	public <T> T callForGetWithParams(RequestInfo requestInfo, Class<T> responseType, Map<String, ?> params, HttpHeaders httpHeaders) {
		if(requestInfo == null || requestInfo.getUrl() == null)
			throw PSD2Exception.populatePSD2Exception("Url cannot be null",ErrorCodeEnum.REST_TRANSPORT_ADAPTER_TECHNICAL_ERROR);
		T outputObject = null;
		try{
			HttpHeaders populatedHttpHeaders = populateHttpHeaders(requestInfo, httpHeaders);
			HttpEntity<?> requestEntity = new HttpEntity<>(populatedHttpHeaders);
			System.out.println(restTemplate.getMessageConverters().toString()); 
			ResponseEntity<T> result;
			if (params == null) {
				List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
				messageConverters.add(new FormHttpMessageConverter());
				messageConverters.add(new StringHttpMessageConverter());
				messageConverters.add(new MappingJackson2HttpMessageConverter());
				restTemplate.setMessageConverters(messageConverters);
				result = restTemplate.exchange(requestInfo.getUrl(), HttpMethod.GET, requestEntity, responseType);
			}
			else{
				result = restTemplate.exchange(requestInfo.getUrl(), HttpMethod.GET, requestEntity, responseType, params);
			}
			outputObject = result.getBody();
		}catch (HttpClientErrorException e ) {
			exceptionHandler.handleException(e);
		}catch (HttpServerErrorException e) {
			exceptionHandler.handleException(e);
		}catch (ResourceAccessException e) {
			exceptionHandler.handleException(e);
		}
		return outputObject;
	}

	@Override
	public <T> T callForDelete(RequestInfo requestInfo, Class<T> responseType, HttpHeaders httpHeaders) {
		if(requestInfo == null || requestInfo.getUrl() == null)
			throw PSD2Exception.populatePSD2Exception("Url cannot be null",ErrorCodeEnum.REST_TRANSPORT_ADAPTER_TECHNICAL_ERROR);
		return callForDeleteWithParams(requestInfo,responseType,null, httpHeaders);
	}

	@Override
	public <T> T callForDeleteWithParams(RequestInfo requestInfo, Class<T> responseType, Map<String, ?> params,
			HttpHeaders httpHeaders) {
		if(requestInfo == null || requestInfo.getUrl() == null)
			throw PSD2Exception.populatePSD2Exception("Url cannot be null",ErrorCodeEnum.REST_TRANSPORT_ADAPTER_TECHNICAL_ERROR);
		T outputObject = null;
		try{
			HttpHeaders populatedHttpHeaders = populateHttpHeaders(requestInfo, httpHeaders);
			HttpEntity<?> requestEntity = new HttpEntity<>(populatedHttpHeaders);
			ResponseEntity<T> result;
			if (params == null) {
				result = restTemplate.exchange(requestInfo.getUrl(), HttpMethod.DELETE, requestEntity, responseType);
			}
			else{
				result = restTemplate.exchange(requestInfo.getUrl(), HttpMethod.DELETE, requestEntity, responseType, params);
			}
			outputObject = result.getBody();
		}catch (HttpClientErrorException e ) {
			exceptionHandler.handleException(e);
		}catch (HttpServerErrorException e) {
			exceptionHandler.handleException(e);
		}catch (ResourceAccessException e) {
			exceptionHandler.handleException(e);
		}
		return outputObject;
	}
	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.sync.RestClientSync#callForPost(com.capgemini.psd2.rest.client.model.RequestInfo, java.lang.Object, java.lang.Class, org.springframework.http.HttpHeaders)
	 */
	@Override
	public <T, R> R callForPost(RequestInfo requestInfo, T input, Class<R> responseType, HttpHeaders httpHeaders) {
		if(requestInfo == null || requestInfo.getUrl() == null)
			throw PSD2Exception.populatePSD2Exception("Url cannot be null",ErrorCodeEnum.REST_TRANSPORT_ADAPTER_TECHNICAL_ERROR);
		R outputObject = null;
		try{
			String correlationId = null;
			
			if(requestInfo.getRequestHeaderAttributes() != null && !NullCheckUtils.isNullOrEmpty(requestInfo.getRequestHeaderAttributes().getCorrelationId())){
			    correlationId = requestInfo.getRequestHeaderAttributes().getCorrelationId();
			}
			
			HttpHeaders populatedHttpHeaders = populateHttpHeaders(requestInfo, httpHeaders);
			Map<String, String> reqHeaders = populatedHttpHeaders.toSingleValueMap();
			
			if(!NullCheckUtils.isNullOrEmpty(correlationId))
				reqHeaders.put(PSD2Constants.CO_RELATION_ID, "" + correlationId);
			
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\"}", "RestClientSyncImpl", "callForPost", reqHeaders);
			HttpEntity<T> requestEntity = new HttpEntity<>(input, populatedHttpHeaders);
			ResponseEntity<R> result = restTemplate.exchange(requestInfo.getUrl(), HttpMethod.POST, requestEntity, responseType);
			HttpHeaders respHeaders = result.getHeaders();
			Map<String,String> responseHeader = respHeaders.toSingleValueMap();
			responseHeader.put("status", ""+result.getStatusCode().value());
			
			if(!NullCheckUtils.isNullOrEmpty(correlationId))
				responseHeader.put(PSD2Constants.CO_RELATION_ID, "" + correlationId);
			
			
			Map<String, Map<String,String>> reqResponseHeadersData = new HashMap<>();
			reqResponseHeadersData.put("requestHeaders", reqHeaders);
			reqResponseHeadersData.put("responseHeaders", responseHeader);
			
			LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\"}", "RestClientSyncImpl", "callForPost", reqResponseHeadersData);
			
			outputObject = result.getBody();
		}catch (HttpClientErrorException e) {
			exceptionHandler.handleException(e);
		}catch (HttpServerErrorException e) {
			exceptionHandler.handleException(e);
		}catch (ResourceAccessException e) {
			exceptionHandler.handleException(e);
		}
		return outputObject;
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.sync.RestClientSync#callForPut(com.capgemini.psd2.rest.client.model.RequestInfo, java.lang.Object, java.lang.Class, org.springframework.http.HttpHeaders)
	 */
	public <T, R> R callForPut(RequestInfo requestInfo, T input, Class<R> responseType, HttpHeaders httpHeaders) {
		if(requestInfo == null || requestInfo.getUrl() == null)
			throw PSD2Exception.populatePSD2Exception("Url cannot be null",ErrorCodeEnum.REST_TRANSPORT_ADAPTER_TECHNICAL_ERROR);
		R outputObject = null;
		try{
			HttpHeaders populatedHttpHeaders = populateHttpHeaders(requestInfo, httpHeaders);
			HttpEntity<T> requestEntity = new HttpEntity<>(input, populatedHttpHeaders);
			ResponseEntity<R> result = restTemplate.exchange(requestInfo.getUrl(), HttpMethod.PUT, requestEntity, responseType);
			outputObject = result.getBody();
		}catch (HttpClientErrorException e) {
			exceptionHandler.handleException(e);
		}catch (HttpServerErrorException e) {
			exceptionHandler.handleException(e);
		}catch (ResourceAccessException e) {
			exceptionHandler.handleException(e);
		}
		return outputObject;
	}


	/**
	 * Populate http headers.
	 *
	 * @param requestInfo the request info
	 * @param httpHeaders the http headers
	 * @return the http headers
	 */
	private HttpHeaders populateHttpHeaders(RequestInfo requestInfo, HttpHeaders httpHeaders) {
		HttpHeaders localHttpHeaders = httpHeaders;
		if(httpHeaders == null)
			localHttpHeaders = new HttpHeaders();
		if(requestInfo.getRequestHeaderAttributes()!=null)
			localHttpHeaders.add(PSD2Constants.CORRELATION_ID, requestInfo.getRequestHeaderAttributes().getCorrelationId());
		return localHttpHeaders;
	}

	/**
	 * Populate trust store.
	 *
	 * @return the key store
	 */
	private KeyStore populateTrustStore() {
		KeyStore ks = null;
		String trustStoreFile = System.getProperty("javax.net.ssl.trustStore");
		String trustStorePwd = System.getProperty("javax.net.ssl.trustStorePassword");
		if(trustStoreFile !=null && !trustStoreFile.isEmpty() && trustStorePwd != null && !trustStorePwd.isEmpty()) {
			try (FileInputStream fi = new FileInputStream(new File(trustStoreFile))) {
				ks= KeyStore.getInstance("jks");
				ks.load(fi,trustStorePwd.toCharArray());
			} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
				throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CRYPTO_TECHNICAL_ERROR);
			}		
		}
		return ks;
	} 	

	/**
	 * Populate Key store.
	 *
	 * @return key store
	 */
	private KeyStore populateKeyStore() {
		KeyStore keystore=null;
		try {
			 Resource resource = new ClassPathResource(keystorePath);
			 InputStream in = resource.getInputStream();
			 keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			 keystore.load(in, keystorePassword.toCharArray());
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CRYPTO_TECHNICAL_ERROR);
		}
		return keystore;
	}
	
	/**
	 * Builds the SSL request.
	 */
	public void buildSSLRequest(final String keyAlias) {
		SSLContext sslcontext = null;
		KeyStore trustStore = populateTrustStore();
		HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory = null;
		try {
			if(trustStore == null) {
				if(proxy){
				httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().setMaxConnPerRoute(maxConnPerRoute).setMaxConnTotal(maxConnTotal)
						.setRoutePlanner(new DefaultProxyRoutePlanner(new HttpHost(proxyHost, Integer.valueOf(proxyPort), protocol)) {

							@Override
							public HttpHost determineProxy(HttpHost target,
									HttpRequest request, HttpContext context)
											throws HttpException {
								if (excludeProxyUrls.contains(target.getHostName())) {
									return null;
								}
								return super.determineProxy(target, request, context);
							}

						}).build());
				}else{
					httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().setMaxConnPerRoute(maxConnPerRoute).setMaxConnTotal(maxConnTotal).build());
				}
				if(connectionTimeOut != null) {
					httpComponentsClientHttpRequestFactory.setConnectionRequestTimeout(Integer.parseInt(connectionTimeOut));
				}
				if(readTimeOut != null) {
					httpComponentsClientHttpRequestFactory.setReadTimeout(Integer.parseInt(readTimeOut));
				}
				this.restTemplate.setRequestFactory(httpComponentsClientHttpRequestFactory);
				return;
			}
			if (isMAenabled && null != keyAlias) {
				try {
				
					sslcontext = SSLContexts.custom()
							.loadKeyMaterial(populateKeyStore(), keystorePassword.toCharArray(),
									(aliases, socket) -> keyAlias)
							.loadTrustMaterial(trustStore, new TrustSelfSignedStrategy()).build();
				} catch (UnrecoverableKeyException e) {
					throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CRYPTO_TECHNICAL_ERROR);
				}

			} else {
				sslcontext = SSLContexts.custom().loadTrustMaterial(trustStore, new TrustSelfSignedStrategy()).build();
			}
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			CloseableHttpClient httpClient;
			if(proxy){
				httpClient = HttpClientBuilder.create().setSSLSocketFactory(sslsf).setMaxConnPerRoute(maxConnPerRoute).setMaxConnTotal(maxConnTotal)
						.setRoutePlanner(new DefaultProxyRoutePlanner(new HttpHost(proxyHost, Integer.valueOf(proxyPort), protocol)) {

							@Override
							public HttpHost determineProxy(HttpHost target,
									HttpRequest request, HttpContext context)
											throws HttpException {
								if (excludeProxyUrls.contains(target.getHostName())) {
									return null;
								}
								return super.determineProxy(target, request, context);
							}

						}).build();
			}else{
				httpClient = HttpClientBuilder.create().setSSLSocketFactory(sslsf).setMaxConnPerRoute(maxConnPerRoute).setMaxConnTotal(maxConnTotal).build();
			}
			httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
			if(connectionTimeOut != null) {
				httpComponentsClientHttpRequestFactory.setConnectionRequestTimeout(Integer.parseInt(connectionTimeOut));
			}
			if(readTimeOut != null) {
				httpComponentsClientHttpRequestFactory.setReadTimeout(Integer.parseInt(readTimeOut));
			}
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CRYPTO_TECHNICAL_ERROR);
		}		
		this.restTemplate.setRequestFactory(httpComponentsClientHttpRequestFactory);
	}


}
