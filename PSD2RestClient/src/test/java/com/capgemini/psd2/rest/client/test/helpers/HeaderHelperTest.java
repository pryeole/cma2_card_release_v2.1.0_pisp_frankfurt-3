package com.capgemini.psd2.rest.client.test.helpers;

import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.rest.client.helpers.HeaderHelper;

@RunWith(SpringJUnit4ClassRunner.class)
public class HeaderHelperTest {

	@InjectMocks
	static HeaderHelper helper;

	@Before
	public void setUp() {

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testHelperImpl() {
		HttpHeaders requestHeaders = helper.populateJSONContentTypeHeader();
		assertNotNull(requestHeaders);
	}

}
