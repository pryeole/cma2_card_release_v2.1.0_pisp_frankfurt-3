package com.capgemini.psd2.account.statements;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.capgemini.psd2.filter.PSD2Filter;


@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" })
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@EnableDiscoveryClient
public class AccountStatementsApplication {

	/** The context. */
	static ConfigurableApplicationContext context = null;

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		context = SpringApplication.run(AccountStatementsApplication.class, args);
	}
	
	@Bean(name="psd2Filter")
	public Filter psd2Filter(){
		return new PSD2Filter(); 
	}
	
	
}
