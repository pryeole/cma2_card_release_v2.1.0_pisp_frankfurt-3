/*******************************************************************************

\ * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.psd2.config.DomainNameConfig;
import com.capgemini.psd2.exceptions.ApiEndExceptionHandler;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.multi.read.request.wrapper.MultiReadHttpServletRequest;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class PSD2Filter.
 */
@EnableConfigurationProperties
@ConfigurationProperties("app")
public class PSD2Filter extends OncePerRequestFilter {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PSD2Filter.class);

	/** The Constant DO_FILTER_INTERNAL_STRING. */
	private static final String DO_FILTER_INTERNAL_STRING = "com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()";

	/** The Constant doFilter. */
	private static final String DO_FILTER = "doFilterInternal";

	/** The req header attribute. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAttribute;

	@Value("${api.internalEndpoint:#{null}}")
	private String internalEndPoint;

	@Autowired
	private DomainNameConfig domainNameMap;

	private List<String> claims = new ArrayList<>();

	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private ApiEndExceptionHandler apiEndExceptionHandler;

	/**
	 * Sets the claims.
	 *
	 * @param claims
	 *            the new claims
	 */
	public void setClaims(List<String> claims) {
		this.claims = claims;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.filter.OncePerRequestFilter#doFilterInternal(
	 * javax. servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse servletResponse,
			FilterChain filterChain) throws ServletException, IOException {

		try {
			// If block has been added in reference with the defect # 1150
			// The jackson Objectmapper was accepting multiroot jsons
			// To handle multi-root Request body we have used
			// com.google.gson.JsonParser for PUT and POST Requests
			// And In order to read the http request multiple time a wrapper has
			// been added that
			// creates cache of the current request and then can be used
			// multiple times
			MultiReadHttpServletRequest multiReadServletrequest = null;
			Boolean multiReadServletrequestFlag = Boolean.FALSE;

			if (RequestMethod.POST.toString().equalsIgnoreCase(httpServletRequest.getMethod())
					|| RequestMethod.PUT.toString().equalsIgnoreCase(httpServletRequest.getMethod())) {
				multiReadServletrequestFlag = Boolean.TRUE;
				multiReadServletrequest = new MultiReadHttpServletRequest((HttpServletRequest) httpServletRequest);
				String requestBody = multiReadServletrequest.getReader().lines()
						.collect(Collectors.joining(System.lineSeparator()));
				JSONUtilities.jsonParser(requestBody);
			}

			String currentCorrId = httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID);
			if (NullCheckUtils.isNullOrEmpty(currentCorrId)) {
				currentCorrId = GenerateUniqueIdUtilities.getUniqueId().toString();
				httpServletRequest.setAttribute(PSD2Constants.CORRELATION_ID, currentCorrId);
				reqHeaderAttribute.setCorrelationId(currentCorrId);
				LOG.info("{\"Enter\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"{}\"}",
						DO_FILTER_INTERNAL_STRING, httpServletRequest.getRemoteAddr(),
						httpServletRequest.getRemoteHost(), loggerUtils.populateLoggerData(DO_FILTER));
			} else {
				if (currentCorrId
						.matches("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$")) {
					reqHeaderAttribute.setCorrelationId(currentCorrId);
				} else {
					reqHeaderAttribute.setCorrelationId(currentCorrId);
					throw PSD2Exception.populatePSD2Exception(
							new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_HEADER_INVALID, ErrorMapKeys.HEADER));
				}
				LOG.info("{\"Enter\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"{}\"}",
						DO_FILTER_INTERNAL_STRING, httpServletRequest.getRemoteAddr(),
						httpServletRequest.getRemoteHost(), loggerUtils.populateLoggerData(DO_FILTER));
			}

			servletResponse.addHeader(PSD2Constants.CORRELATION_ID, currentCorrId);

			/* Adding missing response security header */
			if (servletResponse.getHeader(PSD2Constants.CONTENT_SECURITY_POLICY) == null)
				servletResponse.addHeader(PSD2Constants.CONTENT_SECURITY_POLICY, PSD2Constants.REFERRER_ORIGIN);

			String requestUri = (domainNameMap.getTenantSpecificDomain(
							httpServletRequest.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)))
											.concat(httpServletRequest.getRequestURI());
					 
			reqHeaderAttribute.setSelfUrl(
					requestUri.endsWith("/") ? requestUri.substring(0, requestUri.length() - 1) : requestUri);
			reqHeaderAttribute.setMethodType(httpServletRequest.getMethod());

			/* Skip token and headers validation for internal apis. */
			if (internalEndPoint != null && httpServletRequest.getRequestURI().startsWith(internalEndPoint)) {
				filterChain.doFilter(httpServletRequest, servletResponse);
				LOG.info("{\"Exit\":\"{}\",\"{}\"}", DO_FILTER_INTERNAL_STRING,
						loggerUtils.populateLoggerData(DO_FILTER));
				return;
			}

			validateAndSetHeaders(httpServletRequest);
			retrieveAndValidateTokenIntrospectionData(httpServletRequest);

			if (multiReadServletrequestFlag) {
				filterChain.doFilter(multiReadServletrequest, servletResponse);
			} else {
				filterChain.doFilter(httpServletRequest, servletResponse);
			}
			LOG.info("{\"Exit\":\"{}\",\"{}\"}", DO_FILTER_INTERNAL_STRING, loggerUtils.populateLoggerData(DO_FILTER));
		} catch (Exception e) {
			apiEndExceptionHandler.handleAndLogPsd2FilterException(e, httpServletRequest, servletResponse);
		}
	}

	private boolean validateClaims(Token token) {
		boolean valid = Boolean.FALSE;
		if (this.claims == null || this.claims.isEmpty()) {
			return Boolean.TRUE;
		}
		if (token.getClaims() == null || token.getClaims().isEmpty()) {
			return valid;
		}
		for (String allowedClaim : claims) {
			for (Object tokenClaim : reqHeaderAttribute.getClaims()) {
				if (allowedClaim.equalsIgnoreCase(tokenClaim.toString())) {
					return true;
				}
			}
		}
		return valid;
	}

	/**
	 * Retrieve and Validates Token Introspection Data
	 * 
	 * @param httpServletRequest
	 */
	private void retrieveAndValidateTokenIntrospectionData(HttpServletRequest httpServletRequest) {
		LOG.info("{\"Enter\":\"{}\",\"{}\"}", "com.capgemini.psd2.filter.retrieveAndValidateTokenIntrospectionData()",
 				loggerUtils.populateLoggerData("retrieveAndValidateTokenIntrospectionData"));
		String tokenIntrospectionData = httpServletRequest.getHeader(PSD2Constants.TOKEN_INTROSPECTION_DATA);
		if (StringUtils.isBlank(tokenIntrospectionData)) {
			throw PSD2Exception.populatePSD2Exception(
					PSD2Constants.TOKEN_INTROSPECTION_DATA + " is missing in headers.", ErrorCodeEnum.HEADER_MISSING);
		}
		Token token = null;
		if (tokenIntrospectionData != null && !tokenIntrospectionData.isEmpty()) {
			tokenIntrospectionData = tokenIntrospectionData.replace("\\", "");
			token = JSONUtilities.getObjectFromJSONString(tokenIntrospectionData, Token.class);
			reqHeaderAttribute.setAttributes(token);
			boolean valid = validateClaims(token);
			if (!valid) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_REQUIRED_PERMISSION_PRESENT);
			}
		}
		LOG.info("{\"Exit\":\"{}\",\"{}\",\"tokenIntrospectionData\":{}}",
				"com.capgemini.psd2.logger.PSD2Filter.retrieveAndValidateTokenIntrospectionData()",
				loggerUtils.populateLoggerData("retrieveAndValidateTokenIntrospectionData"),
				JSONUtilities.getJSONOutPutFromObject(token));
	}

	/**
	 * Validate and set headers.
	 *
	 * @param httpServletRequest
	 *            the http servlet request
	 */
	private void validateAndSetHeaders(HttpServletRequest httpServletRequest) {
		LOG.info("{\"Enter\":\"{}\",\"{}\"}", "com.capgemini.psd2.logger.PSD2Filter.validateAndSetHeaders()",
				loggerUtils.populateLoggerData("validateAndSetHeaders"));
		String financialId = httpServletRequest.getHeader(PSD2Constants.FINANCIAL_ID);
		if (httpServletRequest.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME) == null)
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_HEADER_INVALID, ErrorMapKeys.HEADER));

		reqHeaderAttribute.setTenantId(httpServletRequest.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME));

		/*
		 * Load test issue fix, to make thread safe tenantId object holder
		 */
		RequestAttributes springContextAttr = RequestContextHolder.getRequestAttributes();
		springContextAttr.setAttribute("requestTenantId",
						httpServletRequest.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME),
						RequestAttributes.SCOPE_REQUEST);
		
		reqHeaderAttribute.setJwksUrlFiles(httpServletRequest.getHeader(PSD2Constants.FILE_JWKS_URL));
		if (StringUtils.isBlank(financialId)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_HEADER_MISSING, ErrorMapKeys.HEADER));
		}
		reqHeaderAttribute.setFinancialId(financialId);
		String customerIPAddress = httpServletRequest.getHeader(PSD2Constants.CUSTOMER_IP_ADDRESS);
		if (customerIPAddress != null && !customerIPAddress
				.matches("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
						+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$")) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_HEADER_INVALID, ErrorMapKeys.CUSTOMER_IP_ADDRESS_HEADER));

		}

		reqHeaderAttribute.setCustomerIPAddress(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_IP_ADDRESS));

		String customerLastLoggedInTime = httpServletRequest.getHeader(PSD2Constants.CUSTOMER_LAST_LOGGED_TIME);
		DateUtilites.validateStringInRFCDateFormat(customerLastLoggedInTime);
		reqHeaderAttribute.setCustomerLastLoggedTime(customerLastLoggedInTime);

		reqHeaderAttribute.setIdempotencyKey(httpServletRequest.getHeader(PSD2Constants.IDEMPOTENCY_KEY));

		String jwsHeader = httpServletRequest.getHeader(PSD2Constants.JWS_HEADER);
		reqHeaderAttribute.setJwsHeader(jwsHeader);

		String orgId = httpServletRequest.getHeader("ou");
		reqHeaderAttribute.setX_ssl_client_ou(orgId);

		LOG.info(
				"{\"Exit\":\"{}\",\"financialId\":\"{}\",\"customerIPAddress\":\"{}\",\"customerLastLoggedTime\":\"{}\",\"idempotencyKey\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.logger.PSD2Filter.validateAndSetHeaders()", reqHeaderAttribute.getFinancialId(),
				reqHeaderAttribute.getCustomerIPAddress(), reqHeaderAttribute.getCustomerLastLoggedTime(),
				reqHeaderAttribute.getIdempotencyKey(), loggerUtils.populateLoggerData("validateAndSetHeaders"));
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		List<String> excludeUrlPatterns = new ArrayList<>();
		excludeUrlPatterns.add("/restart");
		excludeUrlPatterns.add("/health");
		AntPathMatcher matcher = new AntPathMatcher();
		return excludeUrlPatterns.stream().anyMatch(p -> matcher.match(p, request.getServletPath()));
	}
}
