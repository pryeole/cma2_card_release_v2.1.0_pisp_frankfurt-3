package com.capgemini.psd2.aspect;

import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBPSD2Exception;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.response.filteration.ResponseFilter;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.ResponseSigningUtility;

@Component
public class PSD2AspectUtils {
	
	@Autowired
	private HttpServletResponse response;

	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired(required=false)
	private ResponseSigningUtility responseBodySigningUtility;

	@Autowired
	private LoggerAttribute loggerAttribute;

	@Value("${app.payloadLog:#{false}}")
	private boolean payloadLog;

	/** The mask payload log. */
	@Value("${app.maskPayloadLog:#{false}}")
	private boolean maskPayloadLog;

	/** The mask payload. */
	@Value("${app.maskPayload:#{false}}")
	private boolean maskPayload;

	@Autowired
	private DataMask dataMask;

	@Autowired
	private ResponseFilter responseFilterUtility;

	private static final Logger LOGGER = LoggerFactory.getLogger(PSD2AspectUtils.class);
	
	private static final String ERROR_DETAILS = "{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":{}}";
	
	private static final String ERROR_DETAILS_1 = "{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":\"{}\"}";


	public Object methodAdvice(ProceedingJoinPoint proceedingJoinPoint) {
		loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
		LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\"}", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
				proceedingJoinPoint.getSignature().getName(), loggerAttribute);
		try {
			Object result = proceedingJoinPoint.proceed();
			loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
			LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\"}", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute);
			return result;
		} catch (PSD2Exception e) {
			logException(e, proceedingJoinPoint);
			throw e;
		} catch (OBPSD2Exception e) {
			logOBException(e, proceedingJoinPoint);
			throw e;
		} catch (Throwable e) {
			PSD2Exception psd2Exception = PSD2Exception.generateResponseForInternalErrors(e);
			logException(psd2Exception, proceedingJoinPoint);
			throw psd2Exception;
		}
	}

	public Object methodPayloadAdvice(ProceedingJoinPoint proceedingJoinPoint) {
		loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
		if (maskPayloadLog && payloadLog) {
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\",\"Arguments\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute,
					JSONUtilities.getJSONOutPutFromObject(dataMask.maskRequestLog(proceedingJoinPoint.getArgs(),
							proceedingJoinPoint.getSignature().getName())));

		} else if (!maskPayloadLog && payloadLog) {
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\",\"Arguments\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute,
					JSONUtilities.getJSONOutPutFromObject(dataMask.maskMRequestLog(proceedingJoinPoint.getArgs(),
							proceedingJoinPoint.getSignature().getName())));

		} else {
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\"}", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute);

		}
		try {

			Object result = proceedingJoinPoint.proceed();
			loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
			result = responseFilterUtility.filterResponse(result, proceedingJoinPoint.getSignature().getName());
			if (maskPayloadLog && payloadLog) {
				LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\",\"Payload\":{}}",
						proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute,
						JSONUtilities.getJSONOutPutFromObject(
								dataMask.maskResponseLog(result, proceedingJoinPoint.getSignature().getName())));
			} else if (!maskPayloadLog && payloadLog) {
				LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\",\"Payload\":{}}",
						proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute,
						JSONUtilities.getJSONOutPutFromObject(
								dataMask.maskMResponseLog(result, proceedingJoinPoint.getSignature().getName())));
			} else {
				LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\"}", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute);
			}
			if (maskPayload)
				dataMask.maskResponse(result, proceedingJoinPoint.getSignature().getName());
			else
				dataMask.maskMResponse(result, proceedingJoinPoint.getSignature().getName());
			
			boolean responseBodySigningAllow = 	responseBodySigningUtility != null;
			
			if (responseBodySigningAllow) {
				String jwsHeader = responseBodySigningUtility.signResponseBody(result);
				response.setHeader(PSD2Constants.JWS_HEADER, jwsHeader);
			}
			return result;
		} catch (PSD2Exception e) {
			logException(e, proceedingJoinPoint);
			throw e;
		}catch(OBPSD2Exception e) {
			logOBException(e, proceedingJoinPoint);
			throw e;
		}catch (Throwable e) {
			PSD2Exception psd2Exception = PSD2Exception.generateResponseForInternalErrors(e);
			logException(psd2Exception, proceedingJoinPoint);
			throw psd2Exception;
		}
	}

	public void throwExceptionOnJsonBinding(JoinPoint joinPoint, Throwable error) {
		PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT, error.toString()));
		loggerUtils.populateLoggerData(joinPoint.getSignature().getName());
		LOGGER.error(ERROR_DETAILS,
				joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName(), loggerAttribute,
				psd2Exception.getOBErrorResponse1());
		if (LOGGER.isDebugEnabled()) {
			LOGGER.error(ERROR_DETAILS_1,
					joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName(),
					loggerAttribute, error.getStackTrace());
		}

		throw psd2Exception;
	}

	private void logException(PSD2Exception e, ProceedingJoinPoint proceedingJoinPoint) {
		loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
		
		if(e.getOBErrorResponse1()!=null)
			LOGGER.error(ERROR_DETAILS,
				proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName(),
				loggerAttribute, e.getOBErrorResponse1());
		else
			LOGGER.error(ERROR_DETAILS,
					proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName(),
					loggerAttribute, e.getErrorInfo());
		if (LOGGER.isDebugEnabled()) {
			LOGGER.error(ERROR_DETAILS_1,
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
		}
	}
	
	private void logOBException(OBPSD2Exception e, ProceedingJoinPoint proceedingJoinPoint) {
		loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
		LOGGER.error(ERROR_DETAILS,
				proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName(),
				loggerAttribute, e.getErrorResponse());
		if (LOGGER.isDebugEnabled()) {
			LOGGER.error(ERROR_DETAILS_1,
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
		}
	}
	
	
}
