package com.capgemini.psd2.pisp.file.payments.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.FPaymentsFoundationResource;

public interface FilePaymentsFoundationRepository
		extends MongoRepository<FPaymentsFoundationResource, String> {
	
	public FPaymentsFoundationResource findOneByDataConsentId(String paymentConsentId);
}
