/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.standingorder.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.standingorder.mock.foundationservice.raml.domain.*;
import com.capgemini.psd2.account.standingorder.mock.foundationservice.service.AccountStandingOrderService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;

/**
 * The Class AccountStandingOrderController.
 */
@RestController
@RequestMapping("/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.33/core-banking/p/abt")
public class AccountStandingOrderController {

	/** The account StandingOrder service. */
	@Autowired
	private AccountStandingOrderService accountStandingOrderService;
	
	@Autowired
	private ValidationUtility validationUtility;

	/**
	 * Channel A reterive account StandingOrder.
	 *
	 * @param accountNsc
	 *            the account nsc
	 * @param accountNumber
	 *            the account number
	 * @param boiUser
	 *            the boi user
	 * @param boiChannel
	 *            the boi channel
	 * @param boiPlatform
	 *            the boi platform
	 * @param correlationID
	 *            the correlation ID
	 * @return the accounts
	 * @throws Exception
	 *             the exception
	 */
	@RequestMapping(value = "/{version}/accounts/{nsc}/{accountNumber}/standing-order-schedule-instructions", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public StandingOrderScheduleInstructionsresponse retrieveAccountStandingOrder(@PathVariable("accountNumber") String accountNumber,
			@RequestHeader(required = false, value = "x-api-source-user") String boiUser,
			@RequestHeader(required = false, value = "x-api-source-system") String boiChannel,
			@RequestHeader(required = false, value=	 "x-api-transaction-id") String transactionId,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationID) throws Exception {

		validationUtility.validateErrorCode(transactionId); 
		if (boiUser == null || boiChannel == null || transactionId == null || correlationID == null) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PAC_STO);
		}
		
		if(NullCheckUtils.isNullOrEmpty(accountNumber)){
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PAC_STO);
		}

		return accountStandingOrderService.retrieveAccountStandingOrder(accountNumber);

	}

}
