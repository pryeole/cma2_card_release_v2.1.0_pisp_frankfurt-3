package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.transformer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.fraudnet.domain.DecisionType;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.constants.PispScaConsentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.Channel;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ChannelCode;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.FraudSystemResponse;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.Party2;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.ChargeDetails;
import com.capgemini.psd2.pisp.stage.domain.CreditorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDSOData;
import com.capgemini.psd2.pisp.stage.domain.CustomDSPData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.RemittanceDetails;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;
@Component
public class PispScaConsentFoundationServiceTransformer {

	@Autowired
	private PSD2Validator psd2Validator;

	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	@Value("${foundationService.endToEndIdentificationMockValue}")
	private String endToEndIdentificationMockValue;

	public <T> CustomConsentAppViewData transformPispScaConsentResponse(T inputBalanceObj,Map<String, String> params) {

		/* Root Elements */
		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		PaymentInstructionProposal paymentIns = (PaymentInstructionProposal) inputBalanceObj;

		customConsentAppViewData.setPaymentType(String.valueOf(paymentIns.getPaymentType()));

		if ((!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyAccount())) && (!NullCheckUtils
				.isNullOrEmpty(paymentIns.getAuthorisingPartyAccount().getAccountIdentification()))) {
			CustomDebtorDetails debtorDetails = new CustomDebtorDetails();
			debtorDetails.setIdentification(paymentIns.getAuthorisingPartyAccount().getAccountIdentification());
			debtorDetails.setName(paymentIns.getAuthorisingPartyAccount().getAccountName());
			debtorDetails.setSchemeName(paymentIns.getAuthorisingPartyAccount().getSchemeName());

			if (paymentIns.getAuthorisingPartyAccount().getSecondaryIdentification() != null) {
				debtorDetails.setSecondaryIdentification(
						paymentIns.getAuthorisingPartyAccount().getSecondaryIdentification());
			}
			customConsentAppViewData.setDebtorDetails(debtorDetails);
		}

		CreditorDetails creditorDetails = new CreditorDetails();
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyAccount())) {
			creditorDetails.setIdentification(paymentIns.getProposingPartyAccount().getAccountIdentification());
			creditorDetails.setName(paymentIns.getProposingPartyAccount().getAccountName());
			creditorDetails.setSchemeName(paymentIns.getProposingPartyAccount().getSchemeName());
			if (paymentIns.getProposingPartyAccount().getSecondaryIdentification() != null) {
				creditorDetails
						.setSecondaryIdentification(paymentIns.getProposingPartyAccount().getSecondaryIdentification());
			}
			customConsentAppViewData.setCreditorDetails(creditorDetails);
		}

		ChargeDetails chargeDetails = null;
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getCharges())) {
			chargeDetails = new ChargeDetails();
			List<OBCharge1> obcharge1list = new ArrayList<>();
			chargeDetails.setChargesList(obcharge1list);
			customConsentAppViewData.setChargeDetails(chargeDetails);

			List<PaymentInstructionCharge> paymChargeList = paymentIns.getCharges();
			for (PaymentInstructionCharge pymtCharge : paymChargeList) {
				OBCharge1 obcharge1 = new OBCharge1();
				OBCharge1Amount ob = new OBCharge1Amount();
				ob.setAmount(String.valueOf(pymtCharge.getAmount().getTransactionCurrency()));
				ob.setCurrency(pymtCharge.getCurrency().getIsoAlphaCode());
				obcharge1.setAmount(ob);
				obcharge1.setChargeBearer(
						OBChargeBearerType1Code.fromValue(String.valueOf(pymtCharge.getChargeBearer())));
				obcharge1.setType(pymtCharge.getType());
				obcharge1list.add(obcharge1);
			}

		}

		RemittanceDetails remittanceDetails = new RemittanceDetails();
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
			System.out.println("Channel Details: "+params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
			if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase("B365")) {
				if (paymentIns.getAuthorisingPartyReference() != null) {
					remittanceDetails.setReference(paymentIns.getAuthorisingPartyReference());
				}
			}
			else if(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase("BOL")) {
					if(paymentIns.getInstructionEndToEndReference() != null) 
						remittanceDetails.setReference(paymentIns.getInstructionEndToEndReference());					
			}
		}
		if (paymentIns.getAuthorisingPartyUnstructuredReference() != null) {
			remittanceDetails.setUnstructured(paymentIns.getAuthorisingPartyUnstructuredReference());
		}
		customConsentAppViewData.setRemittanceDetails(remittanceDetails);

		AmountDetails amountDetails = new AmountDetails();
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getFinancialEventAmount())
				&& !NullCheckUtils.isNullOrEmpty(paymentIns.getTransactionCurrency())) {
			amountDetails.setAmount(String.valueOf(paymentIns.getFinancialEventAmount().getTransactionCurrency()));
			amountDetails.setCurrency(paymentIns.getTransactionCurrency().getIsoAlphaCode());
			customConsentAppViewData.setAmountDetails(amountDetails);
		}

		psd2Validator.validate(customConsentAppViewData);
		return customConsentAppViewData;

	}

	public <T> CustomConsentAppViewData transformPispScaConsentResponseScheduled(T inputBalanceObj,Map<String, String> params) {

		/* Root Elements */
		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();

		ScheduledPaymentInstructionProposal scheduledpaymentIns = (ScheduledPaymentInstructionProposal) inputBalanceObj;

		CustomDSPData customDSPData = new CustomDSPData();
		customConsentAppViewData.setCustomDSPData(customDSPData);

		if (scheduledpaymentIns.getRequestedExecutionDateTime() != null) {
			customDSPData.setRequestedExecutionDateTime(scheduledpaymentIns.getRequestedExecutionDateTime());
		}
		CustomDebtorDetails debtorDetails = new CustomDebtorDetails();
		if (!NullCheckUtils.isNullOrEmpty(scheduledpaymentIns.getAuthorisingPartyAccount())) {

			debtorDetails
					.setIdentification(scheduledpaymentIns.getAuthorisingPartyAccount().getAccountIdentification());
			debtorDetails.setName(scheduledpaymentIns.getAuthorisingPartyAccount().getAccountName());
			debtorDetails.setSchemeName(scheduledpaymentIns.getAuthorisingPartyAccount().getSchemeName());

			if (scheduledpaymentIns.getAuthorisingPartyAccount().getSecondaryIdentification() != null) {
				debtorDetails.setSecondaryIdentification(
						scheduledpaymentIns.getAuthorisingPartyAccount().getSecondaryIdentification());
			}

			customConsentAppViewData.setDebtorDetails(debtorDetails);
		}

		CreditorDetails creditorDetails = new CreditorDetails();
		if (!NullCheckUtils.isNullOrEmpty(scheduledpaymentIns.getProposingPartyAccount())) {
			creditorDetails
					.setIdentification(scheduledpaymentIns.getProposingPartyAccount().getAccountIdentification());
			creditorDetails.setName(scheduledpaymentIns.getProposingPartyAccount().getAccountName());
			creditorDetails.setSchemeName(scheduledpaymentIns.getProposingPartyAccount().getSchemeName());
			if (scheduledpaymentIns.getProposingPartyAccount().getSecondaryIdentification() != null) {
				creditorDetails.setSecondaryIdentification(
						scheduledpaymentIns.getProposingPartyAccount().getSecondaryIdentification());
			}
			customConsentAppViewData.setCreditorDetails(creditorDetails);
		}

		ChargeDetails chargeDetails = null;
		if (!NullCheckUtils.isNullOrEmpty(scheduledpaymentIns.getCharges())) {

			chargeDetails = new ChargeDetails();
			List<OBCharge1> obcharge1list = new ArrayList<>();
			chargeDetails.setChargesList(obcharge1list);
			customConsentAppViewData.setChargeDetails(chargeDetails);

			List<PaymentInstructionCharge> paymChargeList = scheduledpaymentIns.getCharges();
			for (PaymentInstructionCharge pymetCharge : paymChargeList) {
				OBCharge1 obcharge = new OBCharge1();
				OBCharge1Amount ob1 = new OBCharge1Amount();
				ob1.setAmount(String.valueOf(pymetCharge.getAmount().getTransactionCurrency()));
				ob1.setCurrency(pymetCharge.getCurrency().getIsoAlphaCode());
				obcharge.setAmount(ob1);
				obcharge.setChargeBearer(
						OBChargeBearerType1Code.fromValue(String.valueOf(pymetCharge.getChargeBearer())));
				obcharge.setType(pymetCharge.getType());
				obcharge1list.add(obcharge);
			}

		}
		System.out.println("scheduledpaymentIns details: "+scheduledpaymentIns);
		RemittanceDetails remittanceDetails = new RemittanceDetails();
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
			System.out.println("Channel Details: "+params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
			if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase("B365")) {
				if (scheduledpaymentIns.getAuthorisingPartyReference() != null) {
					remittanceDetails.setReference(scheduledpaymentIns.getAuthorisingPartyReference());
				}
			}
			else if(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase("BOL")) {
					if(scheduledpaymentIns.getInstructionEndToEndReference() != null) 
						remittanceDetails.setReference(scheduledpaymentIns.getInstructionEndToEndReference());
					else if (scheduledpaymentIns.getAuthorisingPartyReference() != null)
						remittanceDetails.setReference(scheduledpaymentIns.getAuthorisingPartyReference());
					else 
						remittanceDetails.setReference("TPP Payment");	
			}
		}
		if (scheduledpaymentIns.getAuthorisingPartyUnstructuredReference() != null) {
			remittanceDetails.setUnstructured(scheduledpaymentIns.getAuthorisingPartyUnstructuredReference());
		}
		customConsentAppViewData.setRemittanceDetails(remittanceDetails);

		AmountDetails amountDetails = new AmountDetails();
		if (!NullCheckUtils.isNullOrEmpty(scheduledpaymentIns.getFinancialEventAmount())
				&& !NullCheckUtils.isNullOrEmpty(scheduledpaymentIns.getTransactionCurrency())) {
			amountDetails
					.setAmount(String.valueOf(scheduledpaymentIns.getFinancialEventAmount().getTransactionCurrency()));
			amountDetails.setCurrency(scheduledpaymentIns.getTransactionCurrency().getIsoAlphaCode());
			customConsentAppViewData.setAmountDetails(amountDetails);
		}
		System.out.println("customConsentAppViewData data in transformPispScaConsentResponseScheduled: "+customConsentAppViewData);
		psd2Validator.validate(customConsentAppViewData);
		return customConsentAppViewData;

	}

	public PaymentInstructionProposal transformDomesticConsentResponseFromAPIToFDForInsert(
			OBCashAccountDebtor3 selectedDebtorDetails, PaymentInstructionProposal paymentInstructionProposal, Map<String, String> params) {
		
		AuthorisingPartyAccount authorisingPartyAccount1 = new AuthorisingPartyAccount();
		
		if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails)) {

	    if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")){
			
				if ((!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getSchemeName()))
						&& (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getIdentification()))) {
					if (selectedDebtorDetails.getSchemeName()
							.equalsIgnoreCase(PispScaConsentFoundationServiceConstants.UK_OBIE_SORTCODEACCOUNTNUMBER)
							|| selectedDebtorDetails.getSchemeName()
									.equalsIgnoreCase(PispScaConsentFoundationServiceConstants.SORTCODEACCOUNTNUMBER)) {
						authorisingPartyAccount1.setSchemeName(selectedDebtorDetails.getSchemeName());
						authorisingPartyAccount1.setAccountIdentification(selectedDebtorDetails.getIdentification());
					} else if (selectedDebtorDetails.getSchemeName()
							.equalsIgnoreCase(PispScaConsentFoundationServiceConstants.UK_OBIE_IBAN)|| selectedDebtorDetails.getSchemeName()
									.equalsIgnoreCase(PispScaConsentFoundationServiceConstants.IBAN)) {
						authorisingPartyAccount1.setSchemeName(selectedDebtorDetails.getSchemeName());
						authorisingPartyAccount1.setAccountIdentification(params.get("accountIban"));
					}
				}
			
          }
          else if((params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")))
          {
		  
		  	System.out.println("scheme name in ROI"+ selectedDebtorDetails.getSchemeName());
      		if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getSchemeName())) {
				authorisingPartyAccount1.setSchemeName("UK.OBIE.IBAN");
			}
			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getIdentification())) {
				authorisingPartyAccount1.setAccountIdentification(params.get("accountIban"));
			}
          }

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getName())) {
				authorisingPartyAccount1.setAccountName(selectedDebtorDetails.getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getSecondaryIdentification())) {
				authorisingPartyAccount1.setSecondaryIdentification(selectedDebtorDetails.getSecondaryIdentification());
			}
		}
		paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount1);
		
		Channel chanel = new Channel();
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
			if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.API.toString())
					|| params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.BOL.toString())) {
				chanel.setChannelCode(ChannelCode.BOL);
			}
			if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.API.toString())
					|| params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.B365.toString())) {
				chanel.setChannelCode(ChannelCode.B365);
			}
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
				chanel.setBrandCode(BrandCode3.NIGB);
			}
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
				chanel.setBrandCode(BrandCode3.ROI);
			}
		}
		paymentInstructionProposal.setChannel(chanel);
		
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))
				&& params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.BOL.toString())) {
			Party2 authorisingParty = new Party2();
			PartyBasicInformation partyInformation = new PartyBasicInformation();
			partyInformation.setPartySourceIdNumber(params.get(PSD2Constants.PARTY_IDENTIFIER));
			partyInformation.setPartyName(PispScaConsentFoundationServiceConstants.PARTY_NAME);
			authorisingParty.setPartyInformation(partyInformation);
			paymentInstructionProposal.setAuthorisingParty(authorisingParty);
		}
		
		return paymentInstructionProposal;

	}

	public <T> PaymentConsentsValidationResponse transformDomesticConsentResponseFromFDToAPIForInsert(
			T paymentInstructionProposalResponse, CustomPaymentStageIdentifiers stageIdentifiers) {

		PaymentInstructionProposal paymentIns = (PaymentInstructionProposal) paymentInstructionProposalResponse;
		PaymentConsentsValidationResponse paymentConsentsValidationResponse = new PaymentConsentsValidationResponse();

		if ((!NullCheckUtils.isNullOrEmpty(stageIdentifiers))
				&& (!NullCheckUtils.isNullOrEmpty(stageIdentifiers.getPaymentSubmissionId()))) {

			paymentConsentsValidationResponse.setPaymentSubmissionId(stageIdentifiers.getPaymentSubmissionId());
		}

		if ((!NullCheckUtils.isNullOrEmpty(paymentIns))
				&& (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposalStatus()))) {

			paymentConsentsValidationResponse.setPaymentSetupValidationStatus(
					OBTransactionIndividualStatus1Code.fromValue(paymentIns.getProposalStatus().toString()));
		}
		return paymentConsentsValidationResponse;
	}

	public PaymentInstructionProposal transformPispScaConsentResponseForUpdate(CustomPaymentStageUpdateData updateData,
			PaymentInstructionProposal paymentInstructionProposal, Map<String, String> params) {
		if (!NullCheckUtils.isNullOrEmpty(updateData)) {

			if (updateData.getSetupStatusUpdated()) {
				paymentInstructionProposal.setProposalStatusUpdateDatetime(updateData.getSetupStatusUpdateDateTime());

				if (updateData.getSetupStatus().equalsIgnoreCase("AcceptedCustomerProfile")) {
					paymentInstructionProposal.setProposalStatus(ProposalStatus.AcceptedCustomerProfile);
				} else {
					paymentInstructionProposal.setProposalStatus(ProposalStatus.fromValue(updateData.getSetupStatus()));
				}
			}
			
			//Channel Object
			Channel channel = new Channel();
			if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
				if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.API.toString())
						|| params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.BOL.toString())) {
					channel.setChannelCode(ChannelCode.BOL);
				}
				if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.API.toString())
						|| params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.B365.toString())) {
					channel.setChannelCode(ChannelCode.B365);
				}
			}
			if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
				if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
					channel.setBrandCode(BrandCode3.NIGB);
				}
				if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
					channel.setBrandCode(BrandCode3.ROI);
				}
			}
			paymentInstructionProposal.setChannel(channel);

			if (!NullCheckUtils.isNullOrEmpty(updateData.getFraudScore())) {
				FraudServiceResponse fraud = (FraudServiceResponse) updateData.getFraudScore();

				if (updateData.getFraudScoreUpdated()) {
					if (fraud.getDecisionType().equals(DecisionType.APPROVE)) {
						paymentInstructionProposal.setFraudSystemResponse(FraudSystemResponse.Approve);
					}
					if (fraud.getDecisionType().equals(DecisionType.INVESTIGATE_APPROVE)) {
						paymentInstructionProposal.setFraudSystemResponse(FraudSystemResponse.Investigate);
					}
					if (fraud.getDecisionType().equals(DecisionType.INVESTIGATE_CANCEL)) {
						paymentInstructionProposal.setFraudSystemResponse(FraudSystemResponse.Investigate);
					}
					if (fraud.getDecisionType().equals(DecisionType.INVESTIGATE_WAIT)) {
						paymentInstructionProposal.setFraudSystemResponse(FraudSystemResponse.Investigate);
					}
					if (fraud.getDecisionType().equals(DecisionType.REJECT)) {
						paymentInstructionProposal.setFraudSystemResponse(FraudSystemResponse.Investigate);
					}
				}

			}
			if ((updateData.getDebtorDetailsUpdated())
					&& (!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails()))) {

				AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
				System.out.println("NN updateData ::"+updateData.toString());
				System.out.println("NN params ::"+params.toString());
				
				if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")){
					if ((!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails().getSchemeName()))
							&& (!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails().getIdentification()))) {
						if (updateData.getDebtorDetails().getSchemeName().equalsIgnoreCase(
								PispScaConsentFoundationServiceConstants.UK_OBIE_SORTCODEACCOUNTNUMBER)|| updateData.getDebtorDetails().getSchemeName().equalsIgnoreCase(
										PispScaConsentFoundationServiceConstants.SORTCODEACCOUNTNUMBER)) {
							System.out.println("NN update sortcodeAccountNumber");
							authorisingPartyAccount.setSchemeName(updateData.getDebtorDetails().getSchemeName());
							authorisingPartyAccount
									.setAccountIdentification(updateData.getDebtorDetails().getIdentification());
						} else if (updateData.getDebtorDetails().getSchemeName()
								.equalsIgnoreCase(PispScaConsentFoundationServiceConstants.UK_OBIE_IBAN)|| updateData.getDebtorDetails().getSchemeName()
										.equalsIgnoreCase(PispScaConsentFoundationServiceConstants.IBAN)) {
							System.out.println("NN update IBAN");
							authorisingPartyAccount.setSchemeName(updateData.getDebtorDetails().getSchemeName());
							authorisingPartyAccount.setAccountIdentification(params.get(PSD2Constants.IBAN));
						}
					}
				
		          }
		          else if((params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")))
		          {
				  
		        	  	System.out.println("scheme name in ROI"+ updateData.getDebtorDetails().getSchemeName());
		        	  if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.IBAN))){
		        		  authorisingPartyAccount.setSchemeName("UK.OBIE.IBAN");	  
		        		  authorisingPartyAccount.setAccountIdentification(params.get(PSD2Constants.IBAN));
		        	  }
		        	  
		          }
				System.out.println("authorisingPartyAccount in transformer"+ authorisingPartyAccount);
				paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
				/*paymentInstructionProposal.getAuthorisingPartyAccount()
						.setSchemeName(updateData.getDebtorDetails().getSchemeName());
				paymentInstructionProposal.getAuthorisingPartyAccount()
						.setAccountIdentification(updateData.getDebtorDetails().getIdentification());*/
				paymentInstructionProposal.getAuthorisingPartyAccount()
						.setAccountName(updateData.getDebtorDetails().getName());
				paymentInstructionProposal.getAuthorisingPartyAccount()
						.setSecondaryIdentification(updateData.getDebtorDetails().getSecondaryIdentification());
			}
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))
				&& params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.BOL.toString())) {
		Party2 authorisingParty = new Party2();
		PartyBasicInformation partyInformation = new PartyBasicInformation();
		partyInformation.setPartySourceIdNumber(params.get(PSD2Constants.PARTY_IDENTIFIER));
		partyInformation.setPartyName(PispScaConsentFoundationServiceConstants.PARTY_NAME);
		authorisingParty.setPartyInformation(partyInformation);
				
		//Defect Fix Drop2A #3355 
		if(!paymentInstructionProposal.getProposalStatus().equals(ProposalStatus.Rejected) && !NullCheckUtils.isNullOrEmpty(authorisingParty.getPartyInformation().getPartySourceIdNumber()))
		paymentInstructionProposal.setAuthorisingParty(authorisingParty);
		}
		
		return paymentInstructionProposal;
	}

	public ScheduledPaymentInstructionProposal transformDomesticScheduledConsentResponseFromAPIToFDForInsert(
			OBCashAccountDebtor3 selectedDebtorDetails,
			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal, Map<String, String> params) {

		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
		scheduledPaymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);

		if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionProposal.getInstructionEndToEndReference())) {
			scheduledPaymentInstructionProposal.setInstructionEndToEndReference(
					scheduledPaymentInstructionProposal.getInstructionEndToEndReference());
		} else {
			scheduledPaymentInstructionProposal.setInstructionEndToEndReference(endToEndIdentificationMockValue);
		}

		if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails)) {

			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
				if ((!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getSchemeName()))
						&& (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getIdentification()))) {
					if (selectedDebtorDetails.getSchemeName()
							.equalsIgnoreCase(PispScaConsentFoundationServiceConstants.UK_OBIE_SORTCODEACCOUNTNUMBER)
							|| selectedDebtorDetails.getSchemeName()
									.equalsIgnoreCase(PispScaConsentFoundationServiceConstants.SORTCODEACCOUNTNUMBER)) {
						authorisingPartyAccount.setSchemeName(selectedDebtorDetails.getSchemeName());
						authorisingPartyAccount.setAccountIdentification(selectedDebtorDetails.getIdentification());
					} else if (selectedDebtorDetails.getSchemeName()
							.equalsIgnoreCase(PispScaConsentFoundationServiceConstants.UK_OBIE_IBAN)
							|| selectedDebtorDetails.getSchemeName()
									.equalsIgnoreCase(PispScaConsentFoundationServiceConstants.IBAN)) {
						authorisingPartyAccount.setSchemeName(selectedDebtorDetails.getSchemeName());
						authorisingPartyAccount.setAccountIdentification(params.get("accountIban"));
					}
				}
			} else if ((params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI"))) {
				if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getSchemeName())) {
					authorisingPartyAccount.setSchemeName("UK.OBIE.IBAN");
				}
				if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getIdentification())) {
					authorisingPartyAccount.setAccountIdentification(params.get("accountIban"));
				}
			}

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getName())) {
				authorisingPartyAccount.setAccountName(selectedDebtorDetails.getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getSecondaryIdentification())) {
				authorisingPartyAccount.setSecondaryIdentification(selectedDebtorDetails.getSecondaryIdentification());
			}
		}
		Channel chanel = new Channel();
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
			if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.API.toString())
					|| params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.BOL.toString())) {
				chanel.setChannelCode(ChannelCode.BOL);
			}
			if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.API.toString())
					|| params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.B365.toString())) {
				chanel.setChannelCode(ChannelCode.B365);
			}
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
				chanel.setBrandCode(BrandCode3.NIGB);
			}
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
				chanel.setBrandCode(BrandCode3.ROI);
			}
		}
		scheduledPaymentInstructionProposal.setChannel(chanel);
		return scheduledPaymentInstructionProposal;

	}

	public PaymentConsentsValidationResponse transformDomesticScheduledConsentResponseFromFDToAPIForInsert(
			CustomPaymentStageIdentifiers stageIdentifiers) {
		PaymentConsentsValidationResponse paymentConsentsValidationResponse = new PaymentConsentsValidationResponse();

		if ((!NullCheckUtils.isNullOrEmpty(stageIdentifiers))
				&& (!NullCheckUtils.isNullOrEmpty(stageIdentifiers.getPaymentSubmissionId()))) {
			paymentConsentsValidationResponse.setPaymentSubmissionId(stageIdentifiers.getPaymentSubmissionId());
		}

		return paymentConsentsValidationResponse;
	}

	public ScheduledPaymentInstructionProposal transformPispScaConsentScheduledResponseForUpdate(
			CustomPaymentStageUpdateData updateData,
			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal,Map<String, String> params) {

		if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionProposal.getInstructionEndToEndReference())) {
			scheduledPaymentInstructionProposal.setInstructionEndToEndReference(
					scheduledPaymentInstructionProposal.getInstructionEndToEndReference());
		} else {
			scheduledPaymentInstructionProposal.setInstructionEndToEndReference(endToEndIdentificationMockValue);
		}

		if (!NullCheckUtils.isNullOrEmpty(updateData)) {
			if (updateData.getSetupStatusUpdated()) {
				scheduledPaymentInstructionProposal
						.setProposalStatusUpdateDatetime(updateData.getSetupStatusUpdateDateTime());
				scheduledPaymentInstructionProposal
						.setProposalStatus(ProposalStatus.fromValue(updateData.getSetupStatus()));
			}
			Channel channel = new Channel();
			if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
				if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.API.toString())
						|| params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.BOL.toString())) {
					channel.setChannelCode(ChannelCode.BOL);
				}
				if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.API.toString())
						|| params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.B365.toString())) {
					channel.setChannelCode(ChannelCode.B365);
				}
			}
			if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
				if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
					channel.setBrandCode(BrandCode3.NIGB);
				}
				if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
					channel.setBrandCode(BrandCode3.ROI);
				}
			}
			scheduledPaymentInstructionProposal.setChannel(channel);
			if (!NullCheckUtils.isNullOrEmpty(updateData.getFraudScore())) {
				FraudServiceResponse fraud = (FraudServiceResponse) updateData.getFraudScore();

				if (updateData.getFraudScoreUpdated()) {
					if (fraud.getDecisionType().equals(DecisionType.APPROVE)) {
						scheduledPaymentInstructionProposal.setFraudSystemResponse(FraudSystemResponse.Approve);
					}
					if (fraud.getDecisionType().equals(DecisionType.INVESTIGATE_APPROVE)) {
						scheduledPaymentInstructionProposal.setFraudSystemResponse(FraudSystemResponse.Investigate);
					}
					if (fraud.getDecisionType().equals(DecisionType.INVESTIGATE_CANCEL)) {
						scheduledPaymentInstructionProposal.setFraudSystemResponse(FraudSystemResponse.Investigate);
					}
					if (fraud.getDecisionType().equals(DecisionType.INVESTIGATE_WAIT)) {
						scheduledPaymentInstructionProposal.setFraudSystemResponse(FraudSystemResponse.Investigate);
					}
					if (fraud.getDecisionType().equals(DecisionType.REJECT)) {
						scheduledPaymentInstructionProposal.setFraudSystemResponse(FraudSystemResponse.Investigate);
					}
				}

			}
			if ((updateData.getDebtorDetailsUpdated())
					&& (!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails()))) {
				AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
				if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {

					if ((!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails().getSchemeName()))
							&& (!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails().getIdentification()))) {
						if (updateData.getDebtorDetails().getSchemeName().equalsIgnoreCase(
								PispScaConsentFoundationServiceConstants.UK_OBIE_SORTCODEACCOUNTNUMBER)
								|| updateData.getDebtorDetails().getSchemeName().equalsIgnoreCase(
										PispScaConsentFoundationServiceConstants.SORTCODEACCOUNTNUMBER)) {
							authorisingPartyAccount.setSchemeName(updateData.getDebtorDetails().getSchemeName());
							authorisingPartyAccount
									.setAccountIdentification(updateData.getDebtorDetails().getIdentification());
						} else if (updateData.getDebtorDetails().getSchemeName()
								.equalsIgnoreCase(PispScaConsentFoundationServiceConstants.UK_OBIE_IBAN)
								|| updateData.getDebtorDetails().getSchemeName()
										.equalsIgnoreCase(PispScaConsentFoundationServiceConstants.IBAN)) {
							authorisingPartyAccount.setSchemeName(updateData.getDebtorDetails().getSchemeName());
							authorisingPartyAccount.setAccountIdentification(params.get(PSD2Constants.IBAN));
						}
					}

				} else if ((params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI"))) {

					if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.IBAN))) {
						authorisingPartyAccount.setSchemeName("UK.OBIE.IBAN");
						authorisingPartyAccount.setAccountIdentification(params.get(PSD2Constants.IBAN));
					}

				}
				scheduledPaymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
				scheduledPaymentInstructionProposal.getAuthorisingPartyAccount()
						.setAccountName(updateData.getDebtorDetails().getName());
				scheduledPaymentInstructionProposal.getAuthorisingPartyAccount()
						.setSecondaryIdentification(updateData.getDebtorDetails().getSecondaryIdentification());
			}

		}
		return scheduledPaymentInstructionProposal;
	}

	public <T> CustomConsentAppViewData transformPispScaConsentResponseForStandingOrders(T inputBalanceObj,
			Map<String, String> params) {

		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		StandingOrderInstructionProposal proposal = (StandingOrderInstructionProposal) inputBalanceObj;
		params.put("PaymentType", PispScaConsentFoundationServiceConstants.DOMESTIC_ST_ORD);
		customConsentAppViewData.setPaymentType(params.get("PaymentType"));

		CustomDebtorDetails debtorDetails = new CustomDebtorDetails();
		if (!NullCheckUtils.isNullOrEmpty(proposal.getAuthorisingPartyAccount())) {

			debtorDetails.setIdentification(proposal.getAuthorisingPartyAccount().getAccountIdentification());
			debtorDetails.setName(proposal.getAuthorisingPartyAccount().getAccountName());
			debtorDetails.setSchemeName(proposal.getAuthorisingPartyAccount().getSchemeName());

			if (proposal.getAuthorisingPartyAccount().getSecondaryIdentification() != null) {
				debtorDetails
						.setSecondaryIdentification(proposal.getAuthorisingPartyAccount().getSecondaryIdentification());
			}

			customConsentAppViewData.setDebtorDetails(debtorDetails);
		}

		CreditorDetails creditorDetails = new CreditorDetails();
		if (!NullCheckUtils.isNullOrEmpty(proposal.getProposingPartyAccount())) {
			creditorDetails.setIdentification(proposal.getProposingPartyAccount().getAccountIdentification());
			creditorDetails.setName(proposal.getProposingPartyAccount().getAccountName());
			creditorDetails.setSchemeName(proposal.getProposingPartyAccount().getSchemeName());
			if (proposal.getProposingPartyAccount().getSecondaryIdentification() != null) {
				creditorDetails
						.setSecondaryIdentification(proposal.getProposingPartyAccount().getSecondaryIdentification());
			}
			customConsentAppViewData.setCreditorDetails(creditorDetails);
		}

		ChargeDetails chargeDetails = new ChargeDetails();
		if (!NullCheckUtils.isNullOrEmpty(proposal.getCharges())) {

			List<OBCharge1> obcharge1list = new ArrayList<>();
			chargeDetails.setChargesList(obcharge1list);
			customConsentAppViewData.setChargeDetails(chargeDetails);

			List<PaymentInstructionCharge> paymChargeList = proposal.getCharges();
			for (PaymentInstructionCharge pymtCharge : paymChargeList) {
				OBCharge1 obcharge1 = new OBCharge1();
				OBCharge1Amount ob = new OBCharge1Amount();
				ob.setAmount(String.valueOf(pymtCharge.getAmount().getTransactionCurrency()));
				ob.setCurrency(pymtCharge.getCurrency().getIsoAlphaCode());
				obcharge1.setAmount(ob);
				obcharge1.setChargeBearer(
						OBChargeBearerType1Code.fromValue(String.valueOf(pymtCharge.getChargeBearer())));
				obcharge1.setType(pymtCharge.getType());
				obcharge1list.add(obcharge1);
			}

		}

		AmountDetails amountDetails = new AmountDetails();
		CustomDSOData customDSOData = new CustomDSOData();
		customDSOData.setFinalPaymentDateTime((proposal.getFinalPaymentDateTime()));
		customDSOData.setFrequency(proposal.getFrequency().toString()); 
		customDSOData.setFirstPaymentDateTime((proposal.getFirstPaymentDateTime()));
		customDSOData.setNumberOfPayment(String.valueOf(proposal.getNumberOfPayments()));
		if (!NullCheckUtils.isNullOrEmpty(proposal.getRecurringPaymentAmount())) {
			amountDetails.setAmount(String
					.valueOf(proposal.getRecurringPaymentAmount().getFinancialEventAmount().getTransactionCurrency()));
			amountDetails.setCurrency(
					String.valueOf(proposal.getRecurringPaymentAmount().getTransactionCurrency().getIsoAlphaCode()));
			customDSOData.setRecurringChargeDetails(amountDetails);
		}
		if (!NullCheckUtils.isNullOrEmpty(proposal.getFinalPaymentAmount())) {
			amountDetails.setAmount(String
					.valueOf(proposal.getFinalPaymentAmount().getFinancialEventAmount().getTransactionCurrency()));
			amountDetails.setCurrency(
					String.valueOf(proposal.getFinalPaymentAmount().getTransactionCurrency().getIsoAlphaCode()));
			customDSOData.setFinalChargeDetails(amountDetails);
		}
		if (!NullCheckUtils.isNullOrEmpty(proposal.getFirstPaymentAmount())
				&& !NullCheckUtils.isNullOrEmpty(proposal.getFirstPaymentAmount().getFinancialEventAmount())
				&& !NullCheckUtils.isNullOrEmpty(proposal.getFirstPaymentAmount().getTransactionCurrency())) {
			amountDetails.setAmount(String
					.valueOf(proposal.getFirstPaymentAmount().getFinancialEventAmount().getTransactionCurrency()));
			amountDetails.setCurrency(proposal.getFirstPaymentAmount().getTransactionCurrency().getIsoAlphaCode());
			customConsentAppViewData.setAmountDetails(amountDetails);
		}

		customConsentAppViewData.setCustomDSOData(customDSOData);

		return customConsentAppViewData;
	}

	public CustomFraudSystemPaymentData transformFraudSystemDomesticPaymentStagedData(
			PaymentInstructionProposal paymentInstructionProposal) {

		CustomFraudSystemPaymentData customFraudSystemPaymentData = new CustomFraudSystemPaymentData();

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getFinancialEventAmount())) {
			customFraudSystemPaymentData.setTransferAmount(
					String.valueOf(paymentInstructionProposal.getFinancialEventAmount().getTransactionCurrency()));
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getTransactionCurrency())) {
			customFraudSystemPaymentData
					.setTransferCurrency(paymentInstructionProposal.getTransactionCurrency().getIsoAlphaCode());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getAuthorisingPartyReference())) {
			customFraudSystemPaymentData.setTransferMemo(paymentInstructionProposal.getAuthorisingPartyReference());
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getProposingPartyAccount())) {
			CreditorDetails creditor = new CreditorDetails();
			creditor.setSchemeName(paymentInstructionProposal.getProposingPartyAccount().getSchemeName());
			creditor.setIdentification(
					paymentInstructionProposal.getProposingPartyAccount().getAccountIdentification());
			creditor.setName(paymentInstructionProposal.getProposingPartyAccount().getAccountName());
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getProposingPartyAccount().getSecondaryIdentification())) {
				creditor.setSecondaryIdentification(
						paymentInstructionProposal.getProposingPartyAccount().getSecondaryIdentification());
			}
			customFraudSystemPaymentData.setCreditorDetails(creditor);
		}
		Date transferTime = new Date();
		DateFormat dateFormat = new SimpleDateFormat(PispScaConsentFoundationServiceConstants.DATE_FORMAT);
		String transferDate = dateFormat.format(transferTime);
		customFraudSystemPaymentData.setTransferTime(transferDate);

		return customFraudSystemPaymentData;
	}

	public CustomFraudSystemPaymentData transformFraudSystemDomesticScheduledPaymentStagedData(
			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal) {

		CustomFraudSystemPaymentData customFraudSystemPaymentData = new CustomFraudSystemPaymentData();

		if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionProposal.getFinancialEventAmount())) {
			customFraudSystemPaymentData.setTransferAmount(String
					.valueOf(scheduledPaymentInstructionProposal.getFinancialEventAmount().getTransactionCurrency()));
		}

		if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionProposal.getTransactionCurrency())) {
			customFraudSystemPaymentData.setTransferCurrency(
					scheduledPaymentInstructionProposal.getTransactionCurrency().getIsoAlphaCode());
		}

		if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionProposal.getAuthorisingPartyReference())) {
			customFraudSystemPaymentData
					.setTransferMemo(scheduledPaymentInstructionProposal.getAuthorisingPartyReference());
		}
		if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionProposal.getProposingPartyAccount())) {
			CreditorDetails creditor = new CreditorDetails();
			creditor.setSchemeName(scheduledPaymentInstructionProposal.getProposingPartyAccount().getSchemeName());
			creditor.setIdentification(
					scheduledPaymentInstructionProposal.getProposingPartyAccount().getAccountIdentification());
			creditor.setName(scheduledPaymentInstructionProposal.getProposingPartyAccount().getAccountName());
			if (!NullCheckUtils.isNullOrEmpty(
					scheduledPaymentInstructionProposal.getProposingPartyAccount().getSecondaryIdentification())) {
				creditor.setSecondaryIdentification(
						scheduledPaymentInstructionProposal.getProposingPartyAccount().getSecondaryIdentification());
			}
			customFraudSystemPaymentData.setCreditorDetails(creditor);
		}
		Date transferTime = new Date();
		DateFormat dateFormat = new SimpleDateFormat(PispScaConsentFoundationServiceConstants.DATE_FORMAT);
		String transferDate = dateFormat.format(transferTime);
		customFraudSystemPaymentData.setTransferTime(transferDate);

		return customFraudSystemPaymentData;
	}

	public CustomFraudSystemPaymentData transformFraudSystemDomesticStandingOrderPaymentStagedData(
			StandingOrderInstructionProposal standingOrderInstructionProposal) {

		CustomFraudSystemPaymentData customFraudSystemPaymentData = new CustomFraudSystemPaymentData();

		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount()
				.getFinancialEventAmount().getTransactionCurrency())) {
			customFraudSystemPaymentData.setTransferAmount(String.valueOf(standingOrderInstructionProposal
					.getFirstPaymentAmount().getFinancialEventAmount().getTransactionCurrency()));
		}
		if (!NullCheckUtils.isNullOrEmpty(
				standingOrderInstructionProposal.getFirstPaymentAmount().getTransactionCurrency().getIsoAlphaCode())) {
			customFraudSystemPaymentData.setTransferCurrency(standingOrderInstructionProposal.getFirstPaymentAmount()
					.getTransactionCurrency().getIsoAlphaCode());
		}

		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getReference())) {
			customFraudSystemPaymentData.setTransferMemo(standingOrderInstructionProposal.getReference());
		}
		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount())) {
			CreditorDetails creditor = new CreditorDetails();
			creditor.setSchemeName(standingOrderInstructionProposal.getProposingPartyAccount().getSchemeName());
			creditor.setIdentification(
					standingOrderInstructionProposal.getProposingPartyAccount().getAccountIdentification());
			creditor.setName(standingOrderInstructionProposal.getProposingPartyAccount().getAccountName());
			if (!NullCheckUtils.isNullOrEmpty(
					standingOrderInstructionProposal.getProposingPartyAccount().getSecondaryIdentification())) {
				creditor.setSecondaryIdentification(
						standingOrderInstructionProposal.getProposingPartyAccount().getSecondaryIdentification());
			}
			customFraudSystemPaymentData.setCreditorDetails(creditor);
		}
		Date transferTime = new Date();
		DateFormat dateFormat = new SimpleDateFormat(PispScaConsentFoundationServiceConstants.DATE_FORMAT);
		String transferDate = dateFormat.format(transferTime);
		customFraudSystemPaymentData.setTransferTime(transferDate);

		return customFraudSystemPaymentData;
	}

	public StandingOrderInstructionProposal transformDomesticStandingOrderConsentResponseFromAPIToFDForInsert(
			OBCashAccountDebtor3 selectedDebtorDetails,
			StandingOrderInstructionProposal standingOrderInstructionProposal) {
		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();

		if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails)) {

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getSchemeName())) {
				authorisingPartyAccount.setSchemeName(selectedDebtorDetails.getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getIdentification())) {
				authorisingPartyAccount.setAccountIdentification(selectedDebtorDetails.getIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getName())) {
				authorisingPartyAccount.setAccountName(selectedDebtorDetails.getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getSecondaryIdentification())) {
				authorisingPartyAccount.setSecondaryIdentification(selectedDebtorDetails.getSecondaryIdentification());
			}
		}
		standingOrderInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
		return standingOrderInstructionProposal;
	}

	public PaymentConsentsValidationResponse transformDomesticStandingOrderConsentResponseFromFDToAPIForInsert(
			CustomPaymentStageIdentifiers stageIdentifiers) {
		PaymentConsentsValidationResponse paymentConsentsValidationResponse = new PaymentConsentsValidationResponse();

		if ((!NullCheckUtils.isNullOrEmpty(stageIdentifiers))
				&& (!NullCheckUtils.isNullOrEmpty(stageIdentifiers.getPaymentSubmissionId()))) {

			paymentConsentsValidationResponse.setPaymentSubmissionId(stageIdentifiers.getPaymentSubmissionId());
		}
		return paymentConsentsValidationResponse;
	}

	public StandingOrderInstructionProposal transformDomesticStandingOrderConsentResponseForUpdate(
			CustomPaymentStageUpdateData updateData, StandingOrderInstructionProposal standingorderinstructionproposal) {

		if (!NullCheckUtils.isNullOrEmpty(updateData)) {
			if (updateData.getSetupStatusUpdated()) {
				standingorderinstructionproposal
						.setProposalStatusUpdateDatetime(updateData.getSetupStatusUpdateDateTime());
				standingorderinstructionproposal
						.setProposalStatus(ProposalStatus.fromValue(updateData.getSetupStatus()));
			}
          
			if (!NullCheckUtils.isNullOrEmpty(updateData.getFraudScore())) {
				FraudServiceResponse fraud = (FraudServiceResponse) updateData.getFraudScore();

				if (updateData.getFraudScoreUpdated()) {
					if (fraud.getDecisionType().equals(DecisionType.APPROVE)) {
						standingorderinstructionproposal.setFraudSystemResponse(FraudSystemResponse.Approve);
					}
					if (fraud.getDecisionType().equals(DecisionType.INVESTIGATE_APPROVE)) {
						standingorderinstructionproposal.setFraudSystemResponse(FraudSystemResponse.Investigate);
					}
					if (fraud.getDecisionType().equals(DecisionType.INVESTIGATE_CANCEL)) {
						standingorderinstructionproposal.setFraudSystemResponse(FraudSystemResponse.Investigate);
					}
					if (fraud.getDecisionType().equals(DecisionType.INVESTIGATE_WAIT)) {
						standingorderinstructionproposal.setFraudSystemResponse(FraudSystemResponse.Investigate);
					}
					if (fraud.getDecisionType().equals(DecisionType.REJECT)) {
						standingorderinstructionproposal.setFraudSystemResponse(FraudSystemResponse.Investigate);
					}
				}

			}
			if ((updateData.getDebtorDetailsUpdated())
					&& (!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails()))) {
				AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
				standingorderinstructionproposal.setAuthorisingPartyAccount(authorisingPartyAccount);
				standingorderinstructionproposal.getAuthorisingPartyAccount()
						.setSchemeName(updateData.getDebtorDetails().getSchemeName());
				standingorderinstructionproposal.getAuthorisingPartyAccount()
						.setAccountIdentification(updateData.getDebtorDetails().getIdentification());
				standingorderinstructionproposal.getAuthorisingPartyAccount()
						.setAccountName(updateData.getDebtorDetails().getName());
				standingorderinstructionproposal.getAuthorisingPartyAccount()
						.setSecondaryIdentification(updateData.getDebtorDetails().getSecondaryIdentification());
			}

		}

		return standingorderinstructionproposal;
	}
}
