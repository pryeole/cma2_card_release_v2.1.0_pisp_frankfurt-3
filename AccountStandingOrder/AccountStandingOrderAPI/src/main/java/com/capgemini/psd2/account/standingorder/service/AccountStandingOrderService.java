package com.capgemini.psd2.account.standingorder.service;

import com.capgemini.psd2.aisp.domain.OBReadStandingOrder3;

@FunctionalInterface
public interface AccountStandingOrderService 
{
	/**
	 * Retrieve account standing order.
	 *
	 * @param accountId the account id
	 * @return the StandingOrdersGETResponse
	 */
	public OBReadStandingOrder3 retrieveAccountStandingOrders(String accountId);

}
