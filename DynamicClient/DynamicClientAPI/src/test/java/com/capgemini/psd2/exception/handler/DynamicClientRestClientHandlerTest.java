package com.capgemini.psd2.exception.handler;

import java.nio.charset.Charset;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.capgemini.psd2.aspect.DynamicAspectUtils;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.utilities.JSONUtilities;
@RunWith(SpringJUnit4ClassRunner.class)
public class DynamicClientRestClientHandlerTest {

	/** The resource access exception. */
	@Mock
	private ResourceAccessException resourceAccessException;

	/** The http client error exception. */
	@Mock
	private HttpClientErrorException httpClientErrorException;
	
	
	/** The http client error exception. */
	@Mock
	private HttpServerErrorException httpServerErrorException;

	/** The exception handler impl. */
	@InjectMocks
	private DynamicClientRestClientHandler exceptionHandlerImpl = new DynamicClientRestClientHandler();

	@InjectMocks
	LoggerAttribute loggerAttribute;
	
	@Mock
	DynamicAspectUtils aspectutils;
	
	@Before
	public void setup(){
		loggerAttribute= new LoggerAttribute();
		loggerAttribute.setOperationName("test");
		ReflectionTestUtils.setField(exceptionHandlerImpl, "loggerAttribute", loggerAttribute);
	}
	
	/**
	 * Test resource access exception.
	 */
	@Test(expected = ClientRegistrationException.class)
	public void testResourceAccessException() {
		resourceAccessException = new ResourceAccessException("Connetion Time Out");
		exceptionHandlerImpl.handleException(resourceAccessException);
	}

	/**
	 * Test handle exception with client error.
	 */
	@Test(expected = ClientRegistrationException.class)
	public void testHandleExceptionWithClientError() {
		ErrorInfo info = new ErrorInfo("9018", "message", HttpStatus.BAD_REQUEST.toString(),"400");
		byte[] responseBody = JSONUtilities.getJSONOutPutFromObject(info).getBytes();
		httpClientErrorException = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Bad Request", responseBody,
				Charset.forName("UTF-8"));
		exceptionHandlerImpl.handleException(httpClientErrorException);
	}
	
	/**
	 * Test handle exception with server error.
	 */
	@Test(expected = ClientRegistrationException.class)
	public void testHandleExceptionWithServerError() {
		ErrorInfo info = new ErrorInfo("9018", "message", HttpStatus.BAD_REQUEST.toString(),"400");
		byte[] responseBody = JSONUtilities.getJSONOutPutFromObject(info).getBytes();
		httpServerErrorException = new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Bad Request", responseBody,
				Charset.forName("UTF-8"));
		exceptionHandlerImpl.handleException(httpServerErrorException);
	}
	
	/**
	 * Test handle exception with server error with response body as null.
	 */
	@Test(expected = ClientRegistrationException.class)
	public void testServerErrorWithResBodyasNull() {
		httpServerErrorException = new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Bad Request", null,
				Charset.forName("UTF-8"));
		exceptionHandlerImpl.handleException(httpServerErrorException);
	}
	
	/**
	 * Test handle exception with client error.
	 */
	@Test(expected = ClientRegistrationException.class)
	public void testClientErrorWithResBodyasNull() {
		httpClientErrorException = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Bad Request", null,
				Charset.forName("UTF-8"));
		exceptionHandlerImpl.handleException(httpClientErrorException);
	}
}
