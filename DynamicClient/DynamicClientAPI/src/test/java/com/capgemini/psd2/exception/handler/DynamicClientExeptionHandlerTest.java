package com.capgemini.psd2.exception.handler;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;

import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class DynamicClientExeptionHandlerTest {
	
/** The web request. */
@Mock
private WebRequest webRequest;

/** The http client error exception. */
@Mock
private HttpClientErrorException httpClientErrorException;

/** The psd 2 exception. */
@Mock
private PSD2Exception psd2Exception;

@Mock
private ClientRegistrationException clientRegistrationException;

/** The handler. */
@InjectMocks
private DynamicClientExeptionHandler dynamicHandler;

/**
 * Sets the up.
 */
@Before
public void setUp() {
	MockitoAnnotations.initMocks(this);
	when(httpClientErrorException.getStatusCode()).thenReturn(HttpStatus.ACCEPTED);
}

/**
 * Test.
 */
@Test
public void test() {
	dynamicHandler.handleInvalidRequest(httpClientErrorException, webRequest);
	
	ErrorInfo errorInfo = new ErrorInfo("9018","Test","testexception","400");
	Mockito.when(clientRegistrationException.getErrorInfo()).thenReturn(errorInfo);
	Mockito.when(psd2Exception.getErrorInfo()).thenReturn(errorInfo);
	dynamicHandler.handleInvalidRequest(clientRegistrationException, webRequest);
	dynamicHandler.handleInvalidRequest(psd2Exception, webRequest);
	dynamicHandler.handleInvalidRequest(new Exception(), webRequest);
	
	dynamicHandler.handleInvalidRequest(null, webRequest);
}

}
