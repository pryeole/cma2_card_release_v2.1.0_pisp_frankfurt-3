package com.capgemini.psd2.account.product.routing.adapter.test.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.product.routing.adapter.impl.AccountProductRoutingAdapter;
import com.capgemini.psd2.account.product.routing.adapter.routing.AccountProductAdapterFactory;
import com.capgemini.psd2.account.product.routing.adapter.test.adapter.AccountProductTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.Token;

public class AccountProductRoutingAdapterTest 
{
	//** The account products adapter factory. *//*
	@Mock
	private AccountProductAdapterFactory accountProductAdapterFactory;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	//** The account products routing adapter. *//*
	@InjectMocks
	private AccountProductsAdapter accountProductRoutingAdapter = new AccountProductRoutingAdapter();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Test
	public void retrieveAccountProductTest() {

		AccountProductsAdapter accountProductsAdapter = new AccountProductTestRoutingAdapter();
		Token token = new Token();
		Map<String, String> map  = new HashMap<>();
		token.setSeviceParams(map);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		Mockito.when(accountProductAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountProductsAdapter);
		
		PlatformAccountProductsResponse productGETResponse = accountProductRoutingAdapter.retrieveAccountProducts(null,
				null);

		assertTrue(productGETResponse.getObReadProduct2().getData().getProduct().isEmpty());

	}


}
