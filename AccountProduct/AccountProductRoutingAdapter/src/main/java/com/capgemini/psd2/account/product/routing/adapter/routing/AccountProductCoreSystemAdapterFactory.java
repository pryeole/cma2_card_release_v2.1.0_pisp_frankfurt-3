package com.capgemini.psd2.account.product.routing.adapter.routing;


import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;


@Component
public class AccountProductCoreSystemAdapterFactory implements ApplicationContextAware, AccountProductAdapterFactory{

	
	/** The application context. */
	private ApplicationContext applicationContext;

	@Override
	public AccountProductsAdapter getAdapterInstance(String adapterName) {
		return (AccountProductsAdapter) applicationContext.getBean(adapterName);
		
	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
		
	}
}
