package com.capgemini.psd2.mock.foundationservice.account.transactions.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.mock.foundationservice.account.transactions.raml.domain.Transaction;
import com.capgemini.psd2.mock.foundationservice.account.transactions.raml.domain.TransactionList;
import com.capgemini.psd2.mock.foundationservice.account.transactions.repository.AccountTransactionsRepository;
import com.capgemini.psd2.mock.foundationservice.account.transactions.repository.CreditCardTransactionsRepository;
import com.capgemini.psd2.mock.foundationservice.account.transactions.service.AccountTransactionsService;

@Service
public class AccountTransactionsServiceImpl implements AccountTransactionsService {

	@Autowired
	private AccountTransactionsRepository repository;

	@Autowired
	private CreditCardTransactionsRepository ccrepository;

	@Value("${foundationservice.pageSize}")
	private int defaultPageSize;

	public TransactionList retrieveMuleJsonTransactions(String accountclassifier, String accountId,
			String FromBookingDateTime, String ToBookingDateTime, String txnType, String pageNumber, String pageSize)
			throws Exception {
		System.out.println("Before Repo Call: ");
		
		TransactionList accnt = repository.findByAccountId(accountId);
		System.out.println("After Repo Call accnt: "+accnt);
		if (accnt == null) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.NO_TRANSACTIONS_FOUND_PAC_TXN);
		}
		if (accnt.getTransactionsList().isEmpty())
			return accnt;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date FromBookingDate = null;
		Date ToBookingDate = null;
		try {
			FromBookingDate = formatter.parse(FromBookingDateTime);
			ToBookingDate = formatter.parse(ToBookingDateTime);
		} catch (ParseException e) {
			System.out.println(e);
		}

		TransactionList response = new TransactionList();
		response.setTotal(accnt.getTotal());
		List<Transaction> groupByDateList = new ArrayList<Transaction>();
		for (Transaction baseType : accnt.getTransactionsList()) {
			Date postedDate = formatter.parse(baseType.getRunDate());
			if ((postedDate.compareTo(FromBookingDate) == 0 || postedDate.after(FromBookingDate))
					&& (postedDate.compareTo(ToBookingDate) == 0 || postedDate.before(ToBookingDate))) {
				groupByDateList.add(baseType);
			}
		}
		if(groupByDateList.isEmpty() || groupByDateList.equals(null) )
				{
						accnt.getTransactionsList().clear();
						return accnt;
					}
		System.out.println("accnt.getTransactionsList().clear();"+accnt.getTransactionsList()+"groupByDateList: "+groupByDateList);
		accnt.getTransactionsList().clear();
		for (Transaction baseType : groupByDateList) {
			accnt.addTransactionsListItem(baseType);
		}
		Collections.sort(accnt.getTransactionsList(), new Comparator<Transaction>() {
			public int compare(Transaction o1, Transaction o2) {
				return o2.getRunDate().compareTo(o1.getRunDate());
			}
		});

		if (!(("ALL".equalsIgnoreCase(txnType) || "".equalsIgnoreCase(txnType) || txnType == null))) {
			String TransactionFilterValue = null;
			if ("CR".equalsIgnoreCase(txnType)) {
				TransactionFilterValue = "CR";
			} else if ("DR".equalsIgnoreCase(txnType)) {
				TransactionFilterValue = "DR";
			}
			List<Transaction> groupByDateListFiltered = new ArrayList<Transaction>();
			for (Transaction groupByDateTemp : accnt.getTransactionsList()) {
				if (groupByDateTemp.getCreditDebitEventCode().toString().equalsIgnoreCase(TransactionFilterValue)) {
					groupByDateListFiltered.add(groupByDateTemp);
				}
			}
			accnt.getTransactionsList().clear();
			for (Transaction baseType : groupByDateListFiltered) {
				accnt.addTransactionsListItem(baseType);
			}
		}

		System.out.println(accnt);

		/* Applying Pagination */

		List<Transaction> noOfTransactions = new ArrayList<Transaction>();

		int count = 0;
		int totalNoOfPages = 0;
		int tranxMissed = 0;

		for (Transaction runDate : accnt.getTransactionsList()) {
			noOfTransactions.add(count, runDate);
			count++;
		}
		int pageSizeInt = 0;

		if (pageSize != null && !pageSize.isEmpty() && !"null".equalsIgnoreCase(pageSize)
				&& Integer.parseInt(pageSize) > 0) {
			pageSizeInt = Integer.parseInt(pageSize);
		} else {
			pageSizeInt = defaultPageSize;
		}
		totalNoOfPages = (count / pageSizeInt);
		tranxMissed = (count % pageSizeInt);
		if (tranxMissed > 0) {
			totalNoOfPages = totalNoOfPages + 1;
		}

		/*
		 * int offsetValue = Integer.parseInt(offset); int limit = pageSizeInt;
		 * int pageNumber = ((offsetValue/limit)+1);
		 */

		int startTrnx = pageSizeInt * Integer.parseInt(pageNumber) - pageSizeInt;
		int endTranx = pageSizeInt * Integer.parseInt(pageNumber);
		System.out.println("totalNoOfPages: "+totalNoOfPages+"Integer.parseInt(pageNumber): "+Integer.parseInt(pageNumber));
		if (totalNoOfPages < Integer.parseInt(pageNumber))
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.NO_TRANSACTIONS_FOUND_PAC_TXN);
		int tranxCount = 0;
		List<Transaction> runDateListFiltered = new ArrayList<Transaction>();
		for (Transaction runDateTemp : accnt.getTransactionsList()) {
			if (startTrnx <= tranxCount && tranxCount < endTranx && tranxCount < count) {
				runDateListFiltered.add(runDateTemp);
			}
			tranxCount++;
		}
		accnt.getTransactionsList().clear();
		for (Transaction runDate : runDateListFiltered) {
			if (runDate != null)
				accnt.addTransactionsListItem(runDate);
		}
		accnt.setPageNumber(Integer.parseInt(pageNumber));
		if (totalNoOfPages <= Integer.parseInt(pageNumber)) {
			accnt.hasMoreRecords(false);
		}
		/* End pagination */

		return accnt;
	}

	public TransactionList reteriveCreditCardTransaction(String customerReference, String accountId,
			String FromBookingDateTime, String ToBookingDateTime, String txnType) throws Exception {
		TransactionList accnt = ccrepository.findByAccountId(accountId);
		if (accnt == null) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.NO_TRANSACTIONS_FOUND_PAC_TXN);
		}
		if (accnt.getTransactionsList().isEmpty())
			return accnt;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date FromBookingDate = null;
		Date ToBookingDate = null;
		try {
			FromBookingDate = formatter.parse(FromBookingDateTime);
			ToBookingDate = formatter.parse(ToBookingDateTime);
		} catch (ParseException e) {
			System.out.println(e);
		}

		TransactionList response = new TransactionList();
		response.setTotal(accnt.getTotal());
		List<Transaction> groupByDateList = new ArrayList<Transaction>();
		for (Transaction baseType : accnt.getTransactionsList()) {
			Date postedDate = formatter.parse(baseType.getRunDate());
			if ((postedDate.compareTo(FromBookingDate) == 0 || postedDate.after(FromBookingDate))
					&& (postedDate.compareTo(ToBookingDate) == 0 || postedDate.before(ToBookingDate))) {
				groupByDateList.add(baseType);
			}
		}
		if(groupByDateList.isEmpty() || groupByDateList.equals(null) )
		{
				accnt.getTransactionsList().clear();
				return accnt;
			}
		accnt.getTransactionsList().clear();
		for (Transaction baseType : groupByDateList) {
			accnt.addTransactionsListItem(baseType);
		}
		Collections.sort(accnt.getTransactionsList(), new Comparator<Transaction>() {
			public int compare(Transaction o1, Transaction o2) {
				return o2.getRunDate().compareTo(o1.getRunDate());
			}
		});

		if (!(("ALL".equalsIgnoreCase(txnType) || "".equalsIgnoreCase(txnType) || txnType == null))) {
			String TransactionFilterValue = null;
			if ("CR".equalsIgnoreCase(txnType)) {
				TransactionFilterValue = "CR";
			} else if ("DR".equalsIgnoreCase(txnType)) {
				TransactionFilterValue = "DR";
			}
			List<Transaction> groupByDateListFiltered = new ArrayList<Transaction>();
			for (Transaction groupByDateTemp : accnt.getTransactionsList()) {
				if (groupByDateTemp.getCreditDebitEventCode().toString().equalsIgnoreCase(TransactionFilterValue)) {
					groupByDateListFiltered.add(groupByDateTemp);
				}
			}
			accnt.getTransactionsList().clear();
			for (Transaction baseType : groupByDateListFiltered) {
				accnt.addTransactionsListItem(baseType);
			}
		}
		/* Applying Pagination */

		/*
		 * List<Transaction> noOfTransactions = new ArrayList<Transaction>();
		 * 
		 * int count = 0; int totalNoOfPages = 0; int tranxMissed = 0;
		 * 
		 * for(Transaction runDate : accnt.getTransactionsList()) {
		 * noOfTransactions.add(count,runDate); count++; } int pageSizeInt = 0;
		 * 
		 * if(pageSize != null && !pageSize.isEmpty() &&
		 * !"null".equalsIgnoreCase(pageSize) && Integer.parseInt(pageSize)>0) {
		 * pageSizeInt = Integer.parseInt(pageSize); }else{ pageSizeInt =
		 * defaultPageSize; } totalNoOfPages = (count/pageSizeInt); tranxMissed
		 * = (count%pageSizeInt); if(tranxMissed>0) { totalNoOfPages =
		 * totalNoOfPages + 1; }
		 * 
		 * int startTrnx = pageSizeInt*Integer.parseInt(pageNumber)-pageSizeInt;
		 * int endTranx = pageSizeInt*Integer.parseInt(pageNumber);
		 * 
		 * if(totalNoOfPages < (Integer.parseInt(pageNumber))) throw
		 * MockFoundationServiceException.populateMockFoundationServiceException
		 * (ErrorCodeEnum.NO_TRANSACTIONS_FOUND_PAC_TXN); int tranxCount = 0;
		 * List<Transaction> runDateListFiltered = new ArrayList<Transaction>();
		 * for(Transaction runDateTemp : accnt.getTransactionsList()) {
		 * if(startTrnx <= tranxCount && tranxCount< endTranx &&
		 * tranxCount<count) { runDateListFiltered.add(runDateTemp); }
		 * tranxCount++; } accnt.getTransactionsList().clear(); for(Transaction
		 * runDate : runDateListFiltered) { if(runDate != null)
		 * accnt.addTransactionsListItem(runDate); }
		 * accnt.setPageNumber(Integer.parseInt(pageNumber)); if(totalNoOfPages
		 * <= (Integer.parseInt(pageNumber))){ accnt.hasMoreRecords(false); }
		 */
		/* End pagination */

		return accnt;
	}
}
