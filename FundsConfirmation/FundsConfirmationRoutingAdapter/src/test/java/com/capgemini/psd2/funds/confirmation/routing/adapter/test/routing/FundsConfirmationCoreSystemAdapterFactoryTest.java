package com.capgemini.psd2.funds.confirmation.routing.adapter.test.routing;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationAdapter;
import com.capgemini.psd2.cisp.mock.domain.MockFundData;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.funds.confirmation.routing.adapter.routing.FundsConfirmationCoreSystemAdapterFactory;

public class FundsConfirmationCoreSystemAdapterFactoryTest {

	@Mock
	private ApplicationContext context;

	/** The account information core system adapter factory. */
	@InjectMocks
	private FundsConfirmationCoreSystemAdapterFactory obj;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetAdapterInstance() {
		FundsConfirmationAdapter adapter = new FundsConfirmationAdapter() {

			public MockFundData getFundsData(AccountDetails accountDetails) {
				return null;
			}

			@Override
			public OBFundsConfirmationResponse1 getFundsData(AccountDetails accountDetails,
					OBFundsConfirmation1 fundsConfirmationPOSTRequest, Map<String, String> params) {
				return null;
			}
		};
		obj.setApplicationContext(context);
		when(context.getBean(anyString())).thenReturn(null);
		obj.getAdapterInstance("abc");
		assertNotNull(adapter);
	}
}
