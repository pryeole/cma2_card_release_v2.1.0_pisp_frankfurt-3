package com.capgemini.psd2.funds.confirmation.service;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;

public interface FundsConfirmationService {

	public OBFundsConfirmationResponse1 createFundsConfirmation(OBFundsConfirmation1 fundsConfirmationPOSTRequest);
}
