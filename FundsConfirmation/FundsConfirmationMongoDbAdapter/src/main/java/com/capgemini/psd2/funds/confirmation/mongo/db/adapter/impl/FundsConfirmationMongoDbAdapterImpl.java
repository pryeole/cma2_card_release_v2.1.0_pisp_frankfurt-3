package com.capgemini.psd2.funds.confirmation.mongo.db.adapter.impl;

import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationDataResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

public class FundsConfirmationMongoDbAdapterImpl implements FundsConfirmationAdapter {
              
	@Autowired
	private SandboxValidationUtility utility;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(FundsConfirmationMongoDbAdapterImpl.class);

	@Override
	public OBFundsConfirmationResponse1 getFundsData(AccountDetails accountDetails,
			OBFundsConfirmation1 fundsConfirmationPOSTRequest, Map<String, String> params) {
		Boolean fundsAvailable = Boolean.TRUE;
		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));
		
		 OBFundsConfirmationResponse1 fundsConfirmationPOSTResponse = JSONUtilities.getObjectFromJSONString(
				JSONUtilities.getJSONOutPutFromObject(fundsConfirmationPOSTRequest),
				OBFundsConfirmationResponse1.class);
		
		if (utility.isValidAmount(fundsConfirmationPOSTResponse.getData().getInstructedAmount().getAmount(),
				PSD2Constants.PROCESS_EXEC_INCOMPLETE)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}

		if (!utility.isValidCurrency(fundsConfirmationPOSTResponse.getData().getInstructedAmount().getCurrency())
				|| utility.isValidAmount(fundsConfirmationPOSTResponse.getData().getInstructedAmount().getAmount(),
						PSD2Constants.REJECTED_CONDITION)) {
			fundsAvailable = Boolean.FALSE;
		}

		OBFundsConfirmationDataResponse1 fundConfirmationData = fundsConfirmationPOSTResponse.getData();
		fundConfirmationData.setFundsAvailable(fundsAvailable);
		fundConfirmationData.setFundsConfirmationId(UUID.randomUUID().toString());
		fundConfirmationData.setCreationDateTime(DateUtilites.getCurrentDateInISOFormat());

		return fundsConfirmationPOSTResponse;
	}

}
