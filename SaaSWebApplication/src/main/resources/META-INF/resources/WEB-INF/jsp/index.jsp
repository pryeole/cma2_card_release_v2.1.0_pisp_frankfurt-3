<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE html>
<%@ page session="false" %>
<html lang="en" ng-app="loginApp" ng-cloak>
<head>
<meta charset="utf-8">
<base href="/${applicationName}<%=request.getContextPath() %>/">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="b365ScaBuildVersion" content="${b365ScaBuildVersion}" />
<meta name="cdnUrl" content="${cdnBaseURL}" />
<title>Login - Bank of Ireland</title>

<link rel="stylesheet" href="${cdnBaseURL}/sca-ui/${buildVersion}/fonts/open-sans/Bold/OpenSans-Bold.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/sca-ui/${buildVersion}/fonts/open-sans/Italic/OpenSans-Italic.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/sca-ui/${buildVersion}/fonts/open-sans/Light/OpenSans-Light.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/sca-ui/${buildVersion}/fonts/open-sans/Regular/OpenSans-Regular.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/sca-ui/${buildVersion}/fonts/open-sans/Semibold/OpenSans-Semibold.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/sca-ui/${buildVersion}/fonts/font-awesome/fonts/fontawesome-webfont.css" media="none" onload="this.media='all';">

	<c:choose>
		<c:when test="${channel_id == 'bol'}">
			<link rel="stylesheet" type="text/css" href="${cdnBaseURL}/sca-ui/${buildVersion}/css/common.min.css" />
			<link rel="stylesheet" type="text/css" ng-href="${cdnBaseURL}/sca-ui/${buildVersion}/css/boi-sca-login.css" />
 		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${newB365SCAFlow == 'true'}">
					<link rel="stylesheet" type="text/css" href="${cdnBaseURL}/sca-ui/${b365ScaBuildVersion}/static/css/main.css" />
				</c:when>
				<c:otherwise>
					<link rel="stylesheet" type="text/css" href="${cdnBaseURL}/sca-ui/${buildVersion}/css/common.min.css" />
					<link rel="stylesheet" type="text/css" ng-href="${cdnBaseURL}/sca-ui/${buildVersion}/css/bank-of-ireland.css" />
				</c:otherwise>
			</c:choose>			
		</c:otherwise>
	</c:choose>
<link rel="shortcut icon" type="image/x-icon" href="${cdnBaseURL}/sca-ui/${buildVersion}/img/favicon.ico?rev=16" />

<noscript>${JAVASCRIPT_ENABLE_MSG}</noscript>

<%-- <script   src="${cdnBaseURL}/sca-ui/${buildVersion}/js/libs.min.js"></script>
<script  src="${cdnBaseURL}/sca-ui/${buildVersion}/js/templates.js"></script>
<script   src="${cdnBaseURL}/sca-ui/${buildVersion}/js/app.js"></script>
 --%>
 
<script  crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/${b365ScaBuildVersion}/js/connect.js"></script>
<script  crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/${b365ScaBuildVersion}/js/modernizr.js"></script>
<script  crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/${b365ScaBuildVersion}/js/connect_hybrid.js"></script>
<script  crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/${b365ScaBuildVersion}/js/cordova/cordova_loader.js"></script>


</head>

<body class="next-gen-home" block-ui="main" >
 <div class="page-container">
	   
   		<div ui-view autoscroll="false"></div>
		<div class="content" ui-view="modalContainer" autoscroll="false"></div>
		<div id="root"></div>
		
		<!-- <div ui-view="main" class="main-container" block-ui="main" autoscroll="false"></div> -->
		</div>
		<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
			<input type="hidden" id="sec-error" name="secError" value="${fn:escapeXml(SPRING_SECURITY_LAST_EXCEPTION.errorInfo)}">
		</c:if>
		<c:if test="${not empty exception}">				
			<input type="hidden" id="error" name="error" value="${fn:escapeXml(exception)}">
		</c:if>
        <input type="hidden" id="resumePath" name="resumePath" value="${not empty redirectUri ? redirectUri : param.resumePath}"/>
        <input type="hidden" id="serverErrorFlag" name="serverErrorFlag" value="${serverErrorFlag}"/>
		<input type="hidden" id="correlationId" name="correlationId" value="${empty correlationId ? param.correlationId : correlationId}"/>
		<input type="hidden" id="staticContent" name="staticContent" value="${fn:escapeXml(UIContent)}"/>
		<input type="hidden" id="buildversion" name="buildversion" value="${buildVersion}">
		<input type="hidden" id="b365ScaBuildVersion" name="b365ScaBuildVersion" value="${b365ScaBuildVersion}">
		<input type="hidden" id="flowType" name="flowType" value="${flowType}" />
<%-- 		<input type="hidden" id="allowInteraction" name="allowInteraction" value="${param.allowInteraction}"/>
		<input type="hidden" id="scope" name="scope" value="${param.scope}"/>
		<input type="hidden" id="connectionId" name="connectionId" value="${param.connectionId}"/>
		<input type="hidden" id="response_type" name="response_type" value="${param.response_type}"/>
		<input type="hidden" id="state" name="state" value="${param.state}"/>
 		<input type="hidden" id="REF" name="REF" value="${param.REF}"/>
		<input type="hidden" id="nonce" name="nonce" value="${param.nonce}"/>
		<input type="hidden" id="reauth" name="reauth" value="${param.reauth}"/>
 --%>		

		<%-- <input type="hidden" id="redirectUri" name="redirectUri" value="${redirectUri}"/>	   
 --%>		
<%-- 		<c:if test="${not empty param.oAuthUrl}">
			<input type="hidden" name="saasurl" id="saasurl" value="<%=javax.servlet.http.HttpUtils.getRequestURL(request).append("?resumePath=").append(request.getParameter("resumePath"))%>"/>
		</c:if>
 --%>		
 		<input type="hidden" id="brandId" name="brandId" value=""/>
		<input type = "hidden" id="applicationName" name="applicationName" value= "${applicationName}<%=request.getContextPath() %>"/>
		
		<input type="hidden" id="channelId" name="channelId" value="${channel_id}"/>

    	<input id="fraudHdmInfo" name="fraudHdmInfo" value='${fraudHdmInfo}' type="hidden"/>
		<input type="hidden" id="fsHeaders" name="fsHeaders" value='${fsHeaders}'/>
		<input type="hidden" id="cdnBaseURL" name="cdnBaseURL" value="${cdnBaseURL}">
		<c:choose>
		<c:when test="${channel_id == 'bol'}">
			<input type="hidden" id="jsc-file-path" name="jsc-file-path" value="${cdnBaseURL}/sca-ui/${buildVersion}/${jsc_file_path}">
			<script  integrity= "${libsHashCode}"  crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/${buildVersion}/js/libs.min.js"></script>
			<script  integrity="${templateHashCode}" crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/${buildVersion}/js/templates.min.js"></script>
			<script  integrity="${appHashCode}" crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/${buildVersion}/js/app.min.js"></script>
 		</c:when>
		<c:otherwise>		
			<c:choose>
				<c:when test="${newB365SCAFlow == 'true'}">			
					<input type="hidden" id="jsc-file-path" name="jsc-file-path" value="${cdnBaseURL}/sca-ui/${b365ScaBuildVersion}/${jsc_file_path}">					
					<script  crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/${b365ScaBuildVersion}/static/js/runtime~main.js"></script>
					<script  integrity= "${b365ScaHashCode}"  crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/${b365ScaBuildVersion}/static/js/main.js"></script>					
				</c:when>
				<c:otherwise>
					<input type="hidden" id="jsc-file-path" name="jsc-file-path" value="${cdnBaseURL}/sca-ui/${buildVersion}/${jsc_file_path}">
					<script  integrity= "${libsHashCode}"  crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/${buildVersion}/js/libs.min.js"></script>
					<script  integrity="${templateHashCode}" crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/${buildVersion}/js/templates.min.js"></script>
					<script  integrity="${appHashCode}" crossorigin="anonymous" src="${cdnBaseURL}/sca-ui/${buildVersion}/js/app.min.js"></script>
				</c:otherwise>
			</c:choose>			
		</c:otherwise>
	</c:choose>
</body>
		
</html>