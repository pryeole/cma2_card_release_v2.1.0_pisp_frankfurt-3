package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.delegate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import com.capgemini.psd2.adapter.datetime.utility.DateAdapter;
import com.capgemini.psd2.adapter.datetime.utility.StatementDateRange;
import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.datetime.utility.TransactionDateRange;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.Statementsresponse;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.transformer.AccountStatementsFoundationServiceTransformer;
//import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.constants.AccountTransactionsFoundationServiceConstants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;



@Component
public class AccountStatementsFoundationServiceDelegate {

	@Value("${foundationService.correlationIdInReqHeader:#{X-API-TRANSACTION-ID}}")
	private String correlationIdInReqHeader;

	@Value("${foundationService.sourceSystemReqHeader:#{X-API-SOURCE-SYSTEM}}")
	private String sourceSystemReqHeader;

	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;

	@Value("${foundationService.channelInReqHeader:#{X-API-CHANNEL-CODE}}")
	private String channelInReqHeader;

	@Value("${foundationService.maskedPan:#{X-MASKED-PAN}}")
	private String maskedPan;

	@Value("${foundationService.singleAccountStatementsFileBaseURL}")
	private String singleAccountStatementsFileBaseURL;

	@Value("${foundationService.singleAccountStatementsCreditCardFileBaseURL}")
	private String singleAccountStatementsCreditCardFileBaseURL;
	
	@Value("${foundationService.defaultStatementDateRange}")
	private int defaultStatementDateRange;

	@Autowired
	private AccountStatementsFoundationServiceTransformer accountStatementsFSTransformer;
	
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	public HttpHeaders createRequestHeadersFile( AccountMapping accountMapping,
			Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();

		if (!((singleAccountStatementsFileBaseURL.contains("mockservices"))
				|| (singleAccountStatementsCreditCardFileBaseURL.contains("mockservices")))) {
			httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_PDF));
		}
		httpHeaders.add(correlationIdInReqHeader, accountMapping.getCorrelationId());
		httpHeaders.add(sourceSystemReqHeader, AccountStatementsFoundationServiceConstants.SOURCE_SYSTEM_PSD2API);
		httpHeaders.add(sourceUserReqHeader, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeader,
				params.get((AccountStatementsFoundationServiceConstants.CHANNEL_ID)).toUpperCase());

		if (params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE)
				.equals(AccountStatementsFoundationServiceConstants.CREDIT_CARD)) {
			httpHeaders.add(maskedPan, params.get("maskedPan"));
		}
		return httpHeaders;
	}

	public HttpHeaders createRequestHeadersStmt(AccountMapping accountMapping,
			Map<String, String> params) {

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(correlationIdInReqHeader, accountMapping.getCorrelationId());
		httpHeaders.add(sourceSystemReqHeader, AccountStatementsFoundationServiceConstants.SOURCE_SYSTEM_PSD2API);
		httpHeaders.add(sourceUserReqHeader, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeader,
				params.get((AccountStatementsFoundationServiceConstants.CHANNEL_ID)).toUpperCase());
		if (params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE)
				.equals(AccountStatementsFoundationServiceConstants.CREDIT_CARD)) {
			httpHeaders.add(maskedPan, params.get("maskedPan"));
		}
		return httpHeaders;

	}

	public MultiValueMap<String, String> getFromAndToDate(Map<String, String> params) {
		String fromDate = null;
		String toDate = null;
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();

		if (!NullCheckUtils
				.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_DATETIME))
				&& !NullCheckUtils
						.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_TO_DATETIME))) {
			fromDate = params.get(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_DATETIME);
			toDate = params.get(AccountStatementsFoundationServiceConstants.REQUESTED_TO_DATETIME);

		} else if (!NullCheckUtils
				.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME))
				&& !NullCheckUtils.isNullOrEmpty(
						params.get(AccountStatementsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME))) {
			fromDate = params.get(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME);
			toDate = params.get(AccountStatementsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME);

		}

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		DateAdapter dateAdapter = new DateAdapter();
		if (null != fromDate && null != toDate) {
			try {
				fromDate = formatter.format(dateAdapter.parseDate(fromDate));
				toDate = formatter.format(dateAdapter.parseDate(toDate));
				queryParams.add(AccountStatementsFoundationServiceConstants.DATEFROM, fromDate);
				queryParams.add(AccountStatementsFoundationServiceConstants.DATETO, toDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}
		return queryParams;

	}

	public String getFoundationServiceCardURLFile(Map<String, String> params,
			String singleAccountStatementsCreditCardFileBaseURL) {

		String finalURL = null;

		if (NullCheckUtils.isNullOrEmpty(singleAccountStatementsCreditCardFileBaseURL))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = singleAccountStatementsCreditCardFileBaseURL;

		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER)))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = finalURL + "/" + params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER);

		finalURL = finalURL + "/statements";

		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.STATEMENT_ID)))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = finalURL + "/" + params.get(AccountStatementsFoundationServiceConstants.STATEMENT_ID);

		return finalURL + "/file";
	}

	public String getFoundationServiceURLFile(Map<String, String> params, String singleAccountStatementsFileBaseURL) {

		String finalURL = null;

		if (NullCheckUtils.isNullOrEmpty(singleAccountStatementsFileBaseURL))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = singleAccountStatementsFileBaseURL;

		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_NSC)))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = finalURL + "/" + params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_NSC);

		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER)))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = finalURL + "/" + params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER);

		finalURL = finalURL + "/statements";

		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.STATEMENT_ID)))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = finalURL + "/" + params.get(AccountStatementsFoundationServiceConstants.STATEMENT_ID);

		return finalURL + "/file";
	}

	/**
	 * Gets the foundation service URL.
	 *
	 * @param accountNSC
	 *            the account NSC
	 * @param accountNumber
	 *            the account number
	 * @param baseURL
	 *            the base URL
	 * @return the foundation service URL
	 */
	public String getFoundationServiceURL(String accountNSC, String accountNumber, String baseURL) {
		if (NullCheckUtils.isNullOrEmpty(accountNSC)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if (NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if (NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		return baseURL + "/" + accountNSC + "/" + accountNumber + "/" + "statements";
	}

	/**
	 * Gets the foundation service CreditCardURL.
	 *
	 * @param accountNSC
	 *            the account NSC
	 * @param accountNumber
	 *            the account number
	 * @param baseURL
	 *            the base URL
	 * @return the foundation service CreditCardURL
	 */

	public String getFoundationServiceCreditcardURL(String accountNumber, String baseURL) {

		if (NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if (NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		return baseURL + "/" + accountNumber + "/" + "statements";

	}

	public PlatformAccountStatementsResponse transformAccountStatementResponse(Statementsresponse statementsResponse,
			Map<String, String> params) {
		return accountStatementsFSTransformer.transformAccountStatementRes(statementsResponse, params);
	}
	
	
public StatementDateRange createTransactionDateRange(Map<String, String> params) {
		
	StatementDateRange statementDateRange = new StatementDateRange();		
		//Consent Expiry 
		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.CONSENT_EXPIRATION_DATETIME))) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DATE, 1);
			statementDateRange.setConsentExpiryDateTime(calendar.getTime());
		} else {
			statementDateRange.setConsentExpiryDateTime(timeZoneDateTimeAdapter.parseDateTimeFS(params.get(AccountStatementsFoundationServiceConstants.CONSENT_EXPIRATION_DATETIME)));
		}	
		
		
		
		//StatementFromDateTime
	if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME))) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.MONTH, 0);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.YEAR,1900);
			statementDateRange.setTransactionFromDateTime(calendar.getTime());
			statementDateRange.setFilterFromDate(calendar.getTime());
		} else {
			statementDateRange.setTransactionFromDateTime(timeZoneDateTimeAdapter.parseDateFS(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME)));
		}
		//StatementToDateTime
		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME))) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MINUTE, 0);
			statementDateRange.setTransactionToDateTime(calendar.getTime());
		} else {
			statementDateRange.setTransactionToDateTime(timeZoneDateTimeAdapter.parseDateFS(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME)));
		}
		//Request DateTime
		statementDateRange.setRequestDateTime(new Date());
		//FilterFromDate
		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_DATETIME))) {		
			statementDateRange.setFilterFromDate(statementDateRange.getTransactionFromDateTime());
		} else {
			statementDateRange.setFilterFromDate(timeZoneDateTimeAdapter.parseDateFS(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_DATETIME)));
		}
		//FilterToDate
		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_TO_DATETIME))) {
			statementDateRange.setFilterToDate(statementDateRange.getTransactionToDateTime());
		} else {
			statementDateRange.setFilterToDate(timeZoneDateTimeAdapter.parseDateFS(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_TO_DATETIME)));
		}
		//To > ConExp
		if (statementDateRange.getTransactionToDateTime().getTime() > statementDateRange.getConsentExpiryDateTime().getTime()) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			statementDateRange.setTransactionToDateTime(timeZoneDateTimeAdapter.parseDateFS(formatter.format(statementDateRange.getConsentExpiryDateTime())));
		}
		return statementDateRange;
	}
	
	
	//StatementDateRange Validation
	public StatementDateRange fsCallFilter(StatementDateRange statementDateRange) {
		
	
		if (statementDateRange.getRequestDateTime().getTime() > statementDateRange.getConsentExpiryDateTime().getTime()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.CONSENT_EXPIRED);
		}
		
		
		else if (statementDateRange.getFilterFromDate().getTime() > statementDateRange.getFilterToDate().getTime()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.INVALID_FILTER);
		}
		
		else if (statementDateRange.getTransactionFromDateTime().getTime() > statementDateRange.getConsentExpiryDateTime().getTime()
				|| statementDateRange.getTransactionFromDateTime().getTime() > statementDateRange.getFilterToDate().getTime()
				|| statementDateRange.getFilterFromDate().getTime() > statementDateRange.getTransactionToDateTime().getTime()) {
			statementDateRange.setEmptyResponse(true);
		}
		else if (statementDateRange.getTransactionFromDateTime().getTime() <= statementDateRange.getFilterFromDate().getTime()) {
			if (statementDateRange.getFilterToDate().getTime() <= statementDateRange.getTransactionToDateTime().getTime()) {
				statementDateRange.setNewFilterFromDate(statementDateRange.getFilterFromDate());
				statementDateRange.setNewFilterToDate(statementDateRange.getFilterToDate());

			} else {
				statementDateRange.setNewFilterFromDate(statementDateRange.getFilterFromDate());
				statementDateRange.setNewFilterToDate(statementDateRange.getTransactionToDateTime());
			}
		}
		else if (statementDateRange.getFilterToDate().getTime() <= statementDateRange.getTransactionToDateTime().getTime()) {
			statementDateRange.setNewFilterFromDate(statementDateRange.getTransactionFromDateTime());
			statementDateRange.setNewFilterToDate(statementDateRange.getFilterToDate());
		}
		else {
			statementDateRange.setNewFilterFromDate(statementDateRange.getTransactionFromDateTime());
			statementDateRange.setNewFilterToDate(statementDateRange.getTransactionToDateTime());
		}		
		return statementDateRange;		
	}
	
}
	


