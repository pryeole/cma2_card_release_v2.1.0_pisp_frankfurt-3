package com.capgemini.tpp.dtos;

import java.util.List;
import java.util.Map;

import com.capgemini.tpp.ldap.client.model.TPPApplication;

public class DashboardResponse{

	private String indexURL;
	private TPPApplication tppApplication;
	private Map<String, String> samlAttributes;
	private List<String> tppNameList;

	public List<String> getTppNameList() {
		return tppNameList;
	}

	public void setTppNameList(List<String> tppNameList) {
		this.tppNameList = tppNameList;
	}

	public String getIndexURL() {
		return indexURL;
	}

	public void setIndexURL(String indexURL) {
		this.indexURL = indexURL;
	}

	public TPPApplication getTppApplication() {
		return tppApplication;
	}

	public void setTppApplication(TPPApplication tppApplication) {
		this.tppApplication = tppApplication;
	}

	public Map<String, String> getSamlAttributes() {
		return samlAttributes;
	}

	public void setSamlAttributes(Map<String, String> samlAttributes) {
		this.samlAttributes = samlAttributes;
	}

}