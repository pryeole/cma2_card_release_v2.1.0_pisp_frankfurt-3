
package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cs_auth"
})
public class Properties_ implements Serializable
{

    @JsonProperty("cs_auth")
    private CsAuth csAuth;
    private static final long serialVersionUID = 8056346173303389550L;

    @JsonProperty("cs_auth")
    public CsAuth getCsAuth() {
        return csAuth;
    }

    @JsonProperty("cs_auth")
    public void setCsAuth(CsAuth csAuth) {
        this.csAuth = csAuth;
    }

}
