package com.capgemini.tpp.muleapi.schema;

import java.util.List;

public class Api {
	private Audit audit;
	private String masterOrganizationId;
	private String organizationId;
	private int id;
	private String name;
	private String exchangeAssetName;
	private Object groupId;
	private Object assetId;
	private Permissions permissions;
	private List<Version> versions;

	public Audit getAudit() {
		return this.audit;
	}

	public void setAudit(Audit audit) {
		this.audit = audit;
	}

	public String getMasterOrganizationId() {
		return this.masterOrganizationId;
	}

	public void setMasterOrganizationId(String masterOrganizationId) {
		this.masterOrganizationId = masterOrganizationId;
	}

	public String getOrganizationId() {
		return this.organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExchangeAssetName() {
		return this.exchangeAssetName;
	}

	public void setExchangeAssetName(String exchangeAssetName) {
		this.exchangeAssetName = exchangeAssetName;
	}

	public Object getGroupId() {
		return this.groupId;
	}

	public void setGroupId(Object groupId) {
		this.groupId = groupId;
	}

	public Object getAssetId() {
		return this.assetId;
	}

	public void setAssetId(Object assetId) {
		this.assetId = assetId;
	}

	public Permissions getPermissions() {
		return this.permissions;
	}

	public void setPermissions(Permissions permissions) {
		this.permissions = permissions;
	}

	public List<Version> getVersions() {
		return versions;
	}

	public void setVersions(List<Version> versions) {
		this.versions = versions;
	}



}
