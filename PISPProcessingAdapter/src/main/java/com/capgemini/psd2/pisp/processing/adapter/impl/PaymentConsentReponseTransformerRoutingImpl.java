package com.capgemini.psd2.pisp.processing.adapter.impl;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsTransformer;

@Lazy@Component("responseTransformRoutingBean")
public class PaymentConsentReponseTransformerRoutingImpl<T,U> implements PaymentConsentsTransformer<T,U>,ApplicationContextAware{

	private PaymentConsentsTransformer<T,U> beanInstance;
	
	@Value("${app.reponseTransformerBean:null}")
	private String beanName;
	
	private ApplicationContext applicationContext;
	
	@SuppressWarnings("unchecked")
	public T paymentConsentsResponseTransformer(T paymentConsentsResponse,
			PaymentConsentsPlatformResource paymentSetupPlatformResource, String methodType) {
		return (T) getPaymentConsentsValidatorInstance(beanName).paymentConsentsResponseTransformer(paymentConsentsResponse, paymentSetupPlatformResource, methodType);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public PaymentConsentsTransformer getPaymentConsentsValidatorInstance(String beanName) {
		if (beanInstance == null)
			beanInstance= (PaymentConsentsTransformer<T,U>) applicationContext.getBean(beanName);
		return beanInstance;
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@SuppressWarnings("unchecked")
	@Override
	public U paymentConsentRequestTransformer(U paymentConsentRequest) {
		return (U)getPaymentConsentsValidatorInstance(beanName).paymentConsentRequestTransformer(paymentConsentRequest);
	}

}
