package com.capgemini.psd2.pisp.processing.adapter.impl;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionCustomValidator;

@Component("paymentsValidatorRoutingBean")
public class PaymentSubmissionValidatorRoutingImpl<T, V>
		implements PaymentSubmissionCustomValidator<T, V>, ApplicationContextAware {

	private PaymentSubmissionCustomValidator<T, V> beanInstance;

	@Value("${app.customValidationBean:null}")
	private String beanName;

	private ApplicationContext applicationContext;

	@SuppressWarnings("unchecked")
	public PaymentSubmissionCustomValidator<T, V> getValidatorInstance(String beanName) {
		if (beanInstance == null)
			beanInstance = (PaymentSubmissionCustomValidator<T, V>) applicationContext.getBean(beanName);
		return beanInstance;
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public void validatePaymentsPOSTRequest(T submissionRequestBody) {
		getValidatorInstance(beanName).validatePaymentsPOSTRequest(submissionRequestBody);

	}

	@Override
	public void validatePaymentsResponse(V submissionResponse) {
		getValidatorInstance(beanName).validatePaymentsResponse(submissionResponse);

	}
}
