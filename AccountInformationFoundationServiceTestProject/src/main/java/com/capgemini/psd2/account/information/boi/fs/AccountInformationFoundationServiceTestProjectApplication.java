package com.capgemini.psd2.account.information.boi.fs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.AccountInformationFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accounts;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class AccountInformationFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountInformationFoundationServiceTestProjectApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestAccountInformationFSController{
	
	@Autowired
	AccountInformationFoundationServiceAdapter adapter;
	
	@RequestMapping("/testAccountInformation")
	public PlatformAccountInformationResponse getResponse(){
 		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accounts.setAccount(accnt);	
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("301027");
		accDet.setAccountNumber("50091131");
		accDetList.add(accDet);

		
		AccountDetails accDet1 = new AccountDetails();
		accDet1.setAccountId("56789");
		accDet1.setAccountNSC("902047");
		accDet1.setAccountNumber("69984343");
		accDetList.add(accDet1);

		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI999");
		accountMapping.setCorrelationId("23456778");
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "BOL");
	    params.put("consentFlowType", "AISP");
	    params.put("x-channel-id", "BOL");
	    params.put("x-user-id", "BOI999");
	    params.put("X-BOI-PLATFORM", "platform");
	    params.put("x-fapi-interaction-id", "12345678"); 
		return adapter.retrieveMultipleAccountsInformation(accountMapping, params);
		//return adapter.retrieveAccountInformation(accountMapping, params);
	}
}
