package com.capgemini.psd2.pisp.payment.submission.platform.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.payment.submission.platform.repository.PaymentsPlatformRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.mongodb.MongoWriteException;

@Component
public class PaymentsPlatformAdapterImpl implements PaymentsPlatformAdapter {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PaymentsPlatformRepository paymentsPlatformRepository;

	@Autowired
	private CompatibleVersionList compatibleVersionList;

	@Override
	public PaymentsPlatformResource createInitialPaymentsPlatformResource(
			CustomPaymentStageIdentifiers caymentStageIdentifiers, String setupCreationDateTime) {
		PaymentsPlatformResource submissionApiResource = new PaymentsPlatformResource();
		submissionApiResource.setPaymentConsentId(caymentStageIdentifiers.getPaymentConsentId());
		submissionApiResource.setCreatedAt(setupCreationDateTime);
		submissionApiResource.setIdempotencyRequest(Boolean.TRUE.toString());
		submissionApiResource.setPaymentType(caymentStageIdentifiers.getPaymentTypeEnum().getPaymentType());
		submissionApiResource.setSubmissionCmaVersion(caymentStageIdentifiers.getPaymentSetupVersion());
		submissionApiResource.setTppCID(reqHeaderAtrributes.getTppCID());
		submissionApiResource.setIdempotencyKey(reqHeaderAtrributes.getIdempotencyKey());
		submissionApiResource.setProccessState(PaymentConstants.INCOMPLETE);
		try {
			return paymentsPlatformRepository.save(submissionApiResource);
		} catch (DataAccessResourceFailureException | MongoWriteException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public PaymentsPlatformResource retrievePaymentsPlatformResource(String paymentSubmissionId) {

		try {
			return paymentsPlatformRepository.findOneByPaymentSubmissionIdOrSubmissionIdAndSubmissionCmaVersionIn(
					paymentSubmissionId, paymentSubmissionId, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public PaymentsPlatformResource getIdempotentPaymentsPlatformResource(long idempotencyDuration,
			String paymentType) {
		try {
			return paymentsPlatformRepository
					.findOneByTppCIDAndPaymentTypeAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThanAndSubmissionCmaVersionIn(
							reqHeaderAtrributes.getTppCID(), paymentType, reqHeaderAtrributes.getIdempotencyKey(),
							String.valueOf(Boolean.TRUE),
							DateUtilites
									.formatMilisecondsToISODateFormat(System.currentTimeMillis() - idempotencyDuration),
							compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}

	}

	@Override
	public PaymentsPlatformResource retrievePaymentsResourceByConsentId(String paymentConsentId) {
		try {
			return paymentsPlatformRepository.findOneByPaymentConsentIdAndSubmissionCmaVersionIn(paymentConsentId,
					compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public PaymentsPlatformResource updatePaymentsPlatformResource(
			PaymentsPlatformResource submissionPlatformResource) {
		try {
			submissionPlatformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
			return paymentsPlatformRepository.save(submissionPlatformResource);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public void createInvalidPaymentSubmissionPlatformResource(PaymentsPlatformResource paymentsPlatformResource,
			PaymentResponseInfo validationDetails, String currentDateInISOFormat) {
		if (!NullCheckUtils.isNullOrEmpty(validationDetails.getPaymentConsentId())
				&& retrievePaymentsPlatformResource(validationDetails.getPaymentConsentId(),
						validationDetails.getPaymentConsentId()) != null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_DUPLICATE_PAYMENT_ID));

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();

		paymentSetupPlatformResource.setCreatedAt(currentDateInISOFormat);
		paymentSetupPlatformResource.setUpdatedAt(currentDateInISOFormat);
		paymentSetupPlatformResource.setStatusUpdateDateTime(currentDateInISOFormat);

		/*
		 * cma3 pisp status changed from PENDING and ACCEPTEDTECHNICALVALIDATION
		 * to AWAITINGAUTHORISATION. ValidationStatus can be either
		 * AWAITINGAUTHORISATION or REJECTED
		 */
		paymentSetupPlatformResource.setPaymentConsentId(validationDetails.getPaymentConsentId());
		paymentSetupPlatformResource.setStatus(validationDetails.getConsentValidationStatus().toString());
		paymentSetupPlatformResource.setIdempotencyRequest(validationDetails.getIdempotencyRequest());

		paymentSetupPlatformResource.setTppCID(reqHeaderAtrributes.getTppCID());
		paymentSetupPlatformResource.setIdempotencyKey(reqHeaderAtrributes.getIdempotencyKey());
		paymentSetupPlatformResource
				.setTppLegalEntityName(reqHeaderAtrributes.getToken().getTppInformation().getTppLegalEntityName());

		try {
			paymentsPlatformRepository.save(paymentsPlatformResource);

		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public PaymentsPlatformResource retrievePaymentsPlatformResource(String paymentSubmissionId, String submissionId) {
		try {
			// An ASPSP may choose to make ConsentIds accessible across versions
			// E.g., for a PaymentId created in v1, an ASPSP may or may not make
			// it available via v3 - as this is a short-lived consent

			return paymentsPlatformRepository.findOneByPaymentSubmissionIdOrSubmissionIdAndSubmissionCmaVersionIn(
					paymentSubmissionId, submissionId, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public PaymentsPlatformResource retrievePaymentsPlatformResourceByPaySubmissionIdAndType(String submissionId,
			String paymentType) {
		try {
			return paymentsPlatformRepository.findOneBySubmissionIdAndPaymentTypeAndSubmissionCmaVersionIn(submissionId,
					paymentType, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public PaymentsPlatformResource retrievePaymentsPlatformResourceByBackwardSubmissionId(String submissionId) {
		try {
			return paymentsPlatformRepository.findOneByPaymentSubmissionIdAndSubmissionCmaVersionIn(submissionId,
					compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}
}
