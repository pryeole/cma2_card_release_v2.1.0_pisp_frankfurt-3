/*
 * Authentication Service API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Authentication Parameters object
 */
@ApiModel(description = "Authentication Parameters object")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-09T11:13:17.968+05:30")
public class AuthenticationParameters {
  @SerializedName("digitalUser")
  private DigitalUser digitalUser = null;

  @SerializedName("customerAuthenticationSessions")
  private List<CustomerAuthenticationSession> customerAuthenticationSessions = null;

  @SerializedName("electronicDevices")
  private List<ElectronicDevices> electronicDevices = null;

  public AuthenticationParameters digitalUser(DigitalUser digitalUser) {
    this.digitalUser = digitalUser;
    return this;
  }

   /**
   * Get digitalUser
   * @return digitalUser
  **/
  @ApiModelProperty(value = "")
  public DigitalUser getDigitalUser() {
    return digitalUser;
  }

  public void setDigitalUser(DigitalUser digitalUser) {
    this.digitalUser = digitalUser;
  }

  public AuthenticationParameters customerAuthenticationSessions(List<CustomerAuthenticationSession> customerAuthenticationSessions) {
    this.customerAuthenticationSessions = customerAuthenticationSessions;
    return this;
  }

  public AuthenticationParameters addCustomerAuthenticationSessionsItem(CustomerAuthenticationSession customerAuthenticationSessionsItem) {
    if (this.customerAuthenticationSessions == null) {
      this.customerAuthenticationSessions = new ArrayList<CustomerAuthenticationSession>();
    }
    this.customerAuthenticationSessions.add(customerAuthenticationSessionsItem);
    return this;
  }

   /**
   * Get customerAuthenticationSessions
   * @return customerAuthenticationSessions
  **/
  @ApiModelProperty(value = "")
  public List<CustomerAuthenticationSession> getCustomerAuthenticationSessions() {
    return customerAuthenticationSessions;
  }

  public void setCustomerAuthenticationSessions(List<CustomerAuthenticationSession> customerAuthenticationSessions) {
    this.customerAuthenticationSessions = customerAuthenticationSessions;
  }

  public AuthenticationParameters electronicDevices(List<ElectronicDevices> electronicDevices) {
    this.electronicDevices = electronicDevices;
    return this;
  }

  public AuthenticationParameters addElectronicDevicesItem(ElectronicDevices electronicDevicesItem) {
    if (this.electronicDevices == null) {
      this.electronicDevices = new ArrayList<ElectronicDevices>();
    }
    this.electronicDevices.add(electronicDevicesItem);
    return this;
  }

   /**
   * Get electronicDevices
   * @return electronicDevices
  **/
  @ApiModelProperty(value = "")
  public List<ElectronicDevices> getElectronicDevices() {
    return electronicDevices;
  }

  public void setElectronicDevices(List<ElectronicDevices> electronicDevices) {
    this.electronicDevices = electronicDevices;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AuthenticationParameters authenticationParameters = (AuthenticationParameters) o;
    return Objects.equals(this.digitalUser, authenticationParameters.digitalUser) &&
        Objects.equals(this.customerAuthenticationSessions, authenticationParameters.customerAuthenticationSessions) &&
        Objects.equals(this.electronicDevices, authenticationParameters.electronicDevices);
  }

  @Override
  public int hashCode() {
    return Objects.hash(digitalUser, customerAuthenticationSessions, electronicDevices);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AuthenticationParameters {\n");
    
    sb.append("    digitalUser: ").append(toIndentedString(digitalUser)).append("\n");
    sb.append("    customerAuthenticationSessions: ").append(toIndentedString(customerAuthenticationSessions)).append("\n");
    sb.append("    electronicDevices: ").append(toIndentedString(electronicDevices)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

