package com.capgemini.psd2.aisp.consent.adapter.test.impl;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.aisp.consent.adapter.aspect.PSD2AccountMappingAspect;
import com.capgemini.psd2.aspect.PSD2AspectUtils;

public class PSD2AccountMappingAspectTest {
	
	@Mock
	private PSD2AspectUtils aspectUtils;
	
	@InjectMocks
	private PSD2AccountMappingAspect mappingAspect;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testAarroundLoggerAdviceController(){
		when(aspectUtils.methodPayloadAdvice(anyObject())).thenReturn(new Object());
		mappingAspect.arroundLoggerAdviceController(anyObject());
	}
	


}
