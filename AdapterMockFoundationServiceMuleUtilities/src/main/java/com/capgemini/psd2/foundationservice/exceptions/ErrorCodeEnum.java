package com.capgemini.psd2.foundationservice.exceptions;

import org.springframework.http.HttpStatus;

public enum ErrorCodeEnum {

	/*
	 * For PreStageValidatePayment
	 */
	BAD_REQUEST_PMPSV("FS_PMPSV_001", "Bad request. Please check your request", "The request parameters passed are invalid.", HttpStatus.BAD_REQUEST),

	AUTHENTICATION_FAILURE_PMPSV("FS_PMPSV_002", "You are not authorised to use this service.", "Absence of BOI Authentication Headers.", HttpStatus.UNAUTHORIZED),

	GENERAL_ERROR_PMPSV("FS_PMPSV_003", "General error", "General error", HttpStatus.BAD_REQUEST),

	CONNECTION_ERROR_PMPSV("FS_PMPSV_004", "Connection Error", "Connection Error", HttpStatus.INTERNAL_SERVER_ERROR),

	IBAN_PROVIDED_IS_INVALID_PMPSV("FS_PMPSV_006", "IBAN provided is invalid", "IBAN provided is invalid", HttpStatus.BAD_REQUEST),

	BIC_PROVIDED_IS_INVALID_PMPSV("FS_PMPSV_007", "BIC provided is invalid", "BIC provided is invalid", HttpStatus.BAD_REQUEST),

	INVALID_IBAN_CHECKSUM_PMPSV("FS_PMPSV_008", "Invalid IBAN checksum", "Invalid IBAN checksum", HttpStatus.BAD_REQUEST),

	BIC_IS_NOT_REACHABLE_PMPSV("FS_PMPSV_009", "BIC is not reachable", "BIC is not reachable", HttpStatus.BAD_REQUEST),

	ACCOUNT_NOT_FOUND_OR_CLOSED_PMPSV("FS_PMPSV_010", "Account not found", "Account not found", HttpStatus.BAD_REQUEST),

	BIC_AND_IBAN_DO_NOT_MATCH_PMPSV("FS_PMPSV_011", "BIC and IBAN do not match", "BIC and IBAN do not match", HttpStatus.BAD_REQUEST),

	NSC_PROVIDED_IS_INVALID_PMPSV("FS_PMPSV_012", "NSC provided is invalid", "NSC provided is invalid", HttpStatus.BAD_REQUEST),

	ACCOUNT_NUMBER_PROVIDED_IS_INVALID_PMPSV("FS_PMPSV_013", "Account number provided is invalid", "Account number provided is invalid", HttpStatus.BAD_REQUEST),

	TRANSACTION_CURRENCY_CODE_IS_INVALID_PMPSV("FS_PMPSV_014", "Transaction currency code is invalid", "Transaction currency code is invalid", HttpStatus.BAD_REQUEST),	

	BANK_DOES_NOT_PROCESS_COUNTRY_PMPSV("FS_PMPSV_015", "Bank does not process payments to the requested beneficiary country", "Bank does not process payments to the requested beneficiary country", HttpStatus.BAD_REQUEST),	

	BANK_DOES_NOT_PROCESS_CURRENCY_PMPSV("FS_PMPSV_016", "Bank does not deal in the requested transaction currency", "Bank does not deal in the requested transaction currency", HttpStatus.BAD_REQUEST),

	COUNTRY_CODE_IS_INVALID_PMPSV("FS_PMPSV_017", "Country code is invalid", "Country code is invalid", HttpStatus.BAD_REQUEST),

	ACCOUNT_IS_BLOCKED_TO_DEBITS_PMPSV("FS_PMPSV_018", "Account is blocked to debits", "Account is blocked to debits", HttpStatus.BAD_REQUEST),

	ACCOUNT_IS_BLOCKED_TO_CREDITS_PMPSV("FS_PMPSV_019", "Account is blocked to credits", "Account is blocked to credits", HttpStatus.BAD_REQUEST),

	ACCOUNT_IS_BLOCKED_TO_ALL_PMPSV("FS_PMPSV_020", "Account is blocked to All", "Account is blocked to All", HttpStatus.BAD_REQUEST),

	CREDIT_GRADE_NOT_6_OR_7_PMPSV("FS_PMPSV_021","Credit grade cannot be 6 or 7", "Credit grade cannot be 6 or 7", HttpStatus.BAD_REQUEST),

	ACCOUNT_CANNOT_BE_DORMANT_PMPSV("FS_PMPSV_022","Account cannot be dormant", "Account cannot be dormant", HttpStatus.BAD_REQUEST),

	ACCOUNT_CANNOT_BE_LIEN_PMPSV("FS_PMPSV_023","Account cannot be lien", "Account cannot be lien", HttpStatus.BAD_REQUEST),

	JOINT_ACCOUNT_PERMITTED_ALL_CUSTOMER_PMPSV("FS_PMPSV_024","Joint account must be permitted by all the customer associated", "Joint account must be permitted by all the customer associated", HttpStatus.BAD_REQUEST),

	INVALID_PAYMENT_REQUEST_PMPSV("FS_PMPSV_029","Invalid payment request", "Invalid payment request", HttpStatus.BAD_REQUEST),

	SWIFT_ADDRESS_IS_INVALID_PMPSV("FS_PMPSV_030","Swift address is invalid", "Swift address is invalid", HttpStatus.BAD_REQUEST),

	ABA_CODE_IS_INVALID_PMPSV("FS_PMPSV_031","ABA code is invalid", "ABA code is invalid", HttpStatus.BAD_REQUEST),

	INVALID_FUTURE_PAYMENT_DATE_PMPSV("FS_PMPSV_032","Invalid future payment date", "Invalid future payment date", HttpStatus.BAD_REQUEST),

	/*
	 * For ValidatePayment
	 */

	BAD_REQUEST_PMV("FS_PMV_001", "Bad request. Please check your request", "The request parameters passed are invalid.", HttpStatus.BAD_REQUEST),

	AUTHENTICATION_FAILURE_PMV("FS_PMV_002", "You are not authorised to use this service.", "Absence of BOI Authentication Headers.", HttpStatus.UNAUTHORIZED),

	NOT_FOUND_PMV("FS_PMV_003", "No payment instruction found for the requested information", "No payment instruction found for the requested information", HttpStatus.NOT_FOUND),

	GENERAL_ERROR_PMV("FS_PMV_004", "General error", "General error", HttpStatus.BAD_REQUEST),

	INVALID_IBAN_LENGTH_PMV("FS_PMV_006","IBAN length is not valid", "IBAN length is not valid", HttpStatus.BAD_REQUEST),

	NOT_SEPA_IBAN_PMV("FS_PMV_007","IBAN does not belong to SEPA zone", "IBAN does not belong to SEPA zone", HttpStatus.BAD_REQUEST),

	INVALID_IBAN_CHECKSUM_PMV("FS_PMV_008", "IBAN checksum is not valid", "IBAN checksum is not valid", HttpStatus.BAD_REQUEST),

	NO_SUCH_SEPA_BIC_PMV("FS_PMV_009", "BIC does not belong to SEPA zone", "BIC does not belong to SEPA zone", HttpStatus.BAD_REQUEST),

	BIC_IS_NOT_REACHABLE_PMV("FS_PMV_010", "BIC is not reachable", "BIC is not reachable", HttpStatus.BAD_REQUEST),

	BIC_AND_IBAN_DO_NOT_MATCH_PMV("FS_PMV_011", "BIC and IBAN do not match", "BIC and IBAN do not match", HttpStatus.BAD_REQUEST),

	NOT_A_VALID_NSC_PMV("FS_PMV_012", "NSC is not valid", "NSC is not valid", HttpStatus.BAD_REQUEST),

	NOT_A_VALID_ACCOUNT_NUMBER_PMV("FS_PMV_013", "Account Number is not valid", "Account Number is not valid", HttpStatus.BAD_REQUEST),

	NOT_A_VALID_CURRENCY_PMV("FS_PMV_014", "Currency is not valid", "Currency is not valid", HttpStatus.BAD_REQUEST),	

	ACCOUNT_DOES_NOT_EXISTS_PMV("FS_PMV_015", "Account does not exist", "Account does not exist", HttpStatus.BAD_REQUEST),	

	ACCOUNT_STATUS_MUST_BE_ACTIVE_PMV("FS_PMV_016", "Account status must be active", "Account status must be active", HttpStatus.BAD_REQUEST),

	INVALID_ACCOUNT_TYPE_PMV("FS_PMV_017","Account Type is not valid", "Account Type is not valid", HttpStatus.BAD_REQUEST),

	ACCOUNT_IS_BLOCKED_TO_DEBITS_PMV("FS_PMV_018", "Account is blocked to debits", "Account is blocked to debits", HttpStatus.BAD_REQUEST),

	ACCOUNT_IS_BLOCKED_TO_CREDITS_PMV("FS_PMV_019", "Account is blocked to credits", "Account is blocked to credits", HttpStatus.BAD_REQUEST),

	ACCOUNT_IS_BLOCKED_TO_ALL_PMV("FS_PMV_020", "Account is blocked to All", "Account is blocked to All", HttpStatus.BAD_REQUEST),

	CREDIT_GRADE_NOT_6_OR_7_PMV("FS_PMV_021","Credit grade cannot be 6 or 7", "Credit grade cannot be 6 or 7", HttpStatus.BAD_REQUEST),

	ACCOUNT_CANNOT_BE_DORMANT_PMV("FS_PMV_022","Account cannot be dormant", "Account cannot be dormant", HttpStatus.BAD_REQUEST),

	ACCOUNT_CANNOT_BE_LIEN_PMV("FS_PMV_023","Account cannot be lien", "Account cannot be lien", HttpStatus.BAD_REQUEST),

	JOINT_ACCOUNT_PERMITTED_ALL_CUSTOMER_PMV("FS_PMV_024","Joint account must be permitted by all the customer associated", "Joint account must be permitted by all the customer associated", HttpStatus.BAD_REQUEST),

	FUNDS_NOT_AVAILABLE_PMV("FS_PMV_025","Account does not have enough funds", "Account does not have enough funds", HttpStatus.BAD_REQUEST),

	LIMIT_EXCEEDED_FOR_CUSTOMER_PMV("FS_PMV_026","Limit exceeded for the customer", "Limit exceeded for the customer", HttpStatus.BAD_REQUEST),

	LIMIT_EXCEEDED_FOR_PAYMENT_TYPE_CUSTOMER("FS_PMV_027","Limit exceeded on this payment type for the customer", "Limit exceeded on this payment type for the customer", HttpStatus.BAD_REQUEST),

	LIMIT_EXCEEDED_FOR_AGENT_PMV("FS_PMV_028","Limit exceeded for this agent", "Limit exceeded for this agent", HttpStatus.BAD_REQUEST),

	INVALID_PAYMENT_REQUEST_PMV("FS_PMV_029","Invalid payment request", "Invalid payment request", HttpStatus.BAD_REQUEST),

	SWIFT_ADDRESS_IS_INVALID_PMV("FS_PMV_030","Swift address is invalid", "Swift address is invalid", HttpStatus.BAD_REQUEST),

	ABA_CODE_IS_INVALID_PMV("FS_PMV_031","ABA code is invalid", "ABA code is invalid", HttpStatus.BAD_REQUEST),

	COUNTRY_IS_INVALID_PMV("FS_PMV_032","Country is invalid", "Country is invalid", HttpStatus.BAD_REQUEST),

	COUNTRY_RULES_LIMIT_EXCEEDED_PMV("FS_PMV_033","Country rules limit exceeded", "Country rules limit exceeded", HttpStatus.BAD_REQUEST),

	USER_ID_INVALID_PMV("FS_PMV_034","User id is invalid", "User id is invalid", HttpStatus.BAD_REQUEST),

	JURISDICTION_CODE_INVALID_PMV("FS_PMV_035","Jurisdiction code is invalid", "Jurisdiction code is invalid", HttpStatus.BAD_REQUEST),

	CIS_ID_INVALID_PMV("FS_PMV_036","CIS id is invalid", "CIS id is invalid", HttpStatus.BAD_REQUEST),

	PAYER_PAYEE_ACCOUNT_SAME_PMV("FS_PMV_037","The payer and the payee account cannot be the same", "The payer and the payee account cannot be the same", HttpStatus.BAD_REQUEST),

	BIC_PROVIDER_IS_INVALID_PMV("FS_PMV_038","BIC provided is invalid", "BIC provided is invalid", HttpStatus.BAD_REQUEST),

	FUTURE_PAYMENT_DATE_PMV("FS_PMV_039","Invalid future payment date", "Invalid future payment date", HttpStatus.BAD_REQUEST),
	
		BENEFICIARY_COUNTRY_NOT_SUPPORTED_PMV("FS_PMV_040","Payment to beneficiary country not supported", "Payment to beneficiary country not supported", HttpStatus.BAD_REQUEST),
	
	PANEL_LIMIT_EXCEEDED_PMV("FS_PMV_041","Payment is rejected due to panel limit exceeded", "Payment is rejected due to panel limit exceeded", HttpStatus.BAD_REQUEST),
	
	CUSTOMER_LIMIT_EXCEEDED_PMV("FS_PMV_042","Payment is rejected due to customer limit exceeded", "Payment is rejected due to customer limit exceeded", HttpStatus.BAD_REQUEST),
	
	OUTSIDE_BUSINESS_HOURS_PMV("FS_PMV_043","Payment made outside business hours", "Payment made outside business hours", HttpStatus.BAD_REQUEST),
	
	INVALID_DATE_FROM_BOL_PMV("FS_PMV_044","Invalid date from BOL payment customer limit", "Invalid date from BOL payment customer limit", HttpStatus.BAD_REQUEST),
	
	PAYMENT_AUTHORISATION_PERMISSONS_PMV("FS_PMV_045","User doesnt have payment authorisation permissons", "User doesnt have payment authorisation permissons", HttpStatus.BAD_REQUEST),
	
	INVALID_CUSTOMERID_PMV("FS_PMV_046","CustomerId/UserId is invalid", "CustomerId/UserId is invalid", HttpStatus.BAD_REQUEST),
	
	INVALID_PAYMENT_TYPE_PMV("FS_PMV_047","Invalid payment type", "Invalid payment type", HttpStatus.BAD_REQUEST),
	
	INVALID_PAYMENT_DATE_FORMAT_PMV("FS_PMV_048","Invalid payment date format", "Invalid payment date format", HttpStatus.BAD_REQUEST),
	
	NO_RECORDS_FOUND_PMV("FS_PMV_049","No records found for PaymentDate/PaymentType/Currency", "No records found for PaymentDate/PaymentType/Currency", HttpStatus.BAD_REQUEST),
	
	NATIONALID_NOT_EXIST_PMV("FS_PMV_050","Unable to derive BIC from IBAN as national id does not exist. Please correct payee IBAN", "Unable to derive BIC from IBAN as national id does not exist. Please correct payee IBAN", HttpStatus.BAD_REQUEST),
	
	INVALID_PAYEE_NAME_PMV("FS_PMV_051","Invalid payee name", "Invalid payee name", HttpStatus.BAD_REQUEST),
	
	PAYEE_ADDRESS_INVALID_PMV("FS_PMV_052","Payee address format invalid", "Payee address format invalid", HttpStatus.BAD_REQUEST),
	/*
	 *Start For stagePaymentInsert
	 */
	BAD_REQUEST_PMI("FS_PMI_001","Bad Request","Please check your request",HttpStatus.BAD_REQUEST),
	AUTHENTICATION_FAILURE_PMI("FS_PMI_002", "You are not authorised to use this service.", "Absence of BOI Authentication Headers.", HttpStatus.UNAUTHORIZED),
	NO_TXN_DETAILS_FOUND_PMI("FS_PMI_003", "No transaction details found for requested account(s)","Not Found",HttpStatus.BAD_REQUEST),
	GENERAL_ERROR_PMI("FS_PMI_004", "General error","General error", HttpStatus.BAD_REQUEST),
	INVALID_FUTURE_PAYMENT_DATE_PMI("FS_PMI_006","Invalid future payment date","Invalid future payment date",HttpStatus.BAD_REQUEST),

	/*
	 *Start For stagePaymentUpdate
	 */
	BAD_REQUEST_PMU("FS_PMU_001","Bad Request","Please check your request",HttpStatus.BAD_REQUEST),
	AUTHENTICATION_FAILURE_PMU("FS_PMU_002", "You are not authorised to use this service.", "Absence of BOI Authentication Headers.", HttpStatus.UNAUTHORIZED),
	NO_TXN_DETAILS_FOUND_PMU("FS_PMU_003", "No transaction details found for requested account(s)","Not Found",HttpStatus.BAD_REQUEST),
	GENERAL_ERROR_PMU("FS_PMU_004", "General error","General error", HttpStatus.BAD_REQUEST),


	/*
	 * For RetrivevePayment
	 */

	BAD_REQUEST_PMR("FS_PMR_001","Bad Request","Please check your request",HttpStatus.NOT_FOUND),
	AUTHENTICATION_FAILURE_PMR("FS_PMR_002","You are not authorised to use this service.", "Absence of BOI Authentication Headers.", HttpStatus.UNAUTHORIZED),
	NO_PAYMENT_DETAILS_FOUND_PMR("FS_PMR_003","No transaction details found for the requested account(s)","Not Found",HttpStatus.NOT_FOUND),
	GENERAL_ERROR_PMR("FS_PMR_004", "General error","General error", HttpStatus.NOT_FOUND),

	/*
	 *Start For PaymentRetrieveCreate
	 */

	BAD_REQUEST_PME("FS_PME_001", "Bad request.Please check your request", "Bad request", HttpStatus.BAD_REQUEST),

	AUTHENTICATION_FAILURE_PME("FS_PME_002", "You are not authorised to use this service.", "Absence of BOI Authentication Headers.", HttpStatus.UNAUTHORIZED),

	NOT_FOUND_PME("FS_PME_003", "No payment instruction found for the requested information", "No payment instruction found for the requested information", HttpStatus.NOT_FOUND),

	GENERAL_ERROR_PME("FS_PME_004", "General Error", "General Error", HttpStatus.BAD_REQUEST),

	INVALID_IBAN_LENGTH_PME("FS_PME_006", "IBAN length is not valid", "IBAN length is not valid", HttpStatus.BAD_REQUEST),

	NOT_SEPA_IBAN_PME("FS_PME_007", "IBAN does not belong to SEPA zone", "IBAN does not belong to SEPA zone", HttpStatus.BAD_REQUEST),

	INVALID_IBAN_CHEKSUM_PME("FS_PME_008", "IBAN checksum is not valid", "IBAN checksum is not valid", HttpStatus.BAD_REQUEST),

	NO_SUCH_SEPA_BIC_PME("FS_PME_009", "BIC does not belong to SEPA zone", "BIC does not belong to SEPA zone", HttpStatus.BAD_REQUEST),

	BIC_NOT_REACHABLE_PME("FS_PME_010", "BIC is not reachable", "BIC is not reachable", HttpStatus.BAD_REQUEST),

	BIC_IBAN_MISMATCH_PME("FS_PME_011", "BIC and IBAN do not match", "BIC and IBAN do not match", HttpStatus.BAD_REQUEST),

	NSC_INVALID_PME("FS_PME_012", "NSC is not valid", "NSC is not valid", HttpStatus.BAD_REQUEST),

	ACCOUNT_INVALID_PME("FS_PME_013", "Account Number is not valid", "Account Number is not valid", HttpStatus.BAD_REQUEST),

	CURRENCY_INVALID_PME("FS_PME_014", "Currency is not valid", "Currency is not valid", HttpStatus.BAD_REQUEST),

	NO_ACCOUNT_EXISTS_PME("FS_PME_015", "Account does not exist", "Account does not exist", HttpStatus.BAD_REQUEST),

	ACCOUNT_STATUS_PME("FS_PME_016", "Account status must be active", "Account status must be active", HttpStatus.BAD_REQUEST),

	ACCOUNT_TYPE_INVALID_PME("FS_PME_017", "Account Type is not valid", "Account Type is not valid", HttpStatus.BAD_REQUEST),

	ACCOUNT_BLOCKED_TO_DEBITS_PME("FS_PME_018", "Account is blocked to debits", "Account is blocked to debits", HttpStatus.BAD_REQUEST),

	ACCOUNT_BLOCKED_TO_CREDITS_PME("FS_PME_019", "Account is blocked to credits", "Account is blocked to credits", HttpStatus.BAD_REQUEST),

	ACCOUNT_BLOCKED_TO_ALL_PME("FS_PME_020", "Account is blocked to All", "Account is blocked to All", HttpStatus.BAD_REQUEST),

	CREDIT_GRADE_PME("FS_PME_021", "Credit grade cannot be 6 or 7", "Credit grade cannot be 6 or 7", HttpStatus.BAD_REQUEST),

	ACCOUNT_DORMANT_PME("FS_PME_022", "Account cannot be dormant", "Account cannot be dormant", HttpStatus.BAD_REQUEST),

	ACCOUNT_LIEN_PME("FS_PME_023", "Account cannot be lien", "Account cannot be lien", HttpStatus.BAD_REQUEST),

	JOINT_AMOUNT_PME("FS_PME_024", "Joint account must be permitted by all the customer associated", "Joint account must be permitted by all the customer associated", HttpStatus.BAD_REQUEST),

	FUNDS_UNAVAILABLE_PME("FS_PME_025", "Account does not have enough funds", "Account does not have enough funds", HttpStatus.BAD_REQUEST),

	CUSTOMER_LIMITS_EXCEEDED_PME("FS_PME_026", "Limit exceeded for the customer", "Limit exceeded for the customer", HttpStatus.BAD_REQUEST),

	TRANSACTION_LIMITS_EXCEEDED_PME("FS_PME_027", "Limit exceeded on this payment type for the customer", "Limit exceeded on this payment type for the customer", HttpStatus.BAD_REQUEST),

	AGENT_LIMITS_EXCEEDED_PME("FS_PME_028", "Limits exceeded for this agent", "Limits exceeded for this agent", HttpStatus.BAD_REQUEST),

	INVALID_PAYMENT_REQUEST_PME("FS_PME_029","Invalid Payment Request", "Invalid Payment Request", HttpStatus.BAD_REQUEST),

	SWIFT_ADDRESS_IS_INVALID_PME("FS_PME_030","Swift address is invalid", "Swift address is invalid", HttpStatus.BAD_REQUEST),

	ABA_CODE_IS_INVALID_PME("FS_PME_031","ABA code is invalid", "ABA code is invalid", HttpStatus.BAD_REQUEST),

	COUNTRY_IS_INVALID_PME("FS_PME_032","Country is invalid", "Country is invalid", HttpStatus.BAD_REQUEST),

	COUNTRY_RULES_LIMIT_EXCEEDED_PME("FS_PME_033","Country rules limit exceeded", "Country rules limit exceeded", HttpStatus.BAD_REQUEST),

	USER_ID_INVALID_PME("FS_PME_034","User id is invalid", "User id is invalid", HttpStatus.BAD_REQUEST),

	JURISDICTION_CODE_INVALID_PME("FS_PME_035","Jurisdiction code is invalid", "Jurisdiction code is invalid", HttpStatus.BAD_REQUEST),

	CIS_ID_INVALID_PME("FS_PME_036","CIS id is invalid", "CIS id is invalid", HttpStatus.BAD_REQUEST),

	PAYER_PAYEE_ACCOUNT_SAME_PME("FS_PME_037","The payer and the payee account cannot be the same", "The payer and the payee account cannot be the same", HttpStatus.BAD_REQUEST),

	PAYMENT_REJECTED_PME("FS_PME_038","Payment rejected due to the suspicion of fraud", "Payment rejected due to the suspicion of fraud", HttpStatus.BAD_REQUEST),
	
	FUTURE_PAYMENT_DATE("FS_PME_039","Invalid future payment date", "Invalid future payment date", HttpStatus.BAD_REQUEST),

	/*
	 *Start For B365 and BOL Authentication 
	 */

	AUTHENTICATION_FAILURE_LOGIN_AUTH("FS_AUTH_001", "Authentication failed.", "Authentication failed.", HttpStatus.BAD_REQUEST),
	USER_TEMPORARILY_BLOCKED_AUTH("FS_AUTH_002", "User temporarily blocked.", "User temporarily blocked.", HttpStatus.BAD_REQUEST),
	USER_BLOCKED_ON_B365_OR_BOL_AUTH("FS_AUTH_003", "User blocked on B365 or BOL", "User blocked on B365 or BOL", HttpStatus.BAD_REQUEST),
	USER_BLOCKED_ON_HID_AUTH("FS_AUTH_004", "User blocked on HID.", "User blocked on HID.", HttpStatus.BAD_REQUEST),
	USER_IS_NOT_REGISTERED_FOR_HID_AUTH("FS_AUTH_005", "User is not registered for HID.", "User is not registered for HID.", HttpStatus.BAD_REQUEST),
	USER_DOES_NOT_EXIT_ON_HID_AUTH("FS_AUTH_006", "User does not exist on HID.", "User does not exist on HID.", HttpStatus.BAD_REQUEST),
	USER_NAME_DOES_NOT_MATCH_HEADER_AUTH("FS_AUTH_007", "username does not match header.", "username does not match header.", HttpStatus.BAD_REQUEST),
	AUTHENTICATION_FAILURE_HEADER_AUTH("FS_AUTH_008", "You are not authorised to use this service.", "You are not authorised to use this service.", HttpStatus.UNAUTHORIZED),
	GENERAL_ERROR_AUTH("FS_AUTH_010", "General Error", "General Error", HttpStatus.BAD_REQUEST),
	INTERNAL_SERVER_ERROR("FS_AUTH_009", "Technical Error. Please try again later", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),

	/*

	 * For AccountBeneficiary current and savings/credit card

	 */

	BAD_REQUEST_ESBE("FS_ESBE_001","Account Beneficiary API","Please check your request",HttpStatus.BAD_REQUEST),
	AUTHENTICATION_FAILURE_ESBE("FS_ESBE_002", "Account Beneficiary API", "You are not authorised to use this service.", HttpStatus.UNAUTHORIZED),
	NO_BENEFICIARY_FOUND_ESBE("FS_ESBE_003", "Account Beneficiary API", "This request cannot be processed. No beneficiary found", HttpStatus.NOT_FOUND),
	INTERNAL_SERVER_ERROR_ESBE("FS_ESBE_004", "Account Beneficiary API", "Technical Error. Please try again later.", HttpStatus.INTERNAL_SERVER_ERROR),
	BAD_REQUEST_PPR_CPA("PPR_CPA_001", "Account Beneficiary API", "Account number length or data type is invalid", HttpStatus.BAD_REQUEST),
	ACCOUNT_NOT_FOUND_PPR_CPA("PPR_CPA_002","Account Beneficiary API","Account not found",HttpStatus.NOT_FOUND),
	RESOURCE_NOT_FOUND_PPR_CPA("PPR_CPA_003", "Account Beneficiary API", "URL resource not found",HttpStatus.NOT_FOUND),
	REQUEST_TIMED_OUT_PPR_CPA("PPR_CPA_004", "Account Beneficiary API", "Request timed out for MWS Service", HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_PPR_CPA("PPR_CPA_999", "Account Beneficiary API", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_PPR_CPA("PPR_CPA_005", "Account Beneficiary API", "Unsupported media type", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	DOWNSTREAM_SYSTEM_NOT_AVAIL_PPR_CPA("PPR_CPA_006", "Account Beneficiary API", "Downstream system not available", HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_PPR_CPA("PPR_CPA_007", "Account Beneficiary API", "Method not allowed", HttpStatus.METHOD_NOT_ALLOWED),
	RESPONSE_TYPE_UNACCEPTABLE_PPR_CPA("PPR_CPA_008", "Account Beneficiary API", "Response type provided in the accept header is not allowed", HttpStatus.NOT_ACCEPTABLE),
	SERVER_ACCESS_UNAUTHORIZED_PPR_CPA("PPR_CPA_009", "Account Beneficiary API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_ACCESS_UNAUTHORIZED_PPR_CPA("PPR_CPA_010", "Account Beneficiary API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_AUTH_UNAUTHORIZED_PPR_CPA("PPR_CPA_011", "Account Beneficiary API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	SERVER_API_AUTH_ACCESS_PPR_CPA("PPR_CPA_012", "Account Beneficiary API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTEDS_PPR_CPA("PPR_CPA_013", "Account Beneficiary API", "Used for API resource operations which haven’t been implemented", HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWEDSACCPPR_CPA("PPR_CPA_014", "Account Beneficiary API", "Method not allowed in the Downstream system", HttpStatus.METHOD_NOT_ALLOWED),
	SERVER_REQUEST_NOT_ACCEPTABLES_PPR_CPA("PPR_CPA_015", "Account Beneficiary API", "Response type provided in the accept header is not allowed in the Downstream system", HttpStatus.NOT_ACCEPTABLE),
	SERVER_UNSUPPORTEDS_PPR_CPA("PPR_CPA_016", "Account Beneficiary API", "Unsupported media type in the Downstream system", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_REQUEST_EXCEEDEDS_PPR_CPA("PPR_CPA_017", "Account Beneficiary API", "Rate limit exceeded while calling the Downstream system", HttpStatus.TOO_MANY_REQUESTS),
	SERVER_BAD_REQUESTS_PPR_CPA("PPR_CPA_018", "Account Beneficiary API", "Failure received from Downstream system", HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_DOWNSYSTEMS_PPR_CPA("PPR_CPA_019", "Account Beneficiary API", "Request to the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	SERVER_BAD_REQUEST_FAIL_DOWNSYSTEMS_PPR_CPA("PPR_CPA_020", "Account Beneficiary API", "Request from the Downstream system failed validation", HttpStatus.BAD_REQUEST),


	/*

	 *Start For StandingOrders current and savings

	 */

	BAD_REQUEST_ESSO("FS_ESSO_001", "Standing Order API", "Bad request", HttpStatus.BAD_REQUEST),
	AUTHENTICATION_FAILURE_ESSO("FS_ESSO_002", "Standing Order API", "You are not authorised to use this service.", HttpStatus.UNAUTHORIZED),
	NO_STANDINGORDER_FOUND_ESSO("FS_ESSO_003","Standing Order API","No StandingOrders Found",HttpStatus.NOT_FOUND),
	INTERNAL_SERVER_ERROR_ESSO("FS_ESSO_004", "Standing Order API", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),
	NOT_FOUND_BOL_ESSO("FS_ESSO_005", "Standing Order API", "Function not available for that profile", HttpStatus.NOT_FOUND),
	BAD_REQUEST_PAC_STO("PAC_STO_001", "Standing Order API", "Account number length or data type is invalid", HttpStatus.BAD_REQUEST),
	ACCOUNT_NOT_FOUND_PAC_STO("PAC_STO_002", "Standing Order API", "Account not found", HttpStatus.NOT_FOUND),
	RESOURCE_NOT_FOUND_PAC_STO("PAC_STO_003", "Standing Order API", "URL resource not found", HttpStatus.NOT_FOUND),
	REQUEST_TIMED_OUT_PAC_STO("PAC_STO_004", "Standing Order API", "Request timed out for MWS Service", HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_PAC_STO("PAC_STO_999", "Standing Order API", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_PAC_STO("PAC_STO_005", "Standing Order API", "Unsupported media type", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	DOWNSTREAM_SYSTEM_NOT_AVAIL_PAC_STO("PAC_STO_006", "Standing Order API", "Downstream system not available", HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_PAC_STO("PAC_STO_007", "Standing Order API", "Method not allowed", HttpStatus.METHOD_NOT_ALLOWED),
	RESPONSE_TYPE_UNACCEPTABLE_PAC_STO("PAC_STO_008", "Standing Order API", "Response type provided in the accept header is not allowed", HttpStatus.NOT_ACCEPTABLE),
	SERVER_ACCESS_UNAUTHORIZED_PAC_STO("PAC_STO_009", "Standing Order API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_ACCESS_UNAUTHORIZED_PAC_STO("PAC_STO_010", "Standing Order API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_AUTH_UNAUTHORIZED_PAC_STO("PAC_STO_011", "Standing Order API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	SERVER_API_AUTH_ACCESS_PAC_STO("PAC_STO_012", "Standing Order API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTEDS_PAC_STO("PAC_STO_013", "Standing Order API", "Used for API resource operations which haven’t been implemented", HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWEDSACC_PAC_STO("PAC_STO_014", "Standing Order API", "Method not allowed in the Downstream system", HttpStatus.METHOD_NOT_ALLOWED),
	SERVER_REQUEST_NOT_ACCEPTABLES_PAC_STO("PAC_STO_015", "Standing Order API", "Response type provided in the accept header is not allowed in the Downstream system", HttpStatus.NOT_ACCEPTABLE),
	SERVER_UNSUPPORTEDS_PAC_STO("PAC_STO_016", "Standing Order API", "Unsupported media type in the Downstream system", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_REQUEST_EXCEEDEDS_PAC_STO("PAC_STO_017", "Standing Order API", "Rate limit exceeded while calling the Downstream system", HttpStatus.TOO_MANY_REQUESTS),
	SERVER_BAD_REQUESTS_PAC_STO("PAC_STO_018", "Standing Order API", "Failure received from Downstream system", HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_DOWNSYSTEMS_PAC_STO("PAC_STO_019", "Standing Order API", "Request to the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	SERVER_BAD_REQUEST_FAIL_DOWNSYSTEMS_PAC_STO("PAC_STO_020", "Standing Order API", "Request from the Downstream system failed validation", HttpStatus.BAD_REQUEST),


	/*
	 *Start For Product Details
	 */
	AUTHENTICATION_FAILURE_ESPR("FS_ESPR_002", "You are not authorised to use this service.", "You are not authorised to use this service.", HttpStatus.UNAUTHORIZED),
	NO_PRODUCTDETAILS_FOUND_ESPR("FS_ESPR_003","No Product Details Found","No Product Details Found",HttpStatus.NOT_FOUND),
	INTERNAL_SERVER_ERROR_ESPR("FS_ESPR_004", "Technical Error. Please try again later", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),

	/*
	 *Start For Balance
	 */
	BAD_REQUEST_ABTBC("FS_ABTBC_001", "Bad request.Please check your request", "Bad request", HttpStatus.BAD_REQUEST),
	AUTHENTICATION_FAILURE_ABTBC("FS_ABTBC_002", "You are not authorised to use this service.", "You are not authorised to use this service.", HttpStatus.UNAUTHORIZED),
	NO_BALANCE_FOUND_ABTBC("FS_ABTBC_003","No account detail found for the requested account","No Balance Found",HttpStatus.NOT_FOUND),
	INTERNAL_SERVER_ERROR_ABTBC("FS_ABTBC_004", "Technical Error. Please try again later", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),


	/*
	 *Start For AccountInformation current and savings/credit card
	 */

	BAD_REQUEST_ACC_PAC("PAC_ACC_001", "Single Account Information API", "Missing mandatory header value", HttpStatus.BAD_REQUEST),
	NO_ACOUNT_FOUND_ACC_PAC("PAC_ACC_002","Single Account Information API","Account not found",HttpStatus.NOT_FOUND),
	REQUEST_NOT_FOUND_ACC_PAC("PAC_ACC_003", "Single Account Information API", "URL resource not found",HttpStatus.NOT_FOUND),
	REQUEST_TIMED_OUT_ACC_PAC("PAC_ACC_004", "Single Account Information API", "Request timed out for MWS Service", HttpStatus.GATEWAY_TIMEOUT),
	INTERNAL_SERVER_ERROR_ACC_PAC("PAC_ACC_999", "Single Account Information API", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_ACC_PAC("PAC_ACC_005", "Single Account Information API", "Unsupported media type", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	DOWNSTREAM_SYSTEM_NOT_AVIAL_ACC_PAC("PAC_ACC_006","Single Account Information API", "Downstream system not available", HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_ACC_PAC("PAC_ACC_007", "Single Account Information API", "Method not allowed", HttpStatus.METHOD_NOT_ALLOWED),
	REQUEST_TYPE_UNACCEPTABLE_ACC_PAC("PAC_ACC_008", "Single Account Information API", "Response type provided in the accept header is not allowed", HttpStatus.NOT_ACCEPTABLE),
	SERVER_ACCESS_UNAUTHORIZED_ACC_PAC("PAC_ACC_009", "Single Account Information API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_ACCESS_UNAUTHORIZED_ACC_PAC("PAC_ACC_010", "Single Account Information API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_AUTH_UNAUTHORIZED_ACC_PAC("PAC_ACC_011", "Single Account Information API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	SERVER_API_AUTH_ACCESS_ACC_PAC("PAC_ACC_012", "Single Account Information API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTEDS_ACC_PAC("PAC_ACC_013", "Single Account Information API", "Used for API resource operations which haven’t been implemented", HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWEDS_ACC_PAC("PAC_ACC_014", "Single Account Information API", "Method not allowed in the Downstream system", HttpStatus.METHOD_NOT_ALLOWED),
	SERVER_REQUEST_NOT_ACCEPTABLES_ACC_PAC("PAC_ACC_015", "Single Account Information API", "Response type provided in the accept header is not allowed in the Downstream system", HttpStatus.NOT_ACCEPTABLE),
	SERVER_UNSUPPORTEDS_ACC_PAC("PAC_ACC_016", "Single Account Information API", "Unsupported media type in the Downstream system", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_REQUEST_EXCEEDEDS_ACC_PAC("PAC_ACC_017", "Single Account Information API", "Rate limit exceeded while calling the Downstream system", HttpStatus.TOO_MANY_REQUESTS),
	SERVER_BAD_REQUESTS_ACC_PAC("PAC_ACC_018", "Single Account Information API", "Failure received from Downstream system", HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_DOWNSYSTEMS_ACC_PAC("PAC_ACC_019", "Single Account Information API", "Request to the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	SERVER_BAD_REQUEST_FAIL_DOWNSYSTEMS_ACC_PAC("PAC_ACC_020", "Single Account Information API", "Request from the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	REQUEST_TIMED_OUT_ACC_PAC_PRODUCT("PAC_ACC_004", "Single Account Information API", "Request timed out for MWS Service", HttpStatus.GATEWAY_TIMEOUT),
	DOWNSTREAM_SYSTEM_NOT_AVIAL_ACC_PAC_PRODUCT("PAC_ACC_006","Single Account Information API", "Downstream system not available", HttpStatus.SERVICE_UNAVAILABLE),
	REQUEST_TIMED_OUT_ACC_PAC_CREDIT("PAC_ACC_004", "Single Account Information API", "Request timed out for DeClick", HttpStatus.GATEWAY_TIMEOUT),
	DECLICK_SYSTEM_NOT_AVIAL_ACC_PAC_CREDIT("PAC_ACC_006","Single Account Information API", " Declick system not available", HttpStatus.SERVICE_UNAVAILABLE),

	/*

	 *Start For balance current and savings/credit card

	 */

	BAD_REQUEST_PAC_BAL("PAC_BAL_001", "Account Balance API", "Missing mandatory header value", HttpStatus.BAD_REQUEST),
	NO_ACOUNT_FOUND_PAC_BAL("PAC_BAL_002","Account Balance API","Account not found",HttpStatus.NOT_FOUND),
	REQUEST_NOT_FOUND_PAC_BAL("PAC_BAL_003", "Account Balance API", "URL resource not found",HttpStatus.NOT_FOUND),
	REQUEST_TIMED_OUT_PAC_BAL("PAC_BAL_004", "Account Balance API", "Request timed out for MWS Service", HttpStatus.GATEWAY_TIMEOUT),
	INTERNAL_SERVER_ERROR_PAC_BAL("PAC_BAL_999", "Account Balance API", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_PAC_BAL("PAC_BAL_005", "Account Balance API", "Unsupported media type", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	DOWNSTREAM_SYSTEM_NOT_AVIAL_PAC_BAL("PAC_BAL_006","Account Balance API", "Downstream system not available", HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_PAC_BAL("PAC_BAL_007", "Account Balance API", "Method not allowed", HttpStatus.METHOD_NOT_ALLOWED),
	REQUEST_TYPE_UNACCEPTABLE_PAC_BAL("PAC_BAL_008","Account Balance API", "Response type provided in the accept header is not allowed", HttpStatus.NOT_ACCEPTABLE),
	SERVER_ACCESS_UNAUTHORIZED_PAC_BAL("PAC_BAL_009", "Account Balance API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_ACCESS_UNAUTHORIZED_PAC_BAL("PAC_BAL_010", "Account Balance API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_AUTH_UNAUTHORIZED_PAC_BAL("PAC_BAL_011", "Account Balance API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	SERVER_API_AUTH_ACCESS_PAC_BAL("PAC_BAL_012", "Account Balance API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTEDS_PAC_BAL("PAC_BAL_013", "Account Balance API", "Used for API resource operations which haven’t been implemented", HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWEDS_PAC_BAL("PAC_BAL_014", "Account Balance API", "Method not allowed in the Downstream system", HttpStatus.METHOD_NOT_ALLOWED),
	SERVER_REQUEST_NOT_ACCEPTABLES_PAC_BAL("PAC_BAL_015", "Account Balance API", "Response type provided in the accept header is not allowed in the Downstream system", HttpStatus.NOT_ACCEPTABLE),
	SERVER_UNSUPPORTEDS_PAC_BAL("PAC_BAL_016", "Account Balance API", "Unsupported media type in the Downstream system", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_REQUEST_EXCEEDEDS_PAC_BAL("PAC_BAL_017", "Account Balance API", "Rate limit exceeded while calling the Downstream system", HttpStatus.TOO_MANY_REQUESTS),
	SERVER_BAD_REQUESTS_PAC_BAL("PAC_BAL_018", "Account Balance API", "Failure received from Downstream system", HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_DOWNSYSTEMS_PAC_BAL("PAC_BAL_019", "Account Balance API", "Request to the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	SERVER_BAD_REQUEST_FAIL_DOWNSYSTEMS_PAC_BAL("PAC_BAL_020", "Account Balance API", "Request from the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	REQUEST_TIMED_OUT_PAC_BAL_CREDIT("PAC_BAL_004", "Account Balance API", "Request timed out for DeClick", HttpStatus.GATEWAY_TIMEOUT),
	DECLICK_SYSTEM_NOT_AVIAL_PAC_BAL_CREDIT("PAC_BAL_006","Account Balance API", " Declick system not available", HttpStatus.SERVICE_UNAVAILABLE),
	
	/*

	 *Start For Statements File - Current/Savings/Credit Card

	 */

	BAD_REQUEST_CS_STP("PAC_STP_001", "Statement File API", "Missing mandatory header value", HttpStatus.BAD_REQUEST),
	STATEMENT_NOT_FOUND_CS_STP("PAC_STP_002", "Statement File API", "Statement not found", HttpStatus.NOT_FOUND),
	RESOURCE_NOT_FOUND_CS_STP("PAC_STP_003", "Statement File API", "URL resource not found", HttpStatus.NOT_FOUND),
	REQUEST_TIMED_OUT_CS_STP("PAC_STP_004", "Statement File API", "Request timed out for MWS Service", HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_CS_STP("PAC_STP_999", "Statement File API", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_CS_STP("PAC_STP_005", "Statement File API", "Unsupported media type", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	DOWNSTREAM_NOT_AVAILABLE_CS_STP("PAC_STP_006", "Statement File API", "E2 Vault services not available", HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_CS_STP("PAC_STP_007", "Statement File API", "Method not allowed", HttpStatus.METHOD_NOT_ALLOWED),
	RESPONSE_TYPE_UNACCEPTABLE_CS_STP("PAC_STP_008", "Statement File API","Response type provided in the accept header is not allowed", HttpStatus.NOT_ACCEPTABLE),
	SERVER_ACCESS_UNAUTHORIZED_CS_STP("PAC_STP_009", "Statement File API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_ACCESS_UNAUTHORIZED_CS_STP("PAC_STP_010", "Statement File API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_AUTH_UNAUTHORIZED_CS_STP("PAC_STP_011", "Statement File API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	SERVER_API_AUTH_ACCESS_CS_STP("PAC_STP_012", "Statement File API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTED_CS_STP("PAC_STP_013", "Statement File API", "Used for API resource operations which haven’t been implemented", HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWED_CS_STP("PAC_STP_014", "Statement File API", "Method not allowed in the Downstream system", HttpStatus.METHOD_NOT_ALLOWED),
	SERVER_REQUEST_NOT_ACCEPTABLE_CS_STP("PAC_STP_015", "Statement File API", "Response type provided in the accept header is not allowed in the Downstream system", HttpStatus.NOT_ACCEPTABLE),
	SERVER_UNSUPPORTED_CS_STP("PAC_STP_016", "Statement File API", "Unsupported media type in the Downstream system", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_REQUEST_EXCEEDED_CS_STP("PAC_STP_017", "Statement File API", "Rate limit exceeded while calling the Downstream system", HttpStatus.TOO_MANY_REQUESTS),
	SERVER_BAD_REQUEST_CS_STP("PAC_STP_018", "Statement File API", "Failure received from Downstream system", HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_DOWNSYSTEM_CS_STP("PAC_STP_019", "Statement File API", "Request to the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	SERVER_BAD_REQUEST_FAIL_DOWNSYSTEM_CS_STP("PAC_STP_020", "Statement File API", "Request from the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	SERVER_BAD_REQUEST_FAIL_DOWNSYSTEM_CS_STP_CREDIT("PAC_STP_002", "Statement File API", "Account not found", HttpStatus.NOT_FOUND),
	DOWNSTREAM_NOT_AVAILABLE_CS_STP_CREDIT("PAC_STP_006", "Statement File API", "E2 Vault not available", HttpStatus.SERVICE_UNAVAILABLE),

	//Shared Error code

	API_ERROR("API_ERR_999", "API Common Error", "CRITICAL Error when Exception Handling fails", HttpStatus.INTERNAL_SERVER_ERROR),
	API_TECHNICAL_ERROR("API_POL_001", "API Common Error", "An exception was thrown while executing a custom policy", HttpStatus.INTERNAL_SERVER_ERROR),

	//Used only for Credit card account API
	API_ERROR_PCC("PCC_POL_001", "API Common Error", "Unauthorized digital user profile",  HttpStatus.FORBIDDEN),

	//Used only for current account API
	API_ERROR_PAC("PAC_POL_001", "API Common Error", "Joint account holder consent is not present",  HttpStatus.FORBIDDEN),

	//Used only for General error for Account Process API
	API_ERROR_003("PAC_ERR_003", "API Common Error", "URL resource not found",  HttpStatus.NOT_FOUND),

	//Used only for General error for Party Process API
	API_PPR_ERR_003("PPR_ERR_003","API Common Error", "URL resource not found",  HttpStatus.NOT_FOUND),

	API_ERROR_002("API_POL_002", "API Common Error", "Unauthorized",  HttpStatus.INTERNAL_SERVER_ERROR),

	/*
	 *Start For Statements List - Current/Savings/Credit Card
	 */
	BAD_REQUEST_CS_STL("PAC_STL_001", "Statement List API", "Missing mandatory header value", HttpStatus.BAD_REQUEST),
	STATEMENT_NOT_FOUND_CS_STL("PAC_STL_002", "Statement List API", "Statement List not found", HttpStatus.NOT_FOUND),
	RESOURCE_NOT_FOUND_CS_STL("PAC_STL_003", "Statement List API", "URL resource not found", HttpStatus.NOT_FOUND),
	REQUEST_TIMED_OUT_CS_STL("PAC_STL_004", "Statement List API", "Request timed out for MWS Service", HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_CS_STL("PAC_STL_999", "Statement List API", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_CS_STL("PAC_STL_005", "Statement List API", "Unsupported media type", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	DOWNSTREAM_NOT_AVAILABLE_CS_STL("PAC_STL_006", "Statement List API", "E2 Vault services not available", HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_CS_STL("PAC_STL_007", "Statement List API", "Method not allowed", HttpStatus.METHOD_NOT_ALLOWED),
	RESPONSE_TYPE_UNACCEPTABLE_CS_STL("PAC_STL_008", "Statement List API", "Response type provided in the accept header is not allowed", HttpStatus.NOT_ACCEPTABLE),
	SERVER_ACCESS_UNAUTHORIZED_CS_STL("PAC_STL_009", "Statement List API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_ACCESS_UNAUTHORIZED_CS_STL("PAC_STL_010", "Statement List API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_AUTH_UNAUTHORIZED_CS_STL("PAC_STL_011", "Statement List API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	SERVER_API_AUTH_ACCESS_CS_STL("PAC_STL_012", "Statement List API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTED_CS_STL("PAC_STL_013", "Statement List API", "Used for API resource operations which haven’t been implemented", HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWED_CS_STL("PAC_STL_014", "Statement List API", "Method not allowed in the Downstream system", HttpStatus.METHOD_NOT_ALLOWED),
	SERVER_REQUEST_NOT_ACCEPTABLE_CS_STL("PAC_STL_015", "Statement List API", "Response type provided in the accept header is not allowed in the Downstream system", HttpStatus.NOT_ACCEPTABLE),
	SERVER_UNSUPPORTED_CS_STL("PAC_STL_016", "Statement List API", "Unsupported media type in the Downstream system", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_REQUEST_EXCEEDED_CS_STL("PAC_STL_017", "Statement List API", "Rate limit exceeded while calling the Downstream system", HttpStatus.TOO_MANY_REQUESTS),
	SERVER_BAD_REQUEST_CS_STL("PAC_STL_018", "Statement List API", "Failure received from Downstream system", HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_DOWNSYSTEM_CS_STL("PAC_STL_019", "Statement List API", "Request to the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	SERVER_BAD_REQUEST_FAIL_DOWNSYSTEM_CS_STL("PAC_STL_020", "Statement List API", "Request from the Downstream system failed validation", HttpStatus.BAD_REQUEST),

	/*

	 *Start For ScheduledPayments - Current/Savings

	 */

	BAD_REQUEST_CS_SCP("PPR_OFP_001", "Scheduled Payments API", "Missing mandatory header value", HttpStatus.BAD_REQUEST),
	NO_SCHEDULED_STMT_NOT_FOUND_SCP ("PPR_OFP_002", "Scheduled Payments API", "Account not found", HttpStatus.NOT_FOUND),
	RESOURCE_NOT_FOUND_CS_SCP("PPR_OFP_003", "Scheduled Payments API", "URL resource not found", HttpStatus.NOT_FOUND),
	REQUEST_TIMED_OUT_CS_SCP("PPR_OFP_004", "Scheduled Payments API", "Request timed out for MWS Service", HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_CS_SCP("PPR_OFP_999", "Scheduled Payments API", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_TYPE_CS_SCP("PPR_OFP_005","Scheduled Payments API","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	DOWNSTREAM_NOT_AVAILABLE_CS_SCP("PPR_OFP_006", "Scheduled Payments API", "Downstream system not available", HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_CS_SCP("PPR_OFP_007", "Scheduled Payments API", "Method not allowed", HttpStatus.METHOD_NOT_ALLOWED),
	RESPONSE_TYPE_UNACCEPTABLE_CS_SCP("PPR_OFP_008", "Scheduled Payments API", "Response type provided in the accept header is not allowed", HttpStatus.NOT_ACCEPTABLE),
	SERVER_ACCESS_UNAUTHORIZED_CS_SCP("PPR_OFP_009", "Scheduled Payments API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_ACCESS_UNAUTHORIZED_CS_SCP("PPR_OFP_010", "Scheduled Payments API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_AUTH_UNAUTHORIZED_CS_SCP("PPR_OFP_011", "Scheduled Payments API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	SERVER_API_AUTH_ACCESS_CS_SCP("PPR_OFP_012", "Scheduled Payments API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTED_CS_SCP("PPR_OFP_013", "Scheduled Payments API", "Used for API resource operations which haven’t been implemented", HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWED_CS_SCP("PPR_OFP_014", "Scheduled Payments API", "Method not allowed in the Downstream system", HttpStatus.METHOD_NOT_ALLOWED),
	SERVER_REQUEST_NOT_ACCEPTABLE_CS_SCP("PPR_OFP_015", "Scheduled Payments API", "Response type provided in the accept header is not allowed in the Downstream system", HttpStatus.NOT_ACCEPTABLE),
	SERVER_UNSUPPORTED_CS_SCP("PPR_OFP_016", "Scheduled Payments API", "Unsupported media type in the Downstream system", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_REQUEST_EXCEEDED_CS_SCP("PPR_OFP_017", "Scheduled Payments API", "Rate limit exceeded while calling the Downstream system", HttpStatus.TOO_MANY_REQUESTS),
	SERVER_BAD_REQUEST_CS_SCP("PPR_OFP_018", "Scheduled Payments API", "Failure received from Downstream system", HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_DOWNSYSTEM_CS_SCP("PPR_OFP_019", "Scheduled Payments API", "Request to the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	SERVER_BAD_REQUEST_FAIL_DOWNSYSTEM_CS_SCP("PPR_OFP_020", "Scheduled Payments API", "Request from the Downstream system failed validation", HttpStatus.BAD_REQUEST),


	//---Start For Domestic Scheduled Payments consent - Current/Savings
	BAD_REQUEST_PPA_SPIPG("PPA_SPIPG_001","Domestic Scheduled Payments Consents","Bad Request (The request parameters passed are invalid)","Bad Request",HttpStatus.BAD_REQUEST), 
    SERVER_RESOURCE_NOT_FOUND_PPA_SPIPG("PPA_SPIPG_002","Domestic Scheduled Payments Consents","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
    RESOURCE_NOT_FOUND_PPS_PIPR("PPS_PIPR_003","Domestic Scheduled Payments Consents","1. URL resource not found","Resource not found",HttpStatus.NOT_FOUND),
REQUEST_TIMED_OUT_PPA_SPIPG("PPA_SPIPG_004","Domestic Scheduled Payments Consents","Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
    TECHNICAL_ERROR_PPA_SPIPG("PPA_SPIPG_999","Domestic Scheduled Payments Consents","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
UNSUPPORTED_MEDIA_TYPE_PPA_SPIPG("PPA_SPIPG_005","Domestic Scheduled Payments Consents","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
SERVER_NOT_AVAILABLE_PPA_SPIPG("PPA_SPIPG_006","Domestic Scheduled Payments Consents","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
METHOD_NOT_ALLOWED_PPA_SPIPG("PPA_SPIPG_007","Domestic Scheduled Payments Consents","Method not allowed","Method not allowed",HttpStatus.NOT_FOUND),
RESPONSE_TYPE_NOT_ACCEPTABLE_PPA_SPIPG("PPA_SPIPG_008","Domestic Scheduled Payments Consents","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
SERVER_ACCESS_UNAUTHORIZED_PPA_SPIPG("PPA_SPIPG_009","Domestic Scheduled Payments Consents","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
SERVER_ACCESS_DENIED_AUTHENTICATION_PPA_SPIPG("PPA_SPIPG_010","Domestic Scheduled Payments Consents","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
SERVER_ACCESS_FORBIDDEN_PPA_SPIPG("PPA_SPIPG_011","Domestic Scheduled Payments Consents","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
SERVER_ACCESS_DENIED_PPS_AUTHORISATION_PIPR("PPA_SPIPG_012","Domestic Scheduled Payments Consents","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
NOT_IMPLEMENTED_PPA_SPIPG("PPA_SPIPG_013","Domestic Scheduled Payments Consents","Used for API resource operations which haven't been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
SERVER_METHOD_NOT_ALLOWED_PPA_SPIPG("PPA_SPIPG_014","Domestic Scheduled Payments Consents","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.NOT_FOUND),
SERVER_REQUEST_NOT_ACCEPTABLE_PPA_SPIPG("PPA_SPIPG_015","Domestic Scheduled Payments Consents","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
SERVER_REQUEST_UNSUPPORTED_MEDIA_TYPE_PPA_SPIPG("PPA_SPIPG_016","Domestic Scheduled Payments Consents","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
SERVER_REQUEST_RATE_LIMIT_EXCEEDED_PPA_SPIPG("PPA_SPIPG_017","Domestic Scheduled Payments Consents","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
SERVER_BAD_GATEWAY_PPA_SPIPG("PPA_SPIPG_018","Domestic Scheduled Payments Consents","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
SERVER_BAD_REQUEST_PPA_SPIPG("PPA_SPIPG_019","Domestic Scheduled Payments Consents","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
SERVER_BAD_RESPONSE_PPA_SPIPG("PPA_SPIPG_020","Domestic Scheduled Payments Consents","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
BAD_REQUEST_PPA_SPIPP("PPA_SPIPP_001","Domestic Scheduled Payments Consents","The request parameters passed are invalid","Bad Request",HttpStatus.BAD_REQUEST),
SERVER_RESOURCE_NOT_FOUND_PPA_SPIPP("PPA_SPIPP_002","Domestic Scheduled Payments Consents","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
RESOURCE_NOT_FOUND_PPS_PIPC("PPS_PIPC_003","Domestic Scheduled Payments Consents","1. URL resource not found","Resource not found",HttpStatus.NOT_FOUND),
REQUEST_TIMED_OUT_PPA_SPIPP("PPA_SPIPP_004","Domestic Scheduled Payments Consents","Request timed out for FS Service","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
TECHNICAL_ERROR_PPA_SPIPP("PPA_SPIPP_999","Domestic Scheduled Payments Consents","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
UNSUPPORTED_MEDIA_TYPE_PPA_SPIPP("PPA_SPIPP_005","Domestic Scheduled Payments Consents","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
SERVER_NOT_AVAILABLE_PPA_SPIPP("PPA_SPIPP_006","Domestic Scheduled Payments Consents","Connectivity error","Server not available",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
METHOD_NOT_ALLOWED_PPA_SPIPP("PPA_SPIPP_007","Domestic Scheduled Payments Consents","Method not allowed","Method not allowed",HttpStatus.NOT_FOUND),
RESPONSE_TYPE_NOT_ACCEPTABLE_PPA_SPIPP("PPA_SPIPP_008","Domestic Scheduled Payments Consents","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
SERVER_ACCESS_UNAUTHORIZED_PPA_SPIPP("PPA_SPIPP_009","Domestic Scheduled Payments Consents","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
SERVER_ACCESS__DENIED_PPA_SPIPP("PPA_SPIPP_010","Domestic Scheduled Payments Consents","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
SERVER_ACCESS_FORBIDDEN_PPA_SPIPP("PPA_SPIPP_011","Domestic Scheduled Payments Consents","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
SERVER_ACCESS_DENIED_PPA_SPIPP("PPA_SPIPP_012","Domestic Scheduled Payments Consents","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
NOT_IMPLEMENTED_PPA_SPIPP("PPA_SPIPP_013","Domestic Scheduled Payments Consents","Used for API resource operations which haven't been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
SERVER_METHOD_NOT_ALLOWED_PPA_SPIPP("PPA_SPIPP_014","Domestic Scheduled Payments Consents","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.NOT_FOUND),
SERVER_REQUEST_NOT_ACCEPTABLE_PPA_SPIPP("PPA_SPIPP_015","Domestic Scheduled Payments Consents","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
SERVER_REQUEST_UNSUPPORTED_MEDIA_TYPE_PPA_SPIPP("PPA_SPIPP_016","Domestic Scheduled Payments Consents","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
SERVER_REQUEST_RATE_LIMIT_EXCEEDED_PPA_SPIPP("PPA_SPIPP_017","Domestic Scheduled Payments Consents","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
SERVER_BAD_GATEWAY_PPA_SPIPP("PPA_SPIPP_018","Domestic Scheduled Payments Consents","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),

SERVER_BAD_REQUEST_PPA_SPIPP("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
SERVER_BAD_REQUEST_PPS_PMPSV_1("PPA_SPIPP_019","Domestic Scheduled Payments Consents"," Bad request. Please check your request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_001"),
SERVER_BAD_REQUEST_PPS_PMPSV_2("PPA_SPIPP_019","Domestic Scheduled Payments Consents","General Error","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_003"),
SERVER_BAD_REQUEST_PPS_PMPSV_3("PPA_SPIPP_019","Domestic Scheduled Payments Consents","IBAN provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_006"),
SERVER_BAD_REQUEST_PPS_PMPSV_4("PPA_SPIPP_019","Domestic Scheduled Payments Consents","BIC provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_007"),
SERVER_BAD_REQUEST_PPS_PMPSV_5("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Invalid IBAN checksum","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_008"),
SERVER_BAD_REQUEST_PPS_PMPSV_6("PPA_SPIPP_019","Domestic Scheduled Payments Consents","BIC is not reachable","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_009"),
SERVER_BAD_REQUEST_PPS_PMPSV_7("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Account not found or closed","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_010"),
SERVER_BAD_REQUEST_PPS_PMPSV_8("PPA_SPIPP_019","Domestic Scheduled Payments Consents","BIC and IBAN do not match","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_011"),
SERVER_BAD_REQUEST_PPS_PMPSV_9("PPA_SPIPP_019","Domestic Scheduled Payments Consents","NSC provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_012"),
SERVER_BAD_REQUEST_PPS_PMPSV_10("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Account number provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_013"),
SERVER_BAD_REQUEST_PPS_PMPSV_11("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Transaction currency code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_014"),
SERVER_BAD_REQUEST_PPS_PMPSV_12("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Bank does not process payments to the requested beneficiary country","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_015"),
SERVER_BAD_REQUEST_PPS_PMPSV_13("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Bank does not deal in the requested transaction currency","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_016"),
SERVER_BAD_REQUEST_PPS_PMPSV_14("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Country code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_017"),
SERVER_BAD_REQUEST_PPS_PMPSV_15("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Account is blocked to debits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_018"),
SERVER_BAD_REQUEST_PPS_PMPSV_16("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Account is blocked to credits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_019"),
SERVER_BAD_REQUEST_PPS_PMPSV_17("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Account is blocked to All","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_020"),
SERVER_BAD_REQUEST_PPS_PMPSV_18("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Credit grade cannot be 6 or 7","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_021"),
SERVER_BAD_REQUEST_PPS_PMPSV_19("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Account cannot be dormant","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_022"),
SERVER_BAD_REQUEST_PPS_PMPSV_20("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Account cannot be lien","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_023"),
SERVER_BAD_REQUEST_PPS_PMPSV_21("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Joint account must be permitted by all the customer associated","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_024"),
SERVER_BAD_REQUEST_PPS_PMPSV_22("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Invalid payment request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_029"),
SERVER_BAD_REQUEST_PPS_PMPSV_23("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Swift address is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_030"),
SERVER_BAD_REQUEST_PPS_PMPSV_24("PPA_SPIPP_019","Domestic Scheduled Payments Consents","ABA code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_031"),
SERVER_BAD_REQUEST_PPS_PMPSV_25("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Invalid future payment date","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_032"),
SERVER_BAD_REQUEST_PPS_PMI_1("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Bad request. Please check your request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMI_001"),
SERVER_BAD_REQUEST_PPS_PMI_6("PPA_SPIPP_019","Domestic Scheduled Payments Consents","Invalid future payment date","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMI_006"),


SERVER_BAD_RESPONSE_PPA_SPIPP("PPA_SPIPP_020","Domestic Scheduled Payments Consents","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),

	/*Start for Accounts Transactions Current/Savings/ Credit Card*/

	BAD_REQUEST_PAC_TXN("PAC_TXN_001","Accounts Transactions API","Missing mandatory header value", HttpStatus.BAD_REQUEST),
	NO_TRANSACTIONS_FOUND_PAC_TXN("PAC_TXN_002","Accounts Transactions API", "Account not found", HttpStatus.NOT_FOUND),
	RESOURCE_NOT_FOUND_PAC_TXN("PAC_TXN_003","Accounts Transactions API","URL resource not found", HttpStatus.NOT_FOUND),
	REQUEST_TIMED_OUT_PAC_TXN("PAC_TXN_004","Accounts Transactions API","Request timed out for MWS Service",HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_PAC_TXN("PAC_TXN_999","Accounts Transactions API","Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_PAC_TXN("PAC_TXN_005", "Accounts Transactions API", "Unsupported media type", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SYSTEM_NOT_AVAILABLE_PAC_TXN("PAC_TXN_006","Accounts Transactions API","Downstream system not available", HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_PAC_TXN("PAC_TXN_007","Accounts Transactions API","Method not allowed", HttpStatus.METHOD_NOT_ALLOWED),
	RESPONSE_TYPE_UNACCEPTABLE_PAC_TXN("PAC_TXN_008","Accounts Transactions API","Response type provided in the accept header is not allowed", HttpStatus.NOT_ACCEPTABLE),
	SERVER_ACCESS_UNAUTHORIZED_PAC_TXN("PAC_TXN_009", "Accounts Transactions API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_ACCESS_UNAUTHORIZED_PAC_TXN("PAC_TXN_010", "Accounts Transactions API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_AUTH_UNAUTHORIZED_PAC_TXN("PAC_TXN_011", "Accounts Transactions API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	SERVER_API_AUTH_ACCESS_PAC_TXN("PAC_TXN_012", "Accounts Transactions API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTED_PAC_TXN("PAC_TXN_013", "Accounts Transactions API", "Used for API resource operations which haven’t been implemented", HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWED_PAC_TXN("PAC_TXN_014", "Accounts Transactions API", "Method not allowed in the Downstream system", HttpStatus.METHOD_NOT_ALLOWED),
	SERVER_REQUEST_NOT_ACCEPTABLE_PAC_TXN("PAC_TXN_015", "Accounts Transactions API", "Response type provided in the accept header is not allowed in the Downstream system", HttpStatus.NOT_ACCEPTABLE),
	SERVER_UNSUPPORTED_PAC_TXN("PAC_TXN_016", "Accounts Transactions API", "Unsupported media type in the Downstream system", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_REQUEST_EXCEEDED_PAC_TXN("PAC_TXN_017", "Accounts Transactions API", "Rate limit exceeded while calling the Downstream system", HttpStatus.TOO_MANY_REQUESTS),
	SERVER_BAD_REQUEST_PAC_TXN("PAC_TXN_018", "Accounts Transactions API", "Failure received from Downstream system", HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_DOWNSYSTEM_PAC_TXN("PAC_TXN_019", "Accounts Transactions API", "Request to the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	SERVER_BAD_REQUEST_FAIL_DOWNSYSTEM_PAC_TXN("PAC_TXN_020", "Accounts Transactions API", "Request from the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	REQUEST_TIMED_OUT_PAC_TXN_CREDIT("PAC_TXN_004", "Accounts Transactions API", "Request timed out for DeClick", HttpStatus.GATEWAY_TIMEOUT),
	DECLICK_SYSTEM_NOT_AVIAL_PAC_TXN_CREDIT("PAC_TXN_006","Accounts Transactions API", " Declick system not available", HttpStatus.SERVICE_UNAVAILABLE),

	/*Start for Domestic Payment Consents*/
	BAD_REQUEST_PPD_PIP("PPD_PIP_001","Domestic Payment Process - Bad Request","Bad Request (The request parameters passed are invalid)", HttpStatus.BAD_REQUEST),
	RESOURCE_NOT_FOUND_PPD_PIP("PPD_PIP_002","Domestic Payment Process - Resource not found","Resource not found", HttpStatus.NOT_FOUND),
	REQUEST_TIMED_OUT_PPD_PIP("PCC_PIP_003","Domestic Payment Process- Request timed out","Request timed out for FS Servcie", HttpStatus.REQUEST_TIMEOUT),
	TECHNICAL_ERROR_PPD_PIP("PPD_PIP_004","Domestic Payment Process- Technical Error. Please try again later","Technical Error. Please try again later",  HttpStatus.INTERNAL_SERVER_ERROR),
	DOWNSTREAM_SYSTEM_NOT_AVAILABLE_PPD_PIP("PPD_PIP__005","Domestic Payment Process- Downstream system not available","Received an invalid response from FS service", HttpStatus.BAD_GATEWAY),
	METHOD_NOT_ALLOWED_PPD_PIP("PPD_PIP_006","Domestic Payment Process-Method not allowed","Method not allowed", HttpStatus.METHOD_NOT_ALLOWED),
	RESPONSE_TYPE_UNACCEPTABLE_PPD_PIP("PPD_PIP_007","Domestic Payment Process- Response type unacceptable","Response type provided in the accept header is not allowed",  HttpStatus.NOT_ACCEPTABLE),

	/*Start for MultiAccount and Consent - Current/Savings/Credit Card*/

	BAD_REQUEST_PPR_DUP("PPR_DUP_001","MultiAccount Information API","Missing mandatory header value", HttpStatus.BAD_REQUEST),
	NO_USER_FOUND_PPR_DUP("PPR_DUP_002","MultiAccount Information API","User ID not found",HttpStatus.NOT_FOUND),
	RESOURCE_NOT_FOUND_PPR_DUP("PPR_DUP_003","MultiAccount Information API","URL resource not found", HttpStatus.NOT_FOUND),
	NO_ACOUNT_FOUND_PPR_DUP("PPR_DUP_004","MultiAccount Information API","Request timed out for Credit Card Account System (CC-No)",  HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_PPR_DUP("PPR_DUP_999","MultiAccount Information API","Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_PPR_DUP("PPR_DUP_005", "MultiAccount Information API", "Unsupported media type", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SYSTEM_NOT_AVAILABLE_PPR_DUP("PPR_DUP_006","MultiAccount Information API","Credit Card Account System (CC-No) not available", HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_PPR_DUP("PPR_DUP_007","MultiAccount Information API","Method not allowed", HttpStatus.METHOD_NOT_ALLOWED),
	RESPONSE_TYPE_UNACCEPTABLE_PPR_DUP("PPR_DUP_008","MultiAccount Information API","Response type provided in the accept header is not allowed", HttpStatus.NOT_ACCEPTABLE),
	SERVER_ACCESS_UNAUTHORIZED_PPR_DUP("PPR_DUP_009", "MultiAccount Information API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_ACCESS_UNAUTHORIZED_PPR_DUP("PPR_DUP_010", "MultiAccount Information API", "API authentication failed in the Downstream system", HttpStatus.UNAUTHORIZED),
	SERVER_API_AUTH_UNAUTHORIZED_PPR_DUP("PPR_DUP_011", "MultiAccount Information API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	SERVER_API_AUTH_ACCESS_PPR_DUP("PPR_DUP_012", "MultiAccount Information API", "API authorisation failed in the Downstream system", HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTED_PPR_DUP("PPR_DUP_013", "MultiAccount Information API", "Used for API resource operations which haven’t been implemented", HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWED_PPR_DUP("PPR_DUP_014", "MultiAccount Information API", "Method not allowed in the Downstream system", HttpStatus.METHOD_NOT_ALLOWED),
	SERVER_REQUEST_NOT_ACCEPTABLE_PPR_DUP("PPR_DUP_015", "MultiAccount Information API", "Response type provided in the accept header is not allowed in the Downstream system", HttpStatus.NOT_ACCEPTABLE),
	SERVER_UNSUPPORTED_PPR_DUP("PPR_DUP_016", "MultiAccount Information API", "Unsupported media type in the Downstream system", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_REQUEST_EXCEEDED_PPR_DUP("PPR_DUP_017", "MultiAccount Information API", "Rate limit exceeded while calling the Downstream system", HttpStatus.TOO_MANY_REQUESTS),
	SERVER_BAD_REQUEST_PPR_DUP("PPR_DUP_018", "MultiAccount Information API", "Failure received from Downstream system", HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_DOWNSYSTEM_PPR_DUP("PPR_DUP_019", "MultiAccount Information API", "Request to the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	SERVER_BAD_REQUEST_FAIL_DOWNSYSTEM_PPR_DUP("PPR_DUP_020", "MultiAccount Information API", "Request from the Downstream system failed validation", HttpStatus.BAD_REQUEST),
	SERVER_PARTY_LOCKED_CHANNEL("PPR_DUP_502", "MultiAccount Information API", "Digital user locked in channel", HttpStatus.FORBIDDEN),

    /*Start for Domestic Payment Submission*/
	BAD_REQUEST_PPA_DPINP("PPA_DPINP_001","Domestic Payment Submission Process - Bad Request","Bad Request (The request parameters passed are invalid)","Bad Request", HttpStatus.BAD_REQUEST),
	SERVER_RESOURCE_NOT_FOUND_PPA_DPINP("PPA_DPINP_002","Domestic Payment Submission Process - Server resource not found","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
  //RESOURCE_NOT_FOUND_PPA_DPINP("PPA_DPINP_003","Domestic Payment Submission Process - Resource not found","Resource not found","Resource not found",HttpStatus.NOT_FOUND),
	REQUEST_TIMEDOUT_PPA_DPINP("PPA_DPINP_004","Domestic Payment Submission Process - Request timed out", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_PPA_DPINP("PPA_DPINP_999","Domestic Payment Submission Process - Technical Error. Please try again later","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_TYPE_PPA_DPINP("PPA_DPINP_005","Domestic Payment Submission Process - Unsupported media type","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVICE_UNAVAILABLE_PPA_DPINP("PPA_DPINP_006","Domestic Payment Submission Process - Server not available","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_PPA_DPINP("PPA_DPINP_007","Domestic Payment Submission Process - Method not allowed","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	NOT_ACCEPTABLE_PPA_DPINP("PPA_DPINP_008","Domestic Payment Submission Process - Response type not acceptable","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
	UNAUTHORIZED_PPA_DPINP("PPA_DPINP_009","Domestic Payment Submission Process - Server Access Unauthorized","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
	ACCESS_DENIED_PPA_DPINP("PPA_DPINP_010","Domestic Payment Submission Process - Server Access Denied","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	FORBIDDEN_PPA_DPINP("PPA_DPINP_011","Domestic Payment Submission Process - Server Access Forbidden","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
	FORBIDDEN_ACCESS_DENIED_PPA_DPINP("PPA_DPINP_012","Domestic Payment Submission Process - Server Access Denied","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTED_PPA_DPINP("PPA_DPINP_013","Domestic Payment Submission Process - Not Implemented","Used for API resource operations which haven't been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWED_PPA_DPINP("PPA_DPINP_014","Domestic Payment Submission Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	REQUEST_NOT_ACCEPTABLE_PPA_DPINP("PPA_DPINP_015","Domestic Payment Submission Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
	MEDIA_TYPE_UNSUPPORTED_PPA_DPINP("PPA_DPINP_016","Domestic Payment Submission Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	TOO_MANY_REQUESTS_PPA_DPINP("PPA_DPINP_017","Domestic Payment Submission Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
	BAD_GATEWAY_PPA_DPINP("PPA_DPINP_018","Domestic Payment Submission Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_PPA_DPINP("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
	//Errors produced from Validate System API (PMV): 
	SERVER_BAD_REQUEST_PPA_DPINP_v1("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Bad request. Please check your request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_001"),
	SERVER_BAD_REQUEST_PPA_DPINP_v4("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","General Error","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_004"),
	SERVER_BAD_REQUEST_PPA_DPINP_v6("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","IBAN length is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_006"),
	SERVER_BAD_REQUEST_PPA_DPINP_v7("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","IBAN does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_007"),
	SERVER_BAD_REQUEST_PPA_DPINP_v8("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","IBAN checksum is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_008"),
	SERVER_BAD_REQUEST_PPA_DPINP_v9("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","BIC does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_009"),
	SERVER_BAD_REQUEST_PPA_DPINP_v10("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","BIC is not reachable","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_010"),
	SERVER_BAD_REQUEST_PPA_DPINP_v11("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","BIC and IBAN do not match","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_011"),
	SERVER_BAD_REQUEST_PPA_DPINP_v12("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","NSC is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_012"),
	SERVER_BAD_REQUEST_PPA_DPINP_v13("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account Number is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_013"),
	SERVER_BAD_REQUEST_PPA_DPINP_v14("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Currency is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_014"),
	SERVER_BAD_REQUEST_PPA_DPINP_v15("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account does not exist","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_015"),
	SERVER_BAD_REQUEST_PPA_DPINP_v16("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account status must be active","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_016"),
	SERVER_BAD_REQUEST_PPA_DPINP_v17("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account Type is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_017"),
	SERVER_BAD_REQUEST_PPA_DPINP_v18("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account is blocked to debits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_018"),
	SERVER_BAD_REQUEST_PPA_DPINP_v19("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account is blocked to credits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_019"),
	SERVER_BAD_REQUEST_PPA_DPINP_v20("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account is blocked to All","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_020"),
	SERVER_BAD_REQUEST_PPA_DPINP_v21("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Credit grade cannot be 6 or 7","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_021"),
	SERVER_BAD_REQUEST_PPA_DPINP_v22("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account cannot be dormant","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_022"),
	SERVER_BAD_REQUEST_PPA_DPINP_v23("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account cannot be lien","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_023"),
	SERVER_BAD_REQUEST_PPA_DPINP_v24("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Joint account must be permitted by all the customer associated","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_024"),
	SERVER_BAD_REQUEST_PPA_DPINP_v25("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account does not have enough funds","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_025"),
	SERVER_BAD_REQUEST_PPA_DPINP_v26("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Limit exceeded for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_026"),
	SERVER_BAD_REQUEST_PPA_DPINP_v27("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Limit exceeded on this payment type for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_027"),
	SERVER_BAD_REQUEST_PPA_DPINP_v28("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Limit exceeded for this agent","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_028"),
	SERVER_BAD_REQUEST_PPA_DPINP_v29("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Invalid payment request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_029"),
	SERVER_BAD_REQUEST_PPA_DPINP_v30("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Swift address is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_030"),
	SERVER_BAD_REQUEST_PPA_DPINP_v31("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","ABA code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_031"),
	SERVER_BAD_REQUEST_PPA_DPINP_v32("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Country is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_032"),
	SERVER_BAD_REQUEST_PPA_DPINP_v33("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Country rules limit exceeded","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_033"),
	SERVER_BAD_REQUEST_PPA_DPINP_v34("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","User id is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_034"),
	SERVER_BAD_REQUEST_PPA_DPINP_v35("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Jurisdiction code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_035"),
	SERVER_BAD_REQUEST_PPA_DPINP_v36("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","CIS id is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_036"),
	SERVER_BAD_REQUEST_PPA_DPINP_v37("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","The payer and the payee account cannot be the same","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_037"),
	SERVER_BAD_REQUEST_PPA_DPINP_v38("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","BIC provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_038"),
	
	//Error codes produced by Execute System API (PME):
	SERVER_BAD_REQUEST_PPA_DPINP_e1("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Bad request. Please check your request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_001"),
	SERVER_BAD_REQUEST_PPA_DPINP_e4("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","General Error","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_004"),
	SERVER_BAD_REQUEST_PPA_DPINP_e6("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","IBAN length is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_006"),
	SERVER_BAD_REQUEST_PPA_DPINP_e7("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","IBAN does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_007"),
	SERVER_BAD_REQUEST_PPA_DPINP_e8("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","IBAN checksum is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_008"),
	SERVER_BAD_REQUEST_PPA_DPINP_e9("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","BIC does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_009"),
	SERVER_BAD_REQUEST_PPA_DPINP_e10("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","BIC is not reachable","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_010"),
	SERVER_BAD_REQUEST_PPA_DPINP_e11("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","BIC and IBAN do not match","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_011"),
	SERVER_BAD_REQUEST_PPA_DPINP_e12("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","NSC is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_012"),
	SERVER_BAD_REQUEST_PPA_DPINP_e13("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account Number is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_013"),
	SERVER_BAD_REQUEST_PPA_DPINP_e14("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Currency is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_014"),
	SERVER_BAD_REQUEST_PPA_DPINP_e15("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account does not exist","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_015"),
	SERVER_BAD_REQUEST_PPA_DPINP_e16("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account status must be active","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_016"),
	SERVER_BAD_REQUEST_PPA_DPINP_e17("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account Type is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_017"),
	SERVER_BAD_REQUEST_PPA_DPINP_e18("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account is blocked to debits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_018"),
	SERVER_BAD_REQUEST_PPA_DPINP_e19("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account is blocked to credits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_019"),
	SERVER_BAD_REQUEST_PPA_DPINP_e20("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account is blocked to All","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_020"),
	SERVER_BAD_REQUEST_PPA_DPINP_e21("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Credit grade cannot be 6 or 7","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_021"),
	SERVER_BAD_REQUEST_PPA_DPINP_e22("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account cannot be dormant","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_022"),
	SERVER_BAD_REQUEST_PPA_DPINP_e23("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account cannot be lien","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_023"),
	SERVER_BAD_REQUEST_PPA_DPINP_e24("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Joint account must be permitted by all the customer associated","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_024"),
	SERVER_BAD_REQUEST_PPA_DPINP_e25("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account does not have enough funds","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_025"),
	SERVER_BAD_REQUEST_PPA_DPINP_e26("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Limit exceeded for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_026"),
	SERVER_BAD_REQUEST_PPA_DPINP_e27("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Limit exceeded on this payment type for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_027"),
	SERVER_BAD_REQUEST_PPA_DPINP_e28("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Limit exceeded for this agent","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_028"),
	SERVER_BAD_REQUEST_PPA_DPINP_e29("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Invalid payment request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_029"),
	SERVER_BAD_REQUEST_PPA_DPINP_e30("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Swift address is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_030"),
	SERVER_BAD_REQUEST_PPA_DPINP_e31("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","ABA code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_031"),
	SERVER_BAD_REQUEST_PPA_DPINP_e32("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Country is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_032"),
	SERVER_BAD_REQUEST_PPA_DPINP_e33("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Country rules limit exceeded","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_033"),
	SERVER_BAD_REQUEST_PPA_DPINP_e34("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","User id is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_034"),
	SERVER_BAD_REQUEST_PPA_DPINP_e35("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Jurisdiction code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_035"),
	SERVER_BAD_REQUEST_PPA_DPINP_e36("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","CIS id is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_036"),
	SERVER_BAD_REQUEST_PPA_DPINP_e37("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","The payer and the payee account cannot be the same","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_037"),
	SERVER_BAD_REQUEST_PPA_DPINP_e38("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Payment rejected due to the suspicion of fraud","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_038"),
	
	//Sprint 24 error mapping sheet changes
	SERVER_BAD_REQUEST_PPA_DPINP_e40("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","No limit defined for customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_040"),
	SERVER_BAD_REQUEST_PPA_DPINP_e41("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Customer is not active","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_041"),
	SERVER_BAD_REQUEST_PPA_DPINP_e42("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","User is not active","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_042"),
	SERVER_BAD_REQUEST_PPA_DPINP_e43("PPA_DPINP_019","Domestic Payment Submission Process - Server Bad Request","Account does not exist for customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_043"),
	
	SERVER_BAD_RESPONSE_PPA_DPINP("PPA_DPINP_020","Domestic Payment Submission Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
    
	/*Start for Domestic Payment retrieve*/
	BAD_REQUEST_PPA_DPING("PPA_DPING_001","Domestic Payment Retrieve Process - Bad Request","Bad Request (The request parameters passed are invalid)","Bad Request", HttpStatus.BAD_REQUEST),
	SERVER_RESOURCE_NOT_FOUND_PPA_DPING("PPA_DPING_002","Domestic Payment Retrieve Process - Server resource not found","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
  //RESOURCE_NOT_FOUND_PPA_DPING("PPA_DPING_003","Domestic Payment Retrieve Process - Resource not found","Resource not found","Resource not found",HttpStatus.NOT_FOUND),
	REQUEST_TIMEDOUT_PPA_DPING("PPA_DPING_004","Domestic Payment Retrieve Process - Request timed out", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_PPA_DPING("PPA_DPING_999","Domestic Payment Retrieve Process - Technical Error. Please try again later","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_TYPE_PPA_DPING("PPA_DPING_005","Domestic Payment Retrieve Process - Unsupported media type","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVICE_UNAVAILABLE_PPA_DPING("PPA_DPING_006","Domestic Payment Retrieve Process - Server not available","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_PPA_DPING("PPA_DPING_007","Domestic Payment Retrieve Process - Method not allowed","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	NOT_ACCEPTABLE_PPA_DPING("PPA_DPING_008","Domestic Payment Retrieve Process - Response type not acceptable","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
	UNAUTHORIZED_PPA_DPING("PPA_DPING_009","Domestic Payment Retrieve Process - Server Access Unauthorized","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
	ACCESS_DENIED_PPA_DPING("PPA_DPING_010","Domestic Payment Retrieve Process - Server Access Denied","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	FORBIDDEN_PPA_DPING("PPA_DPING_011","Domestic Payment Retrieve Process - Server Access Forbidden","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
	FORBIDDEN_ACCESS_DENIED_PPA_DPING("PPA_DPING_012","Domestic Payment Retrieve Process - Server Access Denied","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTED_PPA_DPING("PPA_DPING_013","Domestic Payment Retrieve Process - Not Implemented","Used for API resource operations which haven't been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWED_PPA_DPING("PPA_DPING_014","Domestic Payment Retrieve Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	REQUEST_NOT_ACCEPTABLE_PPA_DPING("PPA_DPING_015","Domestic Payment Retrieve Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
	MEDIA_TYPE_UNSUPPORTED_PPA_DPING("PPA_DPING_016","Domestic Payment Retrieve Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	TOO_MANY_REQUESTS_PPA_DPING("PPA_DPING_017","Domestic Payment Retrieve Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
	BAD_GATEWAY_PPA_DPING("PPA_DPING_018","Domestic Payment Retrieve Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_PPA_DPING("PPA_DPING_019","Domestic Payment Retrieve Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
	SERVER_BAD_RESPONSE_PPA_DPING("PPA_DPING_020","Domestic Payment Retrieve Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
	
	//start for Domestic Payments consends Pisp Post*/
	BAD_REQUEST_PPA_DPIPP("PPA_DPIPP_001","Domestic Payment API","Bad Request (The request parameters passed are invalid)","Bad Request",HttpStatus.BAD_REQUEST),

    SERVER_RESOURCE_NOT_FOUND_PPA_DPIPP("PPA_DPIPP_002","Domestic Payment API","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),

    RESOURCE_NOT_FOUND_PPA_DPIPP("PPA_DPIPP_003","Domestic Payment API","URL resource not found","Resource not found",HttpStatus.NOT_FOUND),

    REQUEST_TIMEDOUT_PPA_DPIPP("PPA_DPIPP_004","Domestic Payment API", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),

    TECHNICAL_ERROR_PPA_DPIPP("PPA_DPIPP_999","Domestic Payment API","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),

    UNSUPPORTED_MEDIA_TYPE_PPA_DPIPP("PPA_DPIPP_005","Domestic Payment API","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),

    SERVICE_UNAVAILABLE_PPA_DPIPP("PPA_DPIPP_006","Domestic Payment API","Connectivity errors","Server not available",HttpStatus.SERVICE_UNAVAILABLE),

    METHOD_NOT_ALLOWED_PPA_DPIPP("PPA_DPIPP_007","Domestic Payment API","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),

    NOT_ACCEPTABLE_PPA_DPIPP("PPA_DPIPP_008","Domestic Payment API","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),

    UNAUTHORIZED_PPA_DPIPP("PPA_DPIPP_009","Domestic Payment API","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),

    ACCESS_DENIED_PPA_DPIPP("PPA_DPIPP_010","Domestic Payment API","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),

    FORBIDDEN_PPA_DPIPP("PPA_DPIPP_011","Domestic Payment API","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),

    FORBIDDEN_ACCESS_DENIED_PPA_DPIPP("PPA_DPIPP_012","Domestic Payment API","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),

    NOT_IMPLEMENTED_PPA_DPIPP("PPA_DPIPP_013","Domestic Payment API","Used for API resource operations which haven’t been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),

    SERVER_METHOD_NOT_ALLOWED_PPA_DPIPP("PPA_DPIPP_014","Domestic Payment Update Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),

    REQUEST_NOT_ACCEPTABLE_PPA_DPIPP("PPA_DPIPP_015","Domestic Payment Update Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),

    MEDIA_TYPE_UNSUPPORTED_PPA_DPIPP("PPA_DPIPP_016","Domestic Payment Update Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),

    TOO_MANY_REQUESTS_PPA_DPIPP("PPA_DPIPP_017","Domestic Payment Update Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),

    BAD_GATEWAY_PPA_DPIPP("PPA_DPIPP_018","Domestic Payment Update Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),

    SERVER_BAD_REQUEST_PPA_DPIPP("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
  //Errors produced during pre-stage validation system API checks/actions (PMPSV): 
    SERVER_BAD_REQUEST_PPA_DPIPP_1("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Bad request. Please check your request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_001"),
    SERVER_BAD_REQUEST_PPA_DPIPP_3("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","General Error","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_003"),
    SERVER_BAD_REQUEST_PPA_DPIPP_6("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","IBAN provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_006"),
    SERVER_BAD_REQUEST_PPA_DPIPP_7("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","BIC provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_007"),
    SERVER_BAD_REQUEST_PPA_DPIPP_8("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Invalid IBAN checksum","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_008"),
    SERVER_BAD_REQUEST_PPA_DPIPP_9("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","BIC is not reachable","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_009"),
    SERVER_BAD_REQUEST_PPA_DPIPP_10("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Account not found","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_010"),
    SERVER_BAD_REQUEST_PPA_DPIPP_11("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","BIC and IBAN do not match","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_011"),
    SERVER_BAD_REQUEST_PPA_DPIPP_12("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","NSC provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_012"),
    SERVER_BAD_REQUEST_PPA_DPIPP_13("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Account number provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_013"),
    SERVER_BAD_REQUEST_PPA_DPIPP_14("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Transaction currency code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_014"),
    SERVER_BAD_REQUEST_PPA_DPIPP_15("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Bank does not process payments to the requested beneficiary country","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_015"),
    SERVER_BAD_REQUEST_PPA_DPIPP_16("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Bank does not deal in the requested transaction currency","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_016"),
    SERVER_BAD_REQUEST_PPA_DPIPP_17("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Country code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_017"),
    SERVER_BAD_REQUEST_PPA_DPIPP_18("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Account is blocked to debits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_018"),
    SERVER_BAD_REQUEST_PPA_DPIPP_19("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Account is blocked to credits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_019"),
    SERVER_BAD_REQUEST_PPA_DPIPP_20("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Account is blocked to All","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_020"),
    SERVER_BAD_REQUEST_PPA_DPIPP_21("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Credit grade cannot be 6 or 7","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_021"),
    SERVER_BAD_REQUEST_PPA_DPIPP_22("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Account cannot be dormant","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_022"),
    SERVER_BAD_REQUEST_PPA_DPIPP_23("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Account cannot be lien","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_023"),
    SERVER_BAD_REQUEST_PPA_DPIPP_24("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Joint account must be permitted by all the customer associated","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_024"),
    SERVER_BAD_REQUEST_PPA_DPIPP_29("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Invalid payment request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_029"),
    SERVER_BAD_REQUEST_PPA_DPIPP_30("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Swift address is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_030"),
    SERVER_BAD_REQUEST_PPA_DPIPP_31("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","ABA code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_031"),
	SERVER_BAD_REQUEST_PPA_DPIPP_35("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Unable to derive BIC from IBAN as national id does not exist. Please correct payee IBAN","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_035"),
    SERVER_BAD_REQUEST_PPA_DPIPP_36("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Payee address format invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMPSV_036"),
    SERVER_BAD_REQUEST_PPA_DPIPP_32("PPA_DPIPP_019","Domestic Payment Update Process - Server Bad Request","Bad request. Please check your request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMI_001"),
    SERVER_BAD_RESPONSE_PPA_DPIPP("PPA_DPIPP_020","Domestic Payment Update Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
	/*Start for Domestic Payment Update*/
	BAD_REQUEST_PPA_DPINR("PPA_DPINR_001","Domestic Payment Update Process - Bad Request","Bad Request (The request parameters passed are invalid)","Bad Request", HttpStatus.BAD_REQUEST),
	RESOURCE_NOT_FOUND_PPA_DPINR("PPA_DPINR_002","Domestic Payment Update Process - Server resource not found","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
  //SERVER_RESOURCE_NOT_FOUND_PPA_DPINR("PPA_DPINR_003","Domestic Payment Update Process - Resource not found","Resource not found","Resource not found",HttpStatus.NOT_FOUND),
	REQUEST_TIMEDOUT_PPA_DPINR("PPA_DPINR_004","Domestic Payment Update Process - Request timed out", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_PPA_DPINR("PPA_DPINR_999","Domestic Payment Update Process - Technical Error. Please try again later","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_TYPE_PPA_DPINR("PPA_DPINR_005","Domestic Payment Update Process - Unsupported media type","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVICE_UNAVAILABLE_PPA_DPINR("PPA_DPINR_006","Domestic Payment Update Process - Server not available","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_PPA_DPINR("PPA_DPINR_007","Domestic Payment Update Process - Method not allowed","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	NOT_ACCEPTABLE_PPA_DPINR("PPA_DPINR_008","Domestic Payment Update Process - Response type not acceptable","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
	UNAUTHORIZED_PPA_DPINR("PPA_DPINR_009","Domestic Payment Update Process - Server Access Unauthorized","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
	ACCESS_DENIED_PPA_DPINR("PPA_DPINR_010","Domestic Payment Update Process - Server Access Denied","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	FORBIDDEN_PPA_DPINR("PPA_DPINR_011","Domestic Payment Update Process - Server Access Forbidden","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
	FORBIDDEN_ACCESS_DENIED_PPA_DPINR("PPA_DPINR_012","Domestic Payment Update Process - Server Access Denied","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTED_PPA_DPINR("PPA_DPINR_013","Domestic Payment Update Process - Not Implemented","Used for API resource operations which haven't been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWED_PPA_DPINR("PPA_DPINR_014","Domestic Payment Update Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	REQUEST_NOT_ACCEPTABLE_PPA_DPINR("PPA_DPINR_015","Domestic Payment Update Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
	MEDIA_TYPE_UNSUPPORTED_PPA_DPINR("PPA_DPINR_016","Domestic Payment Update Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	TOO_MANY_REQUESTS_PPA_DPINR("PPA_DPINR_017","Domestic Payment Update Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
	BAD_GATEWAY_PPA_DPINR("PPA_DPINR_018","Domestic Payment Update Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_PPA_DPINR("PPA_DPINR_019","Domestic Payment Update Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
	SERVER_BAD_RESPONSE_PPA_DPINR("PPA_DPINR_020","Domestic Payment Update Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
	//Start for Standing Orders GET
		BAD_REQUEST_PPSO_PIPR("PPSO_PIPR_001","Domestic StandingOrders Consents Process - Bad Request","Bad Request (The request parameters passed are invalid)","Bad Request",HttpStatus.BAD_REQUEST),
	    SERVER_RESOURCE_NOT_FOUND_PPSO_PIPR("PPSO_PIPR_002","Domestic StandingOrders Consents Process - Resource not found","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
	    RESOURCE_NOT_FOUND_PPSO_PIPR("PPSO_PIPR_003","Domestic StandingOrders Consents Process - Resource not found","URL Resource not found","Resource not found",HttpStatus.NOT_FOUND),
	    REQUEST_TIMEDOUT_PPSO_PIPR("PPSO_PIPR_004","Domestic StandingOrders Consents Process - Request timed out", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
	    TECHNICAL_ERROR_PPSO_PIPR("PPSO_PIPR_999","Domestic StandingOrders Consents Process - Technical Error. Please try again later","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
	    UNSUPPORTED_MEDIA_TYPE_PPSO_PIPR("PPSO_PIPR_005","Domestic StandingOrders Consents Process - Unsupported media type","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	    SERVICE_UNAVAILABLE_PPSO_PIPR("PPSO_PIPR_006","Domestic StandingOrders Consents Process - Server not available","Received an invalid response from FS service.","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
	    METHOD_NOT_ALLOWED_PPSO_PIPR("PPSO_PIPR_007","Domestic StandingOrders Consents Process - Method not allowed","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	    NOT_ACCEPTABLE_PPSO_PIPR("PPSO_PIPR_008","Domestic StandingOrders Consents Process - Response type not acceptable","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
	    UNAUTHORIZED_PPSO_PIPR("PPSO_PIPR_009","Domestic StandingOrders Consents Process - Server Access Unauthorized","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
	    ACCESS_DENIED_PPSO_PIPR("PPSO_PIPR_010","Domestic StandingOrders Consents Process - Server Access Denied","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	    FORBIDDEN_PPSO_PIPR("PPSO_PIPR_011","Domestic StandingOrders Consents Process - Server Access Forbidden","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
	    FORBIDDEN_ACCESS_DENIED_PPSO_PIPR("PPSO_PIPR_012","Domestic StandingOrders Consents Process - Server Access Denied","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
	    NOT_IMPLEMENTED_PPSO_PIPR("PPSO_PIPR_013","Domestic StandingOrders Consents Process - Not Implemented","Used for API resource operations which haven't been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
	    SERVER_METHOD_NOT_ALLOWED_PPSO_PIPR("PPSO_PIPR_014","Domestic StandingOrders Consents Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	    REQUEST_NOT_ACCEPTABLE_PPSO_PIPR("PPSO_PIPR_015","Domestic StandingOrders Consents Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
	    MEDIA_TYPE_UNSUPPORTED_PPSO_PIPR("PPSO_PIPR_016","Domestic StandingOrders Consents Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	    TOO_MANY_REQUESTS_PPSO_PIPR("PPSO_PIPR_017","Domestic StandingOrders Consents Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
	    BAD_GATEWAY_PPSO_PIPR("PPSO_PIPR_018","Domestic StandingOrders Consents Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
	    SERVER_BAD_REQUEST_PPSO_PIPR("PPSO_PIPR_019","Domestic StandingOrders Consents Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
	    SERVER_BAD_RESPONSE_PPSO_PIPR("PPSO_PIPR_020","Domestic StandingOrders Consents Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
    /*Start for Domestic Payment Validation*/
	    BAD_REQUEST_PPA_DPIPVP("PPA_DPIPVP_001","Domestic Payment Validation Process"," Bad request. Please check your request","Bad Request",HttpStatus.BAD_REQUEST),

	       BAD_REQUEST_PPA_DPIPVP_1("PPA_DPIPVP_019","Domestic Payment Validation Process","Bad request. Please check your request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_001"),

	    BAD_REQUEST_PPA_DPIPVP_2("PPA_DPIPVP_019","Domestic Payment Validation Process","General Error","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_004"),

	    BAD_REQUEST_PPA_DPIPVP_3("PPA_DPIPVP_019","Domestic Payment Validation Process","Payee IBAN length is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_006"),

	    BAD_REQUEST_PPA_DPIPVP_4("PPA_DPIPVP_019","Domestic Payment Validation Process","Payee IBAN does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_007"),

	    BAD_REQUEST_PPA_DPIPVP_5("PPA_DPIPVP_019","Domestic Payment Validation Process","Payee IBAN checksum is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_008"),

	    BAD_REQUEST_PPA_DPIPVP_6("PPA_DPIPVP_019","Domestic Payment Validation Process","Payee BIC does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_009"),

	    BAD_REQUEST_PPA_DPIPVP_7("PPA_DPIPVP_019","Domestic Payment Validation Process","Payee BIC is not reachable","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_010"),

	    BAD_REQUEST_PPA_DPIPVP_8("PPA_DPIPVP_019","Domestic Payment Validation Process","Payee BIC and IBAN do not match","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_011"),

	    BAD_REQUEST_PPA_DPIPVP_9("PPA_DPIPVP_019","Domestic Payment Validation Process","Payer NSC is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_012"),

	    BAD_REQUEST_PPA_DPIPVP_10("PPA_DPIPVP_019","Domestic Payment Validation Process","Payee Account Number is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_013"),

	    BAD_REQUEST_PPA_DPIPVP_11("PPA_DPIPVP_019","Domestic Payment Validation Process","Currency is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_014"),

	    BAD_REQUEST_PPA_DPIPVP_12("PPA_DPIPVP_019","Domestic Payment Validation Process","Payee Account does not exist","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_015"),

	    BAD_REQUEST_PPA_DPIPVP_13("PPA_DPIPVP_019","Domestic Payment Validation Process","Payee Account status must be active","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_016"),

	    BAD_REQUEST_PPA_DPIPVP_14("PPA_DPIPVP_019","Domestic Payment Validation Process","Payer Account Type is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_017"),

	    BAD_REQUEST_PPA_DPIPVP_15("PPA_DPIPVP_019","Domestic Payment Validation Process","Payer Account is blocked to debits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_018"),

	    BAD_REQUEST_PPA_DPIPVP_16("PPA_DPIPVP_019","Domestic Payment Validation Process","Payer Account is blocked to credits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_019"),

	    BAD_REQUEST_PPA_DPIPVP_17("PPA_DPIPVP_019","Domestic Payment Validation Process","Payer Account is blocked to All","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_020"),

	    BAD_REQUEST_PPA_DPIPVP_18("PPA_DPIPVP_019","Domestic Payment Validation Process","Payer Credit grade cannot be 6 or 7","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_021"),

	    BAD_REQUEST_PPA_DPIPVP_19("PPA_DPIPVP_019","Domestic Payment Validation Process","Payer Account cannot be dormant","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_022"),

	    BAD_REQUEST_PPA_DPIPVP_20("PPA_DPIPVP_019","Domestic Payment Validation Process","Payee Account cannot be lien","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_023"),

	    BAD_REQUEST_PPA_DPIPVP_21("PPA_DPIPVP_019","Domestic Payment Validation Process","Payer Joint account must be permitted by all the customer associated","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_024"),

	    BAD_REQUEST_PPA_DPIPVP_22("PPA_DPIPVP_019","Domestic Payment Validation Process","Payer Account does not have enough funds","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_025"),

	    BAD_REQUEST_PPA_DPIPVP_23("PPA_DPIPVP_019","Domestic Payment Validation Process","Payer Limit exceeded for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_026"),

	    BAD_REQUEST_PPA_DPIPVP_24("PPA_DPIPVP_019","Domestic Payment Validation Process","Payer Limit exceeded on this payment type for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_027"),

	    BAD_REQUEST_PPA_DPIPVP_25("PPA_DPIPVP_019","Domestic Payment Validation Process","Limit exceeded for this agent","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_028"),

	    BAD_REQUEST_PPA_DPIPVP_26("PPA_DPIPVP_019","Domestic Payment Validation Process","Invalid payment request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_029"),

	    BAD_REQUEST_PPA_DPIPVP_27("PPA_DPIPVP_019","Domestic Payment Validation Process","Swift address is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_030"),

	    BAD_REQUEST_PPA_DPIPVP_28("PPA_DPIPVP_019","Domestic Payment Validation Process","ABA code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_031"),

	    BAD_REQUEST_PPA_DPIPVP_29("PPA_DPIPVP_019","Domestic Payment Validation Process","Country is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_032"),

	    BAD_REQUEST_PPA_DPIPVP_30("PPA_DPIPVP_019","Domestic Payment Validation Process","Country rules limit exceeded","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_033"),

	    BAD_REQUEST_PPA_DPIPVP_31("PPA_DPIPVP_019","Domestic Payment Validation Process","User id is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_034"),

	    BAD_REQUEST_PPA_DPIPVP_32("PPA_DPIPVP_019","Domestic Payment Validation Process","Jurisdiction code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_035"),

	    BAD_REQUEST_PPA_DPIPVP_33("PPA_DPIPVP_019","Domestic Payment Validation Process","CIS id is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_036"),

	    BAD_REQUEST_PPA_DPIPVP_34("PPA_DPIPVP_019","Domestic Payment Validation Process","The payer and the payee account cannot be the same","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_037"),

	    BAD_REQUEST_PPA_DPIPVP_35("PPA_DPIPVP_019","Domestic Payment Validation Process","BIC provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_038"),
	    
	    //0.56 Error Mapping Sheet
	    
	    BAD_REQUEST_PPA_DPIPVP_36("PPA_DPIPVP_019","Domestic Payment Validation Process","Payment to beneficiary country not supported","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_040"),
	    
	    BAD_REQUEST_PPA_DPIPVP_37("PPA_DPIPVP_019","Domestic Payment Validation Process","Payment is rejected due to panel limit exceeded","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_041"),
	    
	    BAD_REQUEST_PPA_DPIPVP_38("PPA_DPIPVP_019","Domestic Payment Validation Process","Payment is rejected due to customer limit exceeded","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_042"),

	    BAD_REQUEST_PPA_DPIPVP_39("PPA_DPIPVP_019","Domestic Payment Validation Process","Payment made outside business hours","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_043"),
	    
	    BAD_REQUEST_PPA_DPIPVP_40("PPA_DPIPVP_019","Domestic Payment Validation Process","Invalid date from BOL payment customer limit","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_044"),
	    
	    BAD_REQUEST_PPA_DPIPVP_41("PPA_DPIPVP_019","Domestic Payment Validation Process","User doesnt have payment authorisation permissons","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_045"),
	    
	    BAD_REQUEST_PPA_DPIPVP_42("PPA_DPIPVP_019","Domestic Payment Validation Process","CustomerId/UserId is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_046"),
	    
	    BAD_REQUEST_PPA_DPIPVP_43("PPA_DPIPVP_019","Domestic Payment Validation Process","Invalid payment type","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_047"),
	    
	    BAD_REQUEST_PPA_DPIPVP_44("PPA_DPIPVP_019","Domestic Payment Validation Process","Invalid payment date format","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_048"),
	    
	    BAD_REQUEST_PPA_DPIPVP_45("PPA_DPIPVP_019","Domestic Payment Validation Process","No records found for PaymentDate/PaymentType/Currency","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_049"),
	    
	    BAD_REQUEST_PPA_DPIPVP_46("PPA_DPIPVP_019","Domestic Payment Validation Process","Unable to derive BIC from IBAN as national id does not exist. Please correct payee IBAN","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_050"),
	    
	    BAD_REQUEST_PPA_DPIPVP_47("PPA_DPIPVP_019","Domestic Payment Validation Process","Invalid payee name","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_051"),
	    
	    BAD_REQUEST_PPA_DPIPVP_48("PPA_DPIPVP_019","Domestic Payment Validation Process","Payee address format invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_052"),

	       SERVER_RESOURCE_NOT_FOUND_PPA_DPIPVP("PPA_DPIPVP_002","Domestic Payment Validation Process - Server Resource not found","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),

	       RESOURCE_NOT_FOUND_PPA_DPIPVP("PPA_DPIPVP_003","Domestic Payment Valiation Process -Resource not found","URL resource not found","Resource not found",HttpStatus.NOT_FOUND),

	       REQUEST_TIMEDOUT_PPA_DPIPVP("PPA_DPIPVP_004","Domestic Payment Valiation Process - Request timed out", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),

	       TECHNICAL_ERROR_PPA_DPIPVP("PPA_DPIPVP_999","Domestic Payment Validation Process - Technical Error. Please try again later","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),

	       UNSUPPORTED_MEDIA_TYPE_PPA_DPIPVP("PPA_DPIPVP_005","Domestic Payment Validation Process - Unsupported media type","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),

	       SERVICE_UNAVAILABLE_PPA_DPIPVP("PPA_DPIPVP_006","Domestic Payment Validation Process - Server not available","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),

	       METHOD_NOT_ALLOWED_PPA_DPIPVP("PPA_DPIPVP_007","Domestic Payment Validation Process - Method not allowed","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),

	       NOT_ACCEPTABLE_PPA_DPIPVP("PPA_DPIPVP_008","Domestic Payment Validation Process - Response type not acceptable","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),

	       UNAUTHORIZED_PPA_DPIPVP("PPA_DPIPVP_009","Domestic Payment Validation Process - Server Access Unauthorized","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),

	       ACCESS_DENIED_PPA_DPIPVP("PPA_DPIPVP_010","Domestic Payment Validation Process - Server Access Denied","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),

	       FORBIDDEN_PPA_DPIPVP("PPA_DPIPVP_011","Domestic Payment Validation Process - Server Access Forbidden","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),

	       FORBIDDEN_ACCESS_DENIED_PPA_DPIPVP("PPA_DPIPVP_012","Domestic Payment Validation Process - Server Access Denied","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),

	       NOT_IMPLEMENTED_PPA_DPIPVP("PPA_DPIPVP_013","Domestic Payment Validation Process - Not Implemented","Used for API resource operations which haven’t been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),

	       SERVER_METHOD_NOT_ALLOWED_PPA_DPIPVP("PPA_DPIPVP_014","Domestic Payment Validation Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),

	       REQUEST_NOT_ACCEPTABLE_PPA_DPIPVP("PPA_DPIPVP_015","Domestic Payment Validation Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),

	       MEDIA_TYPE_UNSUPPORTED_PPA_DPIPVP("PPA_DPIPVP_016","Domestic Payment Validation Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),

	       TOO_MANY_REQUESTS_PPA_DPIPVP("PPA_DPIPVP_017","Domestic Payment Validation Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),

	       BAD_GATEWAY_PPA_DPIPVP("PPA_DPIPVP_018","Domestic Payment Validation Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),

	       SERVER_BAD_REQUEST_PPA_DPIPVP("PPA_DPIPVP_019","Domestic Payment Validation Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),

	       SERVER_BAD_RESPONSE_PPA_DPIPVP("PPA_DPIPVP_020","Domestic Payment Validation Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
   	/*Start for Domestic Payment PISP Update*/
   	BAD_REQUEST_PPA_DPIPR("PPA_DPIPR_001","Domestic Payment Update Process - Bad Request","Bad Request (The request parameters passed are invalid)","Bad Request", HttpStatus.BAD_REQUEST),

    SERVER_RESOURCE_NOT_FOUND_PPA_DPIPR("PPA_DPIPR_002","Domestic Payment Update Process - Server Resource not found","Resource not found","Server Resource not found",HttpStatus.NOT_FOUND),

    RESOURCE_NOT_FOUND_PPA_DPIPR("PPA_DPIPR_003","Domestic Payment Update Process -Resource not found","URL resource not found","Resource not found",HttpStatus.NOT_FOUND),

    REQUEST_TIMEDOUT_PPA_DPIPR("PPA_DPIPR_004","Domestic Payment Update Process - Request timed out", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),

    TECHNICAL_ERROR_PPA_DPIPR("PPA_DPIPR_999","Domestic Payment Update Process - Technical Error. Please try again later","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),

    UNSUPPORTED_MEDIA_TYPE_PPA_DPIPR("PPA_DPIPR_005","Domestic Payment Update Process - Unsupported media type","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),

    SERVICE_UNAVAILABLE_PPA_DPIPR("PPA_DPIPR_006","Domestic Payment Update Process - Server not available","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),

    METHOD_NOT_ALLOWED_PPA_DPIPR("PPA_DPIPR_007","Domestic Payment Update Process - Method not allowed","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),

    NOT_ACCEPTABLE_PPA_DPIPR("PPA_DPIPR_008","Domestic Payment Update Process - Response type not acceptable","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),

    UNAUTHORIZED_PPA_DPIPR("PPA_DPIPR_009","Domestic Payment Update Process - Server Access Unauthorized","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),

    ACCESS_DENIED_PPA_DPIPR("PPA_DPIPR_010","Domestic Payment Update Process - Server Access Denied","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),

    FORBIDDEN_PPA_DPIPR("PPA_DPIPR_011","Domestic Payment Update Process - Server Access Forbidden","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),

    FORBIDDEN_ACCESS_DENIED_PPA_DPIPR("PPA_DPIPR_012","Domestic Payment Update Process - Server Access Denied","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),

    NOT_IMPLEMENTED_PPA_DPIPR("PPA_DPIPR_013","Domestic Payment Update Process - Not Implemented","Used for API resource operations which haven’t been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),

    SERVER_METHOD_NOT_ALLOWED_PPA_DPIPR("PPA_DPIPR_014","Domestic Payment Update Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),

    REQUEST_NOT_ACCEPTABLE_PPA_DPIPR("PPA_DPIPR_015","Domestic Payment Update Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),

    MEDIA_TYPE_UNSUPPORTED_PPA_DPIPR("PPA_DPIPR_016","Domestic Payment Update Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),

    TOO_MANY_REQUESTS_PPA_DPIPR("PPA_DPIPR_017","Domestic Payment Update Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),

    BAD_GATEWAY_PPA_DPIPR("PPA_DPIPR_018","Domestic Payment Update Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),

    SERVER_BAD_REQUEST_PPA_DPIPR("PPA_DPIPR_019","Domestic Payment Update Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),

    SERVER_BAD_RESPONSE_PPA_DPIPR("PPA_DPIPR_020","Domestic Payment Update Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
   	/*start for Domestic Payments consents PISP get*/
   	BAD_REQUEST_PPA_DPIPG("PPA_DPIPG_001","Domestic Payment Get Process - Bad Request","Bad Request (The request parameters passed are invalid)","Bad Request",HttpStatus.BAD_REQUEST),

    SERVER_RESOURCE_NOT_FOUND_PPA_DPIPG("PPA_DPIPG_002","Domestic Payment Get Process - Server Resource not found","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),

    RESOURCE_NOT_FOUND_PPA_DPIPG("PPA_DPIPG_003","Domestic Payment Get Process -Resource not found","URL resource not found","Resource not found",HttpStatus.NOT_FOUND),

    REQUEST_TIMEDOUT_PPA_DPIPG("PPA_DPIPG_004","Domestic Payment Get Process - Request timed out", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),

    TECHNICAL_ERROR_PPA_DPIPG("PPA_DPIPG_999","Domestic Payment Get Process - Technical Error. Please try again later","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),

    UNSUPPORTED_MEDIA_TYPE_PPA_DPIPG("PPA_DPIPG_005","Domestic Payment Get Process - Unsupported media type","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),

    SERVICE_UNAVAILABLE_PPA_DPIPG("PPA_DPIPG_006","Domestic Payment Get Process - Server not available","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),

    METHOD_NOT_ALLOWED_PPA_DPIPG("PPA_DPIPG_007","Domestic Payment Get Process - Method not allowed","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),

    NOT_ACCEPTABLE_PPA_DPIPG("PPA_DPIPG_008","Domestic Payment Get Process - Response type not acceptable","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),

    UNAUTHORIZED_PPA_DPIPG("PPA_DPIPG_009","Domestic Payment Get Process - Server Access Unauthorized","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),

    ACCESS_DENIED_PPA_DPIPG("PPA_DPIPG_010","Domestic Payment Get Process - Server Access Denied","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),

    FORBIDDEN_PPA_DPIPG("PPA_DPIPG_011","Domestic Payment Get Process - Server Access Forbidden","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),

    FORBIDDEN_ACCESS_DENIED_PPA_DPIPG("PPA_DPIPG_012","Domestic Payment Get Process - Server Access Denied","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),

    NOT_IMPLEMENTED_PPA_DPIPG("PPA_DPIPG_013","Domestic Payment Get Process - Not Implemented","Used for API resource operations which haven’t been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),

    SERVER_METHOD_NOT_ALLOWED_PPA_DPIPG("PPA_DPIPG_014","Domestic Payment Get Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),

    REQUEST_NOT_ACCEPTABLE_PPA_DPIPG("PPA_DPIPG_015","Domestic Payment Get Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),

    MEDIA_TYPE_UNSUPPORTED_PPA_DPIPG("PPA_DPIPG_016","Domestic Payment Get Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),

    TOO_MANY_REQUESTS_PPA_DPIPG("PPA_DPIPG_017","Domestic Payment Get Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),

    BAD_GATEWAY_PPA_DPIPG("PPA_DPIPG_018","Domestic Payment Get Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),

    SERVER_BAD_REQUEST_PPA_DPIPG("PPA_DPIPG_019","Domestic Payment Get Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),

    SERVER_BAD_RESPONSE_PPA_DPIPG("PPA_DPIPG_020","Domestic Payment Get Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
   	
 // domestic scheduled pipv
 	BAD_REQUEST_PPS_PIPV("PPA_SPIPVP_001","Domestic Scheduled Payments Consents"," Bad request. Please check your request","Bad Request",HttpStatus.BAD_REQUEST),
 	SERVER_BAD_REQUEST_PPS_PIPV_1("PPA_SPIPVP_019","Domestic Scheduled Payments Consents"," Bad request. Please check your request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_001"),
 	SERVER_BAD_REQUEST_PPS_PIPV_2("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","General Error","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_004"),
 	SERVER_BAD_REQUEST_PPS_PIPV_3("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payee IBAN length is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_006"),
 	SERVER_BAD_REQUEST_PPS_PIPV_4("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payee IBAN does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_007"),
 	SERVER_BAD_REQUEST_PPS_PIPV_5("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payee IBAN checksum is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_008"),
 	SERVER_BAD_REQUEST_PPS_PIPV_6("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payee BIC does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_009"),
 	SERVER_BAD_REQUEST_PPS_PIPV_7("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payee BIC is not reachable","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_010"),
 	SERVER_BAD_REQUEST_PPS_PIPV_8("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payee BIC and IBAN do not match","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_011"),
 	SERVER_BAD_REQUEST_PPS_PIPV_9("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payer NSC is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_012"),
 	SERVER_BAD_REQUEST_PPS_PIPV_10("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payee Account Number is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_013"),
 	SERVER_BAD_REQUEST_PPS_PIPV_11("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Currency is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_014"),
 	SERVER_BAD_REQUEST_PPS_PIPV_12("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payee Account does not exist","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_015"),
 	SERVER_BAD_REQUEST_PPS_PIPV_13("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payee Account status must be active","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_016"),
 	SERVER_BAD_REQUEST_PPS_PIPV_14("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payer Account Type is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_017"),
 	SERVER_BAD_REQUEST_PPS_PIPV_15("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payer Account is blocked to debits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_018"),
 	SERVER_BAD_REQUEST_PPS_PIPV_16("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payer Account is blocked to credits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_019"),
 	SERVER_BAD_REQUEST_PPS_PIPV_17("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payer Account is blocked to All","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_020"),
 	SERVER_BAD_REQUEST_PPS_PIPV_18("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payer Credit grade cannot be 6 or 7","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_021"),
 	SERVER_BAD_REQUEST_PPS_PIPV_19("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payer Account cannot be dormant","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_022"),
 	SERVER_BAD_REQUEST_PPS_PIPV_20("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payee Account cannot be lien","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_023"),
 	SERVER_BAD_REQUEST_PPS_PIPV_21("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payer Joint account must be permitted by all the customer associated","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_024"),
 	SERVER_BAD_REQUEST_PPS_PIPV_22("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payer Account does not have enough funds","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_025"),
 	SERVER_BAD_REQUEST_PPS_PIPV_23("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payer Limit exceeded for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_026"),
 	SERVER_BAD_REQUEST_PPS_PIPV_24("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Payer Limit exceeded on this payment type for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_027"),
 	SERVER_BAD_REQUEST_PPS_PIPV_25("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Limit exceeded for this agent","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_028"),
 	SERVER_BAD_REQUEST_PPS_PIPV_26("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Invalid payment request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_029"),
 	SERVER_BAD_REQUEST_PPS_PIPV_27("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Swift address is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_030"),
 	SERVER_BAD_REQUEST_PPS_PIPV_28("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","ABA code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_031"),
 	SERVER_BAD_REQUEST_PPS_PIPV_29("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Country is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_032"),
 	SERVER_BAD_REQUEST_PPS_PIPV_30("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Country rules limit exceeded","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_033"),
 	SERVER_BAD_REQUEST_PPS_PIPV_31("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","User id is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_034"),
 	SERVER_BAD_REQUEST_PPS_PIPV_32("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Jurisdiction code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_035"),
 	SERVER_BAD_REQUEST_PPS_PIPV_33("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","CIS id is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_036"),
 	SERVER_BAD_REQUEST_PPS_PIPV_34("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","The payer and the payee account cannot be the same","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_037"),
 	SERVER_BAD_REQUEST_PPS_PIPV_35("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","BIC provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_038"),
 	SERVER_BAD_REQUEST_PPS_PIPV_36("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Invalid future payment date","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_039"),
 	SERVER_BAD_REQUEST_PPS_PIPV_37("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","IBAN country code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_040"),
 	SERVER_RESOURCE_NOT_FOUND_PPS_PIPV("PPA_SPIPVP_002","Domestic Scheduled Payments Consents","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
	RESOURCE_NOT_FOUND_PPS_PIPV("PPA_SPIPVP_003","Domestic Scheduled Payments Consents","1. URL resource not found","Resource not found",HttpStatus.NOT_FOUND),
	REQUEST_TIMED_OUT_PPS_PIPV("PPA_SPIPVP_004","Domestic Scheduled Payments Consents","Request timed out for FS Service","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_PPS_PIPV("PPA_SPIPVP_999","Domestic Scheduled Payments Consents","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_TYPE_PPS_PIPV("PPA_SPIPVP_005","Domestic Scheduled Payments Consents","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_NOT_AVAILABLE_PPS_PIPV("PPA_SPIPVP_006","Domestic Scheduled Payments Consents","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_PPS_PIPV("PPA_SPIPVP_007","Domestic Scheduled Payments Consents","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	RESPONSE_TYPE_NOT_ACCEPTABLE_PPS_PIPV("PPA_SPIPVP_008","Domestic Scheduled Payments Consents","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
	SERVER_ACCESS_UNAUTHORIZED_PPS_PIPV("PPA_SPIPVP_009","Domestic Scheduled Payments Consents","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
	SERVER_ACCESS__DENIED_PPS_PIPV("PPA_SPIPVP_010","Domestic Scheduled Payments Consents","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	SERVER_ACCESS_FORBIDDEN_PPS_PIPV("PPA_SPIPVP_011","Domestic Scheduled Payments Consents","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
	SERVER_ACCESS_DENIED_PPS_PIPV("PPA_SPIPVP_012","Domestic Scheduled Payments Consents","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTED_PPS_PIPV("PPA_SPIPVP_013","Domestic Scheduled Payments Consents","Used for API resource operations which haven't been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWED_PPS_PIPV("PPA_SPIPVP_014","Domestic Scheduled Payments Consents","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	SERVER_REQUEST_NOT_ACCEPTABLE_PPS_PIPV("PPA_SPIPVP_015","Domestic Scheduled Payments Consents","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
	SERVER_REQUEST_UNSUPPORTED_MEDIA_TYPE_PPS_PIPV("PPA_SPIPVP_016","Domestic Scheduled Payments Consents","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_REQUEST_RATE_LIMIT_EXCEEDED_PPS_PIPV("PPA_SPIPVP_017","Domestic Scheduled Payments Consents","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
	SERVER_BAD_GATEWAY_PPS_PIPV("PPA_SPIPVP_018","Domestic Scheduled Payments Consents","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_PPS_PIPV("PPA_SPIPVP_019","Domestic Scheduled Payments Consents","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
	SERVER_BAD_RESPONSE_PPS_PIPV("PPA_SPIPVP_020","Domestic Scheduled Payments Consents","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),

	// DOMESTIC SCHEDULED UPDATE

	BAD_REQUEST_PPS_PIPU("PPA_SPIPR_001","Domestic Scheduled Payments Consents"," Bad request. Please check your request","Bad Request",HttpStatus.BAD_REQUEST),
	SERVER_RESOURCE_NOT_FOUND_PPS_PIPU("PPA_SPIPR_002","Domestic Scheduled Payments Consents","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
	RESOURCE_NOT_FOUND_PPS_PIPU("PPA_SPIPR_003","Domestic Scheduled Payments Consents","1. URL resource not found","Resource not found",HttpStatus.NOT_FOUND),
	REQUEST_TIMED_OUT_PPS_PIPU("PPA_SPIPR_004","Domestic Scheduled Payments Consents","Request timed out for FS Service","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_PPS_PIPU("PPA_SPIPR_999","Domestic Scheduled Payments Consents","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_TYPE_PPS_PIPU("PPA_SPIPR_005","Domestic Scheduled Payments Consents","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_NOT_AVAILABLE_PPS_PIPU("PPA_SPIPR_006","Domestic Scheduled Payments Consents","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_PPS_PIPU("PPA_SPIPR_007","Domestic Scheduled Payments Consents","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	RESPONSE_TYPE_NOT_ACCEPTABLE_PPS_PIPU("PPA_SPIPR_008","Domestic Scheduled Payments Consents","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
	SERVER_ACCESS_UNAUTHORIZED_PPS_PIPU("PPA_SPIPR_009","Domestic Scheduled Payments Consents","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
	SERVER_ACCESS__DENIED_PPS_PIPU("PPA_SPIPR_010","Domestic Scheduled Payments Consents","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	SERVER_ACCESS_FORBIDDEN_PPS_PIPU("PPA_SPIPR_011","Domestic Scheduled Payments Consents","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
	SERVER_ACCESS_DENIED_PPS_PIPU("PPA_SPIPR_012","Domestic Scheduled Payments Consents","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTED_PPS_PIPU("PPA_SPIPR_013","Domestic Scheduled Payments Consents","Used for API resource operations which haven't been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWED_PPS_PIPU("PPA_SPIPR_014","Domestic Scheduled Payments Consents","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	SERVER_REQUEST_NOT_ACCEPTABLE_PPS_PIPU("PPA_SPIPR_015","Domestic Scheduled Payments Consents","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
	SERVER_REQUEST_UNSUPPORTED_MEDIA_TYPE_PPS_PIPU("PPA_SPIPR_016","Domestic Scheduled Payments Consents","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_REQUEST_RATE_LIMIT_EXCEEDED_PPS_PIPU("PPA_SPIPR_017","Domestic Scheduled Payments Consents","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
	SERVER_BAD_GATEWAY_PPS_PIPU("PPA_SPIPR_018","Domestic Scheduled Payments Consents","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_PPS_PIPU("PPA_SPIPR_019","Domestic Scheduled Payments Consents","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
	SERVER_BAD_RESPONSE_PPS_PIPU("PPA_SPIPR_020","Domestic Scheduled Payments Consents","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
	
	/* start for Domestic Standing Orders Consents POST(create) */
	BAD_REQUEST_PPSO_PIPC("PPSO_PIPC_001","Domestic StandingOrders Consents Process - Bad Request","Bad Request (The request parameters passed are invalid)","Bad Request",HttpStatus.BAD_REQUEST),
   	SERVER_RESOURCE_NOT_FOUND_PPSO_PIPC("PPSO_PIPC_002","Domestic StandingOrders Consents Process - Server Resource not found","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
   	RESOURCE_NOT_FOUND_PPSO_PIPC("PPSO_PIPC_003","Domestic StandingOrders Consents Process -Resource not found","URL resource not found","Resource not found",HttpStatus.NOT_FOUND),
   	REQUEST_TIMEDOUT_PPSO_PIPC("PPSO_PIPC_004","Domestic StandingOrders Consents Process - Request timed out", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
   	TECHNICAL_ERROR_PPSO_PIPC("PPSO_PIPC_999","Domestic StandingOrders Consents Process - Technical Error. Please try again later","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
   	UNSUPPORTED_MEDIA_TYPE_PPSO_PIPC("PPSO_PIPC_005","Domestic StandingOrders Consents Process - Unsupported media type","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
   	SERVICE_UNAVAILABLE_PPSO_PIPC("PPSO_PIPC_006","Domestic StandingOrders Consents Process - Server not available","Received an invalid response from FS service.","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
   	METHOD_NOT_ALLOWED_PPSO_PIPC("PPSO_PIPC_007","Domestic StandingOrders Consents Process - Method not allowed","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
   	NOT_ACCEPTABLE_PPSO_PIPC("PPSO_PIPC_008","Domestic StandingOrders Consents Process - Response type not acceptable","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
   	UNAUTHORIZED_PPSO_PIPC("PPSO_PIPC_009","Domestic StandingOrders Consents Process - Server Access Unauthorized","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
   	ACCESS_DENIED_PPSO_PIPC("PPSO_PIPC_010","Domestic StandingOrders Consents Process - Server Access Denied","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
   	FORBIDDEN_PPSO_PIPC("PPSO_PIPC_011","Domestic StandingOrders Consents Process - Server Access Forbidden","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
   	FORBIDDEN_ACCESS_DENIED_PPSO_PIPC("PPSO_PIPC_012","Domestic StandingOrders Consents Process - Server Access Denied","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
   	NOT_IMPLEMENTED_PPSO_PIPC("PPSO_PIPC_013","Domestic StandingOrders Consents Process - Not Implemented","Used for API resource operations which haven’t been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
   	SERVER_METHOD_NOT_ALLOWED_PPSO_PIPC("PPSO_PIPC_014","Domestic StandingOrders Consents Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
   	REQUEST_NOT_ACCEPTABLE_PPSO_PIPC("PPSO_PIPC_015","Domestic StandingOrders Consents Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
   	MEDIA_TYPE_UNSUPPORTED_PPSO_PIPC("PPSO_PIPC_016","Domestic StandingOrders Consents Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
   	TOO_MANY_REQUESTS_PPSO_PIPC("PPSO_PIPC_017","Domestic StandingOrders Consents Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
   	BAD_GATEWAY_PPSO_PIPC("PPSO_PIPC_018","Domestic StandingOrders Consents Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
   	SERVER_BAD_REQUEST_PPSO_PIPC("PPSO_PIPC_019","Domestic StandingOrders Consents Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
   	SERVER_BAD_RESPONSE_PPSO_PIPC("PPSO_PIPC_020","Domestic StandingOrders Consents Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
	
	// Domestic Scheduled Payment Retrieve Get
   	BAD_REQUEST_PPA_SPING("PPA_SPING_001","Domestic Scheduled Payments","Bad Request (The request parameters passed are invalid)","Bad Request",HttpStatus.BAD_REQUEST), 
    SERVER_RESOURCE_NOT_FOUND_PPA_SPING("PPA_SPING_002","Domestic Scheduled Payments","Resource not found","Server resource not found", HttpStatus.NOT_FOUND),
    RESOURCE_NOT_FOUND_PPS_PINR("PPS_PINR_003","Domestic Scheduled Payments","1. URL resource not found","Resource not found",HttpStatus.NOT_FOUND),
    REQUEST_TIMED_OUT_PPA_SPING("PPA_SPING_004","Domestic Scheduled Payments","Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
    TECHNICAL_ERROR_PPA_SPING("PPA_SPING_999","Domestic Scheduled Payments","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
UNSUPPORTED_MEDIA_TYPE_PPA_SPING("PPA_SPING_005","Domestic Scheduled Payments","Unsupported media type","Unsupported media type", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
SERVER_NOT_AVAILABLE_PPA_SPING("PPA_SPING_006","Domestic Scheduled Payments","Connectivity error","Server not available", HttpStatus.SERVICE_UNAVAILABLE),
METHOD_NOT_ALLOWED_PPA_SPING("PPA_SPING_007","Domestic Scheduled Payments","Method not allowed","Method not allowed",HttpStatus.NOT_FOUND),
RESPONSE_TYPE_NOT_ACCEPTABLE_PPA_SPING("PPA_SPING_008","Domestic Scheduled Payments","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
SERVER_ACCESS_UNAUTHORIZED_PPA_SPING("PPA_SPING_009","Domestic Scheduled Payments","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
SERVER_ACCESS_DENIED_AUTHENTICATION_PPA_SPING("PPA_SPING_010","Domestic Scheduled Payments","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
SERVER_ACCESS_FORBIDDEN_PPA_SPING("PPA_SPING_011","Domestic Scheduled Payments","API authorisation failed in the Downstream system","Server Access Forbidden", HttpStatus.FORBIDDEN),
SERVER_ACCESS_DENIED_PPS_AUTHORISATION_PINR("PPA_SPING_012","Domestic Scheduled Payments","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
NOT_IMPLEMENTED_PPA_SPING("PPA_SPING_013","Domestic Scheduled Payments","Used for API resource operations which haven't been implemented","Not Implemented", HttpStatus.NOT_IMPLEMENTED),
SERVER_METHOD_NOT_ALLOWED_PPA_SPING("PPA_SPING_014","Domestic Scheduled Payments","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.NOT_FOUND),
SERVER_REQUEST_NOT_ACCEPTABLE_PPA_SPING("PPA_SPING_015","Domestic Scheduled Payments","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
SERVER_REQUEST_UNSUPPORTED_MEDIA_TYPE_PPA_SPING("PPA_SPING_016","Domestic Scheduled Payments","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
SERVER_REQUEST_RATE_LIMIT_EXCEEDED_PPA_SPING("PPA_SPING_017","Domestic Scheduled Payments","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
SERVER_BAD_GATEWAY_PPA_SPING("PPA_SPING_018","Domestic Scheduled Payments","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
SERVER_BAD_REQUEST_PPA_SPING("PPA_SPING_019","Domestic Scheduled Payments","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
SERVER_BAD_RESPONSE_PPA_SPING("PPA_SPING_020","Domestic Scheduled Payments","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),



	// Domestic Scheduled Payment Post
BAD_REQUEST_PPA_SPINP("PPA_SPINP_001","Domestic Scheduled Payments","Bad Request (The request parameters passed are invalid)","Bad Request",HttpStatus.BAD_REQUEST), 
SERVER_RESOURCE_NOT_FOUND_PPA_SPINP("PPA_SPINP_002","Domestic Scheduled Payments","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
RESOURCE_NOT_FOUND_PPS_PINC("PPS_PINC_003","Domestic Scheduled Payments","1. URL resource not found","Resource not found",HttpStatus.NOT_FOUND),
REQUEST_TIMED_OUT_PPA_SPINP("PPA_SPINP_004","Domestic Scheduled Payments","Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
TECHNICAL_ERROR_PPA_SPINP("PPA_SPINP_999","Domestic Scheduled Payments","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
UNSUPPORTED_MEDIA_TYPE_PPA_SPINP("PPA_SPINP_005","Domestic Scheduled Payments","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
SERVER_NOT_AVAILABLE_PPA_SPINP("PPA_SPINP_006","Domestic Scheduled Payments","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
METHOD_NOT_ALLOWED_PPA_SPINP("PPA_SPINP_007","Domestic Scheduled Payments","Method not allowed","Method not allowed",HttpStatus.NOT_FOUND),
RESPONSE_TYPE_NOT_ACCEPTABLE_PPA_SPINP("PPA_SPINP_008","Domestic Scheduled Payments","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
SERVER_ACCESS_UNAUTHORIZED_PPA_SPINP("PPA_SPINP_009","Domestic Scheduled Payments","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
SERVER_ACCESS_DENIED_AUTHENTICATION_PPA_SPINP("PPA_SPINP_010","Domestic Scheduled Payments","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
SERVER_ACCESS_FORBIDDEN_PPA_SPINP("PPA_SPINP_011","Domestic Scheduled Payments","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
SERVER_ACCESS_DENIED_AUTHORISATION_PPA_SPINP("PPA_SPINP_012","Domestic Scheduled Payments","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
NOT_IMPLEMENTED_PPA_SPINP("PPA_SPINP_013","Domestic Scheduled Payments","Used for API resource operations which haven't been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
SERVER_METHOD_NOT_ALLOWED_PPA_SPINP("PPA_SPINP_014","Domestic Scheduled Payments","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.NOT_FOUND),
SERVER_REQUEST_NOT_ACCEPTABLE_PPA_SPINP("PPA_SPINP_015","Domestic Scheduled Payments","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
SERVER_REQUEST_UNSUPPORTED_MEDIA_TYPE_PPA_SPINP("PPA_SPINP_016","Domestic Scheduled Payments","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
SERVER_REQUEST_RATE_LIMIT_EXCEEDED_PPA_SPINP("PPA_SPINP_017","Domestic Scheduled Payments","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
SERVER_BAD_GATEWAY_PPA_SPINP("PPA_SPINP_018","Domestic Scheduled Payments","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
SERVER_BAD_REQUEST_PPA_SPINP("PPA_SPINP_019","Domestic Scheduled Payments","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),

//Errors produced from Validate System API (PMV): 
SERVER_BAD_REQUEST_PPA_SPINP_v1("PPA_SPINP_019","Domestic Scheduled Payments","Bad request. Please check your request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_001"),
SERVER_BAD_REQUEST_PPA_SPINP_v4("PPA_SPINP_019","Domestic Scheduled Payments","General Error","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_004"),
SERVER_BAD_REQUEST_PPA_SPINP_v6("PPA_SPINP_019","Domestic Scheduled Payments","IBAN length is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_006"),
SERVER_BAD_REQUEST_PPA_SPINP_v7("PPA_SPINP_019","Domestic Scheduled Payments","IBAN does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_007"),
SERVER_BAD_REQUEST_PPA_SPINP_v8("PPA_SPINP_019","Domestic Scheduled Payments","IBAN checksum is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_008"),
SERVER_BAD_REQUEST_PPA_SPINP_v9("PPA_SPINP_019","Domestic Scheduled Payments","BIC does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_009"),
SERVER_BAD_REQUEST_PPA_SPINP_v10("PPA_SPINP_019","Domestic Scheduled Payments","BIC is not reachable","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_010"),
SERVER_BAD_REQUEST_PPA_SPINP_v11("PPA_SPINP_019","Domestic Scheduled Payments","BIC and IBAN do not match","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_011"),
SERVER_BAD_REQUEST_PPA_SPINP_v12("PPA_SPINP_019","Domestic Scheduled Payments","NSC is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_012"),
SERVER_BAD_REQUEST_PPA_SPINP_v13("PPA_SPINP_019","Domestic Scheduled Payments","Account Number is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_013"),
SERVER_BAD_REQUEST_PPA_SPINP_v14("PPA_SPINP_019","Domestic Scheduled Payments","Currency is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_014"),
SERVER_BAD_REQUEST_PPA_SPINP_v15("PPA_SPINP_019","Domestic Scheduled Payments","Account does not exist","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_015"),
SERVER_BAD_REQUEST_PPA_SPINP_v16("PPA_SPINP_019","Domestic Scheduled Payments","Account status must be active","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_016"),
SERVER_BAD_REQUEST_PPA_SPINP_v17("PPA_SPINP_019","Domestic Scheduled Payments","Account Type is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_017"),
SERVER_BAD_REQUEST_PPA_SPINP_v18("PPA_SPINP_019","Domestic Scheduled Payments","Account is blocked to debits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_018"),
SERVER_BAD_REQUEST_PPA_SPINP_v19("PPA_SPINP_019","Domestic Scheduled Payments","Account is blocked to credits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_019"),
SERVER_BAD_REQUEST_PPA_SPINP_v20("PPA_SPINP_019","Domestic Scheduled Payments","Account is blocked to All","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_020"),
SERVER_BAD_REQUEST_PPA_SPINP_v21("PPA_SPINP_019","Domestic Scheduled Payments","Credit grade cannot be 6 or 7","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_021"),
SERVER_BAD_REQUEST_PPA_SPINP_v22("PPA_SPINP_019","Domestic Scheduled Payments","Account cannot be dormant","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_022"),
SERVER_BAD_REQUEST_PPA_SPINP_v23("PPA_SPINP_019","Domestic Scheduled Payments","Account cannot be lien","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_023"),
SERVER_BAD_REQUEST_PPA_SPINP_v24("PPA_SPINP_019","Domestic Scheduled Payments","Joint account must be permitted by all the customer associated","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_024"),
SERVER_BAD_REQUEST_PPA_SPINP_v25("PPA_SPINP_019","Domestic Scheduled Payments","Account does not have enough funds","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_025"),
SERVER_BAD_REQUEST_PPA_SPINP_v26("PPA_SPINP_019","Domestic Scheduled Payments","Limit exceeded for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_026"),
SERVER_BAD_REQUEST_PPA_SPINP_v27("PPA_SPINP_019","Domestic Scheduled Payments","Limit exceeded on this payment type for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_027"),
SERVER_BAD_REQUEST_PPA_SPINP_v28("PPA_SPINP_019","Domestic Scheduled Payments","Limit exceeded for this agent","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_028"),
SERVER_BAD_REQUEST_PPA_SPINP_v29("PPA_SPINP_019","Domestic Scheduled Payments","Invalid payment request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_029"),
SERVER_BAD_REQUEST_PPA_SPINP_v30("PPA_SPINP_019","Domestic Scheduled Payments","Swift address is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_030"),
SERVER_BAD_REQUEST_PPA_SPINP_v31("PPA_SPINP_019","Domestic Scheduled Payments","ABA code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_031"),
SERVER_BAD_REQUEST_PPA_SPINP_v32("PPA_SPINP_019","Domestic Scheduled Payments","Country is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_032"),
SERVER_BAD_REQUEST_PPA_SPINP_v33("PPA_SPINP_019","Domestic Scheduled Payments","Country rules limit exceeded","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_033"),
SERVER_BAD_REQUEST_PPA_SPINP_v34("PPA_SPINP_019","Domestic Scheduled Payments","User id is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_034"),
SERVER_BAD_REQUEST_PPA_SPINP_v35("PPA_SPINP_019","Domestic Scheduled Payments","Jurisdiction code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_035"),
SERVER_BAD_REQUEST_PPA_SPINP_v36("PPA_SPINP_019","Domestic Scheduled Payments","CIS id is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_036"),
SERVER_BAD_REQUEST_PPA_SPINP_v37("PPA_SPINP_019","Domestic Scheduled Payments","The payer and the payee account cannot be the same","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_037"),
SERVER_BAD_REQUEST_PPA_SPINP_v38("PPA_SPINP_019","Domestic Scheduled Payments","BIC provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_038"),
SERVER_BAD_REQUEST_PPA_SPINP_v39("PPA_SPINP_019","Domestic Scheduled Payments","Invalid future payment date","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_039"),

//Error codes produced by Execute System API (PME):
SERVER_BAD_REQUEST_PPA_SPINP_e1("PPA_SPINP_019","Domestic Scheduled Payments","Bad request. Please check your request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_001"),
SERVER_BAD_REQUEST_PPA_SPINP_e4("PPA_SPINP_019","Domestic Scheduled Payments","General Error","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_004"),
SERVER_BAD_REQUEST_PPA_SPINP_e6("PPA_SPINP_019","Domestic Scheduled Payments","IBAN length is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_006"),
SERVER_BAD_REQUEST_PPA_SPINP_e7("PPA_SPINP_019","Domestic Scheduled Payments","IBAN does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_007"),
SERVER_BAD_REQUEST_PPA_SPINP_e8("PPA_SPINP_019","Domestic Scheduled Payments","IBAN checksum is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_008"),
SERVER_BAD_REQUEST_PPA_SPINP_e9("PPA_SPINP_019","Domestic Scheduled Payments","BIC does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_009"),
SERVER_BAD_REQUEST_PPA_SPINP_e10("PPA_SPINP_019","Domestic Scheduled Payments","BIC is not reachable","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_010"),
SERVER_BAD_REQUEST_PPA_SPINP_e11("PPA_SPINP_019","Domestic Scheduled Payments","BIC and IBAN do not match","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_011"),
SERVER_BAD_REQUEST_PPA_SPINP_e12("PPA_SPINP_019","Domestic Scheduled Payments","NSC is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_012"),
SERVER_BAD_REQUEST_PPA_SPINP_e13("PPA_SPINP_019","Domestic Scheduled Payments","Account Number is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_013"),
SERVER_BAD_REQUEST_PPA_SPINP_e14("PPA_SPINP_019","Domestic Scheduled Payments","Currency is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_014"),
SERVER_BAD_REQUEST_PPA_SPINP_e15("PPA_SPINP_019","Domestic Scheduled Payments","Account does not exist","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_015"),
SERVER_BAD_REQUEST_PPA_SPINP_e16("PPA_SPINP_019","Domestic Scheduled Payments","Account status must be active","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_016"),
SERVER_BAD_REQUEST_PPA_SPINP_e17("PPA_SPINP_019","Domestic Scheduled Payments","Account Type is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_017"),
SERVER_BAD_REQUEST_PPA_SPINP_e18("PPA_SPINP_019","Domestic Scheduled Payments","Account is blocked to debits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_018"),
SERVER_BAD_REQUEST_PPA_SPINP_e19("PPA_SPINP_019","Domestic Scheduled Payments","Account is blocked to credits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_019"),
SERVER_BAD_REQUEST_PPA_SPINP_e20("PPA_SPINP_019","Domestic Scheduled Payments","Account is blocked to All","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_020"),
SERVER_BAD_REQUEST_PPA_SPINP_e21("PPA_SPINP_019","Domestic Scheduled Payments","Credit grade cannot be 6 or 7","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_021"),
SERVER_BAD_REQUEST_PPA_SPINP_e22("PPA_SPINP_019","Domestic Scheduled Payments","Account cannot be dormant","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_022"),
SERVER_BAD_REQUEST_PPA_SPINP_e23("PPA_SPINP_019","Domestic Scheduled Payments","Account cannot be lien","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_023"),
SERVER_BAD_REQUEST_PPA_SPINP_e24("PPA_SPINP_019","Domestic Scheduled Payments","Joint account must be permitted by all the customer associated","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_024"),
SERVER_BAD_REQUEST_PPA_SPINP_e25("PPA_SPINP_019","Domestic Scheduled Payments","Account does not have enough funds","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_025"),
SERVER_BAD_REQUEST_PPA_SPINP_e26("PPA_SPINP_019","Domestic Scheduled Payments","Limit exceeded for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_026"),
SERVER_BAD_REQUEST_PPA_SPINP_e27("PPA_SPINP_019","Domestic Scheduled Payments","Limit exceeded on this payment type for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_027"),
SERVER_BAD_REQUEST_PPA_SPINP_e28("PPA_SPINP_019","Domestic Scheduled Payments","Limit exceeded for this agent","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_028"),
SERVER_BAD_REQUEST_PPA_SPINP_e29("PPA_SPINP_019","Domestic Scheduled Payments","Invalid payment request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_029"),
SERVER_BAD_REQUEST_PPA_SPINP_e30("PPA_SPINP_019","Domestic Scheduled Payments","Swift address is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_030"),
SERVER_BAD_REQUEST_PPA_SPINP_e31("PPA_SPINP_019","Domestic Scheduled Payments","ABA code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_031"),
SERVER_BAD_REQUEST_PPA_SPINP_e32("PPA_SPINP_019","Domestic Scheduled Payments","Country is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_032"),
SERVER_BAD_REQUEST_PPA_SPINP_e33("PPA_SPINP_019","Domestic Scheduled Payments","Country rules limit exceeded","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_033"),
SERVER_BAD_REQUEST_PPA_SPINP_e34("PPA_SPINP_019","Domestic Scheduled Payments","User id is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_034"),
SERVER_BAD_REQUEST_PPA_SPINP_e35("PPA_SPINP_019","Domestic Scheduled Payments","Jurisdiction code is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_035"),
SERVER_BAD_REQUEST_PPA_SPINP_e36("PPA_SPINP_019","Domestic Scheduled Payments","CIS id is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_036"),
SERVER_BAD_REQUEST_PPA_SPINP_e37("PPA_SPINP_019","Domestic Scheduled Payments","The payer and the payee account cannot be the same","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_037"),
SERVER_BAD_REQUEST_PPA_SPINP_e38("PPA_SPINP_019","Domestic Scheduled Payments","BIC provided is invalid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_038"),
SERVER_BAD_REQUEST_PPA_SPINP_e39("PPA_SPINP_019","Domestic Scheduled Payments","Invalid future payment date","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PME_039"),



SERVER_BAD_RESPONSE_PPA_SPINP("PPA_SPINP_020","Domestic Scheduled Payments","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),


//Start for Standing Orders POST
BAD_REQUEST_PPA_SOPINP("PPA_SOPINP_001","Domestic StandingOrders Process - Bad Request","Bad Request (The request parameters passed are invalid)","Bad Request",HttpStatus.BAD_REQUEST),
SERVER_RESOURCE_NOT_FOUND_PPA_SOPINP("PPA_SOPINP_002","Domestic StandingOrders Process - Resource not found","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
REQUEST_TIMEDOUT_PPA_SOPINP("PPA_SOPINP_004","Domestic StandingOrders Process - Request timed out", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
TECHNICAL_ERROR_PPA_SOPINP("PPA_SOPINP_999","Domestic StandingOrders Process - Technical Error. Please try again later","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
UNSUPPORTED_MEDIA_TYPE_PPA_SOPINP("PPA_SOPINP_005","Domestic StandingOrders Process - Unsupported media type","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
SERVICE_UNAVAILABLE_PPA_SOPINP("PPA_SOPINP_006","Domestic StandingOrders Process - Server not available","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
METHOD_NOT_ALLOWED_PPA_SOPINP("PPA_SOPINP_007","Domestic StandingOrders Process - Method not allowed","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
NOT_ACCEPTABLE_PPA_SOPINP("PPA_SOPINP_008","Domestic StandingOrders Process - Response type not acceptable","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
UNAUTHORIZED_PPA_SOPINP("PPA_SOPINP_009","Domestic StandingOrders Process - Server Access Unauthorized","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
ACCESS_DENIED_PPA_SOPINP("PPA_SOPINP_010","Domestic StandingOrders Process - Server Access Denied","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
FORBIDDEN_PPA_SOPINP("PPA_SOPINP_011","Domestic StandingOrders Process - Server Access Forbidden","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
FORBIDDEN_ACCESS_DENIED_PPA_SOPINP("PPA_SOPINP_012","Domestic StandingOrders Process - Server Access Denied","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
NOT_IMPLEMENTED_PPA_SOPINP("PPA_SOPINP_013","Domestic StandingOrders Process - Not Implemented","Used for API resource operations which haven't been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
SERVER_METHOD_NOT_ALLOWED_PPA_SOPINP("PPA_SOPINP_014","Domestic StandingOrders Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
REQUEST_NOT_ACCEPTABLE_PPA_SOPINP("PPA_SOPINP_015","Domestic StandingOrders Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
MEDIA_TYPE_UNSUPPORTED_PPA_SOPINP("PPA_SOPINP_016","Domestic StandingOrders Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
TOO_MANY_REQUESTS_PPA_SOPINP("PPA_SOPINP_017","Domestic StandingOrders Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
BAD_GATEWAY_PPA_SOPINP("PPA_SOPINP_018","Domestic StandingOrders Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
SERVER_BAD_REQUEST_PPA_SOPINP("PPA_SOPINP_019","Domestic StandingOrders Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
SERVER_BAD_RESPONSE_PPA_SOPINP("PPA_SOPINP_020","Domestic StandingOrders Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),

/* start for Domestic Standing Orders  Get(retrieve) */
BAD_REQUEST_PPA_SOPING("PPA_SOPING_001","Domestic StandingOrders","Bad Request (The request parameters passed are invalid)","Bad Request",HttpStatus.BAD_REQUEST),
	SERVER_RESOURCE_NOT_FOUND_PPA_SOPING("PPA_SOPING_002","Domestic StandingOrders","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
	REQUEST_TIMEDOUT_PPA_SOPING("PPA_SOPING_004","Domestic StandingOrders", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_PPA_SOPING("PPA_SOPING_999","Domestic StandingOrders","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_TYPE_PPA_SOPING("PPA_SOPING_005","Domestic StandingOrders","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVICE_UNAVAILABLE_PPA_SOPING("PPA_SOPING_006","Domestic StandingOrders","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_PPA_SOPING("PPA_SOPING_007","Domestic StandingOrders","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	NOT_ACCEPTABLE_PPA_SOPING("PPA_SOPING_008","Domestic StandingOrders","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
	UNAUTHORIZED_PPA_SOPING("PPA_SOPING_009","Domestic StandingOrders","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
	ACCESS_DENIED_PPA_SOPING("PPA_SOPING_010","Domestic StandingOrders","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	FORBIDDEN_PPA_SOPING("PPA_SOPING_011","Domestic StandingOrders","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
	FORBIDDEN_ACCESS_DENIED_PPA_SOPING("PPA_SOPING_012","Domestic StandingOrders","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTED_PPA_SOPING("PPA_SOPING_013","Domestic StandingOrders","Used for API resource operations which haven’t been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWED_PPA_SOPING("PPA_SOPING_014","Domestic StandingOrders","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	REQUEST_NOT_ACCEPTABLE_PPA_SOPING("PPA_SOPING_015","Domestic StandingOrders","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
	MEDIA_TYPE_UNSUPPORTED_PPA_SOPING("PPA_SOPING_016","Domestic StandingOrders","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	TOO_MANY_REQUESTS_PPA_SOPING("PPA_SOPING_017","Domestic StandingOrders","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
	BAD_GATEWAY_PPA_SOPING("PPA_SOPING_018","Domestic StandingOrders","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_PPA_SOPING("PPA_SOPING_019","Domestic StandingOrders","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
	SERVER_BAD_RESPONSE_PPA_SOPING("PPA_SOPING_020","Domestic StandingOrders","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
	
	/* start for Domestic Standing Orders Consents POST(create) */
	BAD_REQUEST_PPA_SOPIPP("PPA_SOPIPP_001","Domestic StandingOrders Consents Process - Bad Request","Bad Request (The request parameters passed are invalid)","Bad Request",HttpStatus.BAD_REQUEST),
   	SERVER_RESOURCE_NOT_FOUND_PPA_SOPIPP("PPA_SOPIPP_002","Domestic StandingOrders Consents Process - Server Resource not found","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
  //RESOURCE_NOT_FOUND_PPA_SOPIPP("PPA_SOPIPP_003","Domestic StandingOrders Consents Process -Resource not found","URL resource not found","Resource not found",HttpStatus.NOT_FOUND),
   	REQUEST_TIMEDOUT_PPA_SOPIPP("PPA_SOPIPP_004","Domestic StandingOrders Consents Process - Request timed out", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
   	TECHNICAL_ERROR_PPA_SOPIPP("PPA_SOPIPP_999","Domestic StandingOrders Consents Process - Technical Error. Please try again later","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
   	UNSUPPORTED_MEDIA_TYPE_PPA_SOPIPP("PPA_SOPIPP_005","Domestic StandingOrders Consents Process - Unsupported media type","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
   	SERVICE_UNAVAILABLE_PPA_SOPIPP("PPA_SOPIPP_006","Domestic StandingOrders Consents Process - Server not available","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
   	METHOD_NOT_ALLOWED_PPA_SOPIPP("PPA_SOPIPP_007","Domestic StandingOrders Consents Process - Method not allowed","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
   	NOT_ACCEPTABLE_PPA_SOPIPP("PPA_SOPIPP_008","Domestic StandingOrders Consents Process - Response type not acceptable","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
   	UNAUTHORIZED_PPA_SOPIPP("PPA_SOPIPP_009","Domestic StandingOrders Consents Process - Server Access Unauthorized","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
   	ACCESS_DENIED_PPA_SOPIPP("PPA_SOPIPP_010","Domestic StandingOrders Consents Process - Server Access Denied","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
   	FORBIDDEN_PPA_SOPIPP("PPA_SOPIPP_011","Domestic StandingOrders Consents Process - Server Access Forbidden","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
   	FORBIDDEN_ACCESS_DENIED_PPA_SOPIPP("PPA_SOPIPP_012","Domestic StandingOrders Consents Process - Server Access Denied","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
   	NOT_IMPLEMENTED_PPA_SOPIPP("PPA_SOPIPP_013","Domestic StandingOrders Consents Process - Not Implemented","Used for API resource operations which haven’t been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
   	SERVER_METHOD_NOT_ALLOWED_PPA_SOPIPP("PPA_SOPIPP_014","Domestic StandingOrders Consents Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
   	REQUEST_NOT_ACCEPTABLE_PPA_SOPIPP("PPA_SOPIPP_015","Domestic StandingOrders Consents Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
   	MEDIA_TYPE_UNSUPPORTED_PPA_SOPIPP("PPA_SOPIPP_016","Domestic StandingOrders Consents Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
   	TOO_MANY_REQUESTS_PPA_SOPIPP("PPA_SOPIPP_017","Domestic StandingOrders Consents Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
   	BAD_GATEWAY_PPA_SOPIPP("PPA_SOPIPP_018","Domestic StandingOrders Consents Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
   	SERVER_BAD_REQUEST_PPA_SOPIPP("PPA_SOPIPP_019","Domestic StandingOrders Consents Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
   	SERVER_BAD_RESPONSE_PPA_SOPIPP("PPA_SOPIPP_020","Domestic StandingOrders Consents Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
	
  //Start for Standing Orders GET
  		BAD_REQUEST_PPA_SOPIPG("PPA_SOPIPG_001","Domestic StandingOrders Consents Process - Bad Request","Bad Request (The request parameters passed are invalid)","Bad Request",HttpStatus.BAD_REQUEST),
  	    SERVER_RESOURCE_NOT_FOUND_PPA_SOPIPG("PPA_SOPIPG_002","Domestic StandingOrders Consents Process - Resource not found","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
  	  //RESOURCE_NOT_FOUND_PPA_SOPIPG("PPA_SOPIPG_003","Domestic StandingOrders Consents Process - Resource not found","URL Resource not found","Resource not found",HttpStatus.NOT_FOUND),
  	    REQUEST_TIMEDOUT_PPA_SOPIPG("PPA_SOPIPG_004","Domestic StandingOrders Consents Process - Request timed out", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
  	    TECHNICAL_ERROR_PPA_SOPIPG("PPA_SOPIPG_999","Domestic StandingOrders Consents Process - Technical Error. Please try again later","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
  	    UNSUPPORTED_MEDIA_TYPE_PPA_SOPIPG("PPA_SOPIPG_005","Domestic StandingOrders Consents Process - Unsupported media type","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
  	    SERVICE_UNAVAILABLE_PPA_SOPIPG("PPA_SOPIPG_006","Domestic StandingOrders Consents Process - Server not available","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
  	    METHOD_NOT_ALLOWED_PPA_SOPIPG("PPA_SOPIPG_007","Domestic StandingOrders Consents Process - Method not allowed","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
  	    NOT_ACCEPTABLE_PPA_SOPIPG("PPA_SOPIPG_008","Domestic StandingOrders Consents Process - Response type not acceptable","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
  	    UNAUTHORIZED_PPA_SOPIPG("PPA_SOPIPG_009","Domestic StandingOrders Consents Process - Server Access Unauthorized","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
  	    ACCESS_DENIED_PPA_SOPIPG("PPA_SOPIPG_010","Domestic StandingOrders Consents Process - Server Access Denied","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
  	    FORBIDDEN_PPA_SOPIPG("PPA_SOPIPG_011","Domestic StandingOrders Consents Process - Server Access Forbidden","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
  	    FORBIDDEN_ACCESS_DENIED_PPA_SOPIPG("PPA_SOPIPG_012","Domestic StandingOrders Consents Process - Server Access Denied","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
  	    NOT_IMPLEMENTED_PPA_SOPIPG("PPA_SOPIPG_013","Domestic StandingOrders Consents Process - Not Implemented","Used for API resource operations which haven't been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
  	    SERVER_METHOD_NOT_ALLOWED_PPA_SOPIPG("PPA_SOPIPG_014","Domestic StandingOrders Consents Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
  	    REQUEST_NOT_ACCEPTABLE_PPA_SOPIPG("PPA_SOPIPG_015","Domestic StandingOrders Consents Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
  	    MEDIA_TYPE_UNSUPPORTED_PPA_SOPIPG("PPA_SOPIPG_016","Domestic StandingOrders Consents Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
  	    TOO_MANY_REQUESTS_PPA_SOPIPG("PPA_SOPIPG_017","Domestic StandingOrders Consents Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
  	    BAD_GATEWAY_PPA_SOPIPG("PPA_SOPIPG_018","Domestic StandingOrders Consents Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
  	    SERVER_BAD_REQUEST_PPA_SOPIPG("PPA_SOPIPG_019","Domestic StandingOrders Consents Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
  	    SERVER_BAD_RESPONSE_PPA_SOPIPG("PPA_SOPIPG_020","Domestic StandingOrders Consents Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
   	
	/* domestic standing Orders for Update*/
	BAD_REQUEST_PPA_SOPIPR("PPA_SOPIPR_001","Domestic Standing Orders Consents"," Bad request. Please check your request","Bad Request",HttpStatus.BAD_REQUEST),
	SERVER_RESOURCE_NOT_FOUND_PPA_SOPIPR("PPA_SOPIPR_002","Domestic Standing Orders Consents","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
	REQUEST_TIMED_OUT_PPA_SOPIPR("PPA_SOPIPR_004","Domestic Standing Orders Consents","Request timed out for FS Service","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
	TECHNICAL_ERROR_PPA_SOPIPR("PPA_SOPIPR_999","Domestic Standing Orders Consents","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
	UNSUPPORTED_MEDIA_TYPE_PPA_SOPIPR("PPA_SOPIPR_005","Domestic Standing Orders Consents","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_NOT_AVAILABLE_PPA_SOPIPR("PPA_SOPIPR_006","Domestic Standing Orders Consents","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
	METHOD_NOT_ALLOWED_PPA_SOPIPR("PPA_SOPIPR_007","Domestic Standing Orders Consents","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	RESPONSE_TYPE_NOT_ACCEPTABLE_PPA_SOPIPR("PPA_SOPIPR_008","Domestic Standing Orders Consents","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
	SERVER_ACCESS_UNAUTHORIZED_PPA_SOPIPR("PPA_SOPIPR_009","Domestic Standing Orders Consents","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
	SERVER_ACCESS__DENIED_PPA_SOPIPR("PPA_SOPIPR_010","Domestic Standing Orders Consents","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	SERVER_ACCESS_FORBIDDEN_PPA_SOPIPR("PPA_SOPIPR_011","Domestic Standing Orders Consents","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
	SERVER_ACCESS_DENIED_PPA_SOPIPR("PPA_SOPIPR_012","Domestic Standing Orders Consents","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
	NOT_IMPLEMENTED_PPA_SOPIPR("PPA_SOPIPR_013","Domestic Standing Orders Consents","Used for API resource operations which haven't been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
	SERVER_METHOD_NOT_ALLOWED_PPA_SOPIPR("PPA_SOPIPR_014","Domestic Standing Orders Consents","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	SERVER_REQUEST_NOT_ACCEPTABLE_PPA_SOPIPR("PPA_SOPIPR_015","Domestic Standing Orders Consents","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
	SERVER_REQUEST_UNSUPPORTED_MEDIA_TYPE_PPA_SOPIPR("PPA_SOPIPR_016","Domestic Standing Orders Consents","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	SERVER_REQUEST_RATE_LIMIT_EXCEEDED_PPA_SOPIPR("PPA_SOPIPR_017","Domestic Standing Orders Consents","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
	SERVER_BAD_GATEWAY_PPA_SOPIPR("PPA_SOPIPR_018","Domestic Standing Orders Consents","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
	SERVER_BAD_REQUEST_PPA_SOPIPR("PPA_SOPIPR_019","Domestic Standing Orders Consents","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
	SERVER_BAD_RESPONSE_PPA_SOPIPR("PPA_SOPIPR_020","Domestic Standing Orders Consents","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
	
	/*Start for Domestic Standing Orders Validation*/
    BAD_REQUEST_PPA_SOPIPVP("PPA_SOPIPVP_001","Domestic Standing Orders Validation Process"," Bad request. Please check your request","Bad Request",HttpStatus.BAD_REQUEST),
    BAD_REQUEST_PPA_SOPIPVP_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Bad request. Please check your request","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_001"),
 BAD_REQUEST_PPA_SOPIPVP_2("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","General Error","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_004"),
 BAD_REQUEST_PPA_SOPIPVP_3("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee IBAN length is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_006"),
 BAD_REQUEST_PPA_SOPIPVP_3_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee IBAN length is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_006"),
 BAD_REQUEST_PPA_SOPIPVP_4("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee IBAN does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_007"),
 BAD_REQUEST_PPA_SOPIPVP_4_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee IBAN does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_007"),
 BAD_REQUEST_PPA_SOPIPVP_5("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee IBAN checksum is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_008"),
 BAD_REQUEST_PPA_SOPIPVP_5_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee IBAN checksum is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_008"),
 BAD_REQUEST_PPA_SOPIPVP_6("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee BIC does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_009"),
 BAD_REQUEST_PPA_SOPIPVP_6_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee BIC does not belong to SEPA zone","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_009"),
 BAD_REQUEST_PPA_SOPIPVP_7("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee BIC is not reachable","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_010"),
 BAD_REQUEST_PPA_SOPIPVP_7_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee BIC is not reachable","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_010"),
 BAD_REQUEST_PPA_SOPIPVP_8("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee BIC and IBAN do not match","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_011"),
 BAD_REQUEST_PPA_SOPIPVP_8_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee BIC and IBAN do not match","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_011"),
 BAD_REQUEST_PPA_SOPIPVP_9("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payer NSC is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_012"),
 BAD_REQUEST_PPA_SOPIPVP_9_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee NSC is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_012"),
 BAD_REQUEST_PPA_SOPIPVP_10("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account Number is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_013"),
 BAD_REQUEST_PPA_SOPIPVP_10_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account Number is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_013"),
 BAD_REQUEST_PPA_SOPIPVP_11("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Currency is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_014"),
 BAD_REQUEST_PPA_SOPIPVP_12("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account does not exist","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_015"),
 BAD_REQUEST_PPA_SOPIPVP_12_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account does not exist","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_015"),
 BAD_REQUEST_PPA_SOPIPVP_13("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account status must be active","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_016"),
 BAD_REQUEST_PPA_SOPIPVP_13_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account status must be active","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_016"),
 BAD_REQUEST_PPA_SOPIPVP_14("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payer Account Type is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_017"),
 BAD_REQUEST_PPA_SOPIPVP_14_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account Type is not valid","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_017"),
 BAD_REQUEST_PPA_SOPIPVP_15("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payer Account is blocked to debits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_018"),
 BAD_REQUEST_PPA_SOPIPVP_15_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account is blocked to debits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_018"),
 BAD_REQUEST_PPA_SOPIPVP_16("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payer Account is blocked to credits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_019"),
 BAD_REQUEST_PPA_SOPIPVP_16_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account is blocked to credits","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_019"),
 BAD_REQUEST_PPA_SOPIPVP_17("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payer Account is blocked to All","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_020"),
 BAD_REQUEST_PPA_SOPIPVP_17_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account is blocked to All","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_020"),
 BAD_REQUEST_PPA_SOPIPVP_18("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payer Credit grade cannot be 6 or 7","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_021"),
 BAD_REQUEST_PPA_SOPIPVP_18_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Credit grade cannot be 6 or 7","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_021"),
 BAD_REQUEST_PPA_SOPIPVP_19("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payer Account cannot be dormant","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_022"),
 BAD_REQUEST_PPA_SOPIPVP_19_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account cannot be dormant","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_022"),
 BAD_REQUEST_PPA_SOPIPVP_20("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account cannot be lien","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_023"),
 BAD_REQUEST_PPA_SOPIPVP_20_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Account cannot be lien","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_023"),
 BAD_REQUEST_PPA_SOPIPVP_21("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payer Joint account must be permitted by all the customer associated","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_024"),
 BAD_REQUEST_PPA_SOPIPVP_21_1("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payee Joint account must be permitted by all the customer associated","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_024"),
 BAD_REQUEST_PPA_SOPIPVP_22("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payer Account does not have enough funds","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_025"),
 BAD_REQUEST_PPA_SOPIPVP_23("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payer Limit exceeded for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_026"),
 BAD_REQUEST_PPA_SOPIPVP_24("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","Payer Limit exceeded on this payment type for the customer","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_027"),
 BAD_REQUEST_PPA_SOPIPVP_25("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process","The payer and the payee account cannot be the same","Server Bad Request",HttpStatus.BAD_REQUEST,"FS_PMV_037"),
    SERVER_RESOURCE_NOT_FOUND_PPA_SOPIPVP("PPA_SOPIPVP_002","Domestic Standing Orders Validation Process - Server Resource not found","Resource not found","Server resource not found",HttpStatus.NOT_FOUND),
    RESOURCE_NOT_FOUND_PPA_SOPIPVP("PPA_SOPIPVP_003","Domestic Standing Orders Valiation Process -Resource not found","URL resource not found","Resource not found",HttpStatus.NOT_FOUND),
    REQUEST_TIMEDOUT_PPA_SOPIPVP("PPA_SOPIPVP_004","Domestic Standing Orders Valiation Process - Request timed out", "Request timed out for FS Servcie","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
    TECHNICAL_ERROR_PPA_SOPIPVP("PPA_SOPIPVP_999","Domestic Standing Orders Validation Process - Technical Error. Please try again later","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
    UNSUPPORTED_MEDIA_TYPE_PPA_SOPIPVP("PPA_SOPIPVP_005","Domestic Standing Orders Validation Process - Unsupported media type","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
    SERVICE_UNAVAILABLE_PPA_SOPIPVP("PPA_SOPIPVP_006","Domestic Standing Orders Validation Process - Server not available","Connectivity error","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
    METHOD_NOT_ALLOWED_PPA_SOPIPVP("PPA_SOPIPVP_007","Domestic Standing Orders Validation Process - Method not allowed","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
    NOT_ACCEPTABLE_PPA_SOPIPVP("PPA_SOPIPVP_008","Domestic Standing Orders Validation Process - Response type not acceptable","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
    UNAUTHORIZED_PPA_SOPIPVP("PPA_SOPIPVP_009","Domestic Standing Orders Validation Process - Server Access Unauthorized","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
    ACCESS_DENIED_PPA_SOPIPVP("PPA_SOPIPVP_010","Domestic Standing Orders Validation Process - Server Access Denied","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
    FORBIDDEN_PPA_SOPIPVP("PPA_SOPIPVP_011","Domestic Standing Orders Validation Process - Server Access Forbidden","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
    FORBIDDEN_ACCESS_DENIED_PPA_SOPIPVP("PPA_SOPIPVP_012","Domestic Standing Orders Validation Process - Server Access Denied","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
    NOT_IMPLEMENTED_PPA_SOPIPVP("PPA_SOPIPVP_013","Domestic Standing Orders Validation Process - Not Implemented","Used for API resource operations which haven’t been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
    SERVER_METHOD_NOT_ALLOWED_PPA_SOPIPVP("PPA_SOPIPVP_014","Domestic Standing Orders Validation Process - Server Method not allowed","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
    REQUEST_NOT_ACCEPTABLE_PPA_SOPIPVP("PPA_SOPIPVP_015","Domestic Standing Orders Validation Process - Server Request Not Acceptable","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
    MEDIA_TYPE_UNSUPPORTED_PPA_SOPIPVP("PPA_SOPIPVP_016","Domestic Standing Orders Validation Process - Server Request Unsupported media type","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
    TOO_MANY_REQUESTS_PPA_SOPIPVP("PPA_SOPIPVP_017","Domestic Standing Orders Validation Process - Server Request Rate Limit Exceeded","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
    BAD_GATEWAY_PPA_SOPIPVP("PPA_SOPIPVP_018","Domestic Standing Orders Validation Process - Server Bad Gateway","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
    SERVER_BAD_REQUEST_PPA_SOPIPVP("PPA_SOPIPVP_019","Domestic Standing Orders Validation Process - Server Bad Request","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
   SERVER_BAD_RESPONSE_PPA_SOPIPVP("PPA_SOPIPVP_020","Domestic Standing Orders Validation Process - Server Bad Response","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
	  BAD_REQUEST_PPR_AUT("PPR_AUT_001","Authentication Service","Missing mandatory header value","Bad Request",HttpStatus.BAD_REQUEST),
	   
	   RESOURCE_NOT_FOUND_PPR_AUT("PPR_AUT_002","Authentication Service","URL resource not found","Resource not found",HttpStatus.NOT_FOUND),
	   
	   REQUEST_TIMED_OUT_PPR_AUT("PPR_AUT_003","Authentication Service","Request timed out for MWS Service","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
	   
	   TECHNICAL_ERROR_PPR_AUT("PPR_AUT_999","Authentication Service","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
	   
	   UNSUPPORTED_MEDIA_TYPE_PPR_AUT("PPR_AUT_004","Authentication Service","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	   
	   SERVER_NOT_AVAILABLE_PPR_AUT("PPR_AUT_005","Authentication Service","Downstream system not available","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
	   
	   METHOD_NOT_ALLOWED_PPR_AUT("PPR_AUT_006","Authentication Service","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	   
	   RESPONSE_TYPE_NOT_ACCEPTABLE_PPR_AUT("PPR_AUT_007","Authentication Service","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
	   
	   SERVER_ACCESS_UNAUTHORIZED_PPR_AUT("PPR_AUT_008","Authentication Service","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
	   
	   SERVER_ACCESS_DENIED1_PPR_AUT("PPR_AUT_009","Authentication Service","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	   
	   SERVER_ACCESS_FORBIDDEN_PPR_AUT("PPR_AUT_010","Authentication Service","API authorisation failed in the Downstream system","Server Access Forbidden",HttpStatus.FORBIDDEN),
	   
	   SERVER_ACCESS_DENIED2_PPR_AUT("PPR_AUT_011","Authentication Service","API authorisation failed in the Downstream system","Server Access Denied",HttpStatus.FORBIDDEN),
	   
	   NOT_IMPLEMENTED_PPR_AUT("PPR_AUT_012","Authentication Service","Used for API resource operations which haven’t been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
	   
	   SERVER_METHOD_NOT_ALLOWED_PPR_AUT("PPR_AUT_013","Authentication Service","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	   
	   SERVER_METHOD_NOT_ACCEPTABLE_PPR_AUT("PPR_AUT_014","Authentication Service","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
	   
	   SERVER_REQUEST_UNSUPPORTED_MEDIA_TYPE_PPR_AUT("PPR_AUT_015","Authentication Service","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	   
	   SERVER_REQUEST_RATE_LIMIT_PPR_AUT("PPR_AUT_016","Authentication Service","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
	   
	   SERVER_BAD_GATEWAY_PPR_AUT("PPR_AUT_017","Authentication Service","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
	   
	   SERVER_BAD_REQUEST_PPR_AUT("PPR_AUT_018","Authentication Service","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
	   
	   SERVER_BAD_RESPONSE_PPR_AUT("PPR_AUT_019","Authentication Service","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
	   
	   //SCA Authentication POST
	   
	   BAD_REQUEST_PAU_AUT_001("PAU_AUT_001","Authentication Service","All RAML validations (headers, body, parameters)","Bad Request",HttpStatus.BAD_REQUEST),
	   	
	   BAD_REQUEST_PAU_AUT_021("PAU_AUT_021","Authentication Service","1.Invalid authentication methods or parameters (decision taken in process api)\r\n" + "2.Invalid field value <field name> (invalid date of birth ,invalid pin positions,invalid pin position values)","Bad Request",HttpStatus.BAD_REQUEST),
	   
	   RESOURCE_NOT_FOUND_PAU_AUT("PAU_AUT_003","Authentication Service","URL resource not found","Resource not found",HttpStatus.NOT_FOUND),
	   
	   REQUEST_TIMEOUT_PAU_AUT("PAU_AUT_004","Authentication Service","digital user timeout for DOB OR cbsp service","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
	
	   REQUEST_TIMEOUT_2_PAU_AUT("PAU_AUT_999","Authentication Service","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
	   
	   UNSUPPORTED_MEDIA_PAU_AUT("PAU_AUT_005","Authentication Service","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	   
	   SERVER_NOT_AVAILABLE_PAU_AUT("PAU_AUT_006","Authentication Service","Downstream system not available","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
	   
	   METHOD_NOT_ALLOWED_PAU_AUT("PAU_AUT_007","Authentication Service","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	   
	   RESPONSE_NOT_ACCEPTABLE_PAU_AUT("PAU_AUT_008","Authentication Service","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
	   
	   SERVER_ACCESS_UNAUTHORISED_PAU_AUT("PAU_AUT_009","Authentication Service","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
	   
	   SERVER_ACCESS_DENIED_PAU_AUT("PAU_AUT_010","Authentication Service","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	   
	   SERVER_ACCESS_FORBIDDEN_PAU_AUT("PAU_AUT_011","Authentication Service","API authentication failed in the Downstream system","Server Access Forbidden",HttpStatus.UNAUTHORIZED),
	   
	   SERVER_ACCESS_DENIED2_PAU_AUT("PAU_AUT_012","Authentication Service","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	   
	   NOT_IMPLEMENTED_PAU_AUT("PAU_AUT_013","Authentication Service","Used for API resource operations which haven’t been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
	   
	   SERVER_METHOD_NOT_ALLOWED_PAU_AUT("PAU_AUT_014","Authentication Service","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	   
	   SERVER_METHOD_NOT_ACCEPTABLE_PAU_AUT("PAU_AUT_015","Authentication Service","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
	   
	   SERVER_REQUEST_UNSUPPORTED_MEDIA_TYPE_PAU_AUT("PAU_AUT_016","Authentication Service","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	   
	   SERVER_REQUEST_RATE_LIMIT_PAU_AUT("PAU_AUT_017","Authentication Service","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
	   
	   SERVER_BAD_GATEWAY_PAU_AUT("PAU_AUT_018","Authentication Service","Failure received from Downstream system","Technical Error. Please try again later",HttpStatus.BAD_GATEWAY),
	   
	   SERVER_BAD_REQUEST_PAU_AUT("PAU_AUT_019","Authentication Service","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
	   
	   SERVER_BAD_RESPONSE_PAU_AUT("PAU_AUT_020","Authentication Service","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
	
	//SCA Retrieve Authentication
	   
	   BAD_REQUEST_PAU_AUP_001("PAU_AUP_001","Authentication Service","1. Missing mandatory header value\n"+"2. Invalid header parameter value <parameter name>\n"+"3. Invalid event type\n"+"4. Invalid channel code","Bad Request",HttpStatus.BAD_REQUEST),
	   	
	   SERVER_RESOURCE_NOT_FOUND_PAU_AUP("PAU_AUP_021","Authentication Service","No registered devices exist for the user","Server resource not found",HttpStatus.NOT_FOUND),
	   
	   SERVER_RESOURCE_NOT_FOUND_2_PAU_AUP("PAU_AUP_022","Authentication Service","No active devices exist for this user","Server resource not found",HttpStatus.NOT_FOUND),
	   
	   RESOURCE_NOT_FOUND_PAU_AUP("PAU_AUP_003","Authentication Service","URL resource not found","Resource not found",HttpStatus.NOT_FOUND),
	   
	   REQUEST_TIMEOUT_PAU_AUP("PAU_AUP_004","Authentication Service","Request timed out for downstream system timeout for configuration system api or cbsp services get pin digits or search devices","Request timed out",HttpStatus.GATEWAY_TIMEOUT),
	
	   TECHNICAL_ERROR_PAU_AUP("PAU_AUP_999","Authentication Service","Technical Error. Please try again later","Technical Error. Please try again later",HttpStatus.INTERNAL_SERVER_ERROR),
	   
	   UNSUPPORTED_MEDIA_PAU_AUP("PAU_AUP_005","Authentication Service","Unsupported media type","Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	   
	   SERVER_NOT_AVAILABLE_PAU_AUP("PAU_AUP_006","Authentication Service","Downstream system not available","Server not available",HttpStatus.SERVICE_UNAVAILABLE),
	   
	   METHOD_NOT_ALLOWED_PAU_AUP("PAU_AUP_007","Authentication Service","Method not allowed","Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	   
	   RESPONSE_NOT_ACCEPTABLE_PAU_AUP("PAU_AUP_008","Authentication Service","Response type provided in the accept header is not allowed","Response type not acceptable",HttpStatus.NOT_ACCEPTABLE),
	   
	   SERVER_ACCESS_UNAUTHORISED_PAU_AUP("PAU_AUP_009","Authentication Service","API authentication failed in the Downstream system","Server Access Unauthorized",HttpStatus.UNAUTHORIZED),
	   
	   SERVER_ACCESS_DENIED_PAU_AUP("PAU_AUP_010","Authentication Service","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	   
	   SERVER_ACCESS_FORBIDDEN_PAU_AUP("PAU_AUP_011","Authentication Service","API authentication failed in the Downstream system","Server Access Forbidden",HttpStatus.UNAUTHORIZED),
	   
	   SERVER_ACCESS_DENIED2_PAU_AUP("PAU_AUP_012","Authentication Service","API authentication failed in the Downstream system","Server Access Denied",HttpStatus.UNAUTHORIZED),
	   
	   NOT_IMPLEMENTED_PAU_AUP("PAU_AUP_013","Authentication Service","Used for API resource operations which haven’t been implemented","Not Implemented",HttpStatus.NOT_IMPLEMENTED),
	   
	   SERVER_METHOD_NOT_ALLOWED_PAU_AUP("PAU_AUP_014","Authentication Service","Method not allowed in the Downstream system","Server Method not allowed",HttpStatus.METHOD_NOT_ALLOWED),
	   
	   SERVER_METHOD_NOT_ACCEPTABLE_PAU_AUP("PAU_AUP_015","Authentication Service","Response type provided in the accept header is not allowed in the Downstream system","Server Request Not Acceptable",HttpStatus.NOT_ACCEPTABLE),
	   
	   SERVER_REQUEST_UNSUPPORTED_MEDIA_TYPE_PAU_AUP("PAU_AUP_016","Authentication Service","Unsupported media type in the Downstream system","Server Request Unsupported media type",HttpStatus.UNSUPPORTED_MEDIA_TYPE),
	   
	   SERVER_REQUEST_RATE_LIMIT_PAU_AUP("PAU_AUP_017","Authentication Service","Rate limit exceeded while calling the Downstream system","Server Request Rate Limit Exceeded",HttpStatus.TOO_MANY_REQUESTS),
	   
	   SERVER_BAD_GATEWAY_PAU_AUP("PAU_AUP_018","Authentication Service","Failure received from Downstream system","Server Bad Gateway",HttpStatus.BAD_GATEWAY),
	   
	   SERVER_BAD_REQUEST_PAU_AUP("PAU_AUP_019","Authentication Service","Request to the Downstream system failed validation","Server Bad Request",HttpStatus.BAD_REQUEST),
	   
	   SERVER_BAD_RESPONSE_PAU_AUP("PAU_AUP_020","Authentication Service","Request from the Downstream system failed validation","Server Bad Response",HttpStatus.BAD_REQUEST),
	
	
		//SCA Related XSS Threat Protection Policy
	   XSS_Threat_Protection_Policy_03("API_POL_003","Authentication Service","XSS Threat Protection Policy.","An XSS Attempt was detected in the request body.",HttpStatus.BAD_REQUEST),
	   
	   XSS_Threat_Protection_Policy_04("API_POL_004","Authentication Service","XSS Threat Protection Policy.","An error occurred whilst executing the XSS Policy",HttpStatus.INTERNAL_SERVER_ERROR);


	
	private String errorCode;

	private String api;

	private String description;
	
	private String summary; 

	private HttpStatus status;

	private String fsErrorCode;

	private ErrorCodeEnum(String errorCode, String api, String description, HttpStatus status) {
		this.errorCode = errorCode;
		this.api = api;
		this.description = description;
		this.status = status;
	}
	

	private ErrorCodeEnum(String errorCode, String api, String description, String summary, HttpStatus status) {
		this.errorCode = errorCode;
		this.api = api;
		this.description = description;
		this.summary =summary;
		this.status = status;
	}
	
	private ErrorCodeEnum(String errorCode, String api, String description, String summary, HttpStatus status, String fsErrorCode) {
		this.errorCode = errorCode;
		this.api = api;
		this.description = description;
		this.summary=summary;
		this.status = status;
		this.fsErrorCode = fsErrorCode;
	}

	public String getSummary() {
		return summary;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getApi() {
		return api;
	}

	public String getDescription() {
		return description;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public String getFsErrorCode() {
		return fsErrorCode;
	}
}
