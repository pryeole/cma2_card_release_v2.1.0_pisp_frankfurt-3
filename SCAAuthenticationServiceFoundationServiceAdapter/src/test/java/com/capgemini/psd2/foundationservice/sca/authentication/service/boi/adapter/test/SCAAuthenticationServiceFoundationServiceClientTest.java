package com.capgemini.psd2.foundationservice.sca.authentication.service.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.adapter.security.domain.DigitalUser13;
import com.capgemini.psd2.adapter.security.domain.Login;
import com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.client.SCAAuthenticationServiceFoundationServiceClient;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class SCAAuthenticationServiceFoundationServiceClientTest {

	@InjectMocks
	private SCAAuthenticationServiceFoundationServiceClient authenticationApplicationFoundationServiceClient;

	@Mock
	private RestClientSyncImpl restClient;

	@Mock
	private RestTemplate restTemplate;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Method postConstruct = SCAAuthenticationServiceFoundationServiceClient.class.getDeclaredMethod("init", null);
		postConstruct.setAccessible(true);
		postConstruct.invoke(authenticationApplicationFoundationServiceClient);
	}

	@Test
	public void restTransportForAuthenticationServicePOST() {

		RequestHeaderAttributes requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setChannelId("b365");
		requestHeaderAttributes.setCorrelationId("hiyh");
		requestHeaderAttributes.setIdempotencyKey("iouy");
		requestHeaderAttributes.setIntentId("jk");
		requestHeaderAttributes.setPsuId("uituii");
		assertNotNull(requestHeaderAttributes);
		
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8974/jhvjhf/jiyi");
		requestInfo.setRequestHeaderAttributes(requestHeaderAttributes);

		Login login = new Login();
		DigitalUser13 digitalUser13 = new DigitalUser13();
//		digitalUser13.setDigitalUserIdentifier("guiui98890");
		login.setDigitalUser(digitalUser13);
  
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		httpHeaders.add("X-CORRELATION-ID", "header correlation Id");

		authenticationApplicationFoundationServiceClient.restTransportForAuthenticationServicePOST(requestInfo, login,
				com.capgemini.psd2.adapter.security.domain.LoginResponse.class, httpHeaders);
	}

}