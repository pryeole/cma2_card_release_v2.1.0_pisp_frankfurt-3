package com.capgemini.psd2.foundationservice.sca.authentication.service.boi.adapter.test;

import java.security.Principal;

public class PrincipalImpl implements Principal {

	private String principal;
	
	
	public PrincipalImpl(String principal) {
		super();
		this.principal = principal;
	}

	@Override
	public String toString() {
		return this.principal;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

}
