package com.capgemini.psd2.foundationservice.sca.authentication.service.boi.adapter.test;

import static org.mockito.Matchers.any;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostRequest;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostResponse;
import com.capgemini.psd2.adapter.security.domain.Login;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.SCAAuthenticationServiceFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.client.SCAAuthenticationServiceFoundationServiceClient;
import com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.delegate.SCAAuthenticationServiceFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.transformer.SCAAuthenticationServiceFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class SCAAuthenticationServiceFoundationServiceAdapterTest {

	@InjectMocks
	private SCAAuthenticationServiceFoundationServiceAdapter authenticationApplicationFoundationServiceAdapter;

	@Mock
	private SCAAuthenticationServiceFoundationServiceClient authenticationApplicationFoundationServiceClient;

	@Mock
	private SCAAuthenticationServiceFoundationServiceDelegate authenticationApplicationFoundationServiceDelegate;

	@Mock
	private SCAAuthenticationServiceFoundationServiceTransformer scaAuthenticationServiceFoundationServiceTransformer;

	@Mock
	private AdapterUtility adapterUtility;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

@Test
	public void setretrieveAuthenticationService() {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(AdapterSecurityConstants.USER_HEADER, "test");
		params.put(AdapterSecurityConstants.CHANNELID_HEADER, "test");
		params.put(AdapterSecurityConstants.CORRELATIONID_HEADER, "test");

		CustomAuthenticationServicePostRequest request = new CustomAuthenticationServicePostRequest();
		CustomAuthenticationServicePostResponse postresponse = new CustomAuthenticationServicePostResponse();
		Login login = new Login();
		com.capgemini.psd2.adapter.security.domain.LoginResponse response = new com.capgemini.psd2.adapter.security.domain.LoginResponse();

		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		httpHeaders.add("X-CORRELATION-ID", "header correlation Id");

		requestInfo.setUrl("http://localhost:8181/hgfgh/utuyhjfyjh");

		
		authenticationApplicationFoundationServiceAdapter.retrieveAuthenticationService(request, params);
		Mockito.when(authenticationApplicationFoundationServiceClient.restTransportForAuthenticationServicePOST(any(),
				any(), any(), any())).thenReturn(response);
		Mockito.when(authenticationApplicationFoundationServiceDelegate.createRequestHeaders(any(), any()))
				.thenReturn(httpHeaders);
		Mockito.when(authenticationApplicationFoundationServiceDelegate.postAuthenticationFoundationServiceURL(any()))
				.thenReturn(null);
		Mockito.when(scaAuthenticationServiceFoundationServiceTransformer.transformAuthenticationServiceRequest(any(),any()))
				.thenReturn(login);
		Mockito.when(scaAuthenticationServiceFoundationServiceTransformer.transformAuthenticationServiceResponse(any()))
				.thenReturn(postresponse);

	}

}