package com.capgemini.psd2.foundationservice.sca.authentication.service.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostRequest;
import com.capgemini.psd2.adapter.security.domain.AuthenticationMethodCode1;
import com.capgemini.psd2.adapter.security.domain.AuthenticationParameterTextTypeValue;
import com.capgemini.psd2.adapter.security.domain.BrandCode3;
import com.capgemini.psd2.adapter.security.domain.ChannelCode;
import com.capgemini.psd2.adapter.security.domain.CustomerAuthenticationSession;
import com.capgemini.psd2.adapter.security.domain.DeviceStatusCode;
import com.capgemini.psd2.adapter.security.domain.DeviceType;
import com.capgemini.psd2.adapter.security.domain.DigitalUser;
import com.capgemini.psd2.adapter.security.domain.DigitalUser13;
import com.capgemini.psd2.adapter.security.domain.DigitalUserSession;
import com.capgemini.psd2.adapter.security.domain.EventType1;
import com.capgemini.psd2.adapter.security.domain.LoginResponse;
import com.capgemini.psd2.adapter.security.domain.SecureAccessKeyPositionValue;
import com.capgemini.psd2.adapter.security.domain.SelectedDeviceReference;
import com.capgemini.psd2.adapter.security.domain.Type2;
import com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.transformer.SCAAuthenticationServiceFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.CreditorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;

public class SCAAuthenticationServiceFoundationServiceTransformerTest {

	@InjectMocks
	private SCAAuthenticationServiceFoundationServiceTransformer scaAuthenticationServiceFoundationServiceTransformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	Map<String, Object> params = new HashedMap();

	CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
	CreditorDetails credDetails = new CreditorDetails();
	CustomDebtorDetails debtDetails = new CustomDebtorDetails();
	AmountDetails amountDetails = new AmountDetails();

	@Test
	public void testtransformAuthenticationServiceResponse() {

		LoginResponse loginResponse = new LoginResponse();

		DigitalUser digitalUser = new DigitalUser();
		digitalUser.setDigitalUserIdentifier("hkjh");
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setDigitalUserLockedOutIndicator(true);
		loginResponse.setDigitalUser(digitalUser);
		assertNotNull(digitalUser);

		DigitalUserSession digitalUserSession = new DigitalUserSession();
		digitalUserSession.setSessionInitiationFailureIndicator(false);
		digitalUserSession.setSessionInitiationFailureReasonCode("jhgj");
		digitalUserSession.setSessionStartDateTime("9-8-2019");
		loginResponse.setDigitalUserSession(digitalUserSession);

		scaAuthenticationServiceFoundationServiceTransformer.transformAuthenticationServiceResponse(loginResponse);
	}

	@Test
	public void testtransformAuthenticationServiceRequest() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		DigitalUser13 digitalUser = new DigitalUser13();

		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PIN);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

	}

//	@Test
//	public void testtransformAuthenticationServiceRequest123() {
//
//		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();
//
//		DigitalUser13 digitalUser = new DigitalUser13();
//
//		Map<String, Object> params = new HashMap<String, Object>();
//
//		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
//		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
//		params.put(PSD2Constants.CHANNEL_ID, "b365");
//		assertNotNull(params);
//		List<CustomerAuthenticationSession> list = new ArrayList<>();
//		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
//		// customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PIN);
//		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
//		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);
//
//		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
//		selectedDeviceReference.setDeviceNameText("ijioj");
//		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
//		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
//		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);
//
//		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
//		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
//		secureAccessKeyPositionValue.setPosition(5.0);
//		secureAccessKeyPositionValue.setValue("5");
//		list3.add(secureAccessKeyPositionValue);
//		customerAuthenticationSession.setSecureAccessKeyUsed(list3);
//
//		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
//		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
//		authenticationParameterTextTypeValue.setValue("ujo");
//		list2.add(authenticationParameterTextTypeValue);
//		customerAuthenticationSession.setAuthenticationParameterText(list2);
//
//		list.add(customerAuthenticationSession);
//		digitalUser.setCustomerAuthenticationSession(list);
//		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);
//
//		scaAuthenticationServiceFoundationServiceTransformer
//				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
//
//	}

//	@Test
//	public void testtransformAuthenticationServiceRequest124() {
//
//		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();
//
//		DigitalUser13 digitalUser = new DigitalUser13();
//
//		Map<String, Object> params = new HashMap<String, Object>();
//
//		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
//		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
//		params.put(PSD2Constants.CHANNEL_ID, "b365");
//		assertNotNull(params);
//		customAuthenticationServicePostRequest.setChannelType("WEB");
//		List<CustomerAuthenticationSession> list = new ArrayList<>();
//		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
//		// customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PIN);
//		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
//		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);
//
//		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
//		selectedDeviceReference.setDeviceNameText("ijioj");
//		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
//		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
//		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);
//
//		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
//		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
//		secureAccessKeyPositionValue.setPosition(5.0);
//		secureAccessKeyPositionValue.setValue("5");
//		list3.add(secureAccessKeyPositionValue);
//		customerAuthenticationSession.setSecureAccessKeyUsed(list3);
//
//		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
//		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
//		authenticationParameterTextTypeValue.setValue("ujo");
//		list2.add(authenticationParameterTextTypeValue);
//		customerAuthenticationSession.setAuthenticationParameterText(list2);
//
//		list.add(customerAuthenticationSession);
//		digitalUser.setCustomerAuthenticationSession(list);
//		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);
//
//		scaAuthenticationServiceFoundationServiceTransformer
//				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
//
//	}

	
	@Test
	public void testtransformAuthenticationServiceRequest1() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PIN);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer.transformAuthenticationServiceRequest(null, null);

	}

	@Test
	public void testtransformAuthenticationServiceRequest3() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PIN);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);
		list.add(customerAuthenticationSession);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, null);

		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, null);
	}

	@Test
	public void testtransformAuthenticationServiceRequest4() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PIN);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);
		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, null);

	}

	@Test
	public void testtransformAuthenticationServiceRequest5() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

	}

	@Test
	public void testtransformAuthenticationServiceRequest31() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.DATE_OF_BIRTH);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5-8-2019");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

	}

	@Test
	public void testtransformAuthenticationServiceRequest8() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.DATE_OF_BIRTH);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		// secureAccessKeyPositionValue.setValue("5-8-2019");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
	}

	@Test(expected = Exception.class)
	public void testtransformAuthenticationServiceRequest12() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.DATE_OF_BIRTH);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5-8-2019");
		list3.add(secureAccessKeyPositionValue);
		// customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

	}

	@Test
	public void testtransformAuthenticationServiceRequest9() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.DATE_OF_BIRTH);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5-8-2019");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

	}

	@Test
	public void testtransformAuthenticationServiceRequest11() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setType(Type2.AMOUNT);
		authenticationParameterTextTypeValue.setValue("44");
		authenticationParameterTextTypeValue.setType(Type2.CURRENCY);
		authenticationParameterTextTypeValue.setValue("GBP");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		params.put("intentType", "PISP");
		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
		debtDetails.setIdentification("IE64IRCE92050112345678");
		debtDetails.setSchemeName("UK.OBIE.IBAN");
		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
	}

//	@Test
//	public void testtransformAuthenticationServiceRequest125() {
//
//		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();
//
//		DigitalUser13 digitalUser = new DigitalUser13();
//
//		Map<String, Object> params = new HashMap<String, Object>();
//
//		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
//		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
//		params.put(PSD2Constants.CHANNEL_ID, "b365");
//		assertNotNull(params);
//		List<CustomerAuthenticationSession> list = new ArrayList<>();
//		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
//		
//
//		
//		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
//		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
//		secureAccessKeyPositionValue.setPosition(5.0);
//		secureAccessKeyPositionValue.setValue("5");
//		list3.add(secureAccessKeyPositionValue);
//		customerAuthenticationSession.setSecureAccessKeyUsed(list3);
//
//		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
//		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
//		authenticationParameterTextTypeValue.setType(Type2.AMOUNT);
//		authenticationParameterTextTypeValue.setValue("44");
//		authenticationParameterTextTypeValue.setType(Type2.CURRENCY);
//		authenticationParameterTextTypeValue.setValue("GBP");
//		list2.add(authenticationParameterTextTypeValue);
//		customerAuthenticationSession.setAuthenticationParameterText(list2);
//
//		list.add(customerAuthenticationSession);
//		digitalUser.setCustomerAuthenticationSession(list);
//		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);
//
//		params.put("intentType", "PISP");
//		credDetails.setIdentification("IE64IRCE92050112345678");
//		credDetails.setSchemeName("UK.OBIE.IBAN");
//		credDetails.setName("Manik");
//		customConsentAppViewData.setCreditorDetails(credDetails);
//		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
//		debtDetails.setIdentification("IE64IRCE92050112345678");
//		debtDetails.setSchemeName("UK.OBIE.IBAN");
//		customConsentAppViewData.setDebtorDetails(debtDetails);
//		amountDetails.setAmount("90k");
//		amountDetails.setCurrency("EUR");
//		customConsentAppViewData.setAmountDetails(amountDetails);
//		params.put("custObj", customConsentAppViewData);
//
//		scaAuthenticationServiceFoundationServiceTransformer
//				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
//	}
//
//	
	@Test
	public void testtransformAuthenticationServiceRequest188() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setType(Type2.AMOUNT);
		authenticationParameterTextTypeValue.setValue("44");
		authenticationParameterTextTypeValue.setType(Type2.CURRENCY);
		authenticationParameterTextTypeValue.setValue("GBP");
		

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		params.put("intentType", "PISP");
		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
		debtDetails.setIdentification("IE64IRCE92050112345678");
		debtDetails.setSchemeName("UK.OBIE.IBAN");
		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
	}

	@Test
	public void testtransformAuthenticationServiceRequest114() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		SecureAccessKeyPositionValue secureAccessKeyPositionValue1 = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue1.setPosition(5.0);
		secureAccessKeyPositionValue1.setValue("5");
		list3.add(secureAccessKeyPositionValue1);
		SecureAccessKeyPositionValue secureAccessKeyPositionValue2 = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue2.setPosition(5.0);
		secureAccessKeyPositionValue2.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setType(Type2.AMOUNT);
		authenticationParameterTextTypeValue.setValue("44");
		authenticationParameterTextTypeValue.setType(Type2.CURRENCY);
		authenticationParameterTextTypeValue.setValue("GBP");
		

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		params.put("intentType", "PISP");
		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
		debtDetails.setIdentification("IE64IRCE92050112345678");
		debtDetails.setSchemeName("UK.OBIE.IBAN");
		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
	}

	
	@Test
	public void testtransformAuthenticationServiceRequest15() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);
		params.put("intentType", "PISP");
		params.put("AdapterSecurityConstants.CHANNEL_BRAND", BrandCode3.NIGB.getValue());

		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
//		debtDetails.setIdentification("IE64IRCE92050112345678");
//		debtDetails.setSchemeName("UK.OBIE.IBAN");
//		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setType(Type2.AMOUNT);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.PAYEE_IBAN);
		list2.add(authenticationParameterTextTypeValue);

		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.CURRENCY);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.PAYER_IBAN);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.PAYEE_NAME);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		debtDetails.setIdentification("IE64IRCE92050112345678");
		debtDetails.setSchemeName("UK.OBIE.IBAN");
		customConsentAppViewData.setDebtorDetails(debtDetails);
		params.put("custObj", customConsentAppViewData);
		
		scaAuthenticationServiceFoundationServiceTransformer
		.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
		
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);
		scaAuthenticationServiceFoundationServiceTransformer
		.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
		
	}

	@Test
	public void testtransformAuthenticationServiceRequest154() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setCustomerAuthenticationAppIdentifier("AppIdentifier");
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);
		params.put("intentType", "PISP");
		params.put("AdapterSecurityConstants.CHANNEL_BRAND", BrandCode3.NIGB.getValue());

		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
//		debtDetails.setIdentification("IE64IRCE92050112345678");
//		debtDetails.setSchemeName("UK.OBIE.IBAN");
//		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setType(Type2.AMOUNT);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.PAYEE_IBAN);
		list2.add(authenticationParameterTextTypeValue);

		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.CURRENCY);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.PAYER_IBAN);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.PAYEE_NAME);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		debtDetails.setIdentification("IE64IRCE92050112345678");
		debtDetails.setSchemeName("UK.OBIE.IBAN");
		customConsentAppViewData.setDebtorDetails(debtDetails);
		params.put("custObj", customConsentAppViewData);
		
		scaAuthenticationServiceFoundationServiceTransformer
		.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
		
		scaAuthenticationServiceFoundationServiceTransformer
		.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
	}

	
	@Test
	public void testtransformAuthenticationServiceRequest16() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		params.put("intentType", "PISP");
		params.put("AdapterSecurityConstants.CHANNEL_BRAND", BrandCode3.ROI.getValue());

		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
//		debtDetails.setIdentification("IE64IRCE92050112345678");
//		debtDetails.setSchemeName("UK.OBIE.IBAN");
//		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		authenticationParameterTextTypeValue.setType(Type2.AMOUNT);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.PAYEE_IBAN);
		list2.add(authenticationParameterTextTypeValue);

		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.CURRENCY);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.PAYER_IBAN);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.PAYEE_NAME);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
		
		debtDetails.setIdentification("IE64IRCE92050112345678");
		debtDetails.setSchemeName("UK.OBIE.IBAN");
		customConsentAppViewData.setDebtorDetails(debtDetails);
		params.put("custObj", customConsentAppViewData);
		scaAuthenticationServiceFoundationServiceTransformer
		.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
	}

	@Test
	public void testtransformAuthenticationServiceRequest17() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);
		params.put("intentType", "PISP");
		params.put("AdapterSecurityConstants.CHANNEL_BRAND", BrandCode3.NIGB.getValue());

		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
//		debtDetails.setIdentification("IE64IRCE92050112345678");
//		debtDetails.setSchemeName("UK.OBIE.IBAN");
//		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setType(Type2.AMOUNT);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);
		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);
		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.PAYEE_IBAN);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);
		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);
		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.CURRENCY);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);
		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);
		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.PAYER_IBAN);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);
		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);
		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

		authenticationParameterTextTypeValue.setType(Type2.PAYEE_NAME);
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);
		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);
		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
		
		debtDetails.setIdentification("IE64IRCE92050112345678");
		debtDetails.setSchemeName("UK.OBIE.IBAN");
		customConsentAppViewData.setDebtorDetails(debtDetails);
		params.put("custObj", customConsentAppViewData);
		scaAuthenticationServiceFoundationServiceTransformer
		.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);

	}

	@Test
	public void testtransformAuthenticationServiceRequest18() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setType(Type2.AMOUNT);
		list2.add(authenticationParameterTextTypeValue);
		authenticationParameterTextTypeValue.setType(Type2.PAYEE_IBAN);
		list2.add(authenticationParameterTextTypeValue);
		authenticationParameterTextTypeValue.setType(Type2.CURRENCY);

		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		params.put("intentType", "PISP");
		params.put("AdapterSecurityConstants.CHANNEL_BRAND", BrandCode3.NIGB.getValue());

		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
		debtDetails.setIdentification("IE64IRCE92050112345678");
		debtDetails.setSchemeName("UK.OBIE.IBAN");
		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);
		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
	}

	@Test
	public void testtransformAuthenticationServiceRequest19() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		params.put("intentType", "PISP");
		params.put("AdapterSecurityConstants.CHANNEL_BRAND", BrandCode3.NIGB.getValue());

		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
		debtDetails.setIdentification("IE64IRCE92050112345678");
		debtDetails.setSchemeName("UK.OBIE.IBAN");
		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue1=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue1.setType(Type2.AMOUNT);
		list2.add(authenticationParameterTextTypeValue1);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue2=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue2.setType(Type2.CURRENCY);
		list2.add(authenticationParameterTextTypeValue2);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue3=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue3.setType(Type2.PAYEE_ACCOUNT_NUMBER);
		list2.add(authenticationParameterTextTypeValue3);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue4=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue4.setType(Type2.PAYEE_IBAN);
		list2.add(authenticationParameterTextTypeValue4);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue5=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue5.setType(Type2.PAYEE_NAME);
		list2.add(authenticationParameterTextTypeValue5);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue6=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue6.setType(Type2.PAYER_ACCOUNT_NUMBER);
		list2.add(authenticationParameterTextTypeValue6);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue7=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue7.setType(Type2.PAYER_IBAN);
		list2.add(authenticationParameterTextTypeValue7);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue8=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue8.setType(Type2.TPP_NAME);
		list2.add(authenticationParameterTextTypeValue8);
		
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
	}
	
	@Test
	public void testtransformAuthenticationServiceRequest24() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		params.put("intentType", "PISP");
		params.put("AdapterSecurityConstants.CHANNEL_BRAND", BrandCode3.NIGB.getValue());

		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
		debtDetails.setIdentification("IE64IRCE92050112345678");
		debtDetails.setSchemeName("UK.OBIE.IBAN");
		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue1=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue1.setType(Type2.AMOUNT);
		list2.add(authenticationParameterTextTypeValue1);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue2=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue2.setType(Type2.CURRENCY);
		list2.add(authenticationParameterTextTypeValue2);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue3=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue3.setType(Type2.PAYEE_ACCOUNT_NUMBER);
		list2.add(authenticationParameterTextTypeValue3);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue5=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue5.setType(Type2.PAYEE_NAME);
		list2.add(authenticationParameterTextTypeValue5);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue6=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue6.setType(Type2.PAYER_ACCOUNT_NUMBER);
		list2.add(authenticationParameterTextTypeValue6);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue8=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue8.setType(Type2.TPP_NAME);
		list2.add(authenticationParameterTextTypeValue8);
		
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
		
	}
	
	@Test
	public void testtransformAuthenticationServiceRequest20() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setCustomerAuthenticationAppIdentifier("abc");
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		params.put("intentType", "PISP");
		params.put("AdapterSecurityConstants.CHANNEL_BRAND", BrandCode3.NIGB.getValue());

		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
//		debtDetails.setIdentification("IE64IRCE92050112345678");
//		debtDetails.setSchemeName("UK.OBIE.IBAN");
//		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue1=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue1.setType(Type2.AMOUNT);
		list2.add(authenticationParameterTextTypeValue1);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue2=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue2.setType(Type2.CURRENCY);
		list2.add(authenticationParameterTextTypeValue2);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue3=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue3.setType(Type2.PAYEE_ACCOUNT_NUMBER);
		list2.add(authenticationParameterTextTypeValue3);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue5=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue5.setType(Type2.PAYEE_NAME);
		list2.add(authenticationParameterTextTypeValue5);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue6=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue6.setType(Type2.PAYER_ACCOUNT_NUMBER);
		list2.add(authenticationParameterTextTypeValue6);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue8=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue8.setType(Type2.TPP_NAME);
		list2.add(authenticationParameterTextTypeValue8);
		
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
		
	}
	
	@Test
	public void testtransformAuthenticationServiceRequest21() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		params.put("intentType", "PISP");
		params.put("AdapterSecurityConstants.CHANNEL_BRAND", BrandCode3.ROI.getValue());

		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
		debtDetails.setIdentification("IE64IRCE92050112345678");
		debtDetails.setSchemeName("UK.OBIE.IBAN");
		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue1=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue1.setType(Type2.AMOUNT);
		list2.add(authenticationParameterTextTypeValue1);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue2=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue2.setType(Type2.CURRENCY);
		list2.add(authenticationParameterTextTypeValue2);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue4=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue4.setType(Type2.PAYEE_IBAN);
		list2.add(authenticationParameterTextTypeValue4);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue5=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue5.setType(Type2.PAYEE_NAME);
		list2.add(authenticationParameterTextTypeValue5);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue7=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue7.setType(Type2.PAYER_IBAN);
		list2.add(authenticationParameterTextTypeValue7);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue8=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue8.setType(Type2.TPP_NAME);
		list2.add(authenticationParameterTextTypeValue8);
		
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
	}
	
	@Test
	public void testtransformAuthenticationServiceRequest22() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		params.put("intentType", "PISP");
		params.put("AdapterSecurityConstants.CHANNEL_BRAND", BrandCode3.ROI.getValue());

		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue1=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue1.setType(Type2.AMOUNT);
		list2.add(authenticationParameterTextTypeValue1);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue2=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue2.setType(Type2.CURRENCY);
		list2.add(authenticationParameterTextTypeValue2);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue4=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue4.setType(Type2.PAYEE_IBAN);
		list2.add(authenticationParameterTextTypeValue4);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue5=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue5.setType(Type2.PAYEE_NAME);
		list2.add(authenticationParameterTextTypeValue5);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue7=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue7.setType(Type2.PAYER_IBAN);
		list2.add(authenticationParameterTextTypeValue7);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue8=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue8.setType(Type2.TPP_NAME);
		list2.add(authenticationParameterTextTypeValue8);
		
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
	}
	
	@Test
	public void testtransformAuthenticationServiceRequest28() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		params.put("intentType", "PISP");
		params.put("AdapterSecurityConstants.CHANNEL_BRAND", BrandCode3.NIGB.getValue());

		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
		debtDetails.setIdentification("IE64IRCE92050112345678");
		debtDetails.setSchemeName("UK.OBIE.IBAN");
		customConsentAppViewData.setDebtorDetails(debtDetails);
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue1=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue1.setType(Type2.AMOUNT);
		list2.add(authenticationParameterTextTypeValue1);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue2=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue2.setType(Type2.CURRENCY);
		list2.add(authenticationParameterTextTypeValue2);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue4=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue4.setType(Type2.PAYEE_IBAN);
		list2.add(authenticationParameterTextTypeValue4);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue5=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue5.setType(Type2.PAYEE_NAME);
		list2.add(authenticationParameterTextTypeValue5);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue7=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue7.setType(Type2.PAYER_IBAN);
		list2.add(authenticationParameterTextTypeValue7);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue8=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue8.setType(Type2.TPP_NAME);
		list2.add(authenticationParameterTextTypeValue8);
		
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
		selectedDeviceReference.setCustomerAuthenticationAppIdentifier("abc");
		customAuthenticationServicePostRequest.setChannelType("APP");
		scaAuthenticationServiceFoundationServiceTransformer
		.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
		
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);
		scaAuthenticationServiceFoundationServiceTransformer
		.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
	}
	
	@Test
	public void testtransformAuthenticationServiceRequest29() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "ACCOUNT_INFORMATION_CONSENT");
		params.put(PSD2Constants.CHANNEL_ID, "b365");
		assertNotNull(params);
		
		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		params.put("intentType", "PISP");
		params.put("AdapterSecurityConstants.CHANNEL_BRAND", BrandCode3.NIGB.getValue());

		credDetails.setIdentification("IE64IRCE92050112345678");
		credDetails.setSchemeName("UK.OBIE.IBAN");
		credDetails.setName("Manik");
		customConsentAppViewData.setCreditorDetails(credDetails);
		credDetails.setSecondaryIdentification("IE64IRCE92050112345679");
		amountDetails.setAmount("90k");
		amountDetails.setCurrency("EUR");
		customConsentAppViewData.setAmountDetails(amountDetails);
		params.put("custObj", customConsentAppViewData);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue1=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue1.setType(Type2.AMOUNT);
		list2.add(authenticationParameterTextTypeValue1);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue2=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue2.setType(Type2.CURRENCY);
		list2.add(authenticationParameterTextTypeValue2);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue4=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue4.setType(Type2.PAYEE_IBAN);
		list2.add(authenticationParameterTextTypeValue4);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue5=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue5.setType(Type2.PAYEE_NAME);
		list2.add(authenticationParameterTextTypeValue5);
		
		
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
		
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue7=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue7.setType(Type2.PAYER_IBAN);
		list2.add(authenticationParameterTextTypeValue7);
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue8=new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue8.setType(Type2.TPP_NAME);
		list2.add(authenticationParameterTextTypeValue8);
		
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
		
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);
		scaAuthenticationServiceFoundationServiceTransformer
		.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest, params);
	}
}
