package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3Data;
import com.capgemini.psd2.aisp.domain.OBTransaction3;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.constants.AccountTransactionsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.Balance;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.BalanceAmount;

import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.BalanceType;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.CreditDebitEventCode;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.Currency;

import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.FinancialEvent;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.FinancialEventStatusCode;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.ImpactedAccount;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.Merchant;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.Party20;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.Transaction;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.TransactionList;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.transformer.AccountTransactionsFoundationServiceTransformer;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;

public class AccountTransactionsFoundationServiceTransformerTest {

	/** The account information FS transformer. */
	@InjectMocks
	private AccountTransactionsFoundationServiceTransformer accountTransactionsFSTransformer = new AccountTransactionsFoundationServiceTransformer();

	@Mock
	private PSD2ValidatorImpl psd2Validator;

	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	@Mock
	private CommonAccountValidations commonAccountValidations;
	

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test transform response from FD to API.
	 */

	@Test
	public void testtransformAccountTransaction1a() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		
		Currency currency = new Currency();
		currency.setIsoAlphaCode("CAD");
		
		ImpactedAccount impactedAccount = new ImpactedAccount();
		impactedAccount.setAccountCurrency(currency);
		
		

		List<OBTransaction3> transactionList = new ArrayList<>();


		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmount feabt = new FinancialEventAmount();
		Double transcurr = 1000d;
		feabt.setTransactionCurrency(transcurr);
		feabt.setAccountCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		Currency cbt = new Currency();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCode.CR);
		fe.setFinancialEventStatusCode(FinancialEventStatusCode.BOOKED);
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		Balance balance = new Balance();
		tbt.setBalance(balance);

		BalanceAmount babt = new BalanceAmount();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);
		Currency bcbt = new Currency();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		balance.setBalanceType(BalanceType.LEDGER_BALANCE);
		tbt.setBalance(balance);
		tbt.setImpactedAccount(impactedAccount);

		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	@Test
	public void testtransformAccountTransaction1b() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();
		
		Currency currency = new Currency();
		currency.setIsoAlphaCode("CAD");
		
		ImpactedAccount impactedAccount = new ImpactedAccount();
		impactedAccount.setAccountCurrency(currency);
		

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "Savings");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmount feabt = new FinancialEventAmount();
		Double transcurr = 1000d;
		feabt.setTransactionCurrency(transcurr);
		feabt.setAccountCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		Currency cbt = new Currency();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCode.DR);

		fe.setFinancialEventStatusCode(FinancialEventStatusCode.BOOKED);
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		Balance balance = new Balance();
		tbt.setBalance(balance);

		BalanceAmount babt = new BalanceAmount();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);
		tbt.setImpactedAccount(impactedAccount);
		
		Currency bcbt = new Currency();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);

		balance.setBalanceType(BalanceType.SHADOW_BALANCE);
		tbt.setBalance(balance);

		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	@Test
	public void testtransformAccountTransaction1c() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();
		Currency currency = new Currency();
		currency.setIsoAlphaCode("CAD");
		
		ImpactedAccount impactedAccount = new ImpactedAccount();
		impactedAccount.setAccountCurrency(currency);

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);

		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmount feabt = new FinancialEventAmount();
		Double transcurr = 1000.0;
		feabt.setTransactionCurrency(transcurr);
		feabt.setAccountCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		Currency cbt = new Currency();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCode.DR);

		fe.setFinancialEventStatusCode(FinancialEventStatusCode.BOOKED);
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		Balance balance = new Balance();
		tbt.setBalance(balance);

		BalanceAmount babt = new BalanceAmount();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		Currency bcbt = new Currency();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);

		balance.setBalanceType(BalanceType.LEDGER_BALANCE);
		tbt.setBalance(balance);
		tbt.setImpactedAccount(impactedAccount);

		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	@Test
	public void testtransformAccountTransaction1d() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();
		Currency currency = new Currency();
		currency.setIsoAlphaCode("CAD");
		
		ImpactedAccount impactedAccount = new ImpactedAccount();
		impactedAccount.setAccountCurrency(currency);

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmount feabt = new FinancialEventAmount();
		Double transcurr = -1000d;
		feabt.setTransactionCurrency(transcurr);
		feabt.setAccountCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		Currency cbt = new Currency();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCode.CR);

		fe.setFinancialEventStatusCode(FinancialEventStatusCode.BOOKED);
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		Balance balance = new Balance();
		tbt.setBalance(balance);

		BalanceAmount babt = new BalanceAmount();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		Currency bcbt = new Currency();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);
		tbt.setImpactedAccount(impactedAccount);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		balance.setBalanceType(BalanceType.LEDGER_BALANCE);
		tbt.setBalance(balance);

		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}
	
	@Test
	public void testtransformAccountTransaction2a() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();
		
		Currency currency = new Currency();
		currency.setIsoAlphaCode("CAD");
		
		ImpactedAccount impactedAccount = new ImpactedAccount();
		impactedAccount.setAccountCurrency(currency);

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);

		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmount feabt = new FinancialEventAmount();
		Double transcurr = 1000d;
		feabt.setTransactionCurrency(transcurr);
		feabt.setAccountCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		Currency cbt = new Currency();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCode.CR);

		fe.setFinancialEventStatusCode(FinancialEventStatusCode.BOOKED);
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		Balance balance = new Balance();
		tbt.setBalance(balance);

		BalanceAmount babt = new BalanceAmount();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		Currency bcbt = new Currency();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);
		tbt.setImpactedAccount(impactedAccount);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		balance.setBalanceType(BalanceType.LEDGER_BALANCE);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		Party20 party = new Party20();
		Merchant mer = new Merchant();
		mer.setMerchantName("Akhil");
		mer.setMerchantCategoryCode("Open");
		party.setMerchant(mer);
		tbt.setParty(party);
		
		
		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	@Test
	public void testtransformAccountTransaction2b() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();
		Currency currency = new Currency();
		currency.setIsoAlphaCode("CAD");
		
		ImpactedAccount impactedAccount = new ImpactedAccount();
		impactedAccount.setAccountCurrency(currency);

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmount feabt = new FinancialEventAmount();
		Double transcurr = 1000d;
		feabt.setTransactionCurrency(transcurr);
		feabt.setAccountCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		Currency cbt = new Currency();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCode.DR);

		fe.setFinancialEventStatusCode(FinancialEventStatusCode.BOOKED);
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		Balance balance = new Balance();
		tbt.setBalance(balance);

		BalanceAmount babt = new BalanceAmount();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		Currency bcbt = new Currency();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);
		tbt.setImpactedAccount(impactedAccount);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		balance.setBalanceType(BalanceType.LEDGER_BALANCE);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		Party20 party = new Party20();
		Merchant mer = new Merchant();
		mer.setMerchantName("Akhil");
		mer.setMerchantCategoryCode("Open");
		party.setMerchant(mer);
		tbt.setParty(party);
		
		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	@Test
	public void testtransformAccountTransaction2c() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();
		Currency currency = new Currency();
		currency.setIsoAlphaCode("CAD");
		
		ImpactedAccount impactedAccount = new ImpactedAccount();
		impactedAccount.setAccountCurrency(currency);

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);

		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmount feabt = new FinancialEventAmount();
		Double transcurr = 1000.0;
		feabt.setTransactionCurrency(transcurr);
		feabt.setAccountCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		Currency cbt = new Currency();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCode.DR);

		fe.setFinancialEventStatusCode(FinancialEventStatusCode.PENDING);
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		Balance balance = new Balance();
		tbt.setBalance(balance);

		BalanceAmount babt = new BalanceAmount();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		Currency bcbt = new Currency();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);
		tbt.setImpactedAccount(impactedAccount);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		balance.setBalanceType(BalanceType.AVAILABLE_LIMIT);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		Party20 party = new Party20();
		Merchant mer = new Merchant();
		mer.setMerchantName("Akhil");
		mer.setMerchantCategoryCode("Open");
		party.setMerchant(mer);
		tbt.setParty(party);

		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	@Test
	public void testtransformAccountTransaction2d() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();
		
		Currency currency = new Currency();
		currency.setIsoAlphaCode("CAD");
		
		ImpactedAccount impactedAccount = new ImpactedAccount();
		impactedAccount.setAccountCurrency(currency);

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);

		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmount feabt = new FinancialEventAmount();
		Double transcurr = -1000d;
		feabt.setTransactionCurrency(transcurr);
		feabt.setAccountCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		Currency cbt = new Currency();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCode.CR);

		fe.setFinancialEventStatusCode(FinancialEventStatusCode.BOOKED);
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		Balance balance = new Balance();
		tbt.setBalance(balance);

		BalanceAmount babt = new BalanceAmount();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		Currency bcbt = new Currency();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);
		tbt.setImpactedAccount(impactedAccount);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		balance.setBalanceType(BalanceType.STATEMENT_CLOSING_BALANCE);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		Party20 party = new Party20();
		Merchant mer = new Merchant();
		mer.setMerchantName("Akhil");
		mer.setMerchantCategoryCode("Open");
		party.setMerchant(mer);
		tbt.setParty(party);

		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	
	@Test
	public void testtransformAccountTransaction3() {
		TransactionList transactions = null;
		Map<String, String> params = null;
		accountTransactionsFSTransformer.transformAccountTransactions(transactions, params);
	}
	
}
