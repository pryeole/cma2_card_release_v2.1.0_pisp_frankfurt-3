package com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBDirectDebit1;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountDirectDebitsResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.AccountDirectDebitsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.client.AccountDirectDebitsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.delegate.AccountDirectDebitsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountCurrency;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccountInformation;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.EntitlementsAccount;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyInformation;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.RetailNonRetailCode;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType;
import com.capgemini.psd2.logger.PSD2Constants;


@RunWith(SpringJUnit4ClassRunner.class)
public class AccountDirectDebitsFoundationServiceAdapterTest {
	@InjectMocks
	private AccountDirectDebitsFoundationServiceAdapter accountDirectDebitsFoundationServiceAdapter;

	@Mock
	private AccountDirectDebitsFoundationServiceDelegate accountDirectDebitsFoundationServiceDelegate;

	@Mock
	private AccountDirectDebitsFoundationServiceClient accountDirectDebitsFoundationServiceClient;
	
	@Mock
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Mock
	private AdapterFilterUtility adapterFilterUtility;
	
	private Map<String, String> params = new HashMap<String, String>();
	private AccountMapping accountMapping = new AccountMapping();
	private List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
	private AccountDetails accDet = new AccountDetails();
	private HttpHeaders httpHeaders = new HttpHeaders();
	private PlatformAccountDirectDebitsResponse response = new PlatformAccountDirectDebitsResponse();
	private OBDirectDebit1 directDebit = new OBDirectDebit1();
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		params.put("x-channel-id", "BOL");
		params.put("channelId", "BOL");
		params.put(PSD2Constants.CHANNEL_ID, "BOL");
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "PSD2API");
		accDet.setAccountNSC("123456");
		accDet.setAccountNumber("12345678");
		accDet.setAccountId("1234");
		accDet.setAccountNSC("123456");
		accDet.setAccountNumber("12345678");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("12345678");
		accountMapping.setCorrelationId("12345678");
		httpHeaders.add("X-BOI-USER", "12345678");
		httpHeaders.add("X-BOI-CHANNEL", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
		httpHeaders.add("X-CORRELATION-ID", "Correlation");
		ReflectionTestUtils.setField(accountDirectDebitsFoundationServiceAdapter, "consentFlowType", "AISP");
	//	ReflectionTestUtils.setField(accountDirectDebitsFoundationServiceAdapter, "accountDirectDebitsBaseURL", "http://localhost:9089/fs-entity-service/services/account");
		//Mockito.when(accountDirectDebitsFoundationServiceDelegate.getFoundationServiceURL(anyString(), anyString(), anyString())).thenReturn("https://localhost:9089/fs-entity-service/services/account/123456/12345678/directdebits");
		Mockito.when(accountDirectDebitsFoundationServiceDelegate.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(accountDirectDebitsFoundationServiceClient.restTransportForDirectDebitsFS(anyObject(), anyObject(), anyObject())).thenReturn(directDebit);
		Mockito.when(accountDirectDebitsFoundationServiceDelegate.transform(anyObject(), anyObject())).thenReturn(response);
	}
	@Test
	public void contextLoads() {
	}
	
	@Test(expected = AdapterException.class)
	public void testAccountMappingNull(){
		accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(null, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testAccountMappingPsuIdNull(){
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId(null);
		accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(accountMapping, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testParamsNull(){
		accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(accountMapping, null);
	}
	
	@Test(expected = AdapterException.class)
	public void testAccountMappingAccountDetailsNull(){
		DigitalUser du = new DigitalUser();
		du.setDigitalUserLockedOutIndicator(false);
		DigitalUserProfile dup = new DigitalUserProfile();
		dup.setDigitalUser(du);
		PartyInformation pi = new PartyInformation();
		pi.setPartyActiveIndicator(true);
		dup.setPartyInformation(pi);
		List<EntitlementsAccount> accountEntitlements = new ArrayList<EntitlementsAccount>();
		EntitlementsAccount ea = new EntitlementsAccount();
		List<String> ent = new ArrayList<>();
		ent.add("Display Only Account");
		Account account = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
		account.setAccountNumber("24512786");
		AccountCurrency currency = new AccountCurrency();
		CurrentAccountInformation cuacinfo = new CurrentAccountInformation();
		currency.setIsoAlphaCode("GBP");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
		cuacinfo.setParentNationalSortCodeNSCNumber("902127");
		cuacinfo.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		cuacinfo.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		account.setCurrentAccountInformation(cuacinfo);
		ea.setAccount(account);
		ea.setEntitlements(ent);
		accountEntitlements.add(ea);
		dup.setAccountEntitlements(accountEntitlements);
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("12345678");
		accountMapping.setAccountDetails(null);
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(dup);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(dup, accountMapping)).thenReturn(accountMapping);
		accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(accountMapping, params);
	}
	@Test(expected = AdapterException.class)
	public void testAccountMappingAccountDetailsEmpty(){
		DigitalUser du = new DigitalUser();
		du.setDigitalUserLockedOutIndicator(false);
		DigitalUserProfile dup = new DigitalUserProfile();
		dup.setDigitalUser(du);
		PartyInformation pi = new PartyInformation();
		pi.setPartyActiveIndicator(true);
		dup.setPartyInformation(pi);
		List<EntitlementsAccount> accountEntitlements = new ArrayList<EntitlementsAccount>();
		EntitlementsAccount ea = new EntitlementsAccount();
		List<String> ent = new ArrayList<>();
		ent.add("Display Only Account");
		Account account = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
		account.setAccountNumber("24512786");
		AccountCurrency currency = new AccountCurrency();
		CurrentAccountInformation cuacinfo = new CurrentAccountInformation();
		currency.setIsoAlphaCode("GBP");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
		cuacinfo.setParentNationalSortCodeNSCNumber("902127");
		cuacinfo.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		cuacinfo.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		account.setCurrentAccountInformation(cuacinfo);
		ea.setAccount(account);
		ea.setEntitlements(ent);
		accountEntitlements.add(ea);
		dup.setAccountEntitlements(accountEntitlements);
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("12345678");
		List<AccountDetails> list = new ArrayList<>();
		accountMapping.setAccountDetails(list);
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(dup);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(dup, accountMapping)).thenReturn(accountMapping);
		accountDirectDebitsFoundationServiceAdapter.retrieveAccountDirectDebits(accountMapping, params);
	}
}
