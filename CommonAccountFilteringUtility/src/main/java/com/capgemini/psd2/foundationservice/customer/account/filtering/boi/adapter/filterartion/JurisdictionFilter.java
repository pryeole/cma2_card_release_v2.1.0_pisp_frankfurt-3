package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion;

import java.util.List;
import java.util.Map;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.AccountEntitlements2;

public class JurisdictionFilter implements FilterationChain {
	
	private FilterationChain nextInChain;

	@Override
	public void setNext(FilterationChain next) {
		nextInChain = next;

	}

	@Override
	public List<AccountEntitlements> process(List<AccountEntitlements> accounts,PartyEntitlements partyEntitlements, String consentFlowType,
			Map<String , Map<String,List<String>>> accountFiltering 	) {
		
	/*Disable Jurisdiction Filter.*/	
		
//		List<EntitlementsAccount> accountList = new ArrayList<>();
//		  if(!accounts.isEmpty() || accounts.size()>0) //if (accounts.getAccount().size() > 0||accounts.getCreditCardAccount().size() > 0)
//		
//		{
//			Map<String, List<String>> jurisdictionList = accountFiltering.get(CustomerAccountsFilterFoundationServiceConstants.JURISDICTION);
//			if(jurisdictionList != null && !jurisdictionList.isEmpty()){
//				List<String> validJurisdictionForFlow = (ArrayList<String>) jurisdictionList.get(consentFlowType);
//				Set<String> validJurisdictions = validJurisdictionForFlow.stream().map(u -> u.substring(0, u.indexOf("."))).collect(Collectors.toSet());
//			
//				for (EntitlementsAccount accnt : accounts) {
//					if (accnt.getAccount().getAccountBrand().getBrandCode() != null){
//						if (validJurisdictions.contains(accnt.getAccount().getAccountBrand().getBrandCode().toString())) {
//							EntitlementsAccount accntEntitlment=new EntitlementsAccount();
//							accntEntitlment.setAccount(accnt.getAccount());
//							accntEntitlment.setEntitlements(accnt.getEntitlements());
//							accountList.add(accntEntitlment);
//						}
//					}
//				}
//				return nextInChain.process(accountList,partyEntitlements,consentFlowType,accountFiltering);
//			}else{
//				return nextInChain.process(accounts,partyEntitlements,consentFlowType,accountFiltering);
//			}
//		} 
//		return accountList;
		return accounts;
	}

	@Override
	public List<AccountEntitlements2> processPISP(List<AccountEntitlements2> accounts,
			com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements partyEntitlements, String consentFlowType,
			Map<String, Map<String, List<String>>> accountFiltering) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
