
package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.client.CustomerAccountsFilterFoundationServiceClient;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.delegate.CustomerAccountsFilterFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.AccountEntitlements2;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * CustomerAccountProfileFoundationServiceAdapter This adapter is to get
 * Customer Account List
 */
@Component
public class CustomerAccountsFilterFoundationServiceAdapter {

	/* Customer Account Profile service Delegate */
	@Autowired
	private CustomerAccountsFilterFoundationServiceDelegate customerAccountProfileFoundationServiceDelegate;

	/* Customer Account Service Client */
	@Autowired
	private CustomerAccountsFilterFoundationServiceClient customerAccountProfileFoundationServiceClient;

	/* Jurisdiction Type */
	// @Value("${foundationService.jurisdictionType}")
	// private String jurisdictionType;

	// @Override
	public DigitalUserProfile retrieveCustomerAccountList(String userId, Map<String, String> params) {
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = null;
		DigitalUserProfile digitalUserProfile = null;
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();

		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(userId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		 
		if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI"))
		{
			queryParams.add(CustomerAccountsFilterFoundationServiceConstants.ACCOUNT_BRAND, "BOI-ROI");
		}
		
		else if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK"))
		{
			queryParams.add(CustomerAccountsFilterFoundationServiceConstants.ACCOUNT_BRAND, "BOI-NI,BOI-GB,BOI-NIGB");
		}

			httpHeaders = customerAccountProfileFoundationServiceDelegate.createRequestHeadersAISP(params);
			requestInfo.setUrl(
					customerAccountProfileFoundationServiceDelegate.getFoundationServiceURLAISP(params, userId));
			digitalUserProfile = customerAccountProfileFoundationServiceClient.restTransportForCustomerAccountProfileAISP(
					requestInfo, DigitalUserProfile.class, queryParams, httpHeaders);

		if (digitalUserProfile == null || digitalUserProfile.getAccountEntitlements() == null
				|| digitalUserProfile.getAccountEntitlements().isEmpty()
				|| digitalUserProfile.getAccountEntitlements().size() < 0
				|| NullCheckUtils.isNullOrEmpty(digitalUserProfile.getPartyEntitlements().getEntitlements())) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_FOUND);
		}
		if (!NullCheckUtils.isNullOrEmpty(digitalUserProfile.getPartyEntitlements().getEntitlements())) {
			if (CustomerAccountsFilterFoundationServiceConstants.AISP
					.equalsIgnoreCase(params.get(PSD2Constants.CONSENT_FLOW_TYPE))) {
				if (!(digitalUserProfile.getPartyEntitlements().getEntitlements()
						.contains(CustomerAccountsFilterFoundationServiceConstants.AISP)))
					throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_FOUND);
			}
		}

		if (digitalUserProfile.getDigitalUser().isDigitalUserLockedOutIndicator()
				&& !digitalUserProfile.getPartyInformation().isPartyActiveIndicator() || digitalUserProfile.getDigitalUser().isDigitalUserLockedOutIndicator()
				&& digitalUserProfile.getPartyInformation().isPartyActiveIndicator() || !digitalUserProfile.getDigitalUser().isDigitalUserLockedOutIndicator()
				&& !digitalUserProfile.getPartyInformation().isPartyActiveIndicator()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_FOUND);
		}

		List<AccountEntitlements> listAccountEntitlement = customerAccountProfileFoundationServiceDelegate
				.getFilteredAccounts(digitalUserProfile.getAccountEntitlements(),
						digitalUserProfile.getPartyEntitlements(), params);
		digitalUserProfile.setAccountEntitlements(listAccountEntitlement);
		return digitalUserProfile;
	}
	
	public com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile retrieveCustomerAccountListPISP(String userId, Map<String, String> params) {
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = null;
		Accnts accnts = null;
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile digitalUserProfile = null;
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();

		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(userId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		 
		if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI"))
		{
			queryParams.add(CustomerAccountsFilterFoundationServiceConstants.ACCOUNT_BRAND, "BOI-ROI");
		}
		
		else if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK"))
		{
			queryParams.add(CustomerAccountsFilterFoundationServiceConstants.ACCOUNT_BRAND, "BOI-NI,BOI-GB,BOI-NIGB");
		}

		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_ID))
				&& !NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_ID))) {
		queryParams.add(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTSOURCESYSTEMCODE, "BKK");
		queryParams.add(CustomerAccountsFilterFoundationServiceConstants.SOURCESYSTEMACCOUNTTYPE, "Current Account,Savings Account");
		queryParams.add(CustomerAccountsFilterFoundationServiceConstants.DIGITALUSERID, userId);
			httpHeaders = customerAccountProfileFoundationServiceDelegate.createRequestHeadersPISP(params);
			requestInfo.setUrl(
					customerAccountProfileFoundationServiceDelegate.getFoundationServiceURLPISP(params, userId));
			digitalUserProfile = customerAccountProfileFoundationServiceClient
					.restTransportForCustomerAccountProfilePISP(requestInfo, com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile.class, queryParams,
							httpHeaders);
		} else {
			httpHeaders = customerAccountProfileFoundationServiceDelegate.createRequestHeadersForPISP(params);
			requestInfo.setUrl(customerAccountProfileFoundationServiceDelegate.getFoundationServiceURLforPISP(
					params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER),
					params.get(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER),
					params.get(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER)));
			accnts = customerAccountProfileFoundationServiceClient.restTransportForSingleAccountProfile(requestInfo,
					Accounts.class, httpHeaders);

		}

		if (digitalUserProfile == null || digitalUserProfile.getAccountBalanceEntitlements() == null
				|| digitalUserProfile.getAccountBalanceEntitlements().isEmpty()
				|| digitalUserProfile.getAccountBalanceEntitlements().size() < 0
				|| NullCheckUtils.isNullOrEmpty(digitalUserProfile.getPartyEntitlements().getEntitlements())) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_FOUND);
		}
		if (!NullCheckUtils.isNullOrEmpty(digitalUserProfile.getPartyEntitlements().getEntitlements())) {
			if (CustomerAccountsFilterFoundationServiceConstants.PISP
					.equalsIgnoreCase(params.get(PSD2Constants.CONSENT_FLOW_TYPE))) {
				if (!(digitalUserProfile.getPartyEntitlements().getEntitlements()
						.contains(CustomerAccountsFilterFoundationServiceConstants.PISP)))
					throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_DETAILS_NOT_FOUND);
			}
		}

		if (digitalUserProfile.getDigitalUser().isDigitalUserLockedOutIndicator()
				&& !digitalUserProfile.getPartyInformation().isPartyActiveIndicator() || digitalUserProfile.getDigitalUser().isDigitalUserLockedOutIndicator()
				&& digitalUserProfile.getPartyInformation().isPartyActiveIndicator() || !digitalUserProfile.getDigitalUser().isDigitalUserLockedOutIndicator()
				&& !digitalUserProfile.getPartyInformation().isPartyActiveIndicator()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_FOUND);
		}

		List<AccountEntitlements2> listAccountEntitlement = customerAccountProfileFoundationServiceDelegate
				.getFilteredAccountsPISP(digitalUserProfile.getAccountBalanceEntitlements(),
						digitalUserProfile.getPartyEntitlements(), params);
		digitalUserProfile.setAccountBalanceEntitlements(listAccountEntitlement);
		return digitalUserProfile;
	}
}
