//package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;
//
//import static org.junit.Assert.assertNotNull;
//import static org.mockito.Matchers.any;
//import static org.mockito.Matchers.anyObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.CreditCardAccnt;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.FilteredAccounts;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.FilterationChain;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.JurisdictionFilter;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Brand;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.BrandCode;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.RetailNonRetailCode;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
//
//public class JurisdictionFilterTest {
//
//
//	@InjectMocks
//	private JurisdictionFilter jurisdictionFilter;
//
//	@Mock
//	private FilterationChain nextInChain;
//
//	
//	@Before
//	public void setUp() {
//		MockitoAnnotations.initMocks(this);
//	}
//
//	
//
//	@Test
//	public void testAccountTypeFilterProcessCredit() {
//		String consentFlowType="AISP";
//		Account acc = new Account();
//		acc.setAccountName("Current Account");
//		acc.setAccountNumber("2345");
//		acc.setRetailNonRetailCode(RetailNonRetailCode.NON_RETAIL);
//		Brand accountBrand= new Brand();
//		accountBrand.setBrandCode(BrandCode.AA);
//		acc.setAccountBrand(accountBrand);
//		AccountEntitlements ae= new AccountEntitlements();
//		ae.setAccount(acc);
//		List<String> ent = new ArrayList<String>();
//		ent.add("capg");
//		ent.add("boi");
//		ae.setEntitlements(ent);
//		PartyEntitlements partyEntitlements= new PartyEntitlements();
//		List<AccountEntitlements> accounts =new ArrayList<AccountEntitlements>();
//		accounts.add(ae);
//		List<AccountEntitlements> filteredAccounts=new ArrayList<AccountEntitlements>();
//
//		Map<String, Map<String, List<String>>> accountFiltering = createAccountFilteringList();
//
//		jurisdictionFilter.setNext(nextInChain);
//		Mockito.when(nextInChain.process(any(), any(),any(), any())).thenReturn(filteredAccounts);
//		filteredAccounts =	jurisdictionFilter.process(accounts,partyEntitlements, consentFlowType, accountFiltering);
//		assertNotNull(filteredAccounts); 
//	}
//	@Test
//	public void testAccountTypeFilterProcessCreditNOT() {
//		String consentFlowType="AISP";
//		Account acc = new Account();
//		acc.setAccountName("Current Account");
//		acc.setAccountNumber("2345");
//		acc.setRetailNonRetailCode(RetailNonRetailCode.RETAIL);
//		Brand accountBrand= new Brand();
//		accountBrand.setBrandCode(BrandCode.BOI_GB);
//		acc.setAccountBrand(accountBrand);
//		AccountEntitlements ae= new AccountEntitlements();
//		ae.setAccount(acc);
//		List<String> ent = new ArrayList<String>();
//		ent.add("capg");
//		ent.add("boi");
//		ae.setEntitlements(ent);
//		List<AccountEntitlements> accounts =new ArrayList<AccountEntitlements>();
//		accounts.add(ae);
//		List<AccountEntitlements> filteredAccounts=new ArrayList<AccountEntitlements>();
//		PartyEntitlements partyEntitlements= new PartyEntitlements();
//		Map<String, Map<String, List<String>>> accountFiltering = new HashMap<>();
//		Map<String , Map<String,List<String>>> accountFiltering1 = null;
//		Map<String, List<String>> map = new HashMap<>();
//		map.put("cap", ent);
//		accountFiltering.put(consentFlowType,map);
//		jurisdictionFilter.setNext(nextInChain);
//		Mockito.when(nextInChain.process(any(),any(), any(), any())).thenReturn(filteredAccounts);
//		filteredAccounts =	jurisdictionFilter.process(accounts,partyEntitlements, consentFlowType, accountFiltering1);
//		assertNotNull(filteredAccounts); 
//	}
//
//	public Map<String, Map<String, List<String>>> createAccountFilteringList() {
//
//		Map<String, Map<String, List<String>>> accountFiltering = new HashMap<>();
//
//		Map<String, List<String>> map1 = new HashMap<String, List<String>>();
//
//		List<String> accountType = new ArrayList<>();
//		accountType.add("Current Account");
//		map1.put("AISP", accountType);
//		map1.put("CISP", accountType);
//		map1.put("PISP", accountType);
//
//		accountFiltering.put("accountType", map1);
//
//		Map<String, List<String>> map2 = new HashMap<String, List<String>>();
//
//		List<String> jurisdictionList = new ArrayList<>();
//		jurisdictionList.add("NORTHERN_IRELAND.SchemeName=BBAN");
//		jurisdictionList.add("NORTHERN_IRELAND.Identification=IBAN");
//		jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE");
//		jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=BIC");
//		jurisdictionList.add("GREAT_BRITAIN.SchemeName=BBAN");
//		jurisdictionList.add("GREAT_BRITAIN.Identification=IBAN");
//		jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE");
//		jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=BIC");
//
//		map2.put("AISP", jurisdictionList);
//		map2.put("CISP", jurisdictionList);
//		map2.put("PISP", jurisdictionList);
//
//		accountFiltering.put("jurisdiction", map2);
//
//		Map<String, List<String>> map3 = new HashMap<String, List<String>>();
//
//		List<String> permissionList1 = new ArrayList<>();
//		permissionList1.add("A");
//		permissionList1.add("V");
//		List<String> permissionList2 = new ArrayList<>();
//		permissionList2.add("A");
//		permissionList2.add("X");
//
//		map3.put("AISP", permissionList1);
//		map3.put("CISP", permissionList1);
//		map3.put("PISP", permissionList2);
//
//		accountFiltering.put("permission", map3);
//		return accountFiltering;
//	}
//
//
//
//}
