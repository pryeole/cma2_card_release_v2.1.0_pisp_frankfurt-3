package com.capgemini.psd2.foundationservice.validator;

public enum SuccesCodeEnum {

	/*
	 * For PreStageValidatePayment
	 */
	SUCCESS_PRESTAGE_VALIDATE_PAYMENT("FS_PMPSV_S01", "PASS"),
	/*
	 * For ValidatePayment
	 */
	SUCCESS_VALIDATE_PAYMENT("FS_PMV_S01", "PASS"),
	/*
	 * For stagePaymentInsert
	 */
	SUCCESS_STAGE_PAYMENT_INSERT("FS_PMI_S01", "paymentIdReturn"),
	SUCCESS_STAGE_PAYMENT_UPDATE("FS_PMU_S01", "paymentIdReturn"),
	
	/*
	 * For PaymentSubmissionCreate
	 */
	SUCCESS_EXECUTE_PAYMENT("FS_PME_S01", "TmpSubmissionId");

	// for other success FS services

	private final String successCode;

	private final String successMessage;

	private SuccesCodeEnum(String successCode, String successMessage) {
		this.successCode = successCode;
		this.successMessage = successMessage;
	}

	public String getSuccessCode() {
		return successCode;
	}

	public String getSuccessMessage() {
		return successMessage;
	}
}