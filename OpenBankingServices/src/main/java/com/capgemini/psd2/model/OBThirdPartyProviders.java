package com.capgemini.psd2.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Thirs party providers consisting of payment initiation service providers and account information service providers
 */
@ApiModel(description = "Thirs party providers consisting of payment initiation service providers and account information service providers")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")
@JsonInclude(Include.NON_NULL)
public class OBThirdPartyProviders   {
  @JsonProperty("externalId")
  @JsonInclude(Include.NON_NULL)
  private String externalId = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("meta")
  private Meta meta = null;

  @JsonProperty("schemas")
  private List<String> schemas = new ArrayList<String>();

  private OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 urnopenbankingcompetentauthorityclaims10 = null;

  @JsonProperty("urn:openbanking:legalauthorityclaims:1.0")
  private OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10 urnopenbankinglegalauthorityclaims10 = null;

  private Organisation urnopenbankingorganisation10 = null;

  @JsonProperty("urn:openbanking:softwarestatement:1.0")
  private OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 urnopenbankingsoftwarestatement10 = null;

  public OBThirdPartyProviders externalId(String externalId) {
    this.externalId = externalId;
    return this;
  }

   /**
   * Get externalId
   * @return externalId
  **/
  @ApiModelProperty(value = "")


  public String getExternalId() {
    return externalId;
  }

  public void setExternalId(String externalId) {
    this.externalId = externalId;
  }

  public OBThirdPartyProviders id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(readOnly = true, value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public OBThirdPartyProviders meta(Meta meta) {
    this.meta = meta;
    return this;
  }

   /**
   * Get meta
   * @return meta
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Meta getMeta() {
    return meta;
  }

  public void setMeta(Meta meta) {
    this.meta = meta;
  }

  public OBThirdPartyProviders schemas(List<String> schemas) {
    this.schemas = schemas;
    return this;
  }

  public OBThirdPartyProviders addSchemasItem(String schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Get schemas
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public List<String> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<String> schemas) {
    this.schemas = schemas;
  }

  public OBThirdPartyProviders urnopenbankingcompetentauthorityclaims10(OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 urnopenbankingcompetentauthorityclaims10) {
    this.urnopenbankingcompetentauthorityclaims10 = urnopenbankingcompetentauthorityclaims10;
    return this;
  }

   /**
   * Get urnopenbankingcompetentauthorityclaims10
   * @return urnopenbankingcompetentauthorityclaims10
  **/
  @ApiModelProperty(value = "")

  @Valid
  @JsonGetter("Competentauthorityclaims")
  public OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 getUrnopenbankingcompetentauthorityclaims10() {
    return urnopenbankingcompetentauthorityclaims10;
  }
  @JsonProperty("urn:openbanking:competentauthorityclaims:1.0")
  public void setUrnopenbankingcompetentauthorityclaims10(OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 urnopenbankingcompetentauthorityclaims10) {
    this.urnopenbankingcompetentauthorityclaims10 = urnopenbankingcompetentauthorityclaims10;
  }

  public OBThirdPartyProviders urnopenbankinglegalauthorityclaims10(OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10 urnopenbankinglegalauthorityclaims10) {
    this.urnopenbankinglegalauthorityclaims10 = urnopenbankinglegalauthorityclaims10;
    return this;
  }

   /**
   * Get urnopenbankinglegalauthorityclaims10
   * @return urnopenbankinglegalauthorityclaims10
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10 getUrnopenbankinglegalauthorityclaims10() {
    return urnopenbankinglegalauthorityclaims10;
  }

  public void setUrnopenbankinglegalauthorityclaims10(OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10 urnopenbankinglegalauthorityclaims10) {
    this.urnopenbankinglegalauthorityclaims10 = urnopenbankinglegalauthorityclaims10;
  }

  public OBThirdPartyProviders urnopenbankingorganisation10(Organisation urnopenbankingorganisation10) {
    this.urnopenbankingorganisation10 = urnopenbankingorganisation10;
    return this;
  }

   /**
   * Get urnopenbankingorganisation10
   * @return urnopenbankingorganisation10
  **/
  @ApiModelProperty(value = "")

  @Valid
  @JsonGetter("Organisation")
  public Organisation getUrnopenbankingorganisation10() {
    return urnopenbankingorganisation10;
  }

  @JsonSetter("urn:openbanking:organisation:1.0")
  public void setUrnopenbankingorganisation10(Organisation urnopenbankingorganisation10) {
    this.urnopenbankingorganisation10 = urnopenbankingorganisation10;
  }

  public OBThirdPartyProviders urnopenbankingsoftwarestatement10(OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 urnopenbankingsoftwarestatement10) {
    this.urnopenbankingsoftwarestatement10 = urnopenbankingsoftwarestatement10;
    return this;
  }

   /**
   * Get urnopenbankingsoftwarestatement10
   * @return urnopenbankingsoftwarestatement10
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 getUrnopenbankingsoftwarestatement10() {
    return urnopenbankingsoftwarestatement10;
  }

  public void setUrnopenbankingsoftwarestatement10(OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 urnopenbankingsoftwarestatement10) {
    this.urnopenbankingsoftwarestatement10 = urnopenbankingsoftwarestatement10;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBThirdPartyProviders obThirdPartyProviders = (OBThirdPartyProviders) o;
    return Objects.equals(this.externalId, obThirdPartyProviders.externalId) &&
        Objects.equals(this.id, obThirdPartyProviders.id) &&
        Objects.equals(this.meta, obThirdPartyProviders.meta) &&
        Objects.equals(this.schemas, obThirdPartyProviders.schemas) &&
        Objects.equals(this.urnopenbankingcompetentauthorityclaims10, obThirdPartyProviders.urnopenbankingcompetentauthorityclaims10) &&
        Objects.equals(this.urnopenbankinglegalauthorityclaims10, obThirdPartyProviders.urnopenbankinglegalauthorityclaims10) &&
        Objects.equals(this.urnopenbankingorganisation10, obThirdPartyProviders.urnopenbankingorganisation10) &&
        Objects.equals(this.urnopenbankingsoftwarestatement10, obThirdPartyProviders.urnopenbankingsoftwarestatement10);
  }

  @Override
  public int hashCode() {
    return Objects.hash(externalId, id, meta, schemas, urnopenbankingcompetentauthorityclaims10, urnopenbankinglegalauthorityclaims10, urnopenbankingorganisation10, urnopenbankingsoftwarestatement10);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBThirdPartyProviders {\n");
    
    sb.append("    externalId: ").append(toIndentedString(externalId)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    urnopenbankingcompetentauthorityclaims10: ").append(toIndentedString(urnopenbankingcompetentauthorityclaims10)).append("\n");
    sb.append("    urnopenbankinglegalauthorityclaims10: ").append(toIndentedString(urnopenbankinglegalauthorityclaims10)).append("\n");
    sb.append("    urnopenbankingorganisation10: ").append(toIndentedString(urnopenbankingorganisation10)).append("\n");
    sb.append("    urnopenbankingsoftwarestatement10: ").append(toIndentedString(urnopenbankingsoftwarestatement10)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

