package com.capgemini.psd2.consent.list.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.consent.adapter.impl.CispConsentAdapterImpl;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.consent.list.data.Consent;
import com.capgemini.psd2.consent.list.data.Consent.ConsentType;
import com.capgemini.psd2.consent.list.data.ConsentListResponse;
import com.capgemini.psd2.consent.list.service.ConsentListService;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.consent.adapter.impl.PispConsentAdapterImpl;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.repository.PaymentSetupPlatformRepository;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.JSONUtilities;

@Service
public class ConsentListServiceImpl implements ConsentListService {

	@Autowired
	private AispConsentAdapterImpl aispConsentAdapter;

	@Autowired
	private PispConsentAdapterImpl pispConsentAdapter;

	@Autowired
	private CispConsentAdapterImpl cispConsentAdapter;

	@Autowired
	@Qualifier("AccountRequestAdapter")
	private AccountRequestAdapter accountRequestAdapter;

	@Autowired
	private PaymentSetupPlatformRepository paymentSetupPlatformRepository;

	@Autowired
	@Qualifier("FundsConfirmationConsentAdapter")
	private FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter;

	@Autowired
	private HttpServletRequest httpServletRequest;

	@Autowired
	private CompatibleVersionList compatibleVersionList;

	@Override
	public ConsentListResponse getConsentList(String userId, String status, String tenantId, String consentType) {
		/* Load test issue fix, to make thread safe tenantId object holder */
		RequestAttributes springContextAttr = RequestContextHolder.getRequestAttributes();
		springContextAttr.setAttribute("requestTenantId", tenantId, RequestAttributes.SCOPE_REQUEST);
		ConsentListResponse consentListResponse = new ConsentListResponse();
		List<Consent> consents = new ArrayList<>();

		List<AispConsent> aispConsentList = null;
		if (consentType == null || ("aisp".equalsIgnoreCase(consentType))) {
			if (tenantId == null) {
				if (null == status) {
					aispConsentList = aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId, null);
				} else {
					aispConsentList = aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
							ConsentStatusEnum.valueOf(status.toUpperCase()));
				}
			} else {
				if (null == status) {
					aispConsentList = aispConsentAdapter.retrieveConsentByPsuIdConsentStatusAndTenantId(userId, null,
							tenantId);
				} else {
					aispConsentList = aispConsentAdapter.retrieveConsentByPsuIdConsentStatusAndTenantId(userId,
							ConsentStatusEnum.valueOf(status.toUpperCase()), tenantId);
				}
			}
		}

		if (aispConsentList == null)
			aispConsentList = new ArrayList<>();

		List<PispConsent> pispConsentList = null;
		if (consentType == null || ("pisp".equalsIgnoreCase(consentType))) {
			if (tenantId == null) {
				if (null == status) {
					pispConsentList = pispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId, null);
				} else {
					pispConsentList = pispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
							ConsentStatusEnum.valueOf(status.toUpperCase()));
				}
			} else {
				if (null == status) {
					pispConsentList = pispConsentAdapter.retrieveConsentByPsuIdConsentStatusAndTenantId(userId, null,
							tenantId);
				} else {
					pispConsentList = pispConsentAdapter.retrieveConsentByPsuIdConsentStatusAndTenantId(userId,
							ConsentStatusEnum.valueOf(status.toUpperCase()), tenantId);
				}
			}
		}
		if (pispConsentList == null)
			pispConsentList = new ArrayList<>();

		List<CispConsent> cispConsentList = null;
		if (consentType == null || ("cisp".equalsIgnoreCase(consentType))) {
			if (tenantId == null) {
				if (null == status) {
					cispConsentList = cispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId, null);
				} else {
					cispConsentList = cispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
							ConsentStatusEnum.valueOf(status.toUpperCase()));
				}
			} else {
				if (null == status) {
					cispConsentList = cispConsentAdapter.retrieveConsentByPsuIdConsentStatusAndTenantId(userId, null,
							tenantId);
				} else {
					cispConsentList = cispConsentAdapter.retrieveConsentByPsuIdConsentStatusAndTenantId(userId,
							ConsentStatusEnum.valueOf(status.toUpperCase()), tenantId);
				}
			}

		}
		if (cispConsentList == null)
			cispConsentList = new ArrayList<>();

		consents = formCommonConsentList(aispConsentList, pispConsentList, cispConsentList, consents);

		consentListResponse.setData(consents);
		if (consentListResponse.getLinks() == null)
			consentListResponse.setLinks(new Links());
		if (consentListResponse.getMeta() == null)
			consentListResponse.setMeta(new Meta());
		if (httpServletRequest.getQueryString() != null) {
			consentListResponse.getLinks().setSelf(httpServletRequest.getRequestURI() + PSD2Constants.QUESTIONMARK
					+ httpServletRequest.getQueryString());
		} else {
			consentListResponse.getLinks().setSelf(httpServletRequest.getRequestURI());
		}
		consentListResponse.getMeta().setTotalPages(1);
		return consentListResponse;
	}

	private List<Consent> formCommonConsentList(List<AispConsent> aispConsentList, List<PispConsent> pispConsentList,
			List<CispConsent> cispConsentList, List<Consent> consents) {

		if (aispConsentList.isEmpty() && pispConsentList.isEmpty() && cispConsentList.isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CONSENT_LIST_FOUND);
		} else {
			if (!aispConsentList.isEmpty()) {
				for (AispConsent aispConsent : aispConsentList) {
					Consent data = JSONUtilities
							.getObjectFromJSONString(JSONUtilities.getJSONOutPutFromObject(aispConsent), Consent.class);
					ConsentStatusEnum statusEnum = aispConsent.getStatus();
					// Updating
					data.setStatusName(statusEnum.getStatusCode());
					data.setConsentType(ConsentType.AISP);

					OBReadConsentResponse1 accountRequestPOSTResponse = accountRequestAdapter
							.getAccountRequestGETResponse(aispConsent.getAccountRequestId());

					if (null != accountRequestPOSTResponse) {
						List<OBExternalPermissions1Code> permissions = accountRequestPOSTResponse.getData()
								.getPermissions();
						data.setPermissions(permissions);
						data.setTppLegalEntityName(accountRequestPOSTResponse.getData().getTppLegalEntityName());
						data.setConsentCreationDate(accountRequestPOSTResponse.getData().getCreationDateTime());
					}
					consents.add(data);
				}
			}

			if (!pispConsentList.isEmpty()) {
				for (PispConsent pispConsent : pispConsentList) {
					Consent data = new Consent();
					data.setConsentId(pispConsent.getConsentId());
					data.setPsuId(pispConsent.getPsuId());
					data.setTppCId(pispConsent.getTppCId());
					List<AccountDetails> accountDetails = new ArrayList<>();
					accountDetails.add(pispConsent.getAccountDetails());
					data.setAccountDetails(accountDetails);
					data.setStartDate(pispConsent.getStartDate());
					data.setEndDate(pispConsent.getEndDate());
					ConsentStatusEnum statusEnum = pispConsent.getStatus();
					// Updating
					data.setStatusName(statusEnum.getStatusCode());
					data.setPaymentId(pispConsent.getPaymentId());

					data.setConsentType(ConsentType.PISP);
					PaymentConsentsPlatformResource paymentSetupPlatformResource = paymentSetupPlatformRepository
							.findOneByPaymentConsentIdAndSetupCmaVersionIn(pispConsent.getPaymentId(),
									compatibleVersionList.fetchVersionList());
					if (null != paymentSetupPlatformResource) {
						data.setTppLegalEntityName(paymentSetupPlatformResource.getTppLegalEntityName());
						data.setConsentCreationDate(paymentSetupPlatformResource.getCreatedAt());
					}
					consents.add(data);
				}
			}

			if (!cispConsentList.isEmpty()) {
				for (CispConsent cispConsent : cispConsentList) {
					Consent data = new Consent();
					data.setConsentId(cispConsent.getConsentId());
					data.setPsuId(cispConsent.getPsuId());
					data.setTppCId(cispConsent.getTppCId());
					List<AccountDetails> accountDetails = new ArrayList<>();
					accountDetails.add(cispConsent.getAccountDetails());
					data.setAccountDetails(accountDetails);
					data.setStartDate(cispConsent.getStartDate());
					data.setEndDate(cispConsent.getEndDate());
					ConsentStatusEnum statusEnum = cispConsent.getStatus();
					data.setConsentType(ConsentType.CISP);
					data.setStatusName(statusEnum.getStatusCode());
					data.setCispIntentId(cispConsent.getFundsIntentId());
					OBFundsConfirmationConsentResponse1 fundsConfirmationConsentResponse = fundsConfirmationConsentAdapter
							.getFundsConfirmationConsentPOSTResponse(cispConsent.getFundsIntentId());
					data.setTppLegalEntityName(fundsConfirmationConsentResponse.getData().getTppLegalEntityName());
					data.setConsentCreationDate(fundsConfirmationConsentResponse.getData().getCreationDateTime());
					consents.add(data);
				}
			}
		}

		return consents;
	}

}
