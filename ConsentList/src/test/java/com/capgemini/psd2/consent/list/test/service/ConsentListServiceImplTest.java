package com.capgemini.psd2.consent.list.test.service;

import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.consent.adapter.impl.CispConsentAdapterImpl;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.consent.list.service.impl.ConsentListServiceImpl;
import com.capgemini.psd2.consent.list.test.mock.data.ConsentListMockData;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.consent.adapter.impl.PispConsentAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.repository.PaymentSetupPlatformRepository;
import com.capgemini.psd2.product.common.CompatibleVersionList;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConsentListServiceImplTest {

	@Mock
	private AispConsentAdapterImpl aispConsentAdapter;
	
	@Mock
	private PispConsentAdapterImpl pispConsentAdapter;
	
	@Mock
	private CispConsentAdapterImpl cispConsentAdapter;

	@Mock
	private AccountRequestAdapter accountRequestAdapter;
	
	@Mock
	private PaymentSetupPlatformRepository paymentSetupPlatformRepository;

	@Mock
	private HttpServletRequest httpServletRequest;
	
	@Mock
	private CompatibleVersionList compatibleVersionList;  
	
	@Mock
	@Qualifier("FundsConfirmationConsentAdapter")
	private FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter;

	@InjectMocks
	private ConsentListServiceImpl service;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
	}

	@Test
	public void test() {

	}

	@Test(expected = PSD2Exception.class)
	public void testGetConsentListSuccessNullStatus() {
		String userId = "boi123";

		Mockito.when(aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId, null))
				.thenReturn(ConsentListMockData.getConsentsList());
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(ConsentListMockData.getAccountRequestData());
		Mockito.when(pispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				null)).thenReturn(ConsentListMockData.getPispConsentsList());
		Mockito.when(cispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				null)).thenReturn(ConsentListMockData.getCispConsentList());
		Mockito.when(paymentSetupPlatformRepository.findOneByPaymentConsentIdAndSetupCmaVersionIn(anyString(),anyList())).thenReturn(ConsentListMockData.getPaymentSetupPlatformRepository());
		when(fundsConfirmationConsentAdapter.getFundsConfirmationConsentPOSTResponse(anyString())).thenReturn(ConsentListMockData.getFundsCOnfirmationCOnsentResponse());
		Mockito.when(httpServletRequest.getQueryString()).thenReturn(null);
		Mockito.when(httpServletRequest.getRequestURI()).thenReturn("/consents/" + userId);
		service.getConsentList(userId, null, "BOI", "aisp");
	}
	
	@Test
	public void testGetConsentListSuccessNullStatus_TenantId() {
		String userId = "boi123";

		Mockito.when(aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId, null))
				.thenReturn(ConsentListMockData.getConsentsList());
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(ConsentListMockData.getAccountRequestData());
		Mockito.when(pispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				null)).thenReturn(ConsentListMockData.getPispConsentsList());
		Mockito.when(cispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				null)).thenReturn(ConsentListMockData.getCispConsentList());
		Mockito.when(paymentSetupPlatformRepository.findOneByPaymentConsentIdAndSetupCmaVersionIn(anyString(),anyList())).thenReturn(ConsentListMockData.getPaymentSetupPlatformRepository());
		when(fundsConfirmationConsentAdapter.getFundsConfirmationConsentPOSTResponse(anyString())).thenReturn(ConsentListMockData.getFundsCOnfirmationCOnsentResponse());
		Mockito.when(httpServletRequest.getQueryString()).thenReturn(null);
		Mockito.when(httpServletRequest.getRequestURI()).thenReturn("/consents/" + userId);
		service.getConsentList(userId, null, null, "aisp");
	}

	@Test
	public void testGetConsentListSuccess() {
		String userId = "boi123";

		Mockito.when(aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(ConsentListMockData.getConsentsList());
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(ConsentListMockData.getAccountRequestData());
		Mockito.when(pispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(ConsentListMockData.getPispConsentsList());
		Mockito.when(paymentSetupPlatformRepository.findOneByPaymentConsentIdAndSetupCmaVersionIn(anyString(),anyList())).thenReturn(ConsentListMockData.getPaymentSetupPlatformRepository());
		Mockito.when(httpServletRequest.getQueryString())
				.thenReturn("status=" + ConsentStatusEnum.AWAITINGAUTHORISATION.getStatusCode());
		Mockito.when(httpServletRequest.getRequestURI()).thenReturn("/consents/" + userId);
		service.getConsentList(userId, ConsentStatusEnum.AWAITINGAUTHORISATION.getStatusCode().toUpperCase(), null, null);
	}
	
	@Test
	public void testGetConsentListSuccess1() {
		String userId = "boi123";

		Mockito.when(aispConsentAdapter.retrieveConsentByPsuIdConsentStatusAndTenantId(userId,
				ConsentStatusEnum.AUTHORISED,"BOIUK")).thenReturn(ConsentListMockData.getConsentsList());
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(ConsentListMockData.getAccountRequestData());
		Mockito.when(pispConsentAdapter.retrieveConsentByPsuIdConsentStatusAndTenantId(userId,
				ConsentStatusEnum.AUTHORISED,"BOIUK")).thenReturn(ConsentListMockData.getPispConsentsList());
		Mockito.when(cispConsentAdapter.retrieveConsentByPsuIdConsentStatusAndTenantId(userId,
				ConsentStatusEnum.AUTHORISED,"BOIUK")).thenReturn(ConsentListMockData.getCispConsentList());
		Mockito.when(paymentSetupPlatformRepository.findOneByPaymentConsentIdAndSetupCmaVersionIn(anyString(),anyList())).thenReturn(ConsentListMockData.getPaymentSetupPlatformRepository());
		when(fundsConfirmationConsentAdapter.getFundsConfirmationConsentPOSTResponse(anyString())).thenReturn(ConsentListMockData.getFundsCOnfirmationCOnsentResponse());
		Mockito.when(httpServletRequest.getQueryString())
				.thenReturn("status=" + ConsentStatusEnum.AUTHORISED.getStatusCode());
		Mockito.when(httpServletRequest.getRequestURI()).thenReturn("/consents/" + userId);
		service.getConsentList(userId, ConsentStatusEnum.AUTHORISED.getStatusCode().toUpperCase(), "BOIUK", null);
	}
	
	
	@Test(expected = PSD2Exception.class)
	public void testGetConsentListNoConsentsFoundException() {
		String userId = "boi123";

		Mockito.when(aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(new ArrayList<AispConsent>());
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(ConsentListMockData.getAccountRequestData());
		Mockito.when(pispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(new ArrayList<PispConsent>());
		Mockito.when(paymentSetupPlatformRepository.findOneByPaymentConsentIdAndSetupCmaVersionIn(anyString(),anyList())).thenReturn(ConsentListMockData.getPaymentSetupPlatformRepository());
		Mockito.when(httpServletRequest.getQueryString())
				.thenReturn("status=" + ConsentStatusEnum.AWAITINGAUTHORISATION.getStatusCode());
		Mockito.when(httpServletRequest.getRequestURI()).thenReturn("/consents/" + userId);
		service.getConsentList(userId, ConsentStatusEnum.AWAITINGAUTHORISATION.getStatusCode().toUpperCase(), "BOI", null);
	}
 
	@Test
	public void testGetConsentListSuccessEmptyAccountRequestResponse() {
		String userId = "boi123";

		Mockito.when(aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(ConsentListMockData.getConsentsList());
		Mockito.when(accountRequestAdapter.getAccountRequestGETResponse(anyString())).thenReturn(null);
		Mockito.when(httpServletRequest.getQueryString())
				.thenReturn("status=" + ConsentStatusEnum.AWAITINGAUTHORISATION.getStatusCode());
		Mockito.when(httpServletRequest.getRequestURI()).thenReturn("/consents/" + userId);
		service.getConsentList(userId, ConsentStatusEnum.AWAITINGAUTHORISATION.getStatusCode().toUpperCase(), null, null);
	}

	@After
	public void tearDown() throws Exception {
		aispConsentAdapter = null;
		accountRequestAdapter = null;
		httpServletRequest = null;
	}

}