//package com.capgemini.psd2.security.consent.pisp.test.mock.data;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import com.capgemini.psd2.aisp.domain.OBAccount2;
//import com.capgemini.psd2.aisp.domain.OBAccount2Account;
//import com.capgemini.psd2.aisp.domain.OBAccount2Servicer;
//import com.capgemini.psd2.aisp.domain.OBReadAccount2;
//import com.capgemini.psd2.aisp.domain.OBReadAccount2Data;
//import com.capgemini.psd2.aisp.domain.OBAccount2Account.SchemeNameEnum;
//import com.capgemini.psd2.consent.domain.CustomerAccountInfo;
//import com.capgemini.psd2.consent.domain.PSD2Account;
//import com.capgemini.psd2.logger.PSD2Constants;
//import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
//import com.capgemini.psd2.pisp.domain.DebtorAccount;
//import com.capgemini.psd2.pisp.domain.DebtorAgent;
//import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
//import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
//import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
//import com.capgemini.psd2.pisp.validation.PispUtilities;
//
//public class PispConsentApplicationMockdata {
//
//	public static CustomPaymentSetupPOSTResponse mockPaymentSetupPOSTResponse;
//	public static List<CustomerAccountInfo> mockCustomerAccountInfoList;
//	
//	public static CustomPaymentSetupPOSTResponse getPaymentSetupPOSTResponseInvalidStatus() {
//		mockPaymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
//        PaymentSetupResponse data =  new PaymentSetupResponse();
//		data.setStatus(StatusEnum.ACCEPTEDCUSTOMERPROFILE);
//		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
//		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
//		initiation.setEndToEndIdentification("232445646");
//		data.setInitiation(initiation );
//		data.setPaymentId("345654");
//		mockPaymentSetupPOSTResponse.setData(data);
//		mockPaymentSetupPOSTResponse.setRisk(null);
//		return mockPaymentSetupPOSTResponse;
//	}
//	
//	public static CustomPaymentSetupPOSTResponse getPaymentSetupPOSTResponse() {
//		mockPaymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
//        PaymentSetupResponse data =  new PaymentSetupResponse();
//		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
//		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
//		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
//		initiation.setEndToEndIdentification("232445646");
//		data.setInitiation(initiation );
//		data.setPaymentId("345654");
//		mockPaymentSetupPOSTResponse.setData(data);
//		mockPaymentSetupPOSTResponse.setRisk(null);
//		return mockPaymentSetupPOSTResponse;
//	}
//	
//	public static CustomPaymentSetupPOSTResponse getPaymentSetupPOSTResponsewithAccounts() {
//		mockPaymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
//        PaymentSetupResponse data =  new PaymentSetupResponse();
//		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
//		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
//		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
//		initiation.setEndToEndIdentification("1234566");
//		DebtorAccount debtorAccount =  new DebtorAccount();
//		debtorAccount.setIdentification("1234566");
//		debtorAccount.setSchemeName(DebtorAccount.SchemeNameEnum.IBAN);
//		initiation.setDebtorAccount(debtorAccount);
//		DebtorAgent debtorAgent = new DebtorAgent();
//		debtorAgent.setIdentification("1234566");
//		debtorAgent.setSchemeName(DebtorAgent.SchemeNameEnum.BICFI);
//		initiation.setDebtorAgent(debtorAgent );
//		data.setInitiation(initiation );
//		data.setPaymentId("1234566");
//		mockPaymentSetupPOSTResponse.setData(data);
//		mockPaymentSetupPOSTResponse.setRisk(null);
//		return mockPaymentSetupPOSTResponse;
//	}
//
//	public static OBAccount2 getAccountData(){
//		PSD2Account account=  new PSD2Account();
//		account.setAccountId("2343253464");
//		account.setCurrency("EUR");
//		account.setNickname("John");
//		List<OBAccount2Account> accountList = new ArrayList<>();
//		OBAccount2Account account1 = new OBAccount2Account();
//		account1.setIdentification("12345");
//		account1.setSchemeName(SchemeNameEnum.IBAN);
//		OBAccount2Account account2 = new OBAccount2Account();
//		account1.setIdentification("12345");
//		account1.setSchemeName(SchemeNameEnum.IBAN);
//		accountList.add(account1);
//		accountList.add(account2);
//		OBAccount2Servicer servicer = new OBAccount2Servicer();
//		servicer.setIdentification("35475687");
//		account.setAccount(accountList);
//		account.setServicer(servicer);
//		Map<String, String> additionalInfo = new HashMap<>();
//		additionalInfo.put(PSD2Constants.ACCOUNT_NUMBER, "12345");
//		additionalInfo.put(PSD2Constants.ACCOUNT_NSC,"12345");
//		((PSD2Account) account).setAdditionalInformation(additionalInfo);
//		return account;	
//	}
//	
//	public static PSD2Account getPSD2AccountData(){
//		PSD2Account account=  new PSD2Account();
//		account.setAccountId("2343253464");
//		account.setCurrency("EUR");
//		account.setNickname("John");
//		List<OBAccount2Account> accountList = new ArrayList<>();
//		OBAccount2Account account1 = new OBAccount2Account();
//		account1.setIdentification("12345");
//		account1.setSchemeName(SchemeNameEnum.IBAN);
//		OBAccount2Account account2 = new OBAccount2Account();
//		account1.setIdentification("12345");
//		account1.setSchemeName(SchemeNameEnum.IBAN);
//		accountList.add(account1);
//		accountList.add(account2);
//		OBAccount2Servicer servicer = new OBAccount2Servicer();
//		servicer.setIdentification("35475687");
//		account.setAccount(accountList);
//		account.setServicer(servicer);
//		return account;	
//	}
//	
//	public static OBAccount2 getAccountDataNull(){
//	//	PSD2Account account=  new PSD2Account();
//		return null;
//	}
//	
//	public static List<CustomerAccountInfo> getCustomerAccountInfoList(){
//		mockCustomerAccountInfoList = new ArrayList<>();
//		CustomerAccountInfo customerAccountInfo1 = new CustomerAccountInfo();
//		customerAccountInfo1.setAccountName("John Doe");
//		customerAccountInfo1.setUserId("1234");
//		customerAccountInfo1.setAccountNumber("10203345");
//		customerAccountInfo1.setAccountNSC("SC802001");
//		customerAccountInfo1.setCurrency("EUR");
//		customerAccountInfo1.setNickname("John");
//		customerAccountInfo1.setAccountType("checking");
//
//		CustomerAccountInfo customerAccountInfo2 = new CustomerAccountInfo();
//		customerAccountInfo2.setAccountName("Tiffany Doe");
//		customerAccountInfo2.setUserId("1234");
//		customerAccountInfo2.setAccountNumber("10203346");
//		customerAccountInfo2.setAccountNSC("SC802002");
//		customerAccountInfo2.setCurrency("GRP");
//		customerAccountInfo2.setNickname("Tiffany");
//		customerAccountInfo2.setAccountType("savings");
//
//		mockCustomerAccountInfoList.add(customerAccountInfo1);
//		mockCustomerAccountInfoList.add(customerAccountInfo2);
//
//		return mockCustomerAccountInfoList;
//	}
//	
//	
//	public static OBReadAccount2 getCustomerAccountInfo(){
//		OBReadAccount2 mockOBReadAccount2 =new OBReadAccount2();
//		List<OBAccount2> accountData = new ArrayList<>();
//		PSD2Account acct = new PSD2Account();
//		acct.setAccountId("14556236");
//		acct.setCurrency("EUR");
//		acct.setNickname("John");
//		//acct.setAccountType("savings");
//		List<OBAccount2Account> accountList= new ArrayList<>();
//		OBAccount2Account oBAccount2Account = new OBAccount2Account();
//		oBAccount2Account.setIdentification("12345");
//		oBAccount2Account.setSchemeName(SchemeNameEnum.IBAN);
//		accountList.add(oBAccount2Account);
//		OBAccount2Servicer servicer = new OBAccount2Servicer();
//		servicer.setIdentification("12345");
//		
//		acct.setAccount(accountList);
//		acct.setServicer(servicer );
//		
//		PSD2Account accnt = new PSD2Account();
//		accnt.setAccountId("14556236");
//		accnt.setCurrency("EUR");
//		accnt.setNickname("John");
//		accnt.setServicer(servicer);
//		accnt.setAccount(accountList);
//		//accnt.setAccountType("savings");
//		accountData.add(acct );
//		accountData.add(accnt);
//		OBReadAccount2Data data2 = new OBReadAccount2Data();
//		data2.setAccount(accountData);
//		mockOBReadAccount2.setData(data2);
//	
//		return mockOBReadAccount2;
//	}
//	
//	public static OBReadAccount2 getCustomerAccountList(){
//		OBReadAccount2 mockOBReadAccount2 =new OBReadAccount2();
//		List<OBAccount2> accountData = new ArrayList<>();
//		PSD2Account acct = new PSD2Account();
//		acct.setAccountId("1234566");
//		acct.setCurrency("EUR");
//		acct.setNickname("John");
//		//acct.setAccountType("savings");
//		Map<String, String> additionalInformation = new HashMap<>();
//		additionalInformation.put(SchemeNameEnum.IBAN.name(), "1234566");
//		additionalInformation.put(OBAccount2Servicer.SchemeNameEnum.BICFI.name(), "1234566");
//		acct.setAdditionalInformation(additionalInformation );
//		
//		List<OBAccount2Account> accountList= new ArrayList<>();
//		OBAccount2Account mockData2Account = new OBAccount2Account();
//		mockData2Account.setIdentification("1234566");
//		mockData2Account.setSchemeName(OBAccount2Account.SchemeNameEnum.IBAN);
//		accountList.add(mockData2Account);
//		OBAccount2Servicer servicer = new OBAccount2Servicer();
//		servicer.setIdentification("1234566");
//		
//		acct.setAccount(accountList );
//		acct.setServicer(servicer );
//		
//		PSD2Account accnt = new PSD2Account();
//		accnt.setAccountId("1234566");
//		accnt.setCurrency("EUR");
//		accnt.setNickname("John");
//		accnt.setServicer(servicer);
//		accnt.setAccount(accountList);
//		//accnt.setAccountType("savings");
//		accountData.add(acct );
//		accountData.add(accnt);
//		OBReadAccount2Data data2 = new OBReadAccount2Data();
//		data2.setAccount(accountData);
//		mockOBReadAccount2.setData(data2);
//	
//		return mockOBReadAccount2;
//	}
//	
//	public static List<PSD2Account> getPSD2AcountList(){
//		List<PSD2Account> accountData = new ArrayList<>();
//		PSD2Account acct = new PSD2Account();
//		acct.setAccountId("14556236");
//		acct.setCurrency("EUR");
//		acct.setNickname("John");
//		//acct.setAccountType("savings");
//
//List<OBAccount2Account> accountList= new ArrayList<>();
//		OBAccount2Account mockData2Account = new OBAccount2Account();
//		mockData2Account.setIdentification("1245676");
//		mockData2Account.setSchemeName(OBAccount2Account.SchemeNameEnum.IBAN);
//		accountList.add(mockData2Account);
//		OBAccount2Servicer servicer = new OBAccount2Servicer();
//		servicer.setIdentification("12345");
//		
//		acct.setAccount(accountList );
//		acct.setServicer(servicer );
//		
//		PSD2Account accnt = new PSD2Account();
//		accnt.setAccountId("14556236");
//		accnt.setCurrency("EUR");
//		accnt.setNickname("John");
//		accnt.setServicer(servicer);
//		accnt.setAccount(accountList);
//		//accnt.setAccountType("savings");
//		accountData.add(acct );
//		accountData.add(accnt);
//		
//		return  accountData;
//	}
//
//}
