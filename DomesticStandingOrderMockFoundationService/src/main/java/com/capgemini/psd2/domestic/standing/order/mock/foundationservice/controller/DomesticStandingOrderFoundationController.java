package com.capgemini.psd2.domestic.standing.order.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.exception.handler.InvalidParameterRequestException;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.exception.handler.MissingAuthenticationHeaderException;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.service.DomesticStandingOrderService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;

@RestController
@RequestMapping("/group-payments/p/payments-service")
public class DomesticStandingOrderFoundationController {

	@Autowired
	private DomesticStandingOrderService domesticStandingOrdersConsentsService;

	@Autowired
	private ValidationUtility validationUtility;

	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instruction/{payment-instruction-id}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public StandingOrderInstructionComposite domesticPaymentConsentGet(
			@PathVariable("payment-instruction-id") String paymentInstructionlId,
			@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "x-api-source-user") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId) throws Exception {

		if (paymentInstructionlId == null) {

			throw new InvalidParameterRequestException("Bad request");
		}
		validationUtility.validateErrorCode(transactionId);
		return domesticStandingOrdersConsentsService.retrieveAccountInformation(paymentInstructionlId);

	}

	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instruction", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<StandingOrderInstructionComposite> domesticPaymentConsentPost(
			@RequestBody StandingOrderInstructionComposite standingOrderInstructionCompositeReq,
			@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "x-api-source-user") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId) throws Exception {

		
		
		if (null == standingOrderInstructionCompositeReq) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);
		}

		validationUtility.validateMockBusinessValidationsForSchedulePayment(
				standingOrderInstructionCompositeReq.getPaymentInstructionProposal().getReference());
		
		StandingOrderInstructionComposite standingOrderInstructionProposalCompositeResponse = null;
		try {
			standingOrderInstructionProposalCompositeResponse = domesticStandingOrdersConsentsService
					.createDomesticStandingOrdersConsentsResource(standingOrderInstructionCompositeReq);
			
            
		} catch (RecordNotFoundException e) {
			e.printStackTrace();
		}

		if (null == standingOrderInstructionProposalCompositeResponse) {

			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);
		}

		
		return new ResponseEntity<>(standingOrderInstructionProposalCompositeResponse, HttpStatus.CREATED);

	}

}
