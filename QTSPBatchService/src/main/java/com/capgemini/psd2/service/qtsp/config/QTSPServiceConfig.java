package com.capgemini.psd2.service.qtsp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("qtspservice.writer")
public class QTSPServiceConfig {

	private boolean writeToDB;

	private boolean writeToPingFederate;

	private boolean writeToNetScaler;

	private boolean publishToAWSSNS;

	private boolean batchJobState;

	private boolean connectToOB;

	public boolean isWriteToDB() {
		return writeToDB;
	}

	public void setWriteToDB(boolean writeToDB) {
		this.writeToDB = writeToDB;
	}

	public boolean isWriteToPingFederate() {
		return writeToPingFederate;
	}

	public void setWriteToPingFederate(boolean writeToPingFederate) {
		this.writeToPingFederate = writeToPingFederate;
	}

	public boolean isWriteToNetScaler() {
		return writeToNetScaler;
	}

	public void setWriteToNetScaler(boolean writeToNetScaler) {
		this.writeToNetScaler = writeToNetScaler;
	}

	public boolean isPublishToAWSSNS() {
		return publishToAWSSNS;
	}

	public void setPublishToAWSSNS(boolean publishToAWSSNS) {
		this.publishToAWSSNS = publishToAWSSNS;
	}

	public boolean isBatchJobState() {
		return batchJobState;
	}

	public void setBatchJobState(boolean batchJobState) {
		this.batchJobState = batchJobState;
	}

	public boolean isConnectToOB() {
		return connectToOB;
	}

	public void setConnectToOB(boolean connectToOB) {
		this.connectToOB = connectToOB;
	}
}