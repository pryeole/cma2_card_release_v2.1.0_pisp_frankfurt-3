package com.capgemini.psd2.pisp.validation.adapter;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.FPaymentsRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;

@Component
public interface FilePaymentValidator {
	public boolean validateFilePaymentSetupPOSTRequest(CustomFilePaymentSetupPOSTRequest filePaymentSetupRequest);

	public boolean validateFilePaymentSetupPOSTResponse(
			CustomFilePaymentConsentsPOSTResponse customFilePaymentConsentsPOSTResponse);

	public boolean validateFilePaymentConsentsGETRequest(PaymentRetrieveGetRequest paymentRetrieveRequest);

	public boolean validateFPaymentsDownloadRequest(FPaymentsRetrieveGetRequest paymentsRetrieveGetRequest);

	public boolean validateFilePaymentConsentsPOSTRequest(PaymentRetrieveGetRequest paymentRetrieveRequest);
}
