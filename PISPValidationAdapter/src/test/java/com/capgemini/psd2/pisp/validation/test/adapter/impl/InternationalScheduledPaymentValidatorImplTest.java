package com.capgemini.psd2.pisp.validation.test.adapter.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRate2;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBPartyIdentification43;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternational1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledResponse1;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.pisp.validation.adapter.impl.InternationalPaymentValidatorImpl;
import com.capgemini.psd2.pisp.validation.adapter.impl.InternationalScheduledPaymentValidatorImpl;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalScheduledPaymentValidatorImplTest {

	@InjectMocks
	private InternationalScheduledPaymentValidatorImpl intnationalScheduledPaymentValidatorImpl;

	@Mock
	private PSD2Validator psd2Validator;

	@Mock
	private CommonPaymentValidations commonPaymentValidations;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Before
	public void initializeErrorMessages() {
		Map<String, String> genericErrorMessages = new HashMap<>();
		genericErrorMessages.put("FIELD", "Something wrong with request body");
		genericErrorMessages.put("INTERNAL", "Unexpeced error occured");

		Map<String, String> specificErrorMessageMap = new HashMap<>();
		genericErrorMessages.put("FIELD", "Something wrong with request body");
		genericErrorMessages.put("INTERNAL", "Unexpeced error occured");

		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Test
	public void validatePaymentsResponse_resValidationEnabled() {
		Field resValidationEnabled = null;
		try {
			resValidationEnabled = InternationalScheduledPaymentValidatorImpl.class.getDeclaredField("resValidationEnabled");
			resValidationEnabled.setAccessible(true);
			resValidationEnabled.set(intnationalScheduledPaymentValidatorImpl, true);
		} catch (Exception e) {
		}
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		CustomISPaymentsPOSTResponse updateRes =new CustomISPaymentsPOSTResponse();
		OBWriteDataInternationalScheduledResponse1 data=new OBWriteDataInternationalScheduledResponse1();
		OBInternationalScheduled1 init=new OBInternationalScheduled1();
		init.setExchangeRateInformation(exchangeRateInformation);
		data.setInitiation(init);
		updateRes.setData(data);
		updateRes.getData().getInitiation().setExchangeRateInformation(exchangeRateInformation);

		OBExchangeRate2 exchangeRateInfo2 = new OBExchangeRate2();
		exchangeRateInfo2.setExpirationDateTime(DateTime.now().toString());
		exchangeRateInfo2.setUnitCurrency("USD");
		updateRes.getData().setExchangeRateInformation(exchangeRateInfo2);

		intnationalScheduledPaymentValidatorImpl.validatePaymentsResponse(getPaymentInternationalSubmitPOST201Response());
		verify(psd2Validator, times(1)).validate(any(PaymentInternationalSubmitPOST201Response.class));
	}

	@Test
	public void validatePaymentsResponse_resValidationDisabled() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = InternationalScheduledPaymentValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(intnationalScheduledPaymentValidatorImpl, false);
		} catch (Exception e) {
		}
		
		intnationalScheduledPaymentValidatorImpl.validatePaymentsResponse(getPaymentInternationalSubmitPOST201Response());
		verify(psd2Validator, never()).validate(any(PaymentInternationalSubmitPOST201Response.class));
	}

	@Test
	public void validatePaymentConsentResponse_valid() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = InternationalScheduledPaymentValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(intnationalScheduledPaymentValidatorImpl, true);
		} catch (Exception e) {
		}
		assertTrue(intnationalScheduledPaymentValidatorImpl
				.validatePaymentConsentResponse(getCustomIPaymentConsentsPOSTResponse()));
	}
	
	@Test
	public void validatePaymentConsentResponse_valid1() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = InternationalScheduledPaymentValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(intnationalScheduledPaymentValidatorImpl, true);
		} catch (Exception e) {
		}
		assertTrue(intnationalScheduledPaymentValidatorImpl
				.validatePaymentConsentResponse(getCustomIPaymentConsentsPOSTResponse1()));
	}
	
	@Test
	public void validatePaymentConsentResponse_validDisable() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = InternationalScheduledPaymentValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(intnationalScheduledPaymentValidatorImpl, false);
		} catch (Exception e) {
		}
		assertTrue(intnationalScheduledPaymentValidatorImpl
				.validatePaymentConsentResponse(getCustomIPaymentConsentsPOSTResponse()));
	}

	@Test
	public void validateConsentsGETRequest_valid() {
		assertTrue(intnationalScheduledPaymentValidatorImpl.validateConsentsGETRequest(new PaymentRetrieveGetRequest()));
	}

	@Test(expected = PSD2Exception.class)
	public void validatePaymentsPOSTRequest_invalidConsentId() {
		Field reqValidationEnabled, paymentIdRegexValidator = null;

		try {
			reqValidationEnabled = InternationalScheduledPaymentValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(intnationalScheduledPaymentValidatorImpl, true);
			paymentIdRegexValidator = InternationalScheduledPaymentValidatorImpl.class
					.getDeclaredField("paymentIdRegexValidator");
			paymentIdRegexValidator.setAccessible(true);
			paymentIdRegexValidator.set(intnationalScheduledPaymentValidatorImpl, "[a-zA-Z]");
		} catch (Exception e) {
		}
		intnationalScheduledPaymentValidatorImpl.validatePaymentsPOSTRequest(getCustomIPaymentsPOSTRequest());
	}

	@Test
	public void validatePaymentsPOSTRequest_validConsentId() {
		Field reqValidationEnabled, paymentIdRegexValidator = null;

		try {
			reqValidationEnabled = InternationalScheduledPaymentValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(intnationalScheduledPaymentValidatorImpl, true);
			paymentIdRegexValidator = InternationalScheduledPaymentValidatorImpl.class
					.getDeclaredField("paymentIdRegexValidator");
			paymentIdRegexValidator.setAccessible(true);
			paymentIdRegexValidator.set(intnationalScheduledPaymentValidatorImpl, "[a-zA-Z]*");
		} catch (Exception e) {
		}
		ReflectionTestUtils.setField(intnationalScheduledPaymentValidatorImpl, "merchantCategoryCodeRegexValidator", "\\d+");
		intnationalScheduledPaymentValidatorImpl.validatePaymentsPOSTRequest(getCustomIPaymentsPOSTRequest());
	}
	
	@Test
	public void validatePaymentsPOSTRequest_validConsentIdDisable() {
		Field reqValidationEnabled, paymentIdRegexValidator = null;

		try {
			reqValidationEnabled = InternationalScheduledPaymentValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(intnationalScheduledPaymentValidatorImpl, false);
			paymentIdRegexValidator = InternationalScheduledPaymentValidatorImpl.class
					.getDeclaredField("paymentIdRegexValidator");
			paymentIdRegexValidator.setAccessible(true);
			paymentIdRegexValidator.set(intnationalScheduledPaymentValidatorImpl, "[a-zA-Z]*");
		} catch (Exception e) {
		}
		ReflectionTestUtils.setField(intnationalScheduledPaymentValidatorImpl, "merchantCategoryCodeRegexValidator", "\\d+");
		intnationalScheduledPaymentValidatorImpl.validatePaymentsPOSTRequest(getCustomIPaymentsPOSTRequest());
	}

	@Test
	public void validateConsentRequest() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = InternationalScheduledPaymentValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(intnationalScheduledPaymentValidatorImpl, true);
		} catch (Exception e) {
		}
		ReflectionTestUtils.setField(intnationalScheduledPaymentValidatorImpl, "merchantCategoryCodeRegexValidator", "\\d+");
		assertTrue(intnationalScheduledPaymentValidatorImpl.validateConsentRequest(getCustomIPaymentConsentsPOSTRequest()));
	}
	
	@Test
	public void validateConsentRequestNUllinitiation() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = InternationalScheduledPaymentValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(intnationalScheduledPaymentValidatorImpl, true);
		} catch (Exception e) {
		}
		ReflectionTestUtils.setField(intnationalScheduledPaymentValidatorImpl, "merchantCategoryCodeRegexValidator", "\\d+");
		assertTrue(intnationalScheduledPaymentValidatorImpl.validateConsentRequest(getCustomIPaymentConsentsPOSTRequest1()));
	}
	
	@Test
	public void validateConsentRequestDisable() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = InternationalScheduledPaymentValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(intnationalScheduledPaymentValidatorImpl, false);
		} catch (Exception e) {
		}
		ReflectionTestUtils.setField(intnationalScheduledPaymentValidatorImpl, "merchantCategoryCodeRegexValidator", "\\d+");
		assertTrue(intnationalScheduledPaymentValidatorImpl.validateConsentRequest(getCustomIPaymentConsentsPOSTRequest()));
	}

	public CustomISPConsentsPOSTResponse getCustomIPaymentConsentsPOSTResponse() {
		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		response.setRisk(risk);
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("schemeName");
		debtorAccount.setIdentification("identification");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("schemeName");
		creditorAccount.setIdentification("identification");
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		postalAddress.setCountry("country");
		OBPartyIdentification43 creditor = new OBPartyIdentification43();
		creditor.setPostalAddress(postalAddress);
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("schemeName");
		creditorAgent.setIdentification("identification");
		creditorAgent.setPostalAddress(postalAddress);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setCurrency("currency");
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		initiation.setDebtorAccount(debtorAccount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setCreditorAgent(creditorAgent);
		initiation.setCreditor(creditor);
		initiation.setInstructedAmount(instructedAmount);
		// initiation.setInstructedAmount(instructedAmount);
		initiation.setLocalInstrument("localInstrument");
		OBCharge1Amount amount = new OBCharge1Amount();
		amount.setCurrency("currency");
		OBCharge1 charge = new OBCharge1();
		charge.setAmount(amount);
		List<OBCharge1> charges = new ArrayList<>();
		charges.add(charge);
		OBWriteDataInternationalScheduledConsentResponse1 data = new OBWriteDataInternationalScheduledConsentResponse1();
		data.setInitiation(initiation);
		data.setCharges(charges);
		data.setStatusUpdateDateTime(DateTime.now().toString());
		data.setExpectedExecutionDateTime(DateTime.now().toString());
		data.setExpectedSettlementDateTime(DateTime.now().toString());
		OBExchangeRate2 exchangeRateInformation = new OBExchangeRate2();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setExpirationDateTime(DateTime.now().toString());
		data.setExchangeRateInformation(exchangeRateInformation);
		OBExchangeRate1 exchangeRateInformation2 = new OBExchangeRate1();
		exchangeRateInformation2.setUnitCurrency("USD");
		data.getInitiation().setExchangeRateInformation(exchangeRateInformation2);
		data.setCutOffDateTime(DateTime.now().toString());
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setCompletionDateTime(DateTime.now().toString());
		data.setAuthorisation(authorisation);
		response.setData(data);
		return response;
	}
	
	
	public CustomISPConsentsPOSTResponse getCustomIPaymentConsentsPOSTResponse1() {
		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		response.setRisk(risk);
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("schemeName");
		debtorAccount.setIdentification("identification");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("schemeName");
		creditorAccount.setIdentification("identification");
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		postalAddress.setCountry("country");
		OBPartyIdentification43 creditor = new OBPartyIdentification43();
		creditor.setPostalAddress(postalAddress);
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("schemeName");
		creditorAgent.setIdentification("identification");
		creditorAgent.setPostalAddress(postalAddress);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setCurrency("currency");
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		initiation.setDebtorAccount(null);
		initiation.setCreditorAccount(null);
		initiation.setCreditorAgent(null);
		initiation.setCreditor(null);
		initiation.setInstructedAmount(instructedAmount);
		// initiation.setInstructedAmount(instructedAmount);
		initiation.setLocalInstrument("localInstrument");
		OBCharge1Amount amount = new OBCharge1Amount();
		amount.setCurrency("currency");
		OBCharge1 charge = new OBCharge1();
		charge.setAmount(amount);
		List<OBCharge1> charges = new ArrayList<>();
		charges.add(charge);
		OBWriteDataInternationalScheduledConsentResponse1 data = new OBWriteDataInternationalScheduledConsentResponse1();
		data.setInitiation(initiation);
		data.setCharges(null);
		data.setStatusUpdateDateTime(null);
		data.setExpectedExecutionDateTime(null);
		data.setExpectedSettlementDateTime(null);
		OBExchangeRate2 exchangeRateInformation = new OBExchangeRate2();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setExpirationDateTime(DateTime.now().toString());
		data.setExchangeRateInformation(exchangeRateInformation);
		OBExchangeRate1 exchangeRateInformation2 = new OBExchangeRate1();
		exchangeRateInformation2.setUnitCurrency("USD");
		data.getInitiation().setExchangeRateInformation(null);
		data.setCutOffDateTime(null);
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setCompletionDateTime(DateTime.now().toString());
		data.setAuthorisation(authorisation);
		response.setData(data);
		return response;
	}


	public CustomISPaymentsPOSTRequest getCustomIPaymentsPOSTRequest() {
		CustomISPaymentsPOSTRequest request = new CustomISPaymentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		risk.setMerchantCategoryCode("merchantTestCode");
		request.setRisk(risk);
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("schemeName");
		debtorAccount.setIdentification("identification");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("schemeName");
		creditorAccount.setIdentification("identification");
		creditorAccount.setSecondaryIdentification("secIden");
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		postalAddress.setCountry("country");
		OBPartyIdentification43 creditor = new OBPartyIdentification43();
		creditor.setPostalAddress(postalAddress);
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("schemeName");
		creditorAgent.setIdentification("identification");
		creditorAgent.setPostalAddress(postalAddress);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setCurrency("currency");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		initiation.setDebtorAccount(debtorAccount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setCreditorAgent(creditorAgent);
		initiation.setCreditor(creditor);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setLocalInstrument("localInstrument");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		OBDomestic1InstructedAmount amount = new OBDomestic1InstructedAmount();
		amount.setCurrency("currency");
		OBWriteDataInternationalScheduled1 data = new OBWriteDataInternationalScheduled1();
		data.setInitiation(initiation);
		data.setConsentId("consentId");
		request.setData(data);
		return request;
	}

	public CustomISPConsentsPOSTRequest getCustomIPaymentConsentsPOSTRequest() {
		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		request.setRisk(risk);
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("schemeName");
		debtorAccount.setIdentification("identification");
		debtorAccount.setSecondaryIdentification("secondaryIdentification");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("schemeName");
		creditorAccount.setIdentification("identification");
		creditorAccount.setSecondaryIdentification("secondaryIdentification");
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		postalAddress.setCountry("country");
		OBPartyIdentification43 creditor = new OBPartyIdentification43();
		creditor.setPostalAddress(postalAddress);
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("schemeName");
		creditorAgent.setIdentification("identification");
		creditorAgent.setPostalAddress(postalAddress);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setCurrency("currency");
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		initiation.setDebtorAccount(debtorAccount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setCreditorAgent(creditorAgent);
		initiation.setCreditor(creditor);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setLocalInstrument("localInstrument");
		initiation.setRemittanceInformation(remittanceInformation);
		initiation.setExchangeRateInformation(exchangeRateInformation);
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setCompletionDateTime("completionDateTime");
		OBWriteDataInternationalScheduledConsent1 data = new OBWriteDataInternationalScheduledConsent1();
		data.setInitiation(initiation);
		data.setAuthorisation(authorisation);
		request.setData(data);
		return request;
	}
	
	
	public CustomISPConsentsPOSTRequest getCustomIPaymentConsentsPOSTRequest1() {
		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		request.setRisk(risk);
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("schemeName");
		debtorAccount.setIdentification("identification");
		debtorAccount.setSecondaryIdentification("secondaryIdentification");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("schemeName");
		creditorAccount.setIdentification("identification");
		creditorAccount.setSecondaryIdentification("secondaryIdentification");
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		postalAddress.setCountry("country");
		OBPartyIdentification43 creditor = new OBPartyIdentification43();
		creditor.setPostalAddress(postalAddress);
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("schemeName");
		creditorAgent.setIdentification("identification");
		creditorAgent.setPostalAddress(postalAddress);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setCurrency("currency");
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		initiation.setDebtorAccount(null);
		initiation.setCreditorAccount(null);
		initiation.setCreditorAgent(null);
		initiation.setCreditor(null);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setLocalInstrument("localInstrument");
		initiation.setRemittanceInformation(remittanceInformation);
		initiation.setExchangeRateInformation(null);
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setCompletionDateTime("completionDateTime");
		OBWriteDataInternationalScheduledConsent1 data = new OBWriteDataInternationalScheduledConsent1();
		data.setInitiation(initiation);
		data.setAuthorisation(null);
		request.setData(data);
		return request;
	}

	public CustomISPaymentsPOSTResponse getPaymentInternationalSubmitPOST201Response() {
		CustomISPaymentsPOSTResponse response = new CustomISPaymentsPOSTResponse();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("schemeName");
		debtorAccount.setIdentification("identification");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("schemeName");
		creditorAccount.setIdentification("identification");
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		postalAddress.setCountry("country");
		OBPartyIdentification43 creditor = new OBPartyIdentification43();
		creditor.setPostalAddress(postalAddress);
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("schemeName");
		creditorAgent.setIdentification("identification");
		creditorAgent.setPostalAddress(postalAddress);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setCurrency("currency");
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		initiation.setDebtorAccount(debtorAccount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setCreditorAgent(creditorAgent);
		initiation.setCreditor(creditor);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setLocalInstrument("localInstrument");
		OBCharge1Amount amount = new OBCharge1Amount();
		amount.setCurrency("currency");
		OBCharge1 charge = new OBCharge1();
		charge.setAmount(amount);
		List<OBCharge1> charges = new ArrayList<>();
		charges.add(charge);
		OBWriteDataInternationalScheduledResponse1 data = new OBWriteDataInternationalScheduledResponse1();
		data.setInitiation(initiation);
		data.setCharges(charges);
		data.setStatusUpdateDateTime(DateTime.now().toString());
		data.setExpectedExecutionDateTime(DateTime.now().toString());
		data.setExpectedSettlementDateTime(DateTime.now().toString());
		response.setData(data);
		return response;
	}
}