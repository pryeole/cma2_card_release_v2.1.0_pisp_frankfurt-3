package com.capgemini.psd2.pisp.validation.test.adapter.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.domain.OBReadDataResponse1.StatusEnum;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.FPaymentsRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFile1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteFileConsent1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl.PaymentSetupPlatformAdapterImpl;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.pisp.validation.adapter.impl.FilePaymentValidatorImpl;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class FilePaymentValidatorImplTest {

	CustomFilePaymentSetupPOSTRequest filePaymentSetupRequest = new CustomFilePaymentSetupPOSTRequest();
	OBWriteDataFile1 submissionData = new OBWriteDataFile1();
	OBWriteDataFileConsent1 data = new OBWriteDataFileConsent1();
	OBWriteDataFileConsentResponse1 responseData = new OBWriteDataFileConsentResponse1();
	OBAuthorisation1 authorisation = new OBAuthorisation1();
	OBFile1 initiation = new OBFile1();
	OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
	OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
	OBWriteFileConsent1 paymentConsentsRequest = new OBWriteFileConsent1();
	CustomFilePaymentConsentsPOSTResponse consentsPOSTResponse = new CustomFilePaymentConsentsPOSTResponse();
	PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
	FPaymentsRetrieveGetRequest paymentsRetrieveGetRequest = new FPaymentsRetrieveGetRequest();
	CustomFilePaymentsPOSTRequest submissionRequestBody = new CustomFilePaymentsPOSTRequest();
	PaymentFileSubmitPOST201Response submissionResponse = new PaymentFileSubmitPOST201Response();
	OBWriteDataFileResponse1 resData = new OBWriteDataFileResponse1();

	@InjectMocks
	private FilePaymentValidatorImpl filePaymentValidatorImpl;

	@Mock
	private RequestHeaderAttributes reqAttrib;

	@Mock
	PaymentSetupPlatformAdapterImpl paymentSetupPlatformAdapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> genericErrorMessages = new HashMap<>();
		genericErrorMessages.put("INVALID_PAYLOAD_SRUCTURE", "");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
	}

	@Before
	public void setValues() {
		remittanceInformation.setReference("asdasd");
		remittanceInformation.setUnstructured("unstructured");

		debtorAccount.setIdentification("identification");
		debtorAccount.setSchemeName("schemeName");
		debtorAccount.setSecondaryIdentification("secondaryIdentification");
		authorisation.setCompletionDateTime("datetime");
		initiation.setControlSum(new BigDecimal(55));
		initiation.setDebtorAccount(debtorAccount);
		initiation.setFileHash("gfsdgsdf");
		initiation.setFileReference("asdfas");
		initiation.setFileType("sdgjkshf");
		initiation.setLocalInstrument("sdgsfhksdf");
		initiation.setNumberOfTransactions("500");
		initiation.setRemittanceInformation(remittanceInformation);
		initiation.setRequestedExecutionDateTime("12345gdfgt");

		data.setAuthorisation(authorisation);
		data.setInitiation(initiation);
		filePaymentSetupRequest.setData(data);
		paymentConsentsRequest.setData(data);
		responseData.setAuthorisation(authorisation);
		responseData.setConsentId("ass");
		responseData.setInitiation(initiation);
		responseData.setStatusUpdateDateTime("2019-05-03T10:15:30+01:00");
		responseData.setCutOffDateTime("2019-05-03T10:15:30+01:00");
		responseData.setCreationDateTime("2019-05-03T10:15:30+01:00");
		responseData.setAuthorisation(authorisation);

		consentsPOSTResponse.setData(responseData);

		submissionData.setInitiation(initiation);
		submissionData.setConsentId("consentID");
		submissionRequestBody.setData(submissionData);
		resData.setConsentId("consentId");
		resData.setFilePaymentId("filePaymentId");
		resData.setInitiation(initiation);
		submissionResponse.setData(resData);

	}

	@Mock
	private PSD2Validator psd2Validator;

	@Mock
	private CommonPaymentValidations commonPaymentValidations;

	@Test
	public void validateFilePaymentSetupPOSTRequestTest() {

		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = FilePaymentValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(filePaymentValidatorImpl, true);
		} catch (Exception e) {

		}

		doNothing().when(psd2Validator).validateWithError(paymentConsentsRequest, "");
		doNothing().when(psd2Validator).validate(consentsPOSTResponse);
		assertTrue(filePaymentValidatorImpl.validateFilePaymentSetupPOSTRequest(filePaymentSetupRequest));
	}

	@Test
	public void validateFilePaymentSetupPOSTResponseTest() {

		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = FilePaymentValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(filePaymentValidatorImpl, true);
		} catch (Exception e) {

		}

		doNothing().when(psd2Validator).validate(consentsPOSTResponse);
		assertTrue(filePaymentValidatorImpl.validateFilePaymentSetupPOSTResponse(consentsPOSTResponse));
	}

	@Test
	public void validateFilePaymentConsentsGETRequestTest() {
		assertTrue(filePaymentValidatorImpl.validateFilePaymentConsentsGETRequest(paymentRetrieveRequest));
	}

	@Test
	public void validateFPaymentsDownloadRequestTest() {
		assertTrue(filePaymentValidatorImpl.validateFPaymentsDownloadRequest(paymentsRetrieveGetRequest));
	}

	@Test
	public void validatePaymentsPOSTRequestTest() {
		ReflectionTestUtils.setField(filePaymentValidatorImpl, "paymentIdRegexValidator", "consentID");
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = FilePaymentValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(filePaymentValidatorImpl, true);
		} catch (Exception e) {

		}

		filePaymentValidatorImpl.validatePaymentsPOSTRequest(submissionRequestBody);
		doNothing().when(psd2Validator).validate(submissionRequestBody);
		// verify(psd2Validator, times(1)).validate(submissionRequestBody);
	}

	@Test
	public void validatePaymentsResponseTest() {

		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = FilePaymentValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(filePaymentValidatorImpl, true);
		} catch (Exception e) {

		}

		filePaymentValidatorImpl.validatePaymentsResponse(submissionResponse);
		doNothing().when(psd2Validator).validateWithError(submissionResponse, "");
	}

	@Test(expected = PSD2Exception.class)
	public void validatePaymentsPOSTRequestException() {
		CustomFilePaymentsPOSTRequest request = new CustomFilePaymentsPOSTRequest();
		OBWriteDataFile1 data1 = new OBWriteDataFile1();
		data1.consentId("12345");
		request.setData(data1);
		ReflectionTestUtils.setField(filePaymentValidatorImpl, "paymentIdRegexValidator", "consentID");
		filePaymentValidatorImpl.validatePaymentsPOSTRequest(request);

	}

	@Test
	public void validateFilePaymentConsentsPOSTRequestPaymentConsentsPlatformResponseTest() {
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setIdempotencyKey("123");
		platformResource.setStatus(StatusEnum.AUTHORISED.toString());

		doNothing().when(psd2Validator).validateRequestWithSwagger(anyObject(), anyString());
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(any()))
				.thenReturn(platformResource);
		reqAttrib = new RequestHeaderAttributes();
		reqAttrib.setIdempotencyKey("123");
		ReflectionTestUtils.setField(filePaymentValidatorImpl, "reqAttrib", reqAttrib);
		assertTrue(filePaymentValidatorImpl.validateFilePaymentConsentsPOSTRequest(paymentRetrieveRequest));
	}

	@Test(expected = PSD2Exception.class)
	public void validateFilePaymentConsentsPOSTRequestPaymentConsentsPlatformResponseIdemTest() {
		PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setIdempotencyKey("123");
		platformResource.setStatus(StatusEnum.AUTHORISED.toString());
		reqAttrib = new RequestHeaderAttributes();
		reqAttrib.setIdempotencyKey("234");
		ReflectionTestUtils.setField(filePaymentValidatorImpl, "reqAttrib", reqAttrib);
		doNothing().when(psd2Validator).validateRequestWithSwagger(anyObject(), anyString());
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(any()))
				.thenReturn(platformResource);
		filePaymentValidatorImpl.validateFilePaymentConsentsPOSTRequest(paymentRetrieveRequest);
	}

	@Test
	public void validateFilePaymentConsentsPOSTRequestPaymentConsentsPlatformResponseNullTest() {
		PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setIdempotencyKey("123");
		platformResource.setStatus(StatusEnum.AUTHORISED.toString());

		doNothing().when(psd2Validator).validateRequestWithSwagger(anyObject(), anyString());
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(any())).thenReturn(null);
		reqAttrib = new RequestHeaderAttributes();
		reqAttrib.setIdempotencyKey("123");
		ReflectionTestUtils.setField(filePaymentValidatorImpl, "reqAttrib", reqAttrib);
		filePaymentValidatorImpl.validateFilePaymentConsentsPOSTRequest(paymentRetrieveRequest);
	}

}
