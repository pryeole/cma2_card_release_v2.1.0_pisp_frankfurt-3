package com.capgemini.psd2.pisp.dspc.test.comparator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.dspc.comparator.DSPConsentsPayloadComparator;

@RunWith(SpringJUnit4ClassRunner.class)
public class DSPConsentsPayloadComparatorTest {

	@InjectMocks
	private DSPConsentsPayloadComparator comparator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testCompareValueEquals0() {
		OBWriteDomesticScheduledConsentResponse1 response = new OBWriteDomesticScheduledConsentResponse1();
		OBWriteDomesticScheduledConsentResponse1 adaptedScheduledPaymentConsentsResponse = new OBWriteDomesticScheduledConsentResponse1();

		OBWriteDataDomesticScheduledConsentResponse1 data = new OBWriteDataDomesticScheduledConsentResponse1();
		response.setData(data);
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		response.getData().setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("20.0");
		initiation.setInstructedAmount(instructedAmount );
		response.getData().setAuthorisation(new OBAuthorisation1());
		response.setRisk(new OBRisk1());

		adaptedScheduledPaymentConsentsResponse.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		adaptedScheduledPaymentConsentsResponse.getData().setInitiation(initiation);
		adaptedScheduledPaymentConsentsResponse.getData().setAuthorisation(new OBAuthorisation1());
		adaptedScheduledPaymentConsentsResponse.setRisk(new OBRisk1());

		int i = comparator.compare(response, adaptedScheduledPaymentConsentsResponse);
		assertEquals(0, i);
	}

	@Test
	public void testCompareValueEquals1() {
		OBWriteDomesticScheduledConsentResponse1 response = new OBWriteDomesticScheduledConsentResponse1();
		OBWriteDomesticScheduledConsentResponse1 adaptedScheduledPaymentConsentsResponse = new OBWriteDomesticScheduledConsentResponse1();

		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("20.0");
		initiation.setInstructedAmount(instructedAmount );
		response.getData().setInitiation(initiation);
		response.getData().setAuthorisation(new OBAuthorisation1());
		response.getData().getInitiation().setLocalInstrument("AccountDetails");
		response.setRisk(new OBRisk1());

		adaptedScheduledPaymentConsentsResponse.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		adaptedScheduledPaymentConsentsResponse.getData().setInitiation(initiation);
		adaptedScheduledPaymentConsentsResponse.getData().setAuthorisation(new OBAuthorisation1());
		adaptedScheduledPaymentConsentsResponse.setRisk(new OBRisk1());

		int i = comparator.compare(response, adaptedScheduledPaymentConsentsResponse);
		assertEquals(0, i);
	}

	@Test
	public void testCompareValueEquals1_AuthMisMatch() {
		OBWriteDomesticScheduledConsentResponse1 response = new OBWriteDomesticScheduledConsentResponse1();
		OBWriteDomesticScheduledConsentResponse1 adaptedScheduledPaymentConsentsResponse = new OBWriteDomesticScheduledConsentResponse1();

		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		response.getData().setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("20.0");
		initiation.setInstructedAmount(instructedAmount );
		response.getData().setAuthorisation(new OBAuthorisation1());
		response.getData().getAuthorisation().setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		response.setRisk(new OBRisk1());

		adaptedScheduledPaymentConsentsResponse.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		adaptedScheduledPaymentConsentsResponse.getData().setInitiation(initiation);
		adaptedScheduledPaymentConsentsResponse.getData().setAuthorisation(new OBAuthorisation1());
		adaptedScheduledPaymentConsentsResponse.getData().getAuthorisation().setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		adaptedScheduledPaymentConsentsResponse.setRisk(new OBRisk1());

		int i = comparator.compare(response, adaptedScheduledPaymentConsentsResponse);
		assertEquals(1, i);
	}

	@Test
	public void testCompareScheduledPaymentDetails_TppDetails() {
		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		CustomDSPConsentsPOSTResponse adaptedScheduledPaymentConsentsResponse = new CustomDSPConsentsPOSTResponse();
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();

		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("145.23");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());

		adaptedScheduledPaymentConsentsResponse.setData(response.getData());
		response.getData().setAuthorisation(new OBAuthorisation1());
		response.getData().getAuthorisation().setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		adaptedScheduledPaymentConsentsResponse.getData().getAuthorisation().setAuthorisationType(null);

		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		response.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());

		paymentSetupPlatformResource.setTppDebtorDetails("true");
		comparator.compareScheduledPaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void testCompareScheduledPaymentDetails_TppDetailsFalse() {
		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		CustomDSPConsentsPOSTResponse adaptedScheduledPaymentConsentsResponse = new CustomDSPConsentsPOSTResponse();
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();

		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("145");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());

		adaptedScheduledPaymentConsentsResponse.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		adaptedScheduledPaymentConsentsResponse.getData().setInitiation(new OBDomesticScheduled1());
		adaptedScheduledPaymentConsentsResponse.getData().setAuthorisation(null);

		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("145");
		response.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		deliveryAddress.setBuildingNumber("14");
		deliveryAddress.setCountrySubDivision("Division");
		risk.setDeliveryAddress(deliveryAddress);
		response.setRisk(risk);
		request.setRisk(risk);

		paymentSetupPlatformResource.setTppDebtorDetails("false");
		comparator.compareScheduledPaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void testCompareScheduledPaymentDetails_TppDetailsFalse_Addresslineempty() {
		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		CustomDSPConsentsPOSTResponse adaptedScheduledPaymentConsentsResponse = new CustomDSPConsentsPOSTResponse();
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();

		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("145");

		adaptedScheduledPaymentConsentsResponse.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		adaptedScheduledPaymentConsentsResponse.getData().setInitiation(new OBDomesticScheduled1());
		adaptedScheduledPaymentConsentsResponse.getData().setAuthorisation(new OBAuthorisation1());
		adaptedScheduledPaymentConsentsResponse.getData().getAuthorisation()
		.setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);

		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("145");
		response.getData().getInitiation().setRemittanceInformation(null);
		request.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		request.getData().getInitiation().getDebtorAccount().setName("BOI");
		response.getData().getInitiation().setDebtorAccount(null);

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = null;
		List<String> addressLineList = new ArrayList<>();
		String addressLine = null;
		addressLineList.add(addressLine);
		risk.setDeliveryAddress(deliveryAddress);
		response.setRisk(risk);
		request.setRisk(risk);

		paymentSetupPlatformResource.setTppDebtorDetails("false");
		comparator.compareScheduledPaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@After
	public void tearDown() {
		comparator = null;
	}
}
