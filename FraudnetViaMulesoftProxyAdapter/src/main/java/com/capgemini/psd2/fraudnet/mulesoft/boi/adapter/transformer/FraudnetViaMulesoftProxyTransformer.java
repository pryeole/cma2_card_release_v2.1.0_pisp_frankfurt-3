package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.transformer;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.Cookie;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.constants.FraudnetViaMulesoftProxyConstants;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.Account;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.AccountInfo;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.Action;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.ChannelClassificationCode;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.CounterParty;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.DeviceData;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.DeviceFieldType;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.ElectronicDeviceSession;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.EventData;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.FraudServiceRequest;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.OnlineUser;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.OnlineUserSession;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.Outcome;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.Source;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.SourceSubSystem;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.SourceSystem;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.SourceSystemGroupCountry;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.Transaction;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.Type;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.UserEvent;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.fraudsystem.domain.PSD2FinancialAccount;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;



@Component
public class FraudnetViaMulesoftProxyTransformer  {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FraudnetViaMulesoftProxyTransformer.class);
		
	public FraudServiceRequest transformFraudnetRequest(Map<String, Map<String, Object>> fraudSystemRequest){
		Map<String, Object> eventMap = fraudSystemRequest.get(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_EVENT_MAP);
		Map<String, Object> deviceMap = fraudSystemRequest.get(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_DEVICE_MAP);
		Map<String, Object> customerMap = fraudSystemRequest.get(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_CUSTOMER_MAP);
		Map<String,Object> additionalMap = fraudSystemRequest.get(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_ADDITIONAl_MAP);
		PSD2CustomerInfo psd2CustomerInfo =null;
		List<PSD2FinancialAccount> psd2FinancialAccountList =null;
		if (!NullCheckUtils.isNullOrEmpty(customerMap) && !NullCheckUtils.isNullOrEmpty(eventMap)) {
			psd2CustomerInfo = (PSD2CustomerInfo) customerMap.get(FraudnetViaMulesoftProxyConstants.CONTACT_DETAILS);
			psd2FinancialAccountList = (List<PSD2FinancialAccount>) eventMap.get(FraudnetViaMulesoftProxyConstants.FIN_ACCNTS);
		}
		
		//userEvent
		UserEvent userEvent = new UserEvent();
		
		setUserEventBasicData(eventMap, userEvent, additionalMap);
		
		ElectronicDeviceSession electronicDeviceSession = processDeviceData(deviceMap);
		
		processRequestHeaderData(deviceMap, electronicDeviceSession);
		
		EventData eventData = new EventData();
		
		FraudServiceRequest fraudServiceRequest = new FraudServiceRequest();
		
		userEvent.setEventData(eventData);
		
		fraudServiceRequest.setUserEvent(userEvent);
		
		OnlineUserSession onlineUserSession = processOnlineSessionData(deviceMap, userEvent, eventMap);
		
		if(electronicDeviceSession.getDeviceData() != null && !electronicDeviceSession.getDeviceData().isEmpty())
			eventData.setElectronicDeviceSession(electronicDeviceSession);
			
		eventData.setOnlineUserSession(onlineUserSession);
		
		
		//userEvent/person for valid login
		if(deviceMap!=null && deviceMap.get(FraudnetViaMulesoftProxyConstants.SESSION_LOGGEDIN)!=null /*&& (Boolean)deviceMap.get(FraudnetViaMulesoftProxyConstants.SESSION_LOGGEDIN)*/){
			processLoginPersonnelInfo(eventMap, customerMap, psd2CustomerInfo, psd2FinancialAccountList, userEvent);
		}
		return fraudServiceRequest;
				
	}

	private void setUserEventBasicData(Map<String, Object> eventMap, UserEvent userEvent, Map<String, Object> additionalMap) {
		if(!NullCheckUtils.isNullOrEmpty(eventMap)) {
			
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_MODEL_CODE)) && ((String.valueOf(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_MODEL_CODE)).equals("AISPL")) || (eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_MODEL_CODE).equals("AISPC"))))
			userEvent.setType(Type.ACCOUNT);
		else if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_MODEL_CODE)) && ((String.valueOf(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_MODEL_CODE)).equals("PISPL")) || (String.valueOf(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_MODEL_CODE)).equals("PISPC"))))
			userEvent.setType(Type.PAYMENT);
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TYPE)) && String.valueOf(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TYPE)).equals("Login"))
			{
			userEvent.setAction(Action.AUTHENTICATE);
		userEvent.setOutcome( eventMap.get("eventOutcome").equals("Success") ? Outcome.SUCCESS: Outcome.FAILED  );
			}
		else if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TYPE)) && String.valueOf(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TYPE)).equals("Consent"))
			{
				userEvent.setAction(Action.AUTHORIZE);
				userEvent.setOutcome(Outcome.SUCCESS);
			}
		
		
			
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TIME) ) )
			userEvent.setTime(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TIME).toString()); 
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_CHANNEL) ))
			userEvent.setChannelClassificationCode(ChannelClassificationCode.ONLINE);
		
		//userEvent/source     
		Source source = new Source();
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SYSTEM) ))
			source.setSourceSystem(SourceSystem.API_PLATFORM);
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TYPE)) 
				&& (String.valueOf(String.valueOf(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TYPE)))).equalsIgnoreCase("Login")
				)
			source.setSourceSubSystem(SourceSubSystem.AUTHENTICATION_UI);
		else if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TYPE)) 
				&& (String.valueOf(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TYPE))).equalsIgnoreCase("Consent")
				)
			source.setSourceSubSystem(SourceSubSystem.CONSENT_UI);
		
		
		
		
		//group country
		if(!NullCheckUtils.isNullOrEmpty(additionalMap.get(FraudnetViaMulesoftProxyConstants.TENANT_ID))){
			if(  (additionalMap.get(FraudnetViaMulesoftProxyConstants.TENANT_ID)).toString().equals(FraudnetViaMulesoftProxyConstants.BOIROI)){
				source.setSourceSystemGroupCountry(SourceSystemGroupCountry.IE);
			}
			else if((additionalMap.get(FraudnetViaMulesoftProxyConstants.TENANT_ID)).toString().equals(FraudnetViaMulesoftProxyConstants.BOIUK)){
				source.setSourceSystemGroupCountry(SourceSystemGroupCountry.UK);
			}
		}
		
		
		/*
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SYSTEM) ))
			source.setSourceSystem(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SYSTEM).toString());
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SUBSYSTEM) ))
			source.setSourceSubSystem(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SUBSYSTEM).toString());*/
		
		userEvent.setSource(source);//all must
		}
		}
		
	private OnlineUserSession processOnlineSessionData(Map<String, Object> deviceMap, UserEvent userEvent,Map<String, Object> eventMap) {
		//userEvent/onlineUserSession  optional 
		OnlineUserSession session = new OnlineUserSession();
		if (deviceMap!=null && !NullCheckUtils.isNullOrEmpty(deviceMap.get(FraudnetViaMulesoftProxyConstants.SESSION_ID))) {
			

		
			//Test purpose as clarification pending , To Remove
			if( (Boolean)deviceMap.get(FraudnetViaMulesoftProxyConstants.SESSION_LOGGEDIN)){
				session.setUserSessionActive(true);//login success
				session.setTwoFactorAuthentication(false);
			}else{
				session.setUserSessionActive(false);
				session.setTwoFactorAuthentication(false);
		}
			
			session.setSessionId(generateRandom(11));
			
		}
		if(!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SESSION_DURATION) ) )
			session.setSessionDuration(Long.valueOf( (eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SESSION_DURATION)).toString()));
		return session;
	}

	private void processRequestHeaderData(Map<String, Object> deviceMap,
			ElectronicDeviceSession electronicDeviceSession) {
		//Header optional 
		if(deviceMap!=null && !deviceMap.isEmpty() && deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_HEADERS) != null ){
			Map<String, String> headerData = null;
			try {
				headerData = new ObjectMapper().readValue(deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_HEADERS).toString(), new TypeReference<HashMap<String, String>>() {});
			}catch (Exception e) {
				throw AdapterException.populatePSD2Exception("FraudNet service error processing Header information", AdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
			if (!NullCheckUtils.isNullOrEmpty(headerData)){
				for (Map.Entry<String, String> header : headerData.entrySet()) {
					DeviceData headerDetail = new DeviceData();
					headerDetail.setDeviceFieldName(header.getKey());
					headerDetail.setDeviceFieldType(DeviceFieldType.HEADER);
					headerDetail.setDeviceFieldValue(header.getValue());
					electronicDeviceSession.getDeviceData().add(headerDetail);
				}
			}
			
		}
		
		//Cookie optional
		if(deviceMap!=null && !deviceMap.isEmpty() && deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_COOKIES) != null ){
				Cookie[] cookies = (Cookie[])deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_COOKIES);
			for (Cookie cookie : cookies) {
				DeviceData cookieDetail = new DeviceData();
				cookieDetail.setDeviceFieldName(cookie.getName());
				cookieDetail.setDeviceFieldType(DeviceFieldType.COOKIE);
				cookieDetail.setDeviceFieldValue(String.valueOf(cookie.getValue()));
				electronicDeviceSession.getDeviceData().add(cookieDetail);
			}	
		}
		
		
	}
		
	private ElectronicDeviceSession processDeviceData(Map<String, Object> deviceMap) {
		//userEvent/electronicDeviceSession
		ElectronicDeviceSession electronicDeviceSession = new ElectronicDeviceSession();
		if(deviceMap!=null && !deviceMap.isEmpty() && deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_IP) != null )
			electronicDeviceSession.setDeviceAddress(deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_IP).toString());
		
		 
		//userEvent/electronicDeviceSession/deviceData
		//JSC   
		List<DeviceData> deviceData=new ArrayList<>();
		DeviceData jscData = new DeviceData();
		System.out.println("deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_JSC).toString(): "+deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_JSC).toString());
		if(deviceMap!=null && !deviceMap.isEmpty() && deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_JSC) != null ){
			jscData.setDeviceFieldName("jsc");
			jscData.setDeviceFieldType(DeviceFieldType.JSC);
			jscData.setDeviceFieldValue(deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_JSC).toString());
			deviceData.add(jscData);
			
			}
			
		electronicDeviceSession.setDeviceData(deviceData);
			
		System.out.println("deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_PAYLOAD).toString(): "+deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_PAYLOAD).toString());
		//HDIM  must
		DeviceData hdimData = new DeviceData();
		if(deviceMap!=null && !deviceMap.isEmpty() && deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_PAYLOAD) != null ){
			hdimData.setDeviceFieldName("hdim");
			hdimData.setDeviceFieldType(DeviceFieldType.HDIM);
			hdimData.setDeviceFieldValue(deviceMap.get(FraudnetViaMulesoftProxyConstants.DEVICE_PAYLOAD).toString());//payload 
		
			
			electronicDeviceSession.getDeviceData().add(hdimData);
		}
		return electronicDeviceSession;
		}
		
	private void processLoginPersonnelInfo(Map<String, Object> eventMap, Map<String, Object> customerMap,
			PSD2CustomerInfo psd2CustomerInfo, List<PSD2FinancialAccount> psd2FinancialAccountList,
			UserEvent userEvent) {
	 LOGGER.debug("customerMap" + customerMap);
		
		OnlineUser onlineUser = new OnlineUser();
		
		onlineUser.setUserIdentifier(String.valueOf(customerMap.get(FraudnetViaMulesoftProxyConstants.ACCOUNT_ID)));
		List<Account> accountList = new ArrayList<>();
		//userEvent/person/account  
		if (!NullCheckUtils.isNullOrEmpty(psd2FinancialAccountList)){
		
			for(PSD2FinancialAccount psd2FinancialAccountData : psd2FinancialAccountList){
				Account account = new Account();
				if(!psd2FinancialAccountData.getSubType().trim().equalsIgnoreCase("CreditCard")){
				AccountInfo accountInfo = new AccountInfo();
				accountInfo.setAccountName(psd2FinancialAccountData.getName());
				accountInfo.setAccountNumber(psd2FinancialAccountData.getHashedAccountNumber());
				accountInfo.setBranchSubNationalSortCode(psd2FinancialAccountData.getRoutingNumber());
				account.setAccountInfo(accountInfo);
				}
				account.setSourceSystemAccountType(psd2FinancialAccountData.getSubType());
				account.setBoiCurrencyCode(psd2FinancialAccountData.getCurrency());
				accountList.add(account);
			}
			
		
		
		}
		Transaction transaction = txnPaymentInstruction(eventMap);
		//userEvent.setPerson(person);//person set
		
			userEvent.getEventData().setOnlineUser(onlineUser);
			userEvent.getEventData().setAccount(accountList);
			userEvent.getEventData().setTransaction(transaction);
	}

	/*private void processContactInfo(PSD2CustomerInfo psd2CustomerInfo, Person person) {
			//userEvent/person/contactInformation    must 
			ContactInformation contactInformation = new ContactInformation();
			List<Emails> emailData = psd2CustomerInfo.getEmails();
			if (!NullCheckUtils.isNullOrEmpty(emailData))
				for (Emails email : emailData)
					contactInformation.setEmailAddress(email.getEmail());
			List<PhoneNumbers> phoneNumberData = psd2CustomerInfo.getPhoneNumbers();
			if (!NullCheckUtils.isNullOrEmpty(phoneNumberData))
				for (PhoneNumbers number : phoneNumberData)
					contactInformation.setOtherPhoneNumber(number.getNumber());
			person.setContactInformation(contactInformation);
			
			//userEvent/person/address    must
			Address address = new Address();
			if(!NullCheckUtils.isNullOrEmpty(psd2CustomerInfo.getAddress()))		{
				address.setFirstAddressLine(psd2CustomerInfo.getAddress().getStreetLine1());
				address.setSecondAddressLine(psd2CustomerInfo.getAddress().getStreetLine2());
				address.setThirdAddressLine(psd2CustomerInfo.getAddress().getCity());
				address.setPostCodeNumber(psd2CustomerInfo.getAddress().getPostal());
				person.setAddress(address);
			}
			}*/
			
	private Transaction txnPaymentInstruction(Map<String, Object> eventMap) {
		
		Transaction transaction = null;//optional
		
		if(eventMap != null && !eventMap.isEmpty())
			if(!(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TYPE).toString().equalsIgnoreCase("Login")) && eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SUBSYSTEM).toString().equalsIgnoreCase("PISP") ){
				transaction = new Transaction();
				
				if (!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_ID))) {
					PaymentInstruction paymentInstruction = new PaymentInstruction();
					paymentInstruction.setPaymentInstructionNumber(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_ID).toString()); 
					transaction.setPaymentInstruction(paymentInstruction);
				}
				if (!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_AMNT))) {
					FinancialEventAmount financialEventAmount = new FinancialEventAmount();
					financialEventAmount.setTransactionCurrency(Double.valueOf((String) eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_AMNT)));
					transaction.setFinancialEventAmount(financialEventAmount);
				}
				if (!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_TIME)))
					transaction.setOccurenceDate(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_TIME).toString());
				if (!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_MEMO)))
					transaction.setTransactionDecription(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_MEMO).toString());
				
				if (!NullCheckUtils.isNullOrEmpty(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_CURRENCY)))
					transaction.setBoiCurrencyCode(String.valueOf(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_CURRENCY)));
					
				
				List<CounterParty> counterParties = new ArrayList<>();
				
				if(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_ACCOUNT_NUMBER) instanceof String){
					CounterParty counterParty = new CounterParty();
					AccountInfo accountInfo = new AccountInfo();
					accountInfo.setAccountNumber(String.valueOf(eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_ACCOUNT_NUMBER)));
					counterParty.setAccountInfo(accountInfo);
					counterParties.add(counterParty);
				}
				else{
					List<String> accountNos = (List) eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_ACCOUNT_NUMBER);
					for (String accNo : accountNos)
					{
						CounterParty counterParty = new CounterParty();
						AccountInfo accountInfo = new AccountInfo();
						accountInfo.setAccountNumber(accNo);
						counterParty.setAccountInfo(accountInfo);
						counterParties.add(counterParty);
					}	
				}
				
				transaction.setCounterParty(counterParties);
			}
		
			return transaction;
		}

/*	private String amountProcessing(Map<String, Object> eventMap, UserEvent userEvent) {
		String amount = "00.00";
		if(eventMap!=null && !eventMap.isEmpty() && eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_AMNT) != null ){
			FinancialEventAmount financialEventAmount = new FinancialEventAmount();
			try {
				amount = eventMap.get(FraudnetViaMulesoftProxyConstants.TRANSFER_AMNT).toString();
			} catch (Exception e) {
				financialEventAmount.setTransactionCurrency(amount);
			}
				
			financialEventAmount.setTransactionCurrency(amount);// must
			userEvent.setFinancialEventAmount(financialEventAmount);
		}
		return amount;
	}*/
	
	private long getCurentDateDifferenceInMilliSeconds(String startDate)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		//date = sdf.parse("2018-09-11T06:42:06+00:00"); //working
		//date = sdf.parse(startDate);
		
		Date date = null;
		try {
			date = sdf.parse(startDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Instant instant = date.toInstant();
		ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
		LocalDateTime now = LocalDateTime.now();
		
		LocalDateTime startLocalDate = zdt.toLocalDateTime();
		return Duration.between(startLocalDate, now).toMillis();
	} 
	
	private String generateRandom(int prefix) {
        Random rand = new Random();

        long x = (long)(rand.nextDouble()*100000000000000L);

        String s = String.valueOf(prefix) + String.format("%014d", x);
        return s;
    }

	public <T> T transformFraudnetResponse(FraudServiceResponse fraudServiceResponse) {

		if (NullCheckUtils.isNullOrEmpty(fraudServiceResponse))
			throw AdapterException.populatePSD2Exception("FraudNet service response is null", AdapterErrorCodeEnum.TECHNICAL_ERROR);

		return (T) fraudServiceResponse;

	}
	
}

