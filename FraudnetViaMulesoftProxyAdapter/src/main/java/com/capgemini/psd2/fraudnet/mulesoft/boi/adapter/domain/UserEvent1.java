package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.time.OffsetDateTime;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

/**
 * UserEvent1
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserEvent1 {
	@SerializedName("source")
	private Source source = null;

	@SerializedName("type")
	private Type type = null;

	@SerializedName("action")
	private Action action = null;

	@SerializedName("outcome")
	private Outcome outcome = null;

	@SerializedName("time")
	private OffsetDateTime time = null;

	@SerializedName("channelClassificationCode")
	private ChannelClassificationCode channelClassificationCode = null;

	@SerializedName("eventData")
	private EventData1 eventData = null;

	public UserEvent1 source(Source source) {
		this.source = source;
		return this;
	}

	/**
	 * Get source
	 * 
	 * @return source
	 **/
	@ApiModelProperty(required = true, value = "")
	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}

	public UserEvent1 type(Type type) {
		this.type = type;
		return this;
	}

	/**
	 * Get type
	 * 
	 * @return type
	 **/
	@ApiModelProperty(required = true, value = "")
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public UserEvent1 action(Action action) {
		this.action = action;
		return this;
	}

	/**
	 * Get action
	 * 
	 * @return action
	 **/
	@ApiModelProperty(required = true, value = "")
	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public UserEvent1 outcome(Outcome outcome) {
		this.outcome = outcome;
		return this;
	}

	/**
	 * Get outcome
	 * 
	 * @return outcome
	 **/
	@ApiModelProperty(required = true, value = "")
	public Outcome getOutcome() {
		return outcome;
	}

	public void setOutcome(Outcome outcome) {
		this.outcome = outcome;
	}

	public UserEvent1 time(OffsetDateTime time) {
		this.time = time;
		return this;
	}

	/**
	 * The time when the event occurred
	 * 
	 * @return time
	 **/
	@ApiModelProperty(required = true, value = "The time when the event occurred")
	public OffsetDateTime getTime() {
		return time;
	}

	public void setTime(OffsetDateTime time) {
		this.time = time;
	}

	public UserEvent1 channelClassificationCode(ChannelClassificationCode channelClassificationCode) {
		this.channelClassificationCode = channelClassificationCode;
		return this;
	}

	/**
	 * Get channelClassificationCode
	 * 
	 * @return channelClassificationCode
	 **/
	@ApiModelProperty(required = true, value = "")
	public ChannelClassificationCode getChannelClassificationCode() {
		return channelClassificationCode;
	}

	public void setChannelClassificationCode(ChannelClassificationCode channelClassificationCode) {
		this.channelClassificationCode = channelClassificationCode;
	}

	public UserEvent1 eventData(EventData1 eventData) {
		this.eventData = eventData;
		return this;
	}

	/**
	 * Get eventData
	 * 
	 * @return eventData
	 **/
	@ApiModelProperty(required = true, value = "")
	public EventData1 getEventData() {
		return eventData;
	}

	public void setEventData(EventData1 eventData) {
		this.eventData = eventData;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		UserEvent1 userEvent1 = (UserEvent1) o;
		return Objects.equals(this.source, userEvent1.source) && Objects.equals(this.type, userEvent1.type)
				&& Objects.equals(this.action, userEvent1.action) && Objects.equals(this.outcome, userEvent1.outcome)
				&& Objects.equals(this.time, userEvent1.time)
				&& Objects.equals(this.channelClassificationCode, userEvent1.channelClassificationCode)
				&& Objects.equals(this.eventData, userEvent1.eventData);
	}

	@Override
	public int hashCode() {
		return Objects.hash(source, type, action, outcome, time, channelClassificationCode, eventData);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class UserEvent1 {\n");

		sb.append("    source: ").append(toIndentedString(source)).append("\n");
		sb.append("    type: ").append(toIndentedString(type)).append("\n");
		sb.append("    action: ").append(toIndentedString(action)).append("\n");
		sb.append("    outcome: ").append(toIndentedString(outcome)).append("\n");
		sb.append("    time: ").append(toIndentedString(time)).append("\n");
		sb.append("    channelClassificationCode: ").append(toIndentedString(channelClassificationCode)).append("\n");
		sb.append("    eventData: ").append(toIndentedString(eventData)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
