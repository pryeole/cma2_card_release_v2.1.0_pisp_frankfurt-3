package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

/**
 * AssessmentRules
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssessmentRules {
	@SerializedName("ruleType")
	private String ruleType = null;

	@SerializedName("ruleDescription")
	private String ruleDescription = null;

	public AssessmentRules ruleType(String ruleType) {
		this.ruleType = ruleType;
		return this;
	}

	/**
	 * The type of rule that was fired
	 * 
	 * @return ruleType
	 **/
	@ApiModelProperty(required = true, value = "The type of rule that was fired")
	public String getRuleType() {
		return ruleType;
	}

	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	public AssessmentRules ruleDescription(String ruleDescription) {
		this.ruleDescription = ruleDescription;
		return this;
	}

	/**
	 * The description of the rule type
	 * 
	 * @return ruleDescription
	 **/
	@ApiModelProperty(required = true, value = "The description of the rule type")
	public String getRuleDescription() {
		return ruleDescription;
	}

	public void setRuleDescription(String ruleDescription) {
		this.ruleDescription = ruleDescription;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AssessmentRules assessmentRules = (AssessmentRules) o;
		return Objects.equals(this.ruleType, assessmentRules.ruleType)
				&& Objects.equals(this.ruleDescription, assessmentRules.ruleDescription);
	}

	@Override
	public int hashCode() {
		return Objects.hash(ruleType, ruleDescription);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class AssessmentRules {\n");

		sb.append("    ruleType: ").append(toIndentedString(ruleType)).append("\n");
		sb.append("    ruleDescription: ").append(toIndentedString(ruleDescription)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
