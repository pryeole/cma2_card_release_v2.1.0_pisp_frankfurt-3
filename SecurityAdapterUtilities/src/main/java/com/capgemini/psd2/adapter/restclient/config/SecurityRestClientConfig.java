/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.restclient.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.rest.client.exception.handler.SecurityExceptionHandlerImpl;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

/**
 * The Class SecurityRestClientConfig.
 */
@Configuration
public class SecurityRestClientConfig {

	/**
	 * Rest client sync impl.
	 *
	 * @return the rest client sync
	 */
	@Bean(name="restClientSecurityFoundation")
	public RestClientSync restClientSyncImpl(){
		return new RestClientSyncImpl(adapterExceptionHandlerImpl());
	}
	
	
	/**
	 * Exception handler impl.
	 *
	 * @return the exception handler
	 */
	@Bean
	public SecurityExceptionHandlerImpl adapterExceptionHandlerImpl(){
		return new SecurityExceptionHandlerImpl();
	}
}
