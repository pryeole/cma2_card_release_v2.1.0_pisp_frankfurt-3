package com.capgemini.psd2.adapter.security.constants;

public class AdapterSecurityConstants {
	public static final String BRANDID_PARAM = "brandId";
	public static final String CHANNELID_PARAM = "channelId";
	public static final String CORRELATIONID_HEADER = "X-CORRELATION-ID";
	public static final String API_CORRELATIONID_HEADER = "x-api-correlation-id";
	public static final String CHANNELID_HEADER="X-BOI-CHANNEL";
	public static final String SOURCE_SYSTEM_HEADER = "x-api-source-system";
	public static final String USER_HEADER="X-BOI-USER";
	public static final String SOURCE_USER_HEADER = "x-api-source-user";
	public static final String TRANSACTIONID_HEADER = "x-api-transaction-id";
	public static final String USER_AUTHORITY = "CUSTOMER_ROLE";
	public static final String SAAS_URL_PARAM = "saasurl";
	public static final String CHANNEL_EXCEPTION = "channelexception";
	public static final String SCOPES = "scopes";
	public static final String FS_AUTH_001 = "FS_AUTH_001";
	public static final String SOURCE_SYSTEM_HEADER_VALUE = "PSD2API";
	public static final String CHANNELCODE = "channel-code";
	public static final String EVENTTYPE = "eventType";
	public static final String CHANNEL_BRAND = "x-api-channel-brand";
	

}
