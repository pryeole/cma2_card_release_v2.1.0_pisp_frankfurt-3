package com.capgemini.psd2.pisp.test.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.utilities.PispUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
public class PispUtilitiesTest {
	
	@Before
	public void setUp() {
		Map<String,String> genericErrorMessages=new HashMap<>();
		Map<String,String> specificErrorMessages=new HashMap<>();
		
		genericErrorMessages.put("INTERNAL", "INTERNAL Error");
		specificErrorMessages.put("Idempotency", "Idempotency duration is not configured in correct format.");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessages);
	}

	
	@Test
    public void testConstructorIsPrivate() throws Exception {
      Constructor<PispUtilities> constructor = PispUtilities.class.getDeclaredConstructor();
      assertTrue(Modifier.isPrivate(constructor.getModifiers()));
      constructor.setAccessible(true);
      constructor.newInstance();
    }
	/*
	 * Test with not null value
	 */
	@Test
	public void testIsEmptyNotNull(){
		String str = "Test";
		assertNotEquals(true, PispUtilities.isEmpty(str));
		
	}
	
	/*
	 * Test with null value
	 */
	@Test
	public void testIsEmptyNull(){
		String str = null;
		assertEquals(true, PispUtilities.isEmpty(str));
		
	}
	
	/*
	 * Test with empty string
	 */
	@Test
	public void testIsEmptyString(){
		String str = "";
		assertEquals(true, PispUtilities.isEmpty(str));
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetMilliSecondsNullException(){
		String duration = null;
		PispUtilities.getMilliSeconds(duration);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetMilliSecondsInvalidException(){
		String duration = "ABC";
		PispUtilities.getMilliSeconds(duration);
	}
	
	@Test
	public void testGetMilliSecondsPositive(){
		String[] inputData = {"24H","24M","24S"};
		for(String duration: inputData)
			PispUtilities.getMilliSeconds(duration);
	}
	
	@Test
	public void testGetObjectMapper(){
		PispUtilities.getObjectMapper();		
	}

	@Test
	public void testGetCurrentDateInISOFormat(){
		PispUtilities.getCurrentDateInISOFormat();		
	}
	
	@Test
	public void testGetTemporalAccessorInISOFormat(){
		TemporalAccessor i=ZonedDateTime.now().plusDays(1);
		PispUtilities.getTemporalAccessorInISOFormat(i);		
	}
	
	@Test(expected = NullPointerException.class)
	public void testGetTemporalAccessorInISOFormatNull(){
		TemporalAccessor i=null;
		PispUtilities.getTemporalAccessorInISOFormat(i);		
	}
	
	@Test
	public void testPopulateLinks(){
		
		String methodType = "void";
		String selfUrl = "www.localtesting.com/file-payment-consents/59bb6c31-5831-4a4a-a2b1-d54f5b8f20d7";
		String id = "2";
		PispUtilities.populateLinks(id, methodType, selfUrl);		
	}
	
}
