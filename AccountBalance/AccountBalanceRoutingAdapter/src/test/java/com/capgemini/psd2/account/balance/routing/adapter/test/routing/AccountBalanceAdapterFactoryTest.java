/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.routing.adapter.test.routing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import com.capgemini.psd2.account.balance.routing.adapter.impl.AccounBalanceRoutingAdapter;
import com.capgemini.psd2.account.balance.routing.adapter.routing.AccountBalanceCoreSystemAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;

/**
 * The Class AccountBalanceAdapterFactoryTest.
 */
public class AccountBalanceAdapterFactoryTest {
	
	/** The application context. */
	@Mock
	private ApplicationContext applicationContext;
	
	/** The account balance core system adapter factory. */
	@InjectMocks
	private AccountBalanceCoreSystemAdapterFactory accountBalanceCoreSystemAdapterFactory;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Test account balance adapter.
	 */
	@Test
	public void testAccountBalanceAdapter() {
		AccountBalanceAdapter accountBalanceAdapter = new AccounBalanceRoutingAdapter();
		when(applicationContext.getBean(anyString())).thenReturn(accountBalanceAdapter);
		AccountBalanceAdapter accountBalanceAdapterResult = (AccounBalanceRoutingAdapter) accountBalanceCoreSystemAdapterFactory.getAdapterInstance("accountBalanceAdapter");
		assertEquals(accountBalanceAdapter, accountBalanceAdapterResult);
	}
	
	

}
