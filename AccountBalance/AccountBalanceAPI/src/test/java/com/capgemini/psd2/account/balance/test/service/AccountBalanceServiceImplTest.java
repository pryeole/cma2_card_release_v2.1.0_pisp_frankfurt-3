/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.test.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.account.balance.service.impl.AccountBalanceServiceImpl;
import com.capgemini.psd2.account.balance.test.mock.data.AccountBalanceMockData;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.response.validator.ResponseValidator;

/**
 * The Class AccountBalanceServiceImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceServiceImplTest {

	/** The adapter. */
	@Mock
	private AccountBalanceAdapter adapter;
	
	/** The mapping adapter */
	@Mock
	private AccountMappingAdapter accountMappingAdapter;

	/** The consent mapping. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The header atttributes. */
	@Mock
	private RequestHeaderAttributes headerAtttributes;

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private ResponseValidator responseValidator;

	/** The service. */
	@InjectMocks
	private AccountBalanceServiceImpl service = new AccountBalanceServiceImpl();

	@Mock
	private AISPCustomValidator<OBReadBalance1, OBReadBalance1>  accountsCustomValidator;
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(aispConsentAdapter.retrieveAccountMapping(anyString()))
		.thenReturn(AccountBalanceMockData.getMockAccountMapping());
		when(headerAtttributes.getToken()).thenReturn(AccountBalanceMockData.getToken());
	}

	@Test
	public void retrieveAccountBalanceSuccessWithLinksMetaTest() {
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountBalanceMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountBalanceMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountBalance(anyObject(), anyObject()))
				.thenReturn(AccountBalanceMockData.getMockPlatformResponseWithLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountBalanceMockData.getMockLoggerData());

		accountsCustomValidator.validateResponseParams(anyObject());
		//verify(responseValidator).validateResponse(anyObject());

		OBReadBalance1 actualResponse = service.retrieveAccountBalance("f4483fda-81be-4873-b4a6-20375b7f55c1");

		assertEquals(AccountBalanceMockData.getMockExpectedResponseWithLinksMeta(), actualResponse);

	}
	
	@Test
	public void retrieveAccountBalanceSuccessWithoutLinkMetaTest() {
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountBalanceMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountBalanceMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountBalance(anyObject(), anyObject()))
				.thenReturn(AccountBalanceMockData.getMockPlatformResponseWithoutLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountBalanceMockData.getMockLoggerData());

		responseValidator.validateResponse(anyObject());
		//verify(responseValidator).validateResponse(AccountProductMockData.getMockExpectedResponseWithoutLinkMeta());

		OBReadBalance1 actualResponse = service.retrieveAccountBalance("123");

		assertEquals(AccountBalanceMockData.getMockExpectedResponseWithoutLinkMeta(), actualResponse);

	}
	
	
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}
}
